.. meta::
   :description: Editing in Kdenlive video editor
   :keywords: KDE, Kdenlive, useful information, editing, timeline, documentation, user manual, video editor, open source, free, learn, easy


.. metadata-placeholder

   :authors: - Annew (https://userbase.kde.org/User:Annew)
             - Claus Christensen
             - Yuri Chornoivan
             - Ttguy (https://userbase.kde.org/User:Ttguy)
             - Bushuev (https://userbase.kde.org/User:Bushuev)
             - Torsten R\u00c3\u00b6mer (https://userbase.kde.org/User:Torsten R\u00c3\u00b6mer)
             - Roger (https://userbase.kde.org/User:Roger)
             - Eugen Mohr

   :license: Creative Commons License SA 4.0

.. _useful_information:

Useful Information
==================

.. toctree::
   :caption: Contents:
   :maxdepth: 1
   :glob:

   useful_information/*

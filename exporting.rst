.. meta::
   :description: Render out your final Kdenlive video for distributing
   :keywords: KDE, Kdenlive, render, distribute, documentation, user manual, video editor, open source, free, learn, easy

.. metadata-placeholder

   :authors: - Eugen Mohr
             
   :license: Creative Commons License SA 4.0

.. _exporting:

#########
Exporting
#########

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :glob:
   :hidden:

   exporting/*

Hit :kbd:`Ctrl + Enter` to open the rendering dialog. Or :menuselection:`Project --> Render` and click on the render button |media-record|.


.. image:: /images/exporting/render_dialog_22-12.png
   :alt: File rendering dialog 22.12

Select the :guilabel:`Output file` location

Select the desired :guilabel:`Presets`. :guilabel:`MP4-H264/AAC` works nearly everywhere.

Click on :guilabel:`Render to File` button.
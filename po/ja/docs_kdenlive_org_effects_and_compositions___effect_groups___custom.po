msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-11-20 18:29-0800\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: ../../effects_and_compositions/effect_groups/custom.rst:15
msgid "Custom Effects"
msgstr ""

#: ../../effects_and_compositions/effect_groups/custom.rst:17
msgid "Contents"
msgstr ""

#: ../../effects_and_compositions/effect_groups/custom.rst:19
msgid ""
"The **Custom Group** in the **Effect List** is where effects appear when you "
"choose :menuselection:`Save Effect` from an effect in the  :ref:`effects`."
msgstr ""

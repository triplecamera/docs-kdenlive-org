msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-11-20 18:29-0800\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_gradient.rst:13
msgid "Alpha gradient"
msgstr ""

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_gradient.rst:15
msgid "Contents"
msgstr ""

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_gradient.rst:17
msgid ""
"This is the `Frei0r alphagrad <https://www.mltframework.org/plugins/"
"FilterFrei0r-alphagrad/>`_ MLT filter, see also `Frei0r-alphagrad readme "
"<https://github.com/dyne/frei0r/blob/master/src/filter/alpha0ps/readme>`_ "
"file."
msgstr ""

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_gradient.rst:19
msgid "Fills the alpha channel with a gradient."
msgstr ""

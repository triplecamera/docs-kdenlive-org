msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-16 00:37+0000\n"
"PO-Revision-Date: 2021-11-20 18:29-0800\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: ../../effects_and_compositions.rst:1
msgid ""
"Add in Kdenlive video editor, video effects, audio effects, compositions, "
"title, subtitle, speech to text, color correction."
msgstr ""

#: ../../effects_and_compositions.rst:1
msgid ""
"KDE, Kdenlive, effects, audio, video, title, subtitle, speech to text, color "
"correction, documentation, user manual, video editor, open source, free, "
"learn, easy"
msgstr ""

#: ../../effects_and_compositions.rst:15
msgid "Effects and compositions"
msgstr ""

#: ../../effects_and_compositions.rst:18
msgid "Contents:"
msgstr ""

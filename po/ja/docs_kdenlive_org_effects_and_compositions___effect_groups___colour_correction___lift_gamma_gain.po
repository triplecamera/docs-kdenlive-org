msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-11-20 18:29-0800\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: ../../effects_and_compositions/effect_groups/colour_correction/lift_gamma_gain.rst:10
msgid "Lift/Gamma/Gain"
msgstr ""

#: ../../effects_and_compositions/effect_groups/colour_correction/lift_gamma_gain.rst:12
msgid "Contents"
msgstr ""

#: ../../effects_and_compositions/effect_groups/colour_correction/lift_gamma_gain.rst:14
msgid ""
"This is the `Lift_gamma_gain <https://www.mltframework.org/plugins/"
"FilterLift_gamma_gain/>`_ MLT filter."
msgstr ""

#: ../../effects_and_compositions/effect_groups/colour_correction/lift_gamma_gain.rst:16
msgid ""
"Allows you to adjust the lift (impacting mainly shadows), gain (impacting "
"mainly highlights) and gamma (impacting mainly midtones). The color wheel "
"inputs allow to control the degree to which these effects apply to the R, G "
"& B color channels. By default, the white color at the centre of the colour "
"wheel is selected, meaning the effect applies equally to all three color "
"channels. By choosing another color on the color wheel, the effect will be "
"applied on the R, G & B channels in proportion to the RGB components that "
"make up that color."
msgstr ""

msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-13 00:22+0000\n"
"PO-Revision-Date: 2021-11-20 18:29-0800\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: ../../glossary/tips_tricks.rst:26
msgid "Tips &  Tricks"
msgstr ""

#: ../../glossary/tips_tricks.rst:31
msgid "Contents"
msgstr ""

#: ../../glossary/tips_tricks.rst:33
msgid ""
"You can advance and retard the clip in the project monitor by rotating the "
"mouse wheel when the pointer is over the :ref:`timeline` or over the :ref:"
"`monitors`"
msgstr ""

#: ../../glossary/tips_tricks.rst:36
msgid ""
"You can advance and retard the clip in the :ref:`monitors` by rotating the "
"mouse wheel when the pointer is over the :ref:`monitors`"
msgstr ""

#: ../../glossary/tips_tricks.rst:39
msgid ""
"You can zoom the timeline by vertical drag on the ruler (if enabled in :ref:"
"`configure_kdenlive`)"
msgstr ""

#: ../../glossary/tips_tricks.rst:42
msgid "What's this :ref:`full_luma`"
msgstr ""

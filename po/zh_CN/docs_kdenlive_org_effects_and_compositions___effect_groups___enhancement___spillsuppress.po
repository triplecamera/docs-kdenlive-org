msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2023-01-12 10:32\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/documentation-docs-kdenlive-org/"
"docs_kdenlive_org_effects_and_compositions___effect_groups___enhancement___spillsuppress."
"pot\n"
"X-Crowdin-File-ID: 25845\n"

#: ../../effects_and_compositions/effect_groups/enhancement/spillsuppress.rst:10
msgid "Spill Suppress"
msgstr ""

#: ../../effects_and_compositions/effect_groups/enhancement/spillsuppress.rst:12
msgid "Contents"
msgstr ""

#: ../../effects_and_compositions/effect_groups/enhancement/spillsuppress.rst:14
msgid ""
"Remove green or blue spill light from subjects shot in front of green or "
"blue screen."
msgstr ""

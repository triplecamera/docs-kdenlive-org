msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-03 00:38+0000\n"
"PO-Revision-Date: 2023-01-12 10:32\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/documentation-docs-kdenlive-org/"
"docs_kdenlive_org_effects_and_compositions___effect_groups___misc___k-"
"means_clustering.pot\n"
"X-Crowdin-File-ID: 26227\n"

#: ../../effects_and_compositions/effect_groups/misc/k-means_clustering.rst:13
msgid "K-Means Clustering"
msgstr ""

#: ../../effects_and_compositions/effect_groups/misc/k-means_clustering.rst:15
msgid "Contents"
msgstr ""

#: ../../effects_and_compositions/effect_groups/misc/k-means_clustering.rst:17
msgid ""
"This is the `Frei0r cluster <https://www.mltframework.org/plugins/"
"FilterFrei0r-cluster/>`_ MLT filter."
msgstr ""

#: ../../effects_and_compositions/effect_groups/misc/k-means_clustering.rst:19
msgid "Clusters of a source image by color and spatial distance."
msgstr ""

#: ../../effects_and_compositions/effect_groups/misc/k-means_clustering.rst:21
msgid "https://youtu.be/a3Yz2xJWmN8"
msgstr ""

#: ../../effects_and_compositions/effect_groups/misc/k-means_clustering.rst:23
msgid "https://youtu.be/qwTD__a5oqo"
msgstr ""

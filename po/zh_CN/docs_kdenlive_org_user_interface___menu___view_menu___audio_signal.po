msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-13 00:22+0000\n"
"PO-Revision-Date: 2023-01-12 10:32\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/documentation-docs-kdenlive-org/"
"docs_kdenlive_org_user_interface___menu___view_menu___audio_signal.pot\n"
"X-Crowdin-File-ID: 25925\n"

#: ../../user_interface/menu/view_menu/audio_signal.rst:13
msgid "Audio Signal"
msgstr "音频信号"

#: ../../user_interface/menu/view_menu/audio_signal.rst:16
msgid "Contents"
msgstr "目录"

#: ../../user_interface/menu/view_menu/audio_signal.rst:18
msgid ""
"You can monitor the levels of the audio as the clip plays with this widget."
msgstr "使用这个部件，在播放片段时，您可以监视音频的电平。"

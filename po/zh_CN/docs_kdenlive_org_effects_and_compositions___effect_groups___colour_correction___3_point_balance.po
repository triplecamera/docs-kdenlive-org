msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2023-01-12 10:32\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/documentation-docs-kdenlive-org/"
"docs_kdenlive_org_effects_and_compositions___effect_groups___colour_correction___3_point_balance."
"pot\n"
"X-Crowdin-File-ID: 25781\n"

#: ../../effects_and_compositions/effect_groups/colour_correction/3_point_balance.rst:14
msgid "3 point balance"
msgstr "3 point balance - 3 点平衡"

#: ../../effects_and_compositions/effect_groups/colour_correction/3_point_balance.rst:16
msgid "Contents"
msgstr "目录"

#: ../../effects_and_compositions/effect_groups/colour_correction/3_point_balance.rst:18
msgid ""
"This is the `Frei0r three_point_balance <https://www.mltframework.org/"
"plugins/FilterFrei0r-three_point_balance/>`_ MLT filter."
msgstr ""
"这是 `Frei0r three_point_balance <https://www.mltframework.org/plugins/"
"FilterFrei0r-three_point_balance/>`_ MLT 滤镜。"

#: ../../effects_and_compositions/effect_groups/colour_correction/3_point_balance.rst:20
msgid "https://youtu.be/ZFhfTsl7St8"
msgstr "https://youtu.be/ZFhfTsl7St8"

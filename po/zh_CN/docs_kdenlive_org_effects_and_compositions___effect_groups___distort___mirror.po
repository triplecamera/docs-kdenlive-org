msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2023-01-12 10:32\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/documentation-docs-kdenlive-org/"
"docs_kdenlive_org_effects_and_compositions___effect_groups___distort___mirror."
"pot\n"
"X-Crowdin-File-ID: 26021\n"

#: ../../effects_and_compositions/effect_groups/distort/mirror.rst:13
msgid "Mirror"
msgstr "Mirror - 镜像"

#: ../../effects_and_compositions/effect_groups/distort/mirror.rst:15
msgid "Contents"
msgstr "目录"

#: ../../effects_and_compositions/effect_groups/distort/mirror.rst:17
msgid ""
"This is the `Mirror <https://www.mltframework.org/plugins/FilterMirror/>`_ "
"MLT filter."
msgstr ""
"这是 `Mirror <https://www.mltframework.org/plugins/FilterMirror/>`_ MLT 滤"
"镜。"

#: ../../effects_and_compositions/effect_groups/distort/mirror.rst:19
msgid "Provides various mirror and image reversing effects."
msgstr "提供各种镜像和图像翻转效果。"

#: ../../effects_and_compositions/effect_groups/distort/mirror.rst:21
msgid "https://youtu.be/ao32j0dSVII"
msgstr "https://youtu.be/ao32j0dSVII"

#: ../../effects_and_compositions/effect_groups/distort/mirror.rst:23
msgid "https://youtu.be/3-hcMZu52Vk"
msgstr "https://youtu.be/3-hcMZu52Vk"

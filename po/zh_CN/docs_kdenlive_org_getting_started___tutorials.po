msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-16 00:37+0000\n"
"PO-Revision-Date: 2023-01-12 10:32\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/documentation-docs-kdenlive-org/"
"docs_kdenlive_org_getting_started___tutorials.pot\n"
"X-Crowdin-File-ID: 25847\n"

#: ../../getting_started/tutorials.rst:1
msgid "Kdenlive video editor tutorials"
msgstr ""

#: ../../getting_started/tutorials.rst:1
msgid ""
"KDE, Kdenlive, tutorials, documentation, user manual, video editor, open "
"source, free, learn, easy"
msgstr ""

#: ../../getting_started/tutorials.rst:30
msgid "Tutorials"
msgstr "教程"

#: ../../getting_started/tutorials.rst:32
msgid "Contents"
msgstr "目录"

#: ../../getting_started/tutorials.rst:35
msgid "Written Tutorials"
msgstr "文字版教程"

#: ../../getting_started/tutorials.rst:37
msgid "See :ref:`quickstart` for a step-by-step introductory tutorial"
msgstr "请参阅 :ref:`quickstart` 教程。"

#: ../../getting_started/tutorials.rst:40
msgid "Contents:"
msgstr "目录："

#: ../../getting_started/tutorials.rst:46
msgid ""
"`Introduction to Kdenlive <http://opensource.com/life/11/11/introduction-"
"kdenlive>`_ by Seth Kenlon"
msgstr ""
"`Introduction to Kdenlive <http://opensource.com/life/11/11/introduction-"
"kdenlive>`_ ，由 Seth Kenlon 撰写"

#: ../../getting_started/tutorials.rst:49
msgid ""
"`10 tools for visual effects with Kdenlive <https://opensource.com/"
"life/15/12/10-kdenlive-tools>`_ by Seth Kenlon"
msgstr ""
"`10 tools for visual effects with Kdenlive <https://opensource.com/"
"life/15/12/10-kdenlive-tools>`_ ，由 Seth Kenlon 撰写"

#: ../../getting_started/tutorials.rst:52
msgid ""
"`Basic masking in Kdenlive <https://opensource.com/life/15/11/basic-masking-"
"kdenlive>`_ by Seth Kenlon"
msgstr ""
"`Basic masking in Kdenlive <https://opensource.com/life/15/11/basic-masking-"
"kdenlive>`_ ，由 Seth Kenlon 撰写"

#: ../../getting_started/tutorials.rst:55
msgid ""
"`Kdenlive Challenge (Multiple Masks &  Tracks) <http://www.ocsmag."
"com/2015/12/22/the-video-editing-challenge-part-i-kdenlive/>`_ by Paul Browns"
msgstr ""
"`Kdenlive Challenge (Multiple Masks &  Tracks) <http://www.ocsmag."
"com/2015/12/22/the-video-editing-challenge-part-i-kdenlive/>`_ ，由 Paul "
"Browns 撰写"

#: ../../getting_started/tutorials.rst:58
msgid "`Wikibooks Kdenlive manual <http://en.wikibooks.org/wiki/Kdenlive>`_"
msgstr "`Wikibooks Kdenlive 手册 <http://en.wikibooks.org/wiki/Kdenlive>`_"

#: ../../getting_started/tutorials.rst:64
msgid "Video Tutorials"
msgstr "视频教程"

#: ../../getting_started/tutorials.rst:66
msgid ""
"`Image and Title Layers Transparency Tutorial - Open Source Bug <https://www."
"youtube.com/watch?v=f6VHlOZutm8>`_"
msgstr ""
"`Image and Title Layers Transparency Tutorial - Open Source Bug <https://www."
"youtube.com/watch?v=f6VHlOZutm8>`_"

#: ../../getting_started/tutorials.rst:69
msgid ""
"`How to do pan and zoom with Kdenlive video editor -  Peter Thomson <https://"
"www.youtube.com/watch?v=B8ZPoWaxQrA>`_"
msgstr ""
"`How to do pan and zoom with Kdenlive video editor -  Peter Thomson <https://"
"www.youtube.com/watch?v=B8ZPoWaxQrA>`_"

#: ../../getting_started/tutorials.rst:72
msgid ""
"`Keyframe Animation - Linuceum <https://www.youtube.com/watch?"
"v=M8hC5FbIzdE>`_"
msgstr ""
"`Keyframe Animation - Linuceum <https://www.youtube.com/watch?"
"v=M8hC5FbIzdE>`_"

#: ../../getting_started/tutorials.rst:75
msgid ""
"`Kdenlive Tutorials by Arkengheist 2.0 <https://www.youtube.com/channel/"
"UCtkSBZ0x71aeHmR3NNBTWwg>`_ : Many tutorials including Text effects, "
"Transitions, Timelapse, Animation, Lower Thirds, Rotoscoping, and more."
msgstr ""
"`Kdenlive Tutorials by Arkengheist 2.0 <https://www.youtube.com/channel/"
"UCtkSBZ0x71aeHmR3NNBTWwg>`_ 提供许多教程，包括文本效果、转场、延时、下三分之"
"一字幕效果、动画、旋转等教程。"

#: ../../getting_started/tutorials.rst:78
msgid ""
"More videos can be found using a `YouTube search <https://www.youtube.com/"
"results?search_query=kdenlive+tutorials>`_ and on the `Vimeo Kdenlive "
"Tutorial Channel <https://vimeo.com/groups/kdenlivetutorials/videos>`_"
msgstr ""
"使用`YouTube 搜索 <https://www.youtube.com/results?search_query=kdenlive"
"+tutorials>`_ 和 `Vimeo Kdenlive 教程频道 <https://vimeo.com/groups/"
"kdenlivetutorials/videos>`_ 可以找到更多教程视频"

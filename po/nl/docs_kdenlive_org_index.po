# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-10-24 00:42+0000\n"
"PO-Revision-Date: 2022-10-24 10:22+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.08.2\n"

#: ../../index.rst:1
msgid "The Kdenlive User Manual"
msgstr "De gebruikershandleiding voor Kdenlive"

#: ../../index.rst:1
msgid ""
"KDE, Kdenlive, documentation, user manual, video editor, open source, free, "
"help, learn"
msgstr ""
"KDE, Kdenlive, documentatie, gebruikershandleiding, videobewerker, open-"
"source, vrij, hulp, leren"

#: ../../index.rst:15
msgid "Kdenlive Manual"
msgstr "Handleiding voor Kdenlive"

#: ../../index.rst:17
msgid ""
"Welcome to the manual for `kdenlive <https://kdenlive.org>`_, the free and "
"open source video editor."
msgstr ""
"Welkom in de handleiding van `kdenlive <https://kdenlive.org>`_, de vrije en "
"open-source videobewerker."

#: ../../index.rst:19
msgid ""
"The current Kdenlive version you get `here <https://kdenlive.org/download/"
">`_ ."
msgstr ""
"De huidige versie van Kdenlive krijgt u `hier <https://kdenlive.org/download/"
">`_ ."

#: ../../index.rst:21
msgid ""
"You can download this manual as an `EPUB <https://docs.kdenlive.org/en/epub/"
"KdenliveManual.epub>`_."
msgstr ""
"U kunt deze handleiding als een `EPUB <https://docs.kdenlive.org/en/epub/"
"KdenliveManual.epub>`_ downloaden."

#: ../../index.rst:25
msgid "Getting started"
msgstr "De eerste stapjes"

#: ../../index.rst:31
msgid ":ref:`Introduction`"
msgstr ":ref:`Introduction`"

#: ../../index.rst:37
msgid ":ref:`Installation`"
msgstr ":ref:`Installation`"

#: ../../index.rst:42
msgid ":ref:`quickstart`"
msgstr ":ref:`quickstart`"

#: ../../index.rst:43
msgid "Basic workflow with a video example."
msgstr "Basis werkmethode met een videovoorbeeld."

#: ../../index.rst:47
msgid ":ref:`Tutorials`"
msgstr ":ref:`Tutorials`"

#: ../../index.rst:52
msgid "User Interface"
msgstr "Gebruikersinterface"

#: ../../index.rst:58
msgid ":ref:`user_interface`"
msgstr ":ref:`user_interface`"

#: ../../index.rst:59
msgid "Introduction to Kdenlive's window system and widgets"
msgstr "Introductie in het venstersysteem en widgets van Kdenlive"

#: ../../index.rst:63
msgid ":ref:`Project_Settings`"
msgstr ":ref:`Project_Settings`"

#: ../../index.rst:64
msgid "Setting the correct project values"
msgstr "De juiste projectwaarden instellen"

#: ../../index.rst:69
msgid ":ref:`Project_tree`"
msgstr ":ref:`Project_tree`"

#: ../../index.rst:71
msgid ":ref:`Timeline`"
msgstr ":ref:`Timeline`"

#: ../../index.rst:73
msgid ":ref:`Monitors`"
msgstr ":ref:`Monitors`"

#: ../../index.rst:74
msgid "Key components"
msgstr "Sleutelcomponenten"

#: ../../index.rst:75
msgid ":ref:`toolbars`"
msgstr ":ref:`toolbars`"

#: ../../index.rst:79
msgid ":ref:`shortcuts`"
msgstr ":ref:`shortcuts`"

#: ../../index.rst:80
msgid "Improve your workflow by using the keyboard"
msgstr "Uw werkmethode verbeteren met gebruik van het toetsenbord"

#: ../../index.rst:85
msgid "Workflow"
msgstr "Werkmethode"

#: ../../index.rst:95
msgid "Load files into Kdenlive and be prepared"
msgstr "Bestanden in Kdenlive laden en voorbereid zijn"

#: ../../index.rst:96
msgid ":ref:`importing_and_assets_management`"
msgstr ":ref:`importing_and_assets_management`"

#: ../../index.rst:97
msgid "Starting video editing"
msgstr "Met bewerken van video beginnen"

#: ../../index.rst:105
msgid "Start editing in the Timeline"
msgstr "Met bewerken in de tijdlijn beginnen"

#: ../../index.rst:106
msgid ":ref:`cutting_and_assembling`"
msgstr ":ref:`cutting_and_assembling`"

#: ../../index.rst:107
msgid "See how the time line and the monitors are working"
msgstr "Zie hoe de tijdlijn en de monitors werken"

#: ../../index.rst:115
msgid "Add video and audio effects and compositions."
msgstr "Video- en geluidseffecten en composities toevoegen."

#: ../../index.rst:117
msgid "Create Titles and Subtitles and use Speech to Text."
msgstr "Titels en ondertiteling maken en spraak naar tekst gebruiken."

#: ../../index.rst:118
msgid ":ref:`effects`"
msgstr ":ref:`effects`"

#: ../../index.rst:119
msgid "Make color correction."
msgstr "Kleurcorrectie doen."

#: ../../index.rst:126
msgid ":ref:`exporting`"
msgstr ":ref:`exporting`"

#: ../../index.rst:127
msgid "Render out your final video for distributing."
msgstr "Uw uiteindelijke video renderen voor verspreiding."

#: ../../index.rst:132
msgid "Troubleshooting, Glossary, Get Involved"
msgstr "Probleemoplossing, woordenlijst, meedoen"

#: ../../index.rst:139
msgid "Solving specific :ref:`windows_issues`"
msgstr "Specifieke problemen oplossen :ref:`windows_issues`"

#: ../../index.rst:140
msgid ":ref:`troubleshooting`"
msgstr ":ref:`troubleshooting`"

#: ../../index.rst:141
msgid "General problem solving"
msgstr "Algemene probleemoplossing"

#: ../../index.rst:145
msgid "`Bug Reports <https://kdenlive.org/en/bug-reports/>`__"
msgstr "`Bugrapporten <https://kdenlive.org/en/bug-reports/>`__"

#: ../../index.rst:146
msgid "How to file a bug."
msgstr "Hoe een bug rapporteren."

#: ../../index.rst:150
msgid ":ref:`Glossary`"
msgstr ":ref:`Glossary`"

#: ../../index.rst:151
msgid "References and further information."
msgstr "Verwijzingen en verdere informatie."

#: ../../index.rst:156
msgid ""
"`Get Involved <https://community.kde.org/Kdenlive/Workgroup/Documentation>`__"
msgstr ""
"`Doe mee <https://community.kde.org/Kdenlive/Workgroup/Documentation>`__"

#: ../../index.rst:157
msgid "Contribute to this Manual."
msgstr "Bijdragen aan deze Handleiding."

#~ msgid ":ref:`Windows_Issues`"
#~ msgstr ":ref:`Windows_Issues`"

#~ msgid ":ref:`faq`"
#~ msgstr ":ref:`faq`"

#~ msgid "Answers to questions frequently asked"
#~ msgstr "Antwoorden op veel voorkomende vragen"

# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-29 00:22+0000\n"
"PO-Revision-Date: 2021-11-29 11:21+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.08.3\n"

#: ../../effects_and_compositions/effect_groups/colour_correction/white_balance_lms.rst:12
msgid "White Balance (LMS)"
msgstr "Witbalans (LMS)"

#: ../../effects_and_compositions/effect_groups/colour_correction/white_balance_lms.rst:14
msgid "Contents"
msgstr "Inhoud"

#: ../../effects_and_compositions/effect_groups/colour_correction/white_balance_lms.rst:16
msgid ""
"This is the `Frei0r colgate <https://www.mltframework.org/plugins/"
"FilterFrei0r-colgate/>`_ MLT filter by Steiner H. Gunderson."
msgstr ""
"Dit is het MLT-filter `Frei0r colgate <https://www.mltframework.org/plugins/"
"FilterFrei0r-colgate/>`_ door Steiner H. Gunderson."

#: ../../effects_and_compositions/effect_groups/colour_correction/white_balance_lms.rst:18
msgid "Do simple color correction, in a physically meaningful way."
msgstr ""
"Doe een eenvoudige kleurcorrectie, op een fysiek betekenisvolle manier."

#: ../../effects_and_compositions/effect_groups/colour_correction/white_balance_lms.rst:20
msgid "**Parameters:**"
msgstr "**Parameters:**"

#: ../../effects_and_compositions/effect_groups/colour_correction/white_balance_lms.rst:22
msgid ""
"Neutral Color: Choose a color from the source image that should be white."
msgstr ""
"Neutrale kleur: Kies een kleur uit de bronafbeelding die wit zou moeten zijn."

#: ../../effects_and_compositions/effect_groups/colour_correction/white_balance_lms.rst:24
msgid ""
"Color Temperature: Choose an output color temperature, if different from "
"6500 K."
msgstr ""
"Kleurtemperatuur: Kies een uitvoerkleurtemperatuur, indien anders dan 6500 K."

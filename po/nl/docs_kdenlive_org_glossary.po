# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-16 00:37+0000\n"
"PO-Revision-Date: 2022-01-16 14:28+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.1\n"

#: ../../glossary.rst:1
msgid "Kdenlive references and further information"
msgstr "Kdenlive verwijzingen en verdere informatie"

#: ../../glossary.rst:1
msgid ""
"KDE, Kdenlive, references, information, documentation, user manual, video "
"editor, open source, free, learn, easy"
msgstr ""
"KDE, Kdenlive, verwijzingen, informatie, documentatie, "
"gebruikershandleiding, videobewerker, open-source, vrij, leren, gemakkelijk"

#: ../../glossary.rst:15
msgid "Glossary"
msgstr "Woordenlijst"

#: ../../glossary.rst:17
msgid "Contents:"
msgstr "Inhoud:"

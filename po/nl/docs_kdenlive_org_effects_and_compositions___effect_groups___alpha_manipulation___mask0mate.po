# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-11-18 11:06+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.08.3\n"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/mask0mate.rst:14
msgid "Mask0mate"
msgstr "Mask0mate"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/mask0mate.rst:17
msgid "Rectangular Alpha Mask"
msgstr "Rechthoekig alfa-masker"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/mask0mate.rst:19
msgid "Contents"
msgstr "Inhoud"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/mask0mate.rst:21
msgid ""
"Previously known as Mask0Mate. This is the `Frei0r mask0mate <https://www."
"mltframework.org/plugins/FilterFrei0r-mask0mate/>`_ MLT filter."
msgstr ""
"Eerder bekend als Mask0Mate. Dit is het MLT-filter `Frei0r mask0mate "
"<https://www.mltframework.org/plugins/FilterFrei0r-mask0mate/>`_"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/mask0mate.rst:24
msgid "Creates an rectangular alpha-channel mask."
msgstr "Maakt een rechthoekig alfa-kanaalmasker."

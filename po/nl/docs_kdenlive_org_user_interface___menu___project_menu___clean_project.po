# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-21 00:37+0000\n"
"PO-Revision-Date: 2021-12-31 16:08+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.0\n"

#: ../../user_interface/menu/project_menu/clean_project.rst:15
msgid "Clean Project"
msgstr "Project opschonen"

#: ../../user_interface/menu/project_menu/clean_project.rst:17
msgid "Contents"
msgstr "Inhoud"

#: ../../user_interface/menu/project_menu/clean_project.rst:19
msgid ""
"Available from the :ref:`project_menu` menu this function removes any clips "
"from the Project Bin that are not currently being used on the timeline. The "
"files remain on the hard drive and are only removed from the Project Bin."
msgstr ""
"Beschikbaar uit het menu :ref:`project_menu` verwijdert deze functie elke "
"clip uit de Project-bin die nu niet gebruikt worden in de tijdlijn. De "
"bestanden blijven op de vaste schijf en zijn alleen uit de Project-bin "
"verwijderd."

#: ../../user_interface/menu/project_menu/clean_project.rst:21
msgid "You can undo this action with :kbd:`Ctrl + Z`."
msgstr "U kunt deze actie ongedaan maken met :kbd:`Ctrl + Z`."

#: ../../user_interface/menu/project_menu/clean_project.rst:25
msgid ""
"This is different from the :ref:`project_settings` button on the Project "
"Files tab in Project Settings which deletes files not used by the project "
"from the hard drive."
msgstr ""
"Dit verschilt van de knop :ref:`project_settings` on het tabblad "
"Projectbestanden in Projectinstellingen die bestanden van de vaste schijf "
"verwijdert die niet gebruikt worden door the project."

# Translation of docs_kdenlive_org_glossary___useful_information___kdenlive-transitions.po to Catalan
# Copyright (C) 2021-2022 This_file_is_part_of_KDE
# Licensed under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2021, 2022.
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: documentation-docs-kdenlive-org\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-01 00:38+0000\n"
"PO-Revision-Date: 2022-01-24 18:35+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../glossary/useful_information/kdenlive-transitions.rst:13
msgid "Kdenlive transitions"
msgstr "Transicions del Kdenlive"

#: ../../glossary/useful_information/kdenlive-transitions.rst:15
msgid ""
"Depending on their background in video editing, users may find **Kdenlive "
"transitions** somewhat confusing. Hopefully, this article clears up this "
"confusion surrounding Kdenlive transitions, at least to some degree."
msgstr ""
"Depenent del seu coneixement en l'edició de vídeo, els usuaris poden trobar "
"les **transicions del Kdenlive** una mica confuses. Esperem que aquest "
"article aclareixi aquesta confusió que envolta les transicions del Kdenlive, "
"almenys fins a un cert punt."

#: ../../glossary/useful_information/kdenlive-transitions.rst:19
msgid "Kdenlive Transitions: 3-in-1"
msgstr "Transicions de Kdenlive: 3 en 1"

#: ../../glossary/useful_information/kdenlive-transitions.rst:21
msgid ""
"In Kdenlive, **transitions** can roughly be classified into three different "
"types as follows:"
msgstr ""
"En el Kdenlive, les **transicions** es poden classificar aproximadament en "
"tres tipus diferents de la manera següent:"

#: ../../glossary/useful_information/kdenlive-transitions.rst:24
msgid "Type of Transition"
msgstr "Tipus de transició"

#: ../../glossary/useful_information/kdenlive-transitions.rst:24
msgid "Description"
msgstr "Descripció"

#: ../../glossary/useful_information/kdenlive-transitions.rst:26
msgid "Clip-to-Clip Transition"
msgstr "Transició de clip a clip"

#: ../../glossary/useful_information/kdenlive-transitions.rst:26
msgid ""
"Gradually replaces one clip by another clip. Has exactly two implicit "
"keyframes, for start and end. This is what many users usually understand "
"transitions to be."
msgstr ""
"Un clip se substitueix gradualment per un altre clip. Té exactament dos "
"fotogrames clau implícits, per al principi i el final. Això és el que molts "
"usuaris solen entendre que són les transicions."

#: ../../glossary/useful_information/kdenlive-transitions.rst:30
msgid "Dynamic Compositing"
msgstr "Composició dinàmica"

#: ../../glossary/useful_information/kdenlive-transitions.rst:30
msgid ""
"For combining two clips, and the way of combination may vary with time. "
"Supports user-defined keyframes that allow to control at least certain "
"parameters."
msgstr ""
"Per a combinar dos clips, i la manera de combinació pot variar amb el temps. "
"Permet fotogrames clau definits per l'usuari que permeten controlar almenys "
"certs paràmetres."

#: ../../glossary/useful_information/kdenlive-transitions.rst:34
#: ../../glossary/useful_information/kdenlive-transitions.rst:151
msgid "Layer Compositing"
msgstr "Composició de capa"

#: ../../glossary/useful_information/kdenlive-transitions.rst:34
msgid ""
"For combining two clips in a constant, static way: much like you see layer "
"compositing in image tools. As no keyframes are supported, this type of "
"compositing is static, thus invariant of time."
msgstr ""
"Per combinar dos clips d'una manera constant i estàtica: molt semblant a "
"veure la composició de capes en eines d'imatge. Com que no s'admeten "
"fotogrames clau, aquest tipus de composició és estàtica, per tant, invariant "
"en el temps."

# skip-rule: t-acc_obe
#: ../../glossary/useful_information/kdenlive-transitions.rst:39
msgid ""
"Historically, Kdenlive borrows the term **transition**, with its 3-fold "
"meaning, directly from the `multimedia engine MLT <https://mltframework.org/"
">`_. MLT does all the video and audio processing according in your timeline. "
"In the MLT universe, transitions basically «merge» video frames from upper "
"tracks with video frames from lower tracks, producing result frames."
msgstr ""
"Històricament, el Kdenlive manlleva el terme **transició**, amb el seu "
"significat de 3 parts, directament del motor `multimedia MLT <https://"
"mltframework.org/>`_. MLT fa tot el processament de vídeo i àudio segons la "
"vostra línia de temps. A l'univers MLT, les transicions bàsicament "
"«fusionen» fotogrames de vídeo de les pistes superiors amb fotogrames de "
"vídeo de les pistes inferiors, produint fotogrames resultants."

#: ../../glossary/useful_information/kdenlive-transitions.rst:41
msgid ""
"In contrast, many users experienced in video editing have come to know "
"transitions as a mechanism to transition between to adjacent clips. However, "
"Kdenlive doesn’t even support such in-track transitions. Instead you need to "
"place these clips on separate tracks, and then you add a Kdenlive transition "
"for combining these clips in some clever way."
msgstr ""
"En canvi, molts usuaris experimentats en l'edició de vídeo han arribat a "
"conèixer transicions com un mecanisme de transició entre clips adjacents. No "
"obstant això, el Kdenlive ni tan sols dona suport a aquestes transicions en "
"pista. En lloc d'això, cal col·locar aquests clips en pistes separades, i "
"després afegir una transició del Kdenlive per combinar aquests clips d'una "
"manera intel·ligent."

#: ../../glossary/useful_information/kdenlive-transitions.rst:45
msgid "1. Clip-to-Clip Transitions"
msgstr "1. Transicions de clip a clip"

# skip-rule: kct-wip
#: ../../glossary/useful_information/kdenlive-transitions.rst:47
msgid ""
"Let’s start with those **archetypical transitions** most users would "
"probably expect when they hear the word *transition*: the dissolve, slide, "
"and wipe transitions. See for yourself…"
msgstr ""
"Comencem per aquestes **transicions arquetípiques** que la majoria d'usuaris "
"probablement esperarien quan senten la paraula *transició*: les transicions "
"de dissolució, lliscament i cortineta. Vegem-ho…"

# skip-rule: t-acc_obe
#: ../../glossary/useful_information/kdenlive-transitions.rst:56
msgid ""
"**Dissolve**: gradually transitions from one clip to another. So it’s kind "
"of fading between the two clips. (See also the Wikipedia article on "
"`Dissolve. <https://en.wikipedia.org/wiki/Dissolve_%28filmmaking%29>`_)"
msgstr ""
"**Dissolució**: transicions graduals d'un clip a un altre. Així que és una "
"espècie d'esvaïment entre els dos clips. (Vegeu també l'article de la "
"Viquipèdia sobre `Dissolve. <https://en.wikipedia.org/wiki/Dissolve"
"%28filmmaking%29>`_)"

# skip-rule: t-acc_obe,kct-wip
#: ../../glossary/useful_information/kdenlive-transitions.rst:65
msgid ""
"**Slide**: gradually replaces one clip by another clip, by traveling from "
"one side of the frame to another (See also the Wikipedia article on `Wipe "
"<https://en.wikipedia.org/wiki/Wipe_%28transition%29>`_.)"
msgstr ""
"**Lliscament**: substitueix gradualment un clip per un altre, viatjant d'un "
"costat del fotograma a un altre. (Vegeu també l'article de la Viquipèdia "
"sobre `Wipe <https://en.wikipedia.org/wiki/Wipe%28transition%29>`_)"

# skip-rule: t-acc_obe,kct-wip
#: ../../glossary/useful_information/kdenlive-transitions.rst:74
msgid ""
"**Wipe**: one clip gradually replaces another clip, often in form of some "
"shape. (See also the Wikipedia article on `Wipe <https://en.wikipedia.org/"
"wiki/Wipe_%28transition%29>`_.)"
msgstr ""
"**Cortineta**: un clip substitueix gradualment un altre, sovint creant "
"alguna forma. (Vegeu també l'article de la Viquipèdia sobre `Wipe <https://"
"en.wikipedia.org/wiki/Wipe%28transition%29>`_)"

#: ../../glossary/useful_information/kdenlive-transitions.rst:78
msgid ""
"These three transitions don’t offer any (user-) keyframes. Instead, their "
"*start* and *end* keyframes are *implicit* and fixed to the *start* and "
"*end* of the transition respectively."
msgstr ""
"Aquestes tres transicions no ofereixen cap fotograma clau (d'usuari). En "
"lloc d'això, els seus fotogrames clau *inici* i *final* són *implícits* i "
"fixats a l'*inici* i *final* de la transició respectivament."

#: ../../glossary/useful_information/kdenlive-transitions.rst:80
msgid ""
"**Because Kdenlive always uses two separate tracks for transitions**, "
"there’s a peculiarity to note: normally, as time moves forward in the "
"timeline, these transitions change (or kind of fade) from the clip on the "
"upper track to the clip on the lower track. The somewhat strangely named "
"transition parameter **Reverse** allows you to switch the track roles: if "
"**Reverse** is checked, then these transitions change from the lower track "
"to the upper track, as time progresses."
msgstr ""
"**Perquè el Kdenlive sempre utilitza dues pistes separades per a "
"transicions**, hi ha una peculiaritat a tenir present: normalment, a mesura "
"que el temps avança en la línia de temps, aquests canvis de transicions (o "
"classe d'esvaïment) del clip de la pista superior al clip de la pista "
"inferior. El paràmetre de transició d'un nom estrany **Revers** us permet "
"canviar els rols de la pista: si **Revers** està marcat, llavors aquests "
"canvis de transicions són de la pista inferior a la pista superior, a mesura "
"que el temps avança."

#: ../../glossary/useful_information/kdenlive-transitions.rst:82
msgid "Simply put:"
msgstr "En poques paraules:"

#: ../../glossary/useful_information/kdenlive-transitions.rst:84
msgid "▼ **Reverse off**: transition from upper track to ▼ lower track."
msgstr ""
"▼ **Revers inactiu**: transició de la pista superior a ▼ la pista inferior."

#: ../../glossary/useful_information/kdenlive-transitions.rst:86
msgid "▲ **Reverse on**: transition from lower track to ▲ upper track instead."
msgstr ""
"▲ **Revers actiu**: transició de la pista inferior a ▲ la pista superior."

#: ../../glossary/useful_information/kdenlive-transitions.rst:89
msgid "2. Dynamic Compositing Transitions"
msgstr "2. Transicions de composició dinàmica"

#: ../../glossary/useful_information/kdenlive-transitions.rst:94
msgid ""
"To some degree, Kdenlive supports (simple) compositing in its timeline. "
"Actually, even this simple compositing can get you a long way in many "
"projects (as the above screenshot may hint at). Kdenlive currently offers "
"the following (keyframable) compositing transitions:"
msgstr ""
"En certa manera, el Kdenlive admet la composició (simple) en la seva línia "
"de temps. De fet, fins i tot aquesta composició senzilla pot donar força "
"feina en molts projectes (com pot indicar la captura de pantalla anterior). "
"Actualment, el Kdenlive ofereix les següents transicions de composició (amb "
"fotogrames clau):"

# skip-rule: kct-wip
#: ../../glossary/useful_information/kdenlive-transitions.rst:96
msgid ""
"**Affine** – allows to size, rotate, skew, and position. Together with "
"keyframes, this transition is really versatile. Its only drawbacks are: it "
"is slower than other complex transitions (due to the affine transformation), "
"and it doesn’t support wipes (which only **Composite** and **Region** "
"support in this class of transitions)."
msgstr ""
"**Afí** – permet la mida, el gir, la inclinació i la posició. Juntament amb "
"els fotogrames clau, aquesta transició és realment versàtil. Els seus únics "
"inconvenients són: és més lent que altres transicions complexes (a causa de "
"la transformació afí), i no permet cortinetes (que només s'admeten "
"**Composició** i **Regió** en aquesta classe de transicions)."

#: ../../glossary/useful_information/kdenlive-transitions.rst:97
msgid ""
"**Cairo Blend** – a simple compositing transition, supporting several "
"compositing modes. In addition, the opacity of the upper frames can be "
"controlled. This transition also supports keyframes."
msgstr ""
"**Barreja del Cairo** - una transició simple de composició, que admet "
"diversos modes de composició. A més, es pot controlar l'opacitat dels "
"fotogrames superiors. Aquesta transició també admet fotogrames clau."

#: ../../glossary/useful_information/kdenlive-transitions.rst:98
msgid ""
"**Cairo Affine Blend** – this has the functionality of both **Affine** and "
"**Composite**: position, rotate (you can even control the center of "
"rotation!), and finally skew. And all this is keyframable."
msgstr ""
"**Barreja afí del Cairo** – té la funcionalitat tant d'**Afí** com de "
"**Composició**: posició, gir (fins i tot podeu controlar el centre de gir!), "
"i finalment la inclinació. I tot això és amb fotogrames clau."

# skip-rule: kct-wip
#: ../../glossary/useful_information/kdenlive-transitions.rst:99
msgid ""
"**Composite** – allows keyframed dissolves, wipes, and swipes; and all this "
"in the same transition. In contrast to Affine, it does not support rotation "
"or skewing. The downsides of Composite are: luma bleed, and less precise "
"position control. When compared to Affine, the Composite transition is much "
"faster, albeit at the cost of luma bleed."
msgstr ""
"**Composició** - permet dissolucions, cortinetes i lliscament de fotogrames "
"clau; i tot això en la mateixa transició. A diferència d'Afí, no permet ni "
"el gir ni la inclinació. Els desavantatges de Composició són: sagnat de la "
"luma i un control de posició menys precís. En comparació amb Afí, la "
"transició de Composició és molt més ràpida, encara que a costa del sagnat de "
"la luma."

#: ../../glossary/useful_information/kdenlive-transitions.rst:100
msgid ""
"**Composite & Transform** – this is a rather new transition that made its "
"debut with Kdenlive 16.04. It allows to easily composite clips onto each "
"other (supported several compositing modes), as well as to move the upper "
"track clips. However, there is neither support for scaling, nor for "
"rotation, but for dynamic opacity. But keyframes are supported. In those "
"situations, use **Affine** or **Cairo Affine Blend** instead."
msgstr ""
"**Combinació i transformació** – aquesta és una transició bastant nova que "
"va fer el seu debut amb el Kdenlive 16.04. Permet combinar fàcilment clips "
"entre ells (s'admeten diversos modes de composició), així com moure els "
"clips de la pista superior. No obstant això, no es permet el canvi d'escala, "
"ni el gir, només l'opacitat dinàmica. Però s'admeten fotogrames clau. En "
"aquestes situacions, utilitzeu **Afí** o **Barreja Afí del Cairo**."

#: ../../glossary/useful_information/kdenlive-transitions.rst:101
msgid ""
"**Region** – like **Composite**, but restricted to a region in form of a "
"matte. In the **Region** transition properties, this matte is called the "
"**Transparency clip**."
msgstr ""
"**Regió** - com **Composició**, però restringit a una regió en forma de "
"màscara. En les propietats de transició de **Regió**, aquesta màscara "
"s'anomena el **Clip de transparència**."

#: ../../glossary/useful_information/kdenlive-transitions.rst:103
msgid ""
"**Fun Fact**: Admittedly, MLT and Kdenlive offer a lot of choice here; "
"probably too much choice. A non-representative poll in our official Kdenlive "
"G+ community showed that **Composite is used the most often**, followed by "
"**Composite & Transform** and **Affine**."
msgstr ""
"**Curiositat**: És cert que MLT i Kdenlive ofereixen una gran varietat "
"d'opcions, probablement massa opcions. Una enquesta no representativa en la "
"nostra comunitat oficial Kdenlive G+ va mostrar que **Composició s'utilitza "
"més sovint**, seguit per **Combina i transforma** i **Afí**."

#: ../../glossary/useful_information/kdenlive-transitions.rst:108
msgid "Compositing with Transparency"
msgstr "Composició amb transparència"

#: ../../glossary/useful_information/kdenlive-transitions.rst:110
msgid ""
"**Composite & Transform** is Kdenlive’s new darling, as it will make life "
"much easier for many, if not most Kdenlive users. When compared to "
"**Affine**, this new transition is also faster in the standard compositing "
"cases. Moreover, **Composite & Transform** defaults to the alpha compositing "
"mode (paint) *over* – which is what probably most users need when "
"compositing. In contrast, **Affine** uses the atop alpha compositing "
"strategy that can drive unexpected users mad. But see for yourself…"
msgstr ""
"**Combina i transforma** és la nineta nova del Kdenlive, ja que facilitarà "
"la vida a molts, si no a la majoria dels usuaris de Kdenlive. En comparació "
"amb **Afí**, aquesta nova transició també és més ràpida en els casos de "
"composició estàndard. A més, **Combina i transforma** per defecte és el mode "
"de composició alfa (pintat) *sobre*, que és el que probablement necessiten "
"la majoria d'usuaris en combinar. En canvi, **Afí** utilitza l'estratègia de "
"composició alfa de dalt que pot conduir a inesperats usuaris enfadats. Però "
"vegem-ho…"

#: ../../glossary/useful_information/kdenlive-transitions.rst:119
msgid ""
"**Composite & Transform**: whatever semi or non-transparent is in the frame "
"from the upper track, it will be painted over the frame from the lower "
"track. Hence the name of this compositing mode: over. Please note: in the "
"transition properties, this mode is to be found as **Compositing**: **Alpha "
"Blend** instead."
msgstr ""
"**Combina i transforma**: sigui semi o no transparent el fotograma de la "
"pista superior, es pintarà sobre el fotograma de la pista inferior. D'aquí "
"el nom d'aquest mode de composició: sobre. Tingueu en compte: en les "
"propietats de la transició, aquest mode es troba com a **Composició**: "
"**Barreja d'alfa**."

#: ../../glossary/useful_information/kdenlive-transitions.rst:121
msgid ""
"In addition, **Composite**, **Cairo Blend**, and **Cairo Affine Blend** also "
"use the same over compositing strategy, as Composite & Transform does. For "
"**Cairo Blend** and **Cairo Affine Blend** this **Blend mode** is called "
"**Normal** instead."
msgstr ""
"A més a més, **Composició**, **Barreja del Cairo**, i **Barreja afí del "
"Cairo** també utilitzen la mateixa estratègia de composició, com fa la "
"combinació i transformació. Per a **Barreja del Cairo** i **Barreja afí del "
"Cairo** aquest **mode de barreja** s'anomena **Normal**."

#: ../../glossary/useful_information/kdenlive-transitions.rst:130
msgid ""
"**Affine**: as the simple rule of thumb, transparency is solely controlled "
"by the *lower* track. Any transparency information from the upper track "
"simply gets completely ignored. In consequence, if your lower frame has "
"regions of full transparency, whatever falls within them on the upper frame "
"will be invisible! You can see this result also in the screenshot."
msgstr ""
"**Afí**: com a regla general, la transparència està controlada únicament per "
"la pista *inferior*. Qualsevol informació de transparència de la pista "
"superior simplement s'ignora per complet. En conseqüència, si el fotograma "
"inferior té regions de transparència completa, qualsevol que sigui el seu "
"contingut en el fotograma superior serà invisible! També podeu veure aquest "
"resultat a la captura de pantalla."

#: ../../glossary/useful_information/kdenlive-transitions.rst:132
msgid ""
"At least at this time, **Composite & Transform** does not support this alpha "
"handling as **Affine** does."
msgstr ""
"Almenys en aquest moment, **Combina i transforma** no permet aquesta gestió "
"de l'alfa com ho fa **Afí**."

#: ../../glossary/useful_information/kdenlive-transitions.rst:138
msgid "Please see also for further information:"
msgstr "Vegeu també per a més informació:"

#: ../../glossary/useful_information/kdenlive-transitions.rst:140
msgid "our separate Kdenlive article on :ref:`compositing_with_transparency`."
msgstr ""
"el nostre article separat del Kdenlive sobre la :ref:"
"`compositing_with_transparency`."

# skip-rule: t-acc_obe
#: ../../glossary/useful_information/kdenlive-transitions.rst:141
msgid ""
"the Wikipedia article on `alpha compositing <https://en.wikipedia.org/wiki/"
"Alpha_compositing>`_."
msgstr ""
"l'article de la Viquipèdia sobre la `composició alfa <https://en.wikipedia."
"org/wiki/Alphacompositing>`_."

# skip-rule: t-acc_obe
#: ../../glossary/useful_information/kdenlive-transitions.rst:142
msgid ""
"the SVG Open 2015 paper by Craig Northway on `understanding compositing and "
"color extensions in SVG 1.2 in 30 minutes! <http://www.graphicalweb.org/2005/"
"papers/abstractsvgopen/index.html>`_ – especially the Porter Duff operator "
"table in chapter 6 with resulting alpha calculation column."
msgstr ""
"el document SVG Open 2015 de Craig Northway sobre `comprensió de la "
"composició i les extensions de color en SVG 1.2 en 30 minuts! <http://www."
"graphicalweb.org/2005/papers/abstractsvgopen/index.html>`_, especialment la "
"taula d'operadors de Porter Duff en el capítol 6 amb la columna del càlcul "
"alfa resultant."

#: ../../glossary/useful_information/kdenlive-transitions.rst:146
msgid "3. Layer Compositing Transitions"
msgstr "3. Transicions de composició de capa"

# skip-rule: t-acc_obe
#: ../../glossary/useful_information/kdenlive-transitions.rst:148
msgid ""
"This third kind of Kdenlive/MLT transitions mostly **work similar to layer "
"modes in image editors** (such as `GIMP <https://www.gimp.org/>`_`). **These "
"static layer compositing transitions do not have any parameters at all**. "
"This category actually has the most Kdenlive transitions to offer:"
msgstr ""
"Aquest tercer tipus de transicions del Kdenlive/MLT majoritàriament "
"**treballa de manera similar als modes de capa en els editors d'imatges** "
"(com ara el `GIMP <https://www.gimp.org/>`_). **Aquestes transicions de "
"composició de la capa estàtica no tenen cap paràmetre**. En realitat, "
"aquesta categoria té les transicions més importants que ofereix el Kdenlive:"

#: ../../glossary/useful_information/kdenlive-transitions.rst:154
msgid "Addition"
msgstr "Addition"

#: ../../glossary/useful_information/kdenlive-transitions.rst:155
msgid "Addition Alpha"
msgstr "Addition Alpha"

#: ../../glossary/useful_information/kdenlive-transitions.rst:156
msgid "Burn"
msgstr "Burn"

#: ../../glossary/useful_information/kdenlive-transitions.rst:157
msgid "Color Only"
msgstr "Color Only"

#: ../../glossary/useful_information/kdenlive-transitions.rst:158
msgid "Darken"
msgstr "Darken"

#: ../../glossary/useful_information/kdenlive-transitions.rst:159
msgid "Difference"
msgstr "Difference"

#: ../../glossary/useful_information/kdenlive-transitions.rst:160
msgid "Divide"
msgstr "Divide"

#: ../../glossary/useful_information/kdenlive-transitions.rst:161
msgid "Dodge"
msgstr "Dodge"

#: ../../glossary/useful_information/kdenlive-transitions.rst:162
msgid "Grain Handling: Extract / Merge"
msgstr "Grain Handling: Extract / Merge"

#: ../../glossary/useful_information/kdenlive-transitions.rst:163
msgid "Hardlight / Overlay"
msgstr "Hardlight / Overlay"

#: ../../glossary/useful_information/kdenlive-transitions.rst:164
msgid "Hue"
msgstr "Hue"

#: ../../glossary/useful_information/kdenlive-transitions.rst:165
msgid "Lighten"
msgstr "Lighten"

#: ../../glossary/useful_information/kdenlive-transitions.rst:166
msgid "Multiply"
msgstr "Multiply"

#: ../../glossary/useful_information/kdenlive-transitions.rst:167
msgid "Overlay / Hardlight"
msgstr "Overlay / Hardlight"

#: ../../glossary/useful_information/kdenlive-transitions.rst:168
msgid "Saturation"
msgstr "Saturation"

#: ../../glossary/useful_information/kdenlive-transitions.rst:169
msgid "Screen"
msgstr "Screen"

#: ../../glossary/useful_information/kdenlive-transitions.rst:170
msgid "Softlight"
msgstr "Softlight"

#: ../../glossary/useful_information/kdenlive-transitions.rst:171
msgid "Substract"
msgstr "Substract"

#: ../../glossary/useful_information/kdenlive-transitions.rst:172
msgid "UV Map"
msgstr "UV Map"

#: ../../glossary/useful_information/kdenlive-transitions.rst:173
msgid "Value"
msgstr "Valor"

#: ../../glossary/useful_information/kdenlive-transitions.rst:174
msgid "Video Quality Management"
msgstr "Gestió de la qualitat del vídeo"

#: ../../glossary/useful_information/kdenlive-transitions.rst:177
msgid "Alpha Compositing"
msgstr "Composició alfa"

#: ../../glossary/useful_information/kdenlive-transitions.rst:180
msgid "Alpha atop"
msgstr "«Atop» d'alfa"

#: ../../glossary/useful_information/kdenlive-transitions.rst:181
msgid "Alpha in"
msgstr "«In» d'alfa"

#: ../../glossary/useful_information/kdenlive-transitions.rst:182
msgid "Alpha out"
msgstr "«Out» d'alfa"

#: ../../glossary/useful_information/kdenlive-transitions.rst:183
msgid "Alpha over"
msgstr "«Over» d'alfa"

#: ../../glossary/useful_information/kdenlive-transitions.rst:184
msgid "Alpha XOR"
msgstr "XOR d'alfa"

#: ../../glossary/useful_information/kdenlive-transitions.rst:185
msgid "Matte"
msgstr "Matte"

# skip-rule: common-fixe
#: ../../glossary/useful_information/kdenlive-transitions.rst:189
msgid ""
"Kdenlive’s (or, MLT’s) fixed compositing transitions don’t have a "
"transparency parameter. To some extent, you may substitute the **Cairo "
"Blend** transition, which has an opacity parameter."
msgstr ""
"Les transicions de composició fixes del Kdenlive (o del MLT) no tenen cap "
"paràmetre de transparència. En certa manera, podeu substituir la transició "
"**Barreja del Cairo**, que té un paràmetre d'opacitat."

# skip-rule: kct-wip
#: ../../glossary/useful_information/kdenlive-transitions.rst:193
msgid ""
"Users are often asking for **Kdenlive support of in-track transitions** "
"(Dissolve, Slide, Wipe). Unfortunately, due to the way Kdenlive’s multimedia "
"engine MLT works, this requires a large amount of code rewrite, not to "
"mention extensive testing afterwards to ensure the expected stability. Due "
"to our limited developer resources, **in-track transitions are thus not on "
"our near-term roadmap**. We will gladly accept coding help, so if you are "
"willing to accept this challenge, please let us know!"
msgstr ""
"Els usuaris sovint demanen **suport del Kdenlive de les transicions en "
"pista** (Dissolució, Lliscament, Cortineta). Per desgràcia, a causa de la "
"manera en què funciona el motor multimèdia MLT del Kdenlive, això requereix "
"una gran quantitat de reescriptura de codi, per no esmentar les proves "
"extenses després per assegurar l'estabilitat esperada. A causa dels nostres "
"limitats recursos de desenvolupament, les **transicions en pista no es "
"troben en el nostre full de ruta a curt termini**. Acceptarem amb molt de "
"gust ajuda amb la codificació, així que si esteu disposat a acceptar aquest "
"repte, si us plau, que ho sapiguem!"

#: ../../glossary/useful_information/kdenlive-transitions.rst:197
msgid "In-track or same-track transition is implemented with Kdenlive 21.08."
msgstr ""
"La transició en pista o a la mateixa pista està implementada amb el Kdenlive "
"21.08."

#~ msgid ""
#~ "Addition Addition Alpha Burn Color Only Darken Difference Divide Dodge "
#~ "Grain Handling: Extract / Merge Hardlight / Overlay Hue Lighten Multiply "
#~ "Overlay / Hardlight Saturation Screen Softlight Substract UV Map Value "
#~ "Video Quality Management"
#~ msgstr ""
#~ "Addition Addition Alpha Burn Color Only Darken Difference Divide Dodge "
#~ "Grain Handling: Extract / Merge Hardlight / Overlay Hue Lighten Multiply "
#~ "Overlay / Hardlight Saturation Screen Softlight Substract UV Map Value "
#~ "Video Quality Management"

#~ msgid "Alpha atop Alpha in Alpha out Alpha over Alpha XOR Matte"
#~ msgstr "Alpha atop Alpha in Alpha out Alpha over Alpha XOR Matte"

# Translation of docs_kdenlive_org_glossary___useful_information___extract_frames_in_higher_res_than_project_profile.po to Catalan
# Copyright (C) 2021-2022 This_file_is_part_of_KDE
# Licensed under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2021, 2022.
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: documentation-docs-kdenlive-org\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-06-27 00:37+0000\n"
"PO-Revision-Date: 2022-06-27 10:09+0200\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../glossary/useful_information/extract_frames_in_higher_res_than_project_profile.rst:13
msgid "Working with Extracted Frames in Higher Resolution than Project Profile"
msgstr ""
"Treballar amb fotogrames extrets en una resolució més alta que el perfil del "
"projecte"

#: ../../glossary/useful_information/extract_frames_in_higher_res_than_project_profile.rst:15
msgid ""
"Sometimes, you may need to work in your Kdenlive projects with images stills "
"extracted from your source footage. Now that’s easy, thanks to the “extract "
"frame…” and “extract frame to project…” items in the context menu of the "
"clip monitor. See also our earlier Toolbox post Extract Frame to Project."
msgstr ""
"A vegades, és possible que hàgiu de treballar en els projectes Kdenlive amb "
"imatges que s'extreuen del vostre metratge d'origen. Això és fàcil, gràcies "
"als elements «Extreu un fotograma…» i «Extreu un fotograma al projecte…» del "
"menú contextual del monitor de clips. Vegeu també el nostre missatge "
"anterior del Quadre d'eines Extreu el fotograma al projecte."

#: ../../glossary/useful_information/extract_frames_in_higher_res_than_project_profile.rst:17
msgid ""
"Now, there’s a **gotcha** to watch out for: Kdenlive currently extracts "
"frames according to your project settings. That means: if your source "
"footage happens to be of higher resolution than your project settings and "
"even different orientation, then the extracted frame will be in project "
"frame format. In consequence, extracted frame quality might noticeably "
"differ from the same video footage when used side-by-side in your project. "
"Luckily, there’s help."
msgstr ""
"Ara, hi ha un **parany** a controlar: actualment el Kdenlive extreu "
"fotogrames d'acord amb la configuració del projecte. Això vol dir: si el "
"vostre material d'origen és de major resolució que la configuració del "
"vostre projecte i fins i tot una orientació diferent, llavors el fotograma "
"extret estarà en format de fotograma del projecte. En conseqüència, la "
"qualitat del fotograma extret pot diferir notablement del mateix metratge de "
"vídeo quan s'utilitza en paral·lel en el vostre projecte. Per sort, hi ha "
"una ajuda."

#: ../../glossary/useful_information/extract_frames_in_higher_res_than_project_profile.rst:21
msgid "Project Profile … and Profile-Differing Footage"
msgstr "Perfil del projecte… i metratge amb perfil diferent"

#: ../../glossary/useful_information/extract_frames_in_higher_res_than_project_profile.rst:30
msgid ""
"In some projects you may face source footage that *doesn’t match the project "
"profile at all*. For instance, when working with certain screen recordings, "
"especially when recorded on tablets and smartphone. The recordings might be "
"even in a different orientation."
msgstr ""
"En alguns projectes podeu fer front a un metratge d'origen que *no "
"coincideixi en absolut amb el perfil del projecte*. Per exemple, quan es "
"treballa amb certs enregistraments de pantalla, especialment quan es grava "
"en tauletes i telèfons intel·ligents. Els enregistraments podrien estar fins "
"i tot en una orientació diferent."

# skip-rule: t-acc_obe
#: ../../glossary/useful_information/extract_frames_in_higher_res_than_project_profile.rst:32
msgid ""
"Agreed, such footage *might* be a sign of the – rather hilarious – `Vertical "
"Video Syndrome <https://www.youtube.com/watch?v=f2picMQC-9E>`_ (link to "
"YouTube explanation). Or it might be the sign of an outstanding arthouse "
"production. But there are other sensible reasons, such as their use in "
"tutorial videos: a portrait mobile device screen can be easily composed with "
"a 16:9 scene, leaving room for additional illustrations, explanations, and "
"so on."
msgstr ""
"D'acord, aquest metratge *podria* ser un signe de la, força hilarant, "
"`síndrome de vídeo vertical <https://www.youtube.com/watch?v=f2picMQC-9E>`_ "
"(enllaç a l'explicació de YouTube). O podria ser el signe d'una producció "
"artística destacada. Però hi ha altres raons assenyades, com el seu ús en "
"vídeos de guies d'aprenentatge: una pantalla de dispositius mòbils de retrat "
"pot ser fàcilment composta amb una escena de 16:9, deixant espai per a "
"il·lustracions addicionals, explicacions, etc."

# skip-rule: kct-tab
#: ../../glossary/useful_information/extract_frames_in_higher_res_than_project_profile.rst:41
msgid ""
"To give a concrete example: recording the screen of an Android Tablet, say, "
"a Samsung Galaxy Tab S3 gives raw footage with a frame size of 1536×2048 "
"pixels with 2:3 aspect ratio (because the portrait orientation). Even when "
"recording in landscape orientation, the 3:2 display aspect ratio isn’t "
"ideal. And you may very well want to record in original resolution in order "
"to not loose later downstream during production when you may need the "
"reserve in the raw footage."
msgstr ""
"Per donar un exemple concret: gravar la pantalla d'una tauleta Android, "
"diguem, una Samsung Galaxy Tab S3 dona un metratge en brut amb una mida de "
"fotograma de 1536x2048 píxels amb una relació d'aspecte 2:3 (per "
"l'orientació de retrat). Fins i tot quan s'enregistra en l'orientació de "
"paisatge, la relació d'aspecte 3:2 no és ideal. I és possible que vulgueu "
"enregistrar en la resolució original per a no perdre més tard en la baixada "
"durant la producció, quan potser calgui la reserva en el metratge en brut."

#: ../../glossary/useful_information/extract_frames_in_higher_res_than_project_profile.rst:45
msgid ""
"the frame rate displayed by Kdenlive for this footage (see screenshot) is "
"*not even in the right ballpark*. In fact, this footage has a widely varying "
"frame rate (not to be confused with a variable bitrate), and Kdenlive/MLT/"
"ffmpeg seem to offer wild guesses here, maybe based on the TV color range "
"indicated for the video stream. But a 23500/1001 framerate is used by NTSC, "
"yet this has been recorded with PAL cromaciticies on a PAL device. Anyway…"
msgstr ""
"La velocitat dels fotogrames mostrada pel Kdenlive per a aquest metratge "
"(vegeu la captura de pantalla) *no és ni tan sols la hipòtesi correcta*. De "
"fet, aquest metratge té una velocitat dels fotogrames molt variable (no "
"confondre amb una taxa de bits variable), i els Kdenlive/MLT/ffmpeg semblen "
"oferir estimacions agosarades aquí, potser basant-se en la gamma de color de "
"la TV indicat en el flux de vídeo. Però el NTSC fa servir una freqüència de "
"fotogrames de 23500/1001, tot i que s'ha enregistrat amb cromacitats PAL en "
"un dispositiu PAL. De totes maneres…"

# skip-rule: common-fixe
#: ../../glossary/useful_information/extract_frames_in_higher_res_than_project_profile.rst:54
msgid ""
"Unfortunately, if you need to work with still images extracted from such "
"source footage, then you might have already met a nasty surprise: Kdenlive’s "
"media engine MLT extracts still frames always according to the *project "
"settings*. Consider you have a FullHD 1920×1080 pixels 19:6 project. And the "
"screencast footage is 1536×2048 pixels with 2:3 portrait aspect ratio. Then "
"you end up with extracted frames of 1920×1080 pixels size with a landscape "
"16:9 aspect ratio and a lot of transparent space, but not the expected "
"1536×2048. So not only the resolution changed, but also the aspect ratio."
msgstr ""
"Per desgràcia, si cal treballar amb imatges fixes extretes d'aquest tipus de "
"metratge, llavors podríeu haver trobat ja una sorpresa desagradable: el "
"motor multimèdia MLT del Kdenlive sempre extreu fotogrames segons la "
"*configuració del projecte*. Considereu que teniu un projecte 19:6 FullHD de "
"1920x1080 píxels. I les imatges de captura de pantalla són de 1536x2048 "
"píxels amb una relació d'aspecte de retrat de 2:3. Després acabareu amb els "
"fotogrames extrets de la mida de 1920x1080 píxels amb una relació d'aspecte "
"de 16:9 apaïsada i força espai transparent, però no el 1536x2048 esperat. "
"Per tant, no sols la resolució ha canviat, sinó també la relació d'aspecte."

#: ../../glossary/useful_information/extract_frames_in_higher_res_than_project_profile.rst:63
msgid ""
"Hardly what you want here, as our detail screenshot shows. When comparing "
"this clip monitor screenshot with the clip monitor screenshot taken from the "
"raw footage, you’ll notice their different qualities. It doesn’t matter "
"which we you prefer, the issue is that they noticeably differ."
msgstr ""
"Aquí no hi ha el que voleu, com mostra la captura de pantalla detallada. "
"Quan es compara aquesta captura de pantalla amb la captura de pantalla del "
"monitor de clips presa del metratge en brut, veureu les seves qualitats "
"diferents. No importa el que preferim, la qüestió és que són visiblement "
"diferents."

# skip-rule: t-acc_obe
#: ../../glossary/useful_information/extract_frames_in_higher_res_than_project_profile.rst:65
msgid ""
"This is a no-no when we need to use both the raw footage as well as the "
"extracted frames in the same project, especially adjacent to each other in "
"the timeline. The differences are clearly visible, unless you compress the "
"resulting video to the extreme of looking rather like a fake `Barbapapa "
"<https://en.wikipedia.org/wiki/Barbapapa>`_ episode (yep, I’m *that old* to "
"have this seen in its time)."
msgstr ""
"És un no quan necessitem utilitzar tant el metratge en brut com els "
"fotogrames extrets en el mateix projecte, especialment quan són adjacents "
"els uns als altres en la línia de temps. Les diferències són clarament "
"visibles, llevat que comprimiu el vídeo resultant fins a l'extrem de semblar "
"més aviat un episodi fals de `Barbapapa <https://en.wikipedia.org/wiki/"
"Barbapapa>`_ (sí, *soc tan gran* com per haver-ho visten el seu temps)."

#: ../../glossary/useful_information/extract_frames_in_higher_res_than_project_profile.rst:70
msgid "Original Resolution Frame Extraction"
msgstr "Extracció del fotograma de la resolució original"

#: ../../glossary/useful_information/extract_frames_in_higher_res_than_project_profile.rst:72
msgid ""
"Of course, after all timeline cuts have been mostly settled with the "
"required extracted frames for stills, we could then manually (re-) extract "
"the frames in their original resolution. Of course, this is not only a "
"daunting but also error-prone task. Been there, done that … never again."
msgstr ""
"Per descomptat, després que tots els retalls de la línia de temps s'han "
"resolt en la seva majoria amb els fotogrames extrets necessaris per a les "
"imatges, llavors podríem (tornar a) extreure manualment els fotogrames en la "
"seva resolució original. Naturalment, no es tracta només d'una tasca "
"incòmoda, sinó també d'una tasca susceptible d'errors. Va ser allà, va fer "
"això… mai més."

# skip-rule: t-acc_obe
#: ../../glossary/useful_information/extract_frames_in_higher_res_than_project_profile.rst:74
msgid ""
"Fortunately, there’s the easy route using the so-called `extract-frames "
"<https://gist.github.com/TheDiveO/57fd76e4d15252232aaacc7e422a79a2>`_ bash "
"shell script (courtesy of your blog post author TheDiveO). Download the bash "
"script from the `extract-frames GitHubGist <https://gist.githubusercontent."
"com/TheDiveO/57fd76e4d15252232aaacc7e422a79a2/raw/"
"b3e605eb74737916bffa55bbc1b907e29ee7016d/extract-frames>`_ and make it "
"executable ($ ``chmod u+x extract-frames``)."
msgstr ""
"Afortunadament, hi ha la ruta fàcil utilitzant l'anomenat script de "
"l'intèrpret d'ordres bash `extract-frames <https://gist.github.com/"
"TheDiveO/57fd76e4d15252232aaacc7e422a79a2>`_ (cortesia de l'autor del blog "
"TheDiveO). Baixeu l'script bash des del `GitHubGist de l'extract-frames "
"<https://gist.githubusercontent.com/"
"TheDiveO/57fd76e4d15252232aaacc7e422a79a2/raw/"
"b3e605eb74737916bffa55bbc1b907e29ee7016d/extract-frames>`_ i feu-lo "
"executable ($ ``chmod u+x extract-frames``)."

#: ../../glossary/useful_information/extract_frames_in_higher_res_than_project_profile.rst:76
msgid ""
"**A word of caution: before you let loose anything on your Kdenlive project "
"directory that makes changes, make sure to have backed up your project "
"first. You have been warned. We take no responsibility for any data losses "
"or other losses you may experience.**"
msgstr ""
"**Una precaució: abans de deixar anar qualsevol cosa que faci canvis en el "
"directori del projecte Kdenlive, assegureu-vos abans d'haver fet una còpia "
"de seguretat del vostre projecte. Us hem advertit. No responem per cap "
"pèrdua de dades ni d'altres pèrdues que pugueu experimentar.**"

#: ../../glossary/useful_information/extract_frames_in_higher_res_than_project_profile.rst:78
msgid ""
"Now run the script inside your project directory, where your frame image "
"PNGs are located together with the corresponding video files from which the "
"frames have been extracted. You always need to specify the Kdenlive project "
"filename in order to run the script:"
msgstr ""
"Ara executeu l'script dins del vostre directori de projecte, on es troben "
"els PNG d'imatge de fotograma juntament amb els fitxers de vídeo "
"corresponents dels quals s'han extret els fotogrames. Sempre cal especificar "
"el nom del fitxer del projecte Kdenlive per tal d'executar l'script:"

# skip-rule: t-acc_obe
#: ../../glossary/useful_information/extract_frames_in_higher_res_than_project_profile.rst:80
msgid "$ ``extract-frames myproject.kdenlive``"
msgstr "$ ``extract-frames myproject.kdenlive``"

#: ../../glossary/useful_information/extract_frames_in_higher_res_than_project_profile.rst:82
msgid ""
"That’s all to it! You should now see also Kdenlive’s project bin noticing "
"that the image files have been updated. Thus, you can run the script while "
"your Kdenlive project is opened (at least that’s my experience so far)."
msgstr ""
"Això és tot! Ara també hauríeu de veure la safata del projecte del Kdenlive "
"indicant que els fitxers d'imatge s'han actualitzat. Així, podeu executar "
"l'script mentre el projecte Kdenlive està obert (almenys aquesta és la meva "
"experiència fins ara)."

# skip-rule: t-acc_obe
#: ../../glossary/useful_information/extract_frames_in_higher_res_than_project_profile.rst:84
msgid ""
"What does this script actually do? It first scans for frame image PNGs with "
"filenames in the form ``xxx-f000000.png``. Here, xxx is the filename "
"(without extension) of the corresponding video file from which the frame has "
"been extracted. At this time, the script only looks for corresponding video "
"files ending in ``.mp4`` and ``.MP4``."
msgstr ""
"Què fa realment aquest script? Primer, escaneja els PNG d'imatge de "
"fotograma amb noms de fitxer en la forma ``xxx-f000000.png``. Aquí, xxx és "
"el nom de fitxer (sense extensió) del fitxer de vídeo corresponent del qual "
"s'ha extret el fotograma. En aquest moment, el guió només cerca els fitxers "
"de vídeo corresponents que acabin en ``.mp4`` i ``.MP4``."

# skip-rule: t-acc_obe
#: ../../glossary/useful_information/extract_frames_in_higher_res_than_project_profile.rst:86
msgid ""
"The ``f000000`` part now identifies the frame number from which the still "
"frame has been extracted. Now this is posing an interesting question: in "
"which reference system were these frames counted?"
msgstr ""
"La part ``f000000`` ara identifica el número de fotograma des del qual s'ha "
"extret la imatge del fotograma. Ara això planteja una pregunta interessant: "
"en quin sistema de referència s'han comptat aquests fotogrames?"

# skip-rule: t-acc_obe
#: ../../glossary/useful_information/extract_frames_in_higher_res_than_project_profile.rst:88
msgid ""
"Remember that we wrote above that Kdenlive extracts frames from bin clips "
"using the project properties? So, the reference system for counting "
"extracted frame numbers is your project. And that’s the reason why you need "
"to tell ``extract-frames`` of your project: the script learns the project’s "
"frame rate in order to correctly handle frame number. Without it, we would "
"later extract the wrong frames in case the raw footage has a different frame "
"rate and thus frame counting from your project."
msgstr ""
"Recordeu que més amunt hem escrit que el Kdenlive extreu els fotogrames dels "
"clips de la safata utilitzant les propietats del projecte? Així doncs, el "
"sistema de referència per a comptar els números dels fotogrames extrets és "
"el projecte. I aquesta és la raó per la qual cal dir-li a ``extract-frames`` "
"el vostre projecte: l'script aprèn la velocitat dels fotogrames del projecte "
"per tal de gestionar correctament el número de fotograma. Sense ell, més "
"endavant s'extraurien els fotogrames erronis en cas que el metratge en brut "
"tingui una velocitat dels fotogrames diferent i, per tant, un recompte de "
"fotogrames del vostre projecte."

#: ../../glossary/useful_information/extract_frames_in_higher_res_than_project_profile.rst:91
msgid ""
"The frame image files can be located not only in the project directory, but "
"also in sub directories. The script won’t find any extracted frame PNG files "
"outside your project directory sub-tree, because it doesn’t scan your "
"Kdenlive project for extracted frames."
msgstr ""
"Els fitxers d'imatge de fotograma es poden localitzar no només en el "
"directori del projecte, sinó també en subdirectoris. L'script no trobarà cap "
"fitxer PNG de fotograma extret fora del subarbre del vostre directori de "
"projecte, perquè no escaneja el vostre projecte Kdenlive cercant fotogrames "
"extrets."

#: ../../glossary/useful_information/extract_frames_in_higher_res_than_project_profile.rst:94
msgid ""
"Do not change the project frame rate afterwards, or the frame extraction "
"will get messed up. That’s because the frame numbers from the filenames "
"don’t match the project settings anymore."
msgstr ""
"No canvieu la velocitat dels fotogrames del projecte després, o l'extracció "
"del fotograma s'embolicarà. Això es deu al fet que els números dels "
"fotogrames dels noms de fitxer ja no coincidiran amb els paràmetres del "
"projecte."

# skip-rule: t-acc_obe
#: ../../glossary/useful_information/extract_frames_in_higher_res_than_project_profile.rst:103
msgid ""
"After you’ve run the ``extract-frames`` script, you should notice in "
"Kdenlive’s clip properties pane that the extracted frame PNGs are now having "
"the correct original frame size and aspect ratio. Just for completeness: the "
"clip preview monitor now shows the same image quality as the original raw "
"footage does. So you’re all set to finalize and tape-out, erm, render your "
"project."
msgstr ""
"Després d'executar l'script ``extract-frames``, hauríeu d'observar en la "
"subfinestra de propietats del clip del Kdenlive que el PNG de fotograma "
"extret ara té la mida i la relació d'aspecte originals correctes. I per "
"acabar: el monitor de previsualització del clip ara mostra la mateixa "
"qualitat d'imatge que el metratge original. Així que està tot a punt de "
"finalitzar i de gravar, ehem, renderitzar el projecte."

#: ../../glossary/useful_information/extract_frames_in_higher_res_than_project_profile.rst:112
msgid ""
"If you happen to see color changes at this stage, then please check out our "
"other Toolbox post about :ref:"
"`color_hell_ffmpeg_transcoding_and_preserving_BT.601`."
msgstr ""
"Si veieu canvis de color en aquesta etapa, consulteu l'altre article nostre "
"del Quadre d'eines sobre :ref:"
"`color_hell_ffmpeg_transcoding_and_preserving_BT.601`."

#: ../../glossary/useful_information/extract_frames_in_higher_res_than_project_profile.rst:117
msgid "Work Flow in a Nutshell"
msgstr "El flux de treball en poques paraules"

# skip-rule: t-acc_obe
#: ../../glossary/useful_information/extract_frames_in_higher_res_than_project_profile.rst:119
msgid ""
"Your overall workflow doesn’t change much, just throw running the ``extract-"
"frames`` script inside your Kdenlive project directory whenever you need to "
"update the frame image files with full-resolution images from your raw "
"footage. You can run and rerun the script at any time while you work on your "
"project."
msgstr ""
"El vostre flux de treball general no canvia molt, senzillament executeu "
"l'script ``extract-frames`` dins del directori de projecte del Kdenlive "
"sempre que necessiteu actualitzar els fitxers d'imatge de fotograma amb "
"imatges de màxima resolució del vostre metratge en brut. Podeu executar i "
"tornar a executar l'script en qualsevol moment mentre treballeu en el "
"projecte."

#: ../../glossary/useful_information/extract_frames_in_higher_res_than_project_profile.rst:121
msgid ""
"But remember to not change the project frame rate while working on your "
"project, as this will put the frame numbers encoded in the filenames out of "
"sync."
msgstr ""
"Però recordeu no canviar la velocitat dels fotogrames del projecte mentre "
"treballeu en el projecte, ja que això posarà els números dels fotogrames "
"codificats en els noms dels fitxers fora de sincronització."

#: ../../glossary/useful_information/extract_frames_in_higher_res_than_project_profile.rst:125
msgid "References"
msgstr "Referències"

# skip-rule: t-acc_obe
#: ../../glossary/useful_information/extract_frames_in_higher_res_than_project_profile.rst:127
msgid ""
"`extract-frames GitHubGist <https://gist.githubusercontent.com/"
"TheDiveO/57fd76e4d15252232aaacc7e422a79a2/raw/"
"b3e605eb74737916bffa55bbc1b907e29ee7016d/extract-frames>`_"
msgstr ""
"`extract-frames GitHubGist <https://gist.githubusercontent.com/"
"TheDiveO/57fd76e4d15252232aaacc7e422a79a2/raw/"
"b3e605eb74737916bffa55bbc1b907e29ee7016d/extract-frames>`_"

# skip-rule: t-acc_obe
#: ../../glossary/useful_information/extract_frames_in_higher_res_than_project_profile.rst:129
msgid ""
"`Vertical Video Syndrome <https://www.youtube.com/watch?v=f2picMQC-9E>`_ – A "
"PSA by Glove and Boots on YouTube."
msgstr ""
"`Síndrome de vídeo vertical <https://www.youtube.com/watch?v=f2picMQC-9E>`_ "
"– Un PSA de Glove and Boots a YouTube."

#: ../../glossary/useful_information/extract_frames_in_higher_res_than_project_profile.rst:131
msgid ":ref:`color_hell_ffmpeg_transcoding_and_preserving_BT.601`"
msgstr ":ref:`color_hell_ffmpeg_transcoding_and_preserving_BT.601`"

# Translation of docs_kdenlive_org_effects_and_compositions___transitions___alphaatop.po to Catalan
# Copyright (C) 2021 This_file_is_part_of_KDE
# Licensed under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2021.
# Josep M. Ferrer <txemaq@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: documentation-docs-kdenlive-org\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-03 00:38+0000\n"
"PO-Revision-Date: 2021-12-03 07:47+0100\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.11.90\n"

#: ../../effects_and_compositions/transitions/alphaatop.rst:11
msgid "alphaatop transition"
msgstr "Transició ATOP d'alfa"

#: ../../effects_and_compositions/transitions/alphaatop.rst:13
msgid "Contents"
msgstr "Contingut"

# skip-rule: t-acc_obe
#: ../../effects_and_compositions/transitions/alphaatop.rst:15
msgid ""
"This is the `Frei0r alphaatop <https://www.mltframework.org/plugins/"
"TransitionFrei0r-alphaatop>`_ MLT transition."
msgstr ""
"Aquesta és la `transició «alphaatop» de Frei0r <https://www.mltframework.org/"
"plugins/TransitionFrei0r-alphaatop>`_ del MLT."

#: ../../effects_and_compositions/transitions/alphaatop.rst:17
msgid "The alpha ATOP operation."
msgstr "L'operació ATOP d'alfa."

#: ../../effects_and_compositions/transitions/alphaatop.rst:19
msgid "Yellow clip has a triangle alpha shape with min=0 and max=618."
msgstr "El clip groc té una forma alfa de triangle amb mínim=0 i màxim=618."

#: ../../effects_and_compositions/transitions/alphaatop.rst:21
msgid "Green clip has rectangle alpha shape with min=0 and max=1000."
msgstr "El clip verd té una forma alfa de triangle amb mínim=0 i màxim=1000."

#: ../../effects_and_compositions/transitions/alphaatop.rst:23
msgid "alphaatop is the transition in between."
msgstr "«alphaatop» és la transició entremig."

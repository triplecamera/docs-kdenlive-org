# Translation of docs_kdenlive_org_glossary___useful_information___fixing_slow_audio_fade-ins.po to Catalan
# Copyright (C) 2021 This_file_is_part_of_KDE
# Licensed under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: documentation-docs-kdenlive-org\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-19 00:37+0000\n"
"PO-Revision-Date: 2021-12-19 12:23+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.12.0\n"

# skip-rule: k-Ins-1
#: ../../glossary/useful_information/fixing_slow_audio_fade-ins.rst:13
msgid "Fixing Unwanted Slow Audio Fade-Ins with Some USB Audio Cards"
msgstr ""
"Correcció de foses d'entrada d'àudio lentes no desitjades amb algunes "
"targetes d'àudio USB"

#: ../../glossary/useful_information/fixing_slow_audio_fade-ins.rst:15
msgid ""
"Do you suffer from an unwanted slow audio fade-ins whenever starting "
"playback in the timeline or in the clip monitor, while you don’t have any "
"audio fade effects applied at all? Turns out this is some odd interference "
"between some(!) USB audio cards and the PulseAudio sound backend…"
msgstr ""
"Patiu de foses d'entrada d'àudio lentes no desitjades quan s'inicia la "
"reproducció en la línia de temps o en el monitor de clips, mentre que no "
"s'aplica cap efecte d'esvaïment d'àudio? Resulta que aquesta és una "
"interferència estranya entre algunes targetes d'àudio USB i el dorsal de so "
"PulseAudio…"

#: ../../glossary/useful_information/fixing_slow_audio_fade-ins.rst:18
msgid "ALSA to the Res-Cue"
msgstr "L'ALSA al rescat"

#: ../../glossary/useful_information/fixing_slow_audio_fade-ins.rst:27
msgid "Luckily, there’s an easy remedy in case you’re affected."
msgstr "Per sort, hi ha un remei fàcil en cas que us veieu afectat."

#: ../../glossary/useful_information/fixing_slow_audio_fade-ins.rst:29
msgid ""
"Go to the menu :menuselection:`Settings > Configure Kdenlive`, then in the "
"configuration dialog select the :guilabel:`Playback` section. Change the :"
"guilabel:`Audio driver` from :guilabel:`Automatic` to :guilabel:`ALSA`. "
"Leave the Audio device set to “Default”, so your desktop audio device "
"settings apply."
msgstr ""
"Aneu al menú :menuselection:`Arranjament > Configura el Kdenlive`, després "
"en el diàleg de configuració seleccioneu la secció :guilabel:`Reproducció`. "
"Canvieu el :guilabel:`Controlador d'àudio` d':guilabel:`Automàtic` a :"
"guilabel:`ALSA`. Deixeu el dispositiu d'àudio establert a «Predeterminat», "
"de manera que s'apliqui la configuració del dispositiu d'àudio de "
"l'escriptori."

#: ../../glossary/useful_information/fixing_slow_audio_fade-ins.rst:31
msgid "Click :guilabel:`OK`, and you’re done."
msgstr "Feu clic a :guilabel:`D'acord`, i haureu acabat."

#: ../../glossary/useful_information/fixing_slow_audio_fade-ins.rst:33
msgid ""
"Your timeline and bin clip audio playback should now be working as expected, "
"without any unwanted slow audio fade-ins anymore."
msgstr ""
"La línia de temps i la reproducció d'àudio de clips de paper haurien d'estar "
"funcionant com s'espera, sense que s'hagin fet més foses d'entrada d'àudio "
"lentes no desitjades."

#: ../../glossary/useful_information/fixing_slow_audio_fade-ins.rst:36
msgid ""
"You’ll find the corresponding option in the main menu :menuselection:"
"`Timeline --> Disable Timeline Effects`. This disables or re-enables all "
"timeline effects, that is, timeline clip effects and track effects."
msgstr ""
"Trobareu l'opció corresponent en el menú principal :menuselection:`Línia de "
"temps --> Desactiva els efectes de la línia de temps`. Això desactiva o "
"reactiva tots els efectes de la línia de temps, és a dir, els efectes de la "
"línia de temps i els efectes de la pista."

#: ../../glossary/useful_information/fixing_slow_audio_fade-ins.rst:38
msgid ""
"However, please note that prior to Kdenlive 16.08.1, track effects are not "
"properly disabled or re-enabled by :menuselection:`Timeline --> Disable "
"Timeline Effects`."
msgstr ""
"No obstant això, tingueu en compte que abans del Kdenlive 16.08.1, els "
"efectes de pista no estan correctament desactivats o reactivats per :"
"menuselection:`Línia de temps --> Desactiva els efectes de la línia de "
"temps`."

#: ../../glossary/useful_information/fixing_slow_audio_fade-ins.rst:40
msgid ""
"Please see :ref:`effects_everywhere` about how to temporarily disable bin "
"clip effects."
msgstr ""
"Si us plau, vegeu :ref:`effects_everywhere` sobre com desactivar "
"temporalment els efectes dels clips de la safata."

#: ../../glossary/useful_information/fixing_slow_audio_fade-ins.rst:45
msgid "Background Information"
msgstr "Informació de fons"

#: ../../glossary/useful_information/fixing_slow_audio_fade-ins.rst:47
msgid ""
"Please note that the unwanted ~2 seconds audio fade-in only happens with "
"some USB audio cards, but not others. I’ve noticed when trying a Steinberg "
"UR22mkII USB audio interface."
msgstr ""
"Tingueu en compte que la fosa d'entrada d'àudio no desitjat de ~2 segons "
"només passa amb algunes targetes d'àudio USB, però no amb altres. M'he "
"adonat en provar una interfície d'àudio USB Steinberg UR22mkII."

#: ../../glossary/useful_information/fixing_slow_audio_fade-ins.rst:49
msgid ""
"Using the UR22mkII in Kdenlive using the stock audio settings was "
"impossible, as the automatic fade-in made any voice over editing a complete "
"and utter fail. Curiously, the UR22mkII worked beautifully when I played "
"back audio using an Android tablet (that’s a beautiful aspect of the "
"UR22mkII: it’s designed to be used with mobile devices). For comparism, my "
"(much more bulky) Behringer QX1202USB doesn’t exhibit the strange behavior "
"even with the default audio settings in Kdenlive, or when using PulseAudio."
msgstr ""
"Usar la UR22mkII en el Kdenlive utilitzant la configuració normal d'àudio va "
"ser impossible, ja que la fosa d'entrada automàtica provocava que qualsevol "
"veu sobre l'edició fos un error complet i absolut. Curiosament, la UR22mkII "
"funcionava molt bé quan vaig reproduir àudio utilitzant una tauleta Android "
"(aquest és un aspecte bo de la UR22mkII: està dissenyada per ser utilitzada "
"amb dispositius mòbils). Per comparació, la meva (molt més voluminosa) "
"Behringer QX1202USB no exhibeix el comportament estrany fins i tot amb la "
"configuració d'àudio predeterminada al Kdenlive, o quan s'utilitza el "
"PulseAudio."

#: ../../glossary/useful_information/fixing_slow_audio_fade-ins.rst:51
msgid ""
"And what is even more strange and surprising: at least some other software, "
"such as VLC, are unaffected either, even when using PulseAudio for audio "
"output."
msgstr ""
"I el que és encara més estrany i sorprenent: almenys altre programari, com "
"el VLC, tampoc es veu afectat, fins i tot quan s'utilitza el PulseAudio per "
"a la sortida d'àudio."

#: ../../glossary/useful_information/fixing_slow_audio_fade-ins.rst:53
msgid ""
"Unfortunately, no-one was able to give me the solution; but luckily, in the "
"end I found it myself after some trial and error. Hopefully my solution is "
"of help to those Kdenlive users experiencing the same strange audio fade-in "
"behavior."
msgstr ""
"Malauradament, ningú em va poder donar la solució, però, afortunadament, al "
"final la vaig trobar jo mateix després de diverses proves i errors. Espero "
"que la meva solució serveixi per ajudar els usuaris del Kdenlive que "
"experimentin el mateix estrany comportament de fosa d'entrada d'àudio."

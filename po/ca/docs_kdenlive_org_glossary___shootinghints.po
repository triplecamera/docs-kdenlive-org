# Translation of docs_kdenlive_org_glossary___shootinghints.po to Catalan
# Copyright (C) 2021 This_file_is_part_of_KDE
# Licensed under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2021.
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: documentation-docs-kdenlive-org\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-02-27 00:38+0000\n"
"PO-Revision-Date: 2021-11-30 20:54+0100\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.11.90\n"

#: ../../glossary/shootinghints.rst:15
msgid "Shooting Hints"
msgstr "Consells en disparar"

#~ msgid "Contents"
#~ msgstr "Contingut"

#~ msgid ""
#~ "Using P2 footage from the Panasonic HVX200 on GNU/Linux (tested on Ubuntu)"
#~ msgstr ""
#~ "Usant un metratge P2 des de la Panasonic HVX200 en el GNU/Linux (provat a "
#~ "l'Ubuntu)"

#~ msgid ""
#~ "Using footage from P2 cards is easy when you know how! The MXF files on "
#~ "P2 cards cannot be read until you convert them with **mxfsplit**, a part "
#~ "of **FreeMXF**. The conversion is lossless and the resulting files "
#~ "contain both video and audio and can be edited in real time with "
#~ "**Kdenlive** (or **Blender 2.5+**) on most computers made within the last "
#~ "five years or so. Also, **FFMPEG** can read these files. This process is "
#~ "very fast because there is no transcoding and so can be done in the field "
#~ "while shooting just as fast as simply transferring the original P2 files."
#~ msgstr ""
#~ "Usar un metratge de les targetes P2 és fàcil quan saps com fer-ho! Els "
#~ "fitxers MXF de les targetes P2 no es poden llegir fins que els convertiu "
#~ "amb **mxfsplit**, una part de **FreeMXF**. La conversió és sense pèrdua i "
#~ "els fitxers resultants contenen vídeo i àudio i es poden editar en temps "
#~ "real amb el **Kdenlive** (o el **Blender 2.5+**) en la majoria dels "
#~ "ordinadors fets en els últims cinc anys, aproximadament. A més, el "
#~ "**FFMPEG** pot llegir aquests fitxers. Aquest procés és molt ràpid perquè "
#~ "no hi ha transcodificació i així es pot fer en el moment mentre es "
#~ "dispara i es transfereixen els fitxers P2 originals."

#~ msgid "Step One: FreeMXF"
#~ msgstr "Primer pas: FreeMXF"

# skip-rule: t-acc_obe
#~ msgid ""
#~ "Get the source code for **MFXlib** from `here <http://sourceforge.net/"
#~ "projects/mxflib/>`_."
#~ msgstr ""
#~ "Obteniu el codi font de **MFXlib** des d'`aquí <http://sourceforge.net/"
#~ "projects/mxflib/>`_."

#~ msgid ""
#~ "Then configure, compile, and install it by running the following code in "
#~ "the directory where you saved the source files:"
#~ msgstr ""
#~ "Després configureu, compileu i instal·leu-lo executant el codi següent en "
#~ "el directori on heu desat els fitxers del codi font:"

#~ msgid "This will get **mxfsplit** (part of **mxflib**) working."
#~ msgstr ""
#~ "Amb això obtindreu un **mxfsplit** (part del **mxflib**) que funcioni."

#~ msgid "Step Two: Using mxfsplit"
#~ msgstr "Segon pas: Ús de «mxfsplit»"

#~ msgid ""
#~ "Here is a simple script that can be run in the terminal. It will convert "
#~ "all MXF files in a chosen directory into usable files. Do a search and "
#~ "replace for /source/directory and /destination/directory"
#~ msgstr ""
#~ "Aquí hi ha un script senzill que es pot executar al terminal. Converteix "
#~ "tots els fitxers MXF en un directori escollit en fitxers utilitzables. "
#~ "Feu una cerca i substitució de /source/directory i /destination/directory"

#~ msgid "Conclusion"
#~ msgstr "Conclusió"

#~ msgid ""
#~ "Now you have a script that can easily prepare footage for editing (e.g. "
#~ "with **Kdenlive** or **Blender**) and for transcoding. **FFMPEG** can be "
#~ "used to transcode the resulting .MXF files to whatever format is "
#~ "preferred. For example, the following code would get the files ready for "
#~ "**Youtube**, **Vimeo**, etc.:"
#~ msgstr ""
#~ "Ara teniu un script que pot preparar amb facilitat el metratge per a "
#~ "l'edició (p. ex. amb el **Kdenlive** o el **Blender**) i per a la "
#~ "transcodificació. El **FFMPEG** es pot utilitzar per a transcodificar els "
#~ "fitxers .MXF resultants a qualsevol format que es prefereixi. Per "
#~ "exemple, el codi següent permetria que els fitxers estiguin llestos per a "
#~ "**Youtube**, **Vimeo**, etc.:"

# Translation of docs_kdenlive_org_user_interface___menu___file_menu.po to Catalan
# Copyright (C) 2021 This_file_is_part_of_KDE
# Licensed under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2021.
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: documentation-docs-kdenlive-org\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-18 00:38+0000\n"
"PO-Revision-Date: 2021-12-21 10:37+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../user_interface/menu/file_menu.rst:17
msgid "File Menu"
msgstr "Menú Fitxer"

#: ../../user_interface/menu/file_menu.rst:19
msgid "Contents"
msgstr "Contingut"

#: ../../user_interface/menu/file_menu.rst:24
msgid "New"
msgstr "Nou"

#: ../../user_interface/menu/file_menu.rst:26
msgid ""
"Creates a new Kdenlive project. The default keyboard shortcut is :kbd:`Ctrl "
"+ N`."
msgstr ""
"Crea un projecte nou del Kdenlive. La drecera predeterminada del teclat és :"
"kbd:`Ctrl + N`."

#: ../../user_interface/menu/file_menu.rst:28
msgid "See :ref:`quickstart`."
msgstr "Vegeu :ref:`quickstart`."

#: ../../user_interface/menu/file_menu.rst:30
msgid ""
"The default settings that appear on this feature are defined under :"
"menuselection:`Settings --> Configure Kdenlive` > :ref:`configure_kdenlive`."
msgstr ""
"Els paràmetres predeterminats que apareixen en aquesta característica es "
"defineixen a :menuselection:`Arranjament --> Configura el Kdenlive` > :ref:"
"`configure_kdenlive`."

#: ../../user_interface/menu/file_menu.rst:33
msgid "Open..."
msgstr "Obre..."

#: ../../user_interface/menu/file_menu.rst:35
msgid ""
"Opens a project that has been saved in a :ref:`project` format file. The "
"default keyboard shortcut is :kbd:`Ctrl + O`."
msgstr ""
"Obre un projecte que s'ha desat en un fitxer amb format, :ref:`project`. La "
"drecera predeterminada del teclat és :kbd:`Ctrl + O`."

#: ../../user_interface/menu/file_menu.rst:38
msgid "Open Recent"
msgstr "Obre'n un de recent"

#: ../../user_interface/menu/file_menu.rst:40
msgid ""
"Displays a picklist of recently saved files (up to 10) to choose from. Click "
"the :menuselection:`Clear List` choice when you want to start over with a "
"fresh list."
msgstr ""
"Mostra una llista de selecció dels fitxers desats recentment (fins a 10) per "
"a triar. Feu clic a l'opció :menuselection:`Neteja la llista` quan vulgueu "
"començar amb una llista buida."

#: ../../user_interface/menu/file_menu.rst:43
msgid "Save"
msgstr "Desa"

#: ../../user_interface/menu/file_menu.rst:45
msgid ""
"Saves the current state of the project in a :ref:`project` format file. "
"Prompts for a file name if this is the first time the file is being saved. "
"The default keyboard shortcut is :kbd:`Ctrl + S`."
msgstr ""
"Desa l'estat actual del projecte en un fitxer amb format, :ref:`project`. "
"Demanarà un nom de fitxer si aquesta és la primera vegada que es desa el "
"fitxer. La drecera predeterminada del teclat és :kbd:`Ctrl + S`."

#: ../../user_interface/menu/file_menu.rst:48
msgid "Save As..."
msgstr "Desa com a..."

#: ../../user_interface/menu/file_menu.rst:50
msgid ""
"Saves the current state of the project in a :ref:`project` format file of "
"your choice. The default keyboard shortcut is :kbd:`Ctrl + Shift + S`."
msgstr ""
"Desa l'estat actual del projecte en un fitxer amb el format que trieu, :ref:"
"`project`. La drecera predeterminada del teclat és :kbd:`Ctrl + Maj + S`."

#: ../../user_interface/menu/file_menu.rst:53
msgid "Save Copy..."
msgstr "Desa una còpia..."

#: ../../user_interface/menu/file_menu.rst:56
msgid "Revert"
msgstr "Reverteix"

#: ../../user_interface/menu/file_menu.rst:58
msgid ""
"This abandons any changes to the project you have made since last saving and "
"reverts back to the last saved version of the project."
msgstr ""
"Això abandonarà qualsevol canvi al projecte que hàgiu fet des de l'última "
"desada i reverteix a la darrera versió desada del projecte."

#: ../../user_interface/menu/file_menu.rst:61
msgid "Transcode Clips..."
msgstr "Transcodifica els clips..."

#: ../../user_interface/menu/file_menu.rst:68
msgid ""
"Use this to convert a video or audio clip from one codec/format to another."
msgstr ""
"Empreu-ho per a convertir un clip de vídeo o àudio des d'un còdec/format a "
"un altre."

#: ../../user_interface/menu/file_menu.rst:70
msgid ""
"Choose one source file or multiple source files and a profile that "
"represents the desired destination codec/format. Optionally change the "
"destination path and file name and hit :menuselection:`Start`. Otherwise, "
"hit :menuselection:`Abort` to close the windows."
msgstr ""
"Trieu un fitxer d'origen o diversos fitxers d'origen i un perfil que "
"representi el còdec/format de destinació desitjat. De manera opcional, "
"canvieu el camí de destinació i el nom de fitxer, i premeu :menuselection:"
"`Inicia`. Altrament, premeu :menuselection:`Interromp` per a tancar les "
"finestres."

#: ../../user_interface/menu/file_menu.rst:72
msgid ""
"Transcoding a clip should be faster than loading the clip into the timeline "
"and re-encoding it into a different format."
msgstr ""
"Transcodificar un clip hauria de ser més ràpid que carregar el clip en la "
"línia de temps i tornar a codificar-lo en un format diferent."

#: ../../user_interface/menu/file_menu.rst:74
msgid ""
":menuselection:`Add clip to project` controls if after the conversion, the "
"new clip is added to the :ref:`project_tree`."
msgstr ""
":menuselection:`Afegeix el clip al projecte`: controla si després de la "
"conversió, el clip nou s'afegirà a :ref:`project_tree`."

#: ../../user_interface/menu/file_menu.rst:76
msgid ""
":menuselection:`Close after encode` Uncheck this checkbox if there is the "
"need to convert to another format after the conversion."
msgstr ""
":menuselection:`Tanca després de codificar`: desmarqueu aquesta casella de "
"selecció si hi ha necessitat de convertir a un altre format després de la "
"conversió."

#: ../../user_interface/menu/file_menu.rst:79
msgid "Close"
msgstr "Tanca"

#: ../../user_interface/menu/file_menu.rst:81
msgid ""
"Not sure what this is supposed to do. It is always greyed out on my "
"**Kdenlive**."
msgstr ""
"No està clar que se suposa que fa això. Sempre està en gris en el meu "
"**Kdenlive**."

#: ../../user_interface/menu/file_menu.rst:83
msgid ""
"Maybe it is there ready for a version of **Kdenlive** that can have more "
"than one project open at a time."
msgstr ""
"Potser està aquí preparat per a una versió del **Kdenlive** que pugui tenir "
"obert més d'un projecte alhora."

#: ../../user_interface/menu/file_menu.rst:86
msgid "Quit"
msgstr "Surt"

#: ../../user_interface/menu/file_menu.rst:88
msgid ""
"Exits **Kdenlive**. Prompts you to save any unsaved changes. The default "
"keyboard shortcut is :kbd:`Ctrl + Q`."
msgstr ""
"Surt del **Kdenlive**. Pregunta si cal desar qualsevol canvi sense desar. La "
"drecera predeterminada del teclat és :kbd:`Ctrl + Q`."

#: ../../user_interface/menu/file_menu.rst:90
msgid "Contents:"
msgstr "Contingut:"

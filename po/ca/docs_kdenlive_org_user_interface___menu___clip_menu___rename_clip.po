# Translation of docs_kdenlive_org_user_interface___menu___clip_menu___rename_clip.po to Catalan
# Copyright (C) 2021 This_file_is_part_of_KDE
# Licensed under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: documentation-docs-kdenlive-org\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-21 00:37+0000\n"
"PO-Revision-Date: 2021-11-15 10:34+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../user_interface/menu/clip_menu/rename_clip.rst:12
msgid "Rename Clip"
msgstr "Reanomena el clip"

#: ../../user_interface/menu/clip_menu/rename_clip.rst:14
msgid "Contents"
msgstr "Contingut"

#: ../../user_interface/menu/clip_menu/rename_clip.rst:16
msgid ""
"Change the name of a clip in the Project Bin to an arbitrary name. Does not "
"rename the file on the file system."
msgstr ""
"Canvia el nom d'un clip a la safata del projecte a un nom arbitrari. No "
"reanomena el fitxer en el sistema de fitxers."

#: ../../user_interface/menu/clip_menu/rename_clip.rst:18
msgid ""
"This menu item is available from :ref:`project_tree` on a clip in the "
"Project Bin."
msgstr ""
"Aquest element de menú és disponible des de :ref:`project_tree` en un clip a "
"la safata del projecte."

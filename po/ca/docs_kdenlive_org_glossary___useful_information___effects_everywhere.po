# Translation of docs_kdenlive_org_glossary___useful_information___effects_everywhere.po to Catalan
# Copyright (C) 2021 This_file_is_part_of_KDE
# Licensed under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2021.
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: documentation-docs-kdenlive-org\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-27 00:37+0000\n"
"PO-Revision-Date: 2021-12-27 09:15+0100\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.0\n"

#: ../../glossary/useful_information/effects_everywhere.rst:13
msgid "Effects everywhere"
msgstr "Efectes per tot arreu"

#: ../../glossary/useful_information/effects_everywhere.rst:15
msgid ""
"Did you know that you can **apply effects** not only to clips in the "
"timeline, but also to **project bin clips** and even to specific **tracks in "
"the timeline**?"
msgstr ""
"Sabíeu que podeu **aplicar els efectes** no només als clips de la línia de "
"temps, sinó també a **clips de la safata del projecte** i fins i tot a "
"**pistes específiques de la línia de temps**?"

#: ../../glossary/useful_information/effects_everywhere.rst:19
msgid "Clip Effects"
msgstr "Efectes de clip"

#: ../../glossary/useful_information/effects_everywhere.rst:23
msgid ""
"Probably most of the time, many Kdenlive users will simply slap **individual "
"effects on individual timeline clips**. For instance, as lighting conditions "
"vary *within* the same source clip, multiple scenes taken from it might be "
"in need of individual grading. Or you need to crop and place an individual "
"clip, separately from others. So we’re all used to it, and we do it almost "
"unconsciously."
msgstr ""
"Probablement la major part del temps, molts usuaris del Kdenlive simplement "
"ensopegaran amb **efectes individuals en clips individuals de la línia de "
"temps**. Per exemple, com que les condicions d'il·luminació varien *dins* "
"del mateix clip d'origen, diverses escenes preses poden necessitar "
"graduacions individuals. O cal tallar i col·locar un clip individual, per "
"separat d'altres. Així que estem acostumats a això, i ho fem gairebé "
"inconscientment."

#: ../../glossary/useful_information/effects_everywhere.rst:25
msgid ""
"But in some situations, we would like to ease and speed up our timeline "
"work. Instead of laboriously setting up effect after effect on individual "
"timeline clips, we want to add effects to either a specific bin clip or a "
"timeline track once and for all…"
msgstr ""
"Però en algunes situacions ens agradaria facilitar i accelerar el nostre "
"treball en la línia de temps. En lloc de definir laboriosament un efecte "
"després d'un altre efecte en els clips individuals de línia de temps, volem "
"afegir efectes a un clip específic de la safata o a tots els d'una pista de "
"la línia de temps…"

#: ../../glossary/useful_information/effects_everywhere.rst:28
msgid ""
"Did you know that you can temporarily :ref:`disable_all_timeline_effects`? "
"Use :menuselection:`Timeline --> Disable timeline effects`."
msgstr ""
"Sabíeu que podeu :ref:`disable_all_timeline_effects` temporalment? "
"Utilitzeu :menuselection:`Línia de temps --> Desactiva els efectes de la "
"línia de temps`."

#: ../../glossary/useful_information/effects_everywhere.rst:31
msgid "Effects on Project Bin Clips"
msgstr "Efectes en els clips de la safata del projecte"

#: ../../glossary/useful_information/effects_everywhere.rst:35
msgid ""
"Effects on bin clips allow you to, for instance, color grade a clip itself. "
"All copies of it that you use in the timeline then will automatically use "
"these effects. Also, all changes you make to the bin clip will immediately "
"become effective on all copies in the timeline."
msgstr ""
"Els efectes en els clips de la safata us permeten, per exemple, graduar el "
"color en el clip mateix. Totes les còpies que utilitzeu a la línia de temps "
"utilitzaran automàticament aquests efectes. A més, tots els canvis que feu "
"al clip de la safata seran immediatament efectius en totes les còpies a la "
"línia de temps."

#: ../../glossary/useful_information/effects_everywhere.rst:39
msgid ""
"Please note that bin clip effects are applied first, before any timeline "
"clip effects."
msgstr ""
"Tingueu en compte que primer s'apliquen els efectes del clip de la safata, "
"abans de qualsevol efecte del clip a la línia de temps."

#: ../../glossary/useful_information/effects_everywhere.rst:45
msgid "Apply Effects to Bin Clips"
msgstr "Aplicar efectes als clips de la safata"

#: ../../glossary/useful_information/effects_everywhere.rst:47
msgid ""
"To apply effects to a clip in the project bin in general, simply drag and "
"drop an effect from the effects pane **(1)** into your clip in the project "
"bin **(2)**. The parameters pane **(3)** then will switch to the effects "
"applied to this particular bin clip. Adjust as you like."
msgstr ""
"Per aplicar efectes a un clip en la safata del projecte en general, "
"simplement arrossegueu i deixeu anar un efecte de la subfinestra d'efectes "
"**(1)** al vostre clip en la safata del projecte **(2)**. Llavors la "
"subfinestra de paràmetres **(3)** canviarà als efectes aplicats a aquest "
"clip de la safata en particular. Ajusteu com vulgueu."

#: ../../glossary/useful_information/effects_everywhere.rst:49
msgid ""
"If you later need to return to the bin clip effects in order to edit them "
"again, simply select the clip in the project bin. The parameters pane "
"**(3)** then will automatically switch to your bin clip’s effect stack."
msgstr ""
"Si més tard heu de tornar als efectes del clip de la safata per tal d'editar-"
"los de nou, simplement seleccioneu el clip a la safata del projecte. La "
"subfinestra de paràmetres **(3)** llavors canviarà automàticament a la pila "
"d'efectes del clip de la safata."

#: ../../glossary/useful_information/effects_everywhere.rst:52
msgid "Compare Before/After Effects"
msgstr "Compara els efectes d'abans/després"

#: ../../glossary/useful_information/effects_everywhere.rst:54
msgid ""
"**Please note** the split compare button at the top of the parameters pane "
"**(3)**: when active, the **clip monitor (4)** shows your clip in a before/"
"after fashion:"
msgstr ""
"**Tingueu present** que el botó de divisió de la comparació a la part "
"superior de la subfinestra de paràmetres **(3)**: quan està actiu, el "
"**monitor de clips (4)** mostra el vostre clip de manera anterior/després:"

#: ../../glossary/useful_information/effects_everywhere.rst:56
msgid ""
"**left half** of clip monitor **(4)**: your bin clip with all effects "
"applied; the text «effect» to the left of the red divider **(4)** is a "
"reminder of which side is showing effects, and which side is without effects."
msgstr ""
"**meitat esquerra** del monitor de clips **(4)**: el clip de la safata amb "
"tots els efectes aplicats; el text «efecte» a l'esquerra del divisor vermell "
"**(4)** és un recordatori de quin costat està mostrant els efectes, i quin "
"costat és sense efectes."

#: ../../glossary/useful_information/effects_everywhere.rst:57
msgid "**right half**: your bin clip **without any effects applied**."
msgstr ""
"**meitat dreta**: el vostre clip de la safata **sense cap efecte aplicat**."

#: ../../glossary/useful_information/effects_everywhere.rst:59
msgid ""
"While hovering your mouse cursor over the clip monitor, you should notice a "
"red vertical divider line appearing. Drag it to dynamically change the split "
"between the clip parts with/without effects."
msgstr ""
"Mentre passeu el cursor del ratolí per sobre del monitor de clips, haureu "
"d'observar que apareix una línia divisòria vermella vertical. Arrossegueu-la "
"per canviar dinàmicament la divisió entre les parts del clip amb/sense "
"efectes."

#: ../../glossary/useful_information/effects_everywhere.rst:62
msgid "Temporarily Disable Bin Clip Effects"
msgstr "Desactivar temporalment els efectes dels clips de la safata"

#: ../../glossary/useful_information/effects_everywhere.rst:64
msgid ""
"You can (temporarily) **disable all effects on a single bin clip**, by "
"selecting it and then **un-checking** the **Bin effects for…** box at the "
"top of the parameters pane **(3)**. This works exactly the same as with "
"effects applied to timeline clips."
msgstr ""
"Podeu (temporalment) **desactivar tots els efectes d'un únic clip de la "
"safata**, seleccionant-lo i després **desmarcant** la casella **Efectes de "
"la safata per a…** a la part superior de la subfinestra de paràmetres "
"**(3)**. Això funciona exactament igual que amb els efectes aplicats als "
"clips de línia de temps."

#: ../../glossary/useful_information/effects_everywhere.rst:73
msgid ""
"Bin clips that have effects directly applied on them show the effects "
"signet: a star. It is overlaid on the clip thumbnail, as you can see to the "
"right."
msgstr ""
"Els clips de la safata que tenen efectes directament aplicats en ells "
"mostren un senyal d'efecte: una estrella. Està sobreposat a la miniatura del "
"clip, com es pot veure a la dreta."

#: ../../glossary/useful_information/effects_everywhere.rst:78
msgid "Temporarily disable ALL bin effects"
msgstr "Desactivar temporalment TOTS els efectes de la safata"

#: ../../glossary/useful_information/effects_everywhere.rst:80
msgid ""
"You can also temporarily disable all bin effects at once, using either a "
"keyboard shortcut or a toolbar button. Unfortunately, this function is "
"slightly hidden, as there is (yet) no menu item for it. You’ll need to "
"either configure a shortcut for this action, or add the action to a toolbar:"
msgstr ""
"També es poden desactivar temporalment tots els efectes de la safata alhora, "
"utilitzant una drecera de teclat o un botó de la barra d'eines. "
"Malauradament, aquesta funció està lleugerament oculta, ja que (encara) no "
"hi ha cap element de menú per a ella. Haureu de configurar una drecera per "
"aquesta acció, o afegir l'acció a una barra d'eines:"

#: ../../glossary/useful_information/effects_everywhere.rst:82
msgid ""
"To configure a keyboard shortcut, go to :menuselection:`Settings --> "
"Configure Shortcuts…`, then search for :guilabel:`Disable Bin Effects`. Now "
"set your desired shortcut, click :guilabel:`OK`. Done."
msgstr ""
"Per configurar una drecera de teclat, aneu a :menuselection:`Arranjament --> "
"Configura les dreceres…`, després cerqueu :guilabel:`Desactiva els efectes "
"de la safata`. Ara definiu la drecera desitjada, feu clic a :guilabel:"
"`D'acord`. Fet."

#: ../../glossary/useful_information/effects_everywhere.rst:83
msgid ""
"Alternatively, go to :menuselection:`Settings --> Configure Toolbars…`, then "
"search for the available action :guilabel:`Disable Bin Effects`. Add it to "
"whatever toolbar you like, such as the **Timeline Toolbar** by clicking the :"
"guilabel:`>` button. Click :guilabel:`OK`. Done."
msgstr ""
"Alternativament, aneu a :menuselection:`Arranjament --> Configura les barres "
"d'eines…`, i després cerqueu l'acció disponible :guilabel:`Desactiva els "
"efectes de la safata`. Afegiu-la a qualsevol barra d'eines que vulgueu, com "
"ara la **Barra d'eines de la línia de temps** fent clic al botó :guilabel:"
"`>`. Feu clic a :guilabel:`D'acord`. Fet."

#: ../../glossary/useful_information/effects_everywhere.rst:85
msgid ""
"You can now quickly disable and enable all bin effects at once using either "
"the shortcut or toolbar button you’ve configured above."
msgstr ""
"Ara podeu desactivar ràpidament i activar tots els efectes de la safata "
"alhora utilitzant la drecera o el botó de la barra d'eines que heu "
"configurat més amunt."

#: ../../glossary/useful_information/effects_everywhere.rst:89
msgid "Effects on Tracks"
msgstr "Efectes a les pistes"

#: ../../glossary/useful_information/effects_everywhere.rst:91
msgid ""
"Similar to effects on bin clips, you can also add effects to a specific "
"timeline track. For instance, you can set the crop and placement of clips on "
"a specific track, so you don’t need to copy these settings over and over "
"again onto all clips in this track. When you change a track effects, it "
"immediately applies to all clips on this track. Sweet."
msgstr ""
"De manera similar als efectes en els clips de la safata, també es poden "
"afegir efectes a una pista específica de la línia de temps. Per exemple, es "
"pot establir el retall i la col·locació dels clips en una pista específica, "
"així que no cal copiar aquests paràmetres una vegada i una altra en tots els "
"clips d'aquesta pista. Quan canvieu els efectes d'una pista, s'aplica "
"immediatament a tots els clips d'aquesta pista. Molt apropiat."

#: ../../glossary/useful_information/effects_everywhere.rst:97
msgid "Apply Effects to Tracks"
msgstr "Aplicar els efectes a les pistes"

#: ../../glossary/useful_information/effects_everywhere.rst:99
msgid ""
"To apply effects to a track in the timeline, simply drag and drop an effect "
"from the effects pane **(1)** into the desired track in your timeline "
"**(2)**. The parameters pane **(3)** then will switch to the effects applied "
"to this track. Adjust effects as you like."
msgstr ""
"Per aplicar efectes a una pista en la línia de temps, simplement arrossegueu "
"i deixeu anar un efecte des de la subfinestra d'efectes **(1)** a la pista "
"desitjada en la vostra línia de temps **(2)**. La subfinestra de paràmetres "
"**(3)** llavors canviarà als efectes aplicats a aquesta pista. Ajusteu els "
"efectes com vulgueu."

#: ../../glossary/useful_information/effects_everywhere.rst:101
msgid ""
"There’s one minor catch here: the split compare button unfortunately doesn’t "
"work here, as it applies to individual clips only. It doesn’t work for "
"timeline tracks."
msgstr ""
"Aquí hi ha una petita captura: lamentablement, el botó de divisió de la "
"comparació no funciona, ja que s'aplica només a clips individuals. No "
"funciona per a pistes de la línia de temps."

#: ../../glossary/useful_information/effects_everywhere.rst:103
msgid ""
"If you later need to return to track effects in order to edit them again, "
"simply click into the header of the desired track. The parameters pane "
"**(3)** then will automatically switch to your track effect stack."
msgstr ""
"Si més tard necessiteu tornar als efectes de la pista per editar-los de nou, "
"simplement feu clic a la capçalera de la pista desitjada. La subfinestra de "
"paràmetres **(3)** llavors canviarà automàticament a la pila d'efectes de la "
"pista."

#: ../../glossary/useful_information/effects_everywhere.rst:106
msgid "Temporarily Disable Track Effects"
msgstr "Desactivar temporalment els efectes de la pista"

#: ../../glossary/useful_information/effects_everywhere.rst:108
msgid ""
"You can (temporarily) **disable all effects for a track**, by clicking into "
"the track header and then **un-checking** the **Bin effects for…** box at "
"the top of the parameters pane **(3)**. This works exactly the same as with "
"effects applied to timeline clips."
msgstr ""
"Podeu (temporalment) **desactivar tots els efectes d'una pista**, fent clic "
"a la capçalera de la pista i després **desmarcar** la casella **Efectes de "
"la safata per a…** a la part superior de la subfinestra de paràmetres "
"**(3)**. Això funciona exactament igual que amb els efectes aplicats als "
"clips de la línia de temps."

#: ../../glossary/useful_information/effects_everywhere.rst:117
msgid ""
"Timeline tracks that have effects directly applied on them show the (usual) "
"effects signet: a star. It shows up after the clip title. In single-line "
"layout, the effects signet shows up in between the clip title and the track "
"controls."
msgstr ""
"Les pistes de la línia de temps que tenen efectes directament aplicats en "
"elles mostren el senyal (normal) d'efectes: una estrella. Es mostra després "
"del títol del clip. En la disposició d'una sola línia, el senyal d'efectes "
"apareix entre el títol del clip i els controls de la pista."

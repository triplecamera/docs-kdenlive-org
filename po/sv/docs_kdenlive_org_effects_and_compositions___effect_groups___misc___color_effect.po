# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-12-18 11:31+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.08.1\n"

#: ../../effects_and_compositions/effect_groups/misc/color_effect.rst:11
msgid "Color Effect"
msgstr "Färgeffekt"

#: ../../effects_and_compositions/effect_groups/misc/color_effect.rst:13
msgid "Contents"
msgstr "Innehåll"

#: ../../effects_and_compositions/effect_groups/misc/color_effect.rst:15
msgid ""
"This is the `Frei0r colortap <https://www.mltframework.org/plugins/"
"FilterFrei0r-colortap/>`_ MLT filter."
msgstr ""
"Det här är MLT-filtret `Frei0r colortap <https://www.mltframework.org/"
"plugins/FilterFrei0r-colortap/>`_."

#: ../../effects_and_compositions/effect_groups/misc/color_effect.rst:17
msgid "Applies a pre-made color effect to image."
msgstr "Lägger till en färdig färgeffekt i bilden."

#: ../../effects_and_compositions/effect_groups/misc/color_effect.rst:19
msgid "Possible effects are:"
msgstr "Möjliga effekter är:"

#: ../../effects_and_compositions/effect_groups/misc/color_effect.rst:21
msgid "xpro, sepia, heat, red_green, old_photo, xraym, esses and yellow_blue."
msgstr "xpro, sepia, heat, red_green, old_photo, xraym, esses and yellow_blue."

# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-13 00:45+0000\n"
"PO-Revision-Date: 2021-11-13 18:53+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.08.1\n"

#: ../../user_interface/menu/timeline_menu.rst:1
msgid "Timeline menu, Editing in Kdenlive video editor"
msgstr ""

#: ../../user_interface/menu/timeline_menu.rst:1
msgid ""
"KDE, Kdenlive, timeline, menu, editing, timeline, documentation, user "
"manual, video editor, open source, free, learn, easy"
msgstr ""

#: ../../user_interface/menu/timeline_menu.rst:24
msgid "Timeline Menu"
msgstr ""

#: ../../user_interface/menu/timeline_menu.rst:26
msgid "Contents"
msgstr "Innehåll"

#: ../../user_interface/menu/timeline_menu.rst:30
msgid "Menu :menuselection:`Current track`"
msgstr ""

#: ../../user_interface/menu/timeline_menu.rst:36
msgid ":ref:`selection`"
msgstr ""

#: ../../user_interface/menu/timeline_menu.rst:37
msgid ":ref:`insert_clip_zone`"
msgstr ""

#: ../../user_interface/menu/timeline_menu.rst:38
msgid ":ref:`remove_space`"
msgstr ""

#: ../../user_interface/menu/timeline_menu.rst:39
msgid "Removal"
msgstr ""

#: ../../user_interface/menu/timeline_menu.rst:40
msgid ":ref:`timeline-preview-rendering`"
msgstr ""

#: ../../user_interface/menu/timeline_menu.rst:41
msgid "Resize Item Start"
msgstr ""

#: ../../user_interface/menu/timeline_menu.rst:42
msgid "Resize Item End"
msgstr ""

#: ../../user_interface/menu/timeline_menu.rst:43
msgid ":ref:`current_clip`"
msgstr ""

#: ../../user_interface/menu/timeline_menu.rst:44
msgid ""
"Current track -> :ref:`Remove All Spaces After Cursor <remove_spaces>`, :ref:"
"`Remove All Clips After Cursor <delete_items>`"
msgstr ""

#: ../../user_interface/menu/timeline_menu.rst:45
msgid "Grab Current Item"
msgstr ""

#: ../../user_interface/menu/timeline_menu.rst:46
msgid ":ref:`guides`"
msgstr ""

#: ../../user_interface/menu/timeline_menu.rst:47
msgid ":ref:`space`"
msgstr ""

#: ../../user_interface/menu/timeline_menu.rst:48
msgid ":ref:`Group Clips <group_clips>`"
msgstr ""

#: ../../user_interface/menu/timeline_menu.rst:49
msgid ":ref:`Ungroup Clips <ungroup_clips>`"
msgstr ""

#: ../../user_interface/menu/timeline_menu.rst:50
msgid ":ref:`Add Timeline Selection to Library <the_library>`"
msgstr ""

#: ../../user_interface/menu/timeline_menu.rst:51
msgid ":ref:`Tracks <tracks>`"
msgstr ""

#: ../../user_interface/menu/timeline_menu.rst:52
msgid ":ref:`Add Effect <effects>`"
msgstr ""

#: ../../user_interface/menu/timeline_menu.rst:53
msgid "Disable Timeline Effects"
msgstr ""

#: ../../user_interface/menu/timeline_menu.rst:54
msgid "Show video thumbnails"
msgstr ""

#: ../../user_interface/menu/timeline_menu.rst:55
msgid "Show audio thumbnails"
msgstr ""

#: ../../user_interface/menu/timeline_menu.rst:56
msgid "Show markers comments"
msgstr ""

#: ../../user_interface/menu/timeline_menu.rst:57
msgid "Snap"
msgstr ""

#: ../../user_interface/menu/timeline_menu.rst:58
msgid "Zoom In"
msgstr ""

#: ../../user_interface/menu/timeline_menu.rst:59
msgid "Zoom Out"
msgstr ""

#: ../../user_interface/menu/timeline_menu.rst:60
msgid "Fit zoom to project"
msgstr ""

#: ../../user_interface/menu/timeline_menu.rst:61
msgid ":menuselection:`All clips --> Ripple Delete`"
msgstr ""

#: ../../user_interface/menu/timeline_menu.rst:70
msgid "Contents:"
msgstr ""

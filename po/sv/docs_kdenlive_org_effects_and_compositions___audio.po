# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-12 00:40+0000\n"
"PO-Revision-Date: 2022-09-25 09:33+0200\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.08.1\n"

#: ../../effects_and_compositions/audio.rst:1
msgid "Mix audio in Kdenlive video editor"
msgstr ""

#: ../../effects_and_compositions/audio.rst:1
msgid ""
"KDE, Kdenlive, timeline, audio mixer, multiple audio streams, audio "
"recording, documentation, user manual, video editor, open source, free, "
"learn, easy"
msgstr ""

#: ../../effects_and_compositions/audio.rst:16
msgid "Audio"
msgstr "Ljud"

#: ../../effects_and_compositions/audio.rst:18
msgid ""
"Kdenlive has some tools for handling audio. Beside the audio spectrum viewer "
"and some audio effects, you have following possibilities:"
msgstr ""
"Kdenlive har några verktyg för att hantera ljud. Förutom ljudspektrumvisning "
"och några ljudeffekter, finns följande möjligheter:"

#: ../../effects_and_compositions/audio.rst:23
msgid "Audio Mixer"
msgstr "Ljudmixer"

#: ../../effects_and_compositions/audio.rst:32
msgid "The audio mixer has following functions for each channel:"
msgstr "Ljudmixern har följande funktioner för varje kanal:"

#: ../../effects_and_compositions/audio.rst:34
msgid "Channel number (audio track number) or Master channel"
msgstr "Kanalnummer (ljudspårnummer) eller Huvudkanal"

#: ../../effects_and_compositions/audio.rst:35
msgid "Mute an audio channel"
msgstr "Tysta en ljudkanal"

#: ../../effects_and_compositions/audio.rst:36
msgid "Solo an audio channel"
msgstr "Spela bara en ljudkanal"

#: ../../effects_and_compositions/audio.rst:37
msgid ""
":ref:`Record audio <audio-recording>` direct on the track of the related "
"audio channel"
msgstr ""
":ref:`Spela in ljud <audio-recording>` direkt på spåret för den relaterade "
"ljudkanalen"

#: ../../effects_and_compositions/audio.rst:38
msgid "Opens the effect stack of the related audio channel"
msgstr "Visa effektstapeln för den relaterade ljudkanalen"

#: ../../effects_and_compositions/audio.rst:39
msgid "Balance the audio channel. Either with the slider or with values"
msgstr "Balansera ljudkanalen, antingen med skjutreglaget eller med värden"

#: ../../effects_and_compositions/audio.rst:40
msgid "Adjustment of the volume"
msgstr "Volymjustering"

#: ../../effects_and_compositions/audio.rst:43
msgid "Multiple audio streams"
msgstr "Flera ljudströmmar"

#: ../../effects_and_compositions/audio.rst:47
msgid ""
"Multiple audio streams of a video clip. In clip properties on the tab audio "
"you can adjust and manipulate each audio stream. More details see here :ref:"
"`audio_properties`"
msgstr ""
"Flera ljudströmmar för ett videoklipp. I klippegenskaper under fliken ljud, "
"kan man justera och manipulera varje ljudström. För mer information, se :ref:"
"`audio_properties`."

#: ../../effects_and_compositions/audio.rst:52
msgid "Audio recording"
msgstr "Ljudinspelning"

#: ../../effects_and_compositions/audio.rst:56
msgid ""
"There is now a :guilabel:`mic` button in the mixers (number 4 in above "
"picture) instead of the :guilabel:`record` button. Pressing the :guilabel:"
"`mic` button will enter in audio monitoring mode (levels show mic input and "
"volume slider selects the mic level). While recording you see a live "
"waveform appearing on timeline."
msgstr ""

#: ../../effects_and_compositions/audio.rst:63
msgid ""
"Enabling :guilabel:`mic` displays the track head record control and it get "
"colorized."
msgstr ""

#: ../../effects_and_compositions/audio.rst:69
msgid ""
"**Start record:** press :kbd:`spacebar` or click the :guilabel:`record` "
"button on the track head. A countdown start in project monitor (disable "
"countdown see :ref:`configure_audio_capture` settings)."
msgstr ""

#: ../../effects_and_compositions/audio.rst:71
msgid "**Pause:** press :kbd:`spacebar`"
msgstr ""

#: ../../effects_and_compositions/audio.rst:73
msgid "**To resume:** press :kbd:`spacebar` again"
msgstr ""

#: ../../effects_and_compositions/audio.rst:75
msgid ""
"**Stop record:** press :kbd:`esc` or click the :guilabel:`record` button in "
"the track head. The audio clip get added in the timeline and project bin."
msgstr ""

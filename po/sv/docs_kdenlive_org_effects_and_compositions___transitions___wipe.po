# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-04-19 00:38+0000\n"
"PO-Revision-Date: 2022-05-26 16:24+0200\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.08.1\n"

#: ../../effects_and_compositions/transitions/wipe.rst:16
msgid "Composition - Wipe"
msgstr "Sammansättning - Övergång"

#: ../../effects_and_compositions/transitions/wipe.rst:18
msgid "Contents"
msgstr "Innehåll"

#: ../../effects_and_compositions/transitions/wipe.rst:20
msgid ""
"In this composition one shot replaces another by traveling from one side of "
"the frame to another or with a special shape."
msgstr ""
"Med den här sammansättningen ersätter en tagning en annan genom att färdas "
"från en sida av bildrutan till den andra med en särskild form."

#: ../../effects_and_compositions/transitions/wipe.rst:22
msgid "The Wipe composition contains the following parameters:"
msgstr "Övergångssammansättningen innehåller följande parametrar:"

#: ../../effects_and_compositions/transitions/wipe.rst:31
msgid ""
"**Softness**: Determines the softness of the transition between the top and "
"bottom clips."
msgstr ""
"**Mjukhet**: Bestämmer övergångens mjukhet mellan det övre och undre klippet."

#: ../../effects_and_compositions/transitions/wipe.rst:40
msgid ""
"**Wipe Method**: Selecting a luma file allows you to shape the composition "
"in different shapes."
msgstr ""
"**Övergångsmetod**: Genom att välja en luminositetsfil kan man forma "
"övergången till olika former."

#: ../../effects_and_compositions/transitions/wipe.rst:42
msgid ""
"**Invert**: changes the direction of motion of the luma file. That is, if "
"the file :file:`radial.pgm` is selected in the \"Wipe Method\" parameter and "
"the image of the clip of the upper track disappears in a gradually "
"decreasing circle until it is replaced by the clip of the lower track. And "
"if the parameter is selected, on the contrary, the clip image on the lower "
"track will appear in a growing circle until it fills the entire monitor."
msgstr ""
"**Invertera**: Ändrar rörelseriktning för luminansfilen. Det vill säga, om "
"filen :file:`radial.pgm` är vald som parametern \"Övergångsmetod\" så "
"försvinner bilden för det övre spårets klipp med en gradvis minskande cirkel "
"tills den är ersatt av det undre spårets klipp. Men om parameter används, "
"dyker tvärtom det undre spårets klipp upp i en växande cirkel tills det "
"fyller hela bildskärmen."

#: ../../effects_and_compositions/transitions/wipe.rst:44
msgid ""
"**Revert**: If there is a sharp transition (without the correct effect) "
"between the clips and only then the luma file, turn on the \"Revert\" "
"parameter so that the composition works correctly."
msgstr ""
"**Återställ**: Om en abrupt övergång (utan den riktiga effekten) sker mellan "
"klippen, och bara därefter luminansfilen, sätt på parametern \"Återställ\" "
"så att sammansättningen fungerar riktigt."

#: ../../effects_and_compositions/transitions/wipe.rst:52
msgid "You can :ref:`download_new_wipes` from the KDE server."
msgstr "Man kan :ref:`download_new_wipes` från KDE:s server."

#: ../../effects_and_compositions/transitions/wipe.rst:56
msgid ""
"There was a defect with the download new wipe files - it did not download "
"them to where they are needed. They should go here :file:`~/.local/share/"
"kdenlive/lumas/HD/`, but were going here :file:`~/.local/share/kdenlive/"
"lumas/`."
msgstr ""
"Det fanns en defekt med nerladdning av nya övergångsfiler: de laddades inte "
"ner till stället där de behövs. De ska hamna här: :file:`~/.local/share/"
"kdenlive/lumas/HD/`, men hamnade här: :file:`~/.local/share/kdenlive/lumas/`."

#: ../../effects_and_compositions/transitions/wipe.rst:59
msgid ""
"To fix manually create a HD folder and move the :file:`.pgm` files there."
msgstr ""
"För att rätta det manuellt, skapa en HD-katalog och flytta :file:`.pgm`-"
"filer dit."

#: ../../effects_and_compositions/transitions/wipe.rst:61
msgid "See also :ref:`composite`."
msgstr "Se också :ref:`composite`."

# German translations for Kdenlive Manual package.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Frederik Schwarzer <schwarzer@kde.org>, 2022.
#
# Automatically generated, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-03 00:38+0000\n"
"PO-Revision-Date: 2022-06-08 01:19+0200\n"
"Last-Translator: Frederik Schwarzer <schwarzer@kde.org>\n"
"Language-Team: German <kde-i18n-de@kde.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.2\n"

#: ../../effects_and_compositions/effect_groups/misc/audio_wave.rst:12
msgid "Audio Wave"
msgstr "Audiowelle"

#: ../../effects_and_compositions/effect_groups/misc/audio_wave.rst:14
msgid "Contents"
msgstr "Inhalt"

#: ../../effects_and_compositions/effect_groups/misc/audio_wave.rst:16
msgid ""
"This is the `Audiowave <https://www.mltframework.org/plugins/FilterAudiowave/"
">`_ MLT filter."
msgstr ""

#: ../../effects_and_compositions/effect_groups/misc/audio_wave.rst:18
msgid ""
"In ver 17.04 this is found in the :ref:`analysis_and_data` category of "
"Effects."
msgstr ""

#: ../../effects_and_compositions/effect_groups/misc/audio_wave.rst:20
msgid "Display the audio waveform instead of the video. Author Dan Dennedy."
msgstr ""

#: ../../effects_and_compositions/effect_groups/misc/audio_wave.rst:22
msgid ""
"This does not work alone on audio-only clips. It must have video to "
"overwrite. A workaround is to apply this to a multi-track with a color "
"generator."
msgstr ""

#: ../../effects_and_compositions/effect_groups/misc/audio_wave.rst:27
msgid "Overlaying the Wave"
msgstr ""

#: ../../effects_and_compositions/effect_groups/misc/audio_wave.rst:29
msgid ""
"This effect replaces the video. If you want the effect overlaying the video "
"you can do something like shown below."
msgstr ""

#: ../../effects_and_compositions/effect_groups/misc/audio_wave.rst:33
msgid ""
"Duplicate the video track on a track below the one with the Audio wave on it."
msgstr ""

#: ../../effects_and_compositions/effect_groups/misc/audio_wave.rst:35
msgid "Add a composite transition."
msgstr ""

#: ../../effects_and_compositions/effect_groups/misc/audio_wave.rst:37
msgid ""
"On the top video track (the one with the audio wave effect) add a :ref:"
"`color_selection` effect."
msgstr ""

#: ../../effects_and_compositions/effect_groups/misc/audio_wave.rst:39
msgid "Make the color you are selecting black and check the invert selection."
msgstr ""

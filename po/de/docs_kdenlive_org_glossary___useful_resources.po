# German translations for Kdenlive Manual package.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Frederik Schwarzer <schwarzer@kde.org>, 2022.
#
# Automatically generated, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-13 00:22+0000\n"
"PO-Revision-Date: 2022-06-08 01:28+0200\n"
"Last-Translator: Frederik Schwarzer <schwarzer@kde.org>\n"
"Language-Team: German <kde-i18n-de@kde.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.2\n"

#: ../../glossary/useful_resources.rst:17
msgid "Useful Resources"
msgstr ""

#: ../../glossary/useful_resources.rst:20
msgid "Contents"
msgstr "Inhalt"

#: ../../glossary/useful_resources.rst:22
msgid ""
"Another Kdenlive manual: `flossmanuals <http://www.flossmanuals.net/how-to-"
"use-video-editing-software/>`_"
msgstr ""

#: ../../glossary/useful_resources.rst:23
msgid ""
"`Cutting and Splicing Video in KDEnlive <http://www.linuceum.com/Desktop/"
"KDEnliveVideo.php>`_  by Linuceum"
msgstr ""

#: ../../glossary/useful_resources.rst:24
msgid ""
"`opensource.com tutorial <http://opensource.com/life/11/11/introduction-"
"kdenlive>`_"
msgstr ""

#: ../../glossary/useful_resources.rst:25
msgid "`Kdenlive Forum <https://forum.kde.org/viewforum.php?f=262>`_"
msgstr ""

#: ../../glossary/useful_resources.rst:26
msgid ""
"Kdenlive `Developer Wiki <https://community.kde.org/Kdenlive/Development>`_"
msgstr ""

#: ../../glossary/useful_resources.rst:29
msgid "Keyboard Stickers - courtesy of Weevil"
msgstr ""

#: ../../glossary/useful_resources.rst:32
msgid "|Kdenlive_Keyboard_074|"
msgstr ""

#: ../../glossary/useful_resources.rst:35
msgid "|Kdenlive_Keyboard_074_A4|"
msgstr ""

# German translations for Kdenlive Manual package.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Frederik Schwarzer <schwarzer@kde.org>, 2022.
#
# Automatically generated, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-16 00:37+0000\n"
"PO-Revision-Date: 2022-06-08 01:27+0200\n"
"Last-Translator: Frederik Schwarzer <schwarzer@kde.org>\n"
"Language-Team: German <kde-i18n-de@kde.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.2\n"

#: ../../getting_started.rst:1
msgid "Do your first steps with Kdenlive video editor"
msgstr ""

#: ../../getting_started.rst:1
msgid ""
"KDE, Kdenlive, quick start, first steps, documentation, user manual, video "
"editor, open source, free, learn, easy"
msgstr ""

#: ../../getting_started.rst:15
msgid "Getting started"
msgstr "Erste Schritte"

#: ../../getting_started.rst:17
msgid "A short overview to start with Kdenlive."
msgstr ""

#: ../../getting_started.rst:19
msgid "Contents:"
msgstr "Inhalt:"

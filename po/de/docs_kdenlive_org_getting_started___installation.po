# German translations for Kdenlive Manual package.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Frederik Schwarzer <schwarzer@kde.org>, 2022.
#
# Automatically generated, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-17 00:43+0000\n"
"PO-Revision-Date: 2022-12-23 17:04+0100\n"
"Last-Translator: Frederik Schwarzer <schwarzer@kde.org>\n"
"Language-Team: German <kde-i18n-de@kde.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.0\n"

#: ../../getting_started/installation.rst:1
msgid "How to install Kdenlive video editor"
msgstr ""

#: ../../getting_started/installation.rst:1
msgid ""
"KDE, Kdenlive, install, Installation, documentation, user manual, video "
"editor, open source, free, learn, easy"
msgstr ""

#: ../../getting_started/installation.rst:30
msgid "Installation"
msgstr "Installation"

#: ../../getting_started/installation.rst:32
msgid ""
"Visit the `download <https://kdenlive.org/download/>`_ page of the Kdenlive "
"Web site for up to date information on installing **Kdenlive**."
msgstr ""
"Besuchen Sie die `Download <https://kdenlive.org/download/>`_ -Seite der "
"Kdenlive-Webseite für die neusten Informationen zum Installieren von "
"**Kdenlive**."

#: ../../getting_started/installation.rst:34
msgid ""
"You’ll find all previous Kdenlive versions in the `attic <https://download."
"kde.org/Attic/kdenlive/>`_."
msgstr ""

#: ../../getting_started/installation.rst:37
msgid "Minimum system requirements"
msgstr "Minimale Systemvoraussetzungen"

#: ../../getting_started/installation.rst:39
msgid ""
"**Operating system:** 64-bit Windows 7 or newer, Apple macOS 10.15 "
"(Catalina) [1]_ or newer and on M1, 64-bit Linux. Details see below."
msgstr ""
"**Betriebssystem:** 64-bit Windows 7 oder neuer (Windows 10 oder neuer "
"empfohlen), macOS 10.15 (Catalina) [1]_ oder neuer, 64-bit Linux. Details "
"siehe unten."

#: ../../getting_started/installation.rst:41
msgid ""
"**CPU:** x86 Intel or AMD; at least one 2 GHz core for SD-Video, 4 cores for "
"HD-Video, and 8 cores for 4K-Video. Details see below."
msgstr ""
"**CPU:** x86 Intel- oder AMD-Prozessor mit 2 GHz Taktgeschwindigkeit. 1 Kern "
"für SD-Video, 4 Kerne für HD-Video und 8 Kerne für UHD-Video. Details siehe "
"unten."

#: ../../getting_started/installation.rst:43
msgid ""
"**GPU:** OpenGL 2.0 that works correctly and is compatible. On Windows, you "
"can also use a card with good, compatible DirectX 9 or 11 drivers."
msgstr ""
"**GPU:** Grafikkarte kompatibel mit OpenGL 2.0. Auf Windows können Sie auch "
"eine Grafikkarte mit gutem, kompatiblen Treiber für DirectX 9 oder 11 "
"verwenden."

#: ../../getting_started/installation.rst:45
msgid ""
"**RAM:** At least 4 GB for SD-Video, 8 GB for HD-Video, and 16 GB for 4K-"
"Video."
msgstr ""

#: ../../getting_started/installation.rst:49
msgid ""
"If your computer is at the lower end of CPU and RAM requirements, you should "
"use both the :ref:`Preview Resolution <preview_resolution>` and :ref:`Proxy "
"<proxy_clips_tab>` features to help reduce preview lag."
msgstr ""
"Wenn Ihr Computer am unteren Ende der CPU- und RAM-Anforderungen liegt, "
"sollten Sie sowohl die :ref:`Vorschauauflösung <preview_resolution>` als "
"auch die :ref:`Proxy-Funktionen <proxy_clips_tab>` verwenden, um die "
"Verzögerung der Vorschau zu verringern."

#: ../../getting_started/installation.rst:52
msgid "Kdenlive on Linux"
msgstr "Kdenlive unter Linux"

#: ../../getting_started/installation.rst:54
msgid "**Kdenlive** can be installed on non-KDE Desktops without any issues."
msgstr ""

#: ../../getting_started/installation.rst:56
msgid ""
"**Packages:** Minimum Ubuntu 22.04 for PPA. AppImage, Snap or Flatpak has no "
"such minimal requirements."
msgstr ""

#: ../../getting_started/installation.rst:59
msgid "Kdenlive on Windows"
msgstr "Kdenlive unter Window"

#: ../../getting_started/installation.rst:61
msgid ""
"**Kdenlive** runs only on 64bit version of Windows. Kdenlive runs on Windows "
"7 and newer. We cannot guarantee that Kdenlive runs on server or embedded "
"Windows version."
msgstr ""
"**Kdenlive** läuft nur mit der 64-Bit-Version von Windows. Kdenlive läuft "
"unter Windows 7 oder neuer (Windows 10 oder neuer empfohlen). Wir können "
"nicht garantieren, dass Kdenlive unter Windows-Server- oder Windows-Embedded-"
"Versionen läuft."

#: ../../getting_started/installation.rst:63
msgid "Kdenlive is available as an install and as a standalone version."
msgstr ""
"Kdenlive ist verfügbar als eine installierbare und als Standalone-Version."

#: ../../getting_started/installation.rst:65
msgid ""
"Install version: Needs administrator rights and gets installed on your local "
"machine. It's also listed as a program."
msgstr ""
"Die installierbare Version benötigt Administratorrechte und wird auf dem "
"lokalen Computer installiert. Kdenlive ist dann ebenfalls als Programm "
"gelistet."

#: ../../getting_started/installation.rst:67
msgid "It's available for all users on your computer."
msgstr "Kdenlive ist für alle Benutzer auf dem Computer verfügbar."

#: ../../getting_started/installation.rst:69
msgid "The Kdenlive files are always located in the same folder."
msgstr "Die Programmdateien sind immer im selben Ordner gespeichert."

#: ../../getting_started/installation.rst:71
msgid ""
"Standalone version: **Doesn't** need administrator rights and isn't "
"installed. It's **not** listed as a program. Is only accessible for the user "
"who has downloaded the file."
msgstr ""
"Die Standalone-Version benötigt **keine** Administratorrechte und wird nicht "
"installiert. Kdenlive wird **nicht** als Programm gelistet. Zudem ist es nur "
"verfügbar für den Benutzer, welcher die Programmdateien heruntergeladen hat."

#: ../../getting_started/installation.rst:73
msgid "If you work with a normal user on your computer, you can use Kdenlive."
msgstr ""
"Wenn Sie mit einem Standardbenutzer auf Ihrem Computer arbeiten, können Sie "
"Kdenlive trotzdem verwenden."

#: ../../getting_started/installation.rst:75
msgid ""
"You can copy the Kdenlive folder on any external drive and run it on a "
"different computer without installing it. However, your personal settings "
"and downloads within Kdenlive are related to the computer you work on."
msgstr ""
"Sie können den Kdenlive-Programmordner auf eine externe Festplatte kopieren "
"und von dort aus Kdenlive auch auf einem anderen Computer benutzen ohne es "
"zu installieren. Die persönlichen Einstellungen und Downloads in Kdenlive "
"sind aber abhängig vom Computer, auf welchem man arbeitet."

#: ../../getting_started/installation.rst:79
msgid "Double click the downloaded file."
msgstr "Doppelklicken Sie auf die heruntergeladene Datei."

#: ../../getting_started/installation.rst:85
msgid "Point to the folder you like to store the Kdenlive folder"
msgstr ""
"Geben Sie den Ordner an, in welchem Sie den Kdenlive-Ordner speichern "
"möchten."

#: ../../getting_started/installation.rst:91
msgid ""
"To start Kdenlive navigate to the `bin folder` and double click Kdenlive"
msgstr ""
"Um Kdenlive zu starten, navigieren Sie in den Ordner `bin` und machen Sie "
"einen Doppelklick auf `kdenlive`."

#: ../../getting_started/installation.rst:96
msgid "Kdenlive in a Windows domain"
msgstr ""

#: ../../getting_started/installation.rst:98
msgid ""
"If you want to use Kdenlive with domain users with using Windows Active "
"Directory and/or Group Policies (GPOs) make sure all users have read/write "
"rights to the following folders:"
msgstr ""

#: ../../getting_started/installation.rst:102
msgid "%AppData%\\\\kdenlive"
msgstr ""

#: ../../getting_started/installation.rst:104
msgid "%LocalAppData%\\\\kdenlive"
msgstr ""

#: ../../getting_started/installation.rst:106
msgid "%LocalAppData%\\\\kdenliverc"
msgstr ""

#: ../../getting_started/installation.rst:108
msgid "%LocalAppData%\\\\kdenlive-layoutsrc"
msgstr ""

#: ../../getting_started/installation.rst:110
msgid "%LocalAppData%\\\\kxmlgui5\\\\kdenlive\\kdenliveui.rc"
msgstr ""

#: ../../getting_started/installation.rst:112
msgid "%AppData%\\\\kdenlive\\\\.backup"
msgstr ""

#: ../../getting_started/installation.rst:114
msgid "%LocalAppData%\\\\knewstuff3"
msgstr ""

#: ../../getting_started/installation.rst:116
msgid "Do also make sure no GPO is blocking the access to these folders."
msgstr ""

#: ../../getting_started/installation.rst:121
msgid "Kdenlive on macOS"
msgstr "Kdenlive unter macOS"

#: ../../getting_started/installation.rst:123
msgid ""
"**Kdenlive** runs with Intel based Mac's on macOS 10.15 (Catalina) and newer "
"(available on the `download <https://kdenlive.org/download/>`_ page)."
msgstr ""

#: ../../getting_started/installation.rst:127
msgid ""
"Kdenlive is running with Intel based Mac's not older than macOS 10.15 "
"(Catalina) [1]_ and on M1."
msgstr ""

#: ../../getting_started/installation.rst:130
msgid "Install procedure"
msgstr ""

#: ../../getting_started/installation.rst:136
msgid "Choose the option *Open with DiskImageMounter (Default)*."
msgstr ""

#: ../../getting_started/installation.rst:142
msgid ""
"When the dmg file is downloaded, the *DiskImageMounter* will open. Drag the "
"*Kdenlive* Logo into the *Applications* Folder."
msgstr ""

#: ../../getting_started/installation.rst:148
msgid "The files get copied."
msgstr ""

#: ../../getting_started/installation.rst:154
msgid "MacOS will try to check the files for malware."
msgstr ""

#: ../../getting_started/installation.rst:160
msgid ""
"The message *“kdenlive\" cannot be opened, because Apple cannot search for "
"malware in it* will appear. Here you have to click :guilabel:`Show in "
"Finder`."
msgstr ""

#: ../../getting_started/installation.rst:166
msgid ""
"The Finder opens. Now right click on *Kdenlive* and choose :guilabel:`Open`."
msgstr ""

#: ../../getting_started/installation.rst:172
msgid ""
"The message that Apple can't search for malware will appear again. Just "
"click on :guilabel:`Open` and Kdenlive will open up."
msgstr ""

#: ../../getting_started/installation.rst:177
msgid "Configuration Information"
msgstr ""

#: ../../getting_started/installation.rst:179
msgid ""
"Kdenlive's application-wide persistent settings are stored in the following "
"locations, depending on your platform."
msgstr ""

#: ../../getting_started/installation.rst:185
msgid "Linux"
msgstr "Linux"

#: ../../getting_started/installation.rst:186
msgid "Windows"
msgstr "Windows"

#: ../../getting_started/installation.rst:187
msgid "macOS"
msgstr "macOS"

#: ../../getting_started/installation.rst:188
msgid "Description"
msgstr "Beschreibung"

#: ../../getting_started/installation.rst:189
msgid ":file:`~/.config/kdenliverc`"
msgstr ""

#: ../../getting_started/installation.rst:190
msgid ":file:`%LocalAppData%\\\\kdenliverc`"
msgstr ""

#: ../../getting_started/installation.rst:192
msgid ""
"General settings of the application. Delete this and restart Kdenlive to "
"reset the application to \"factory\" settings"
msgstr ""

#: ../../getting_started/installation.rst:193
msgid ":file:`~/.config/kdenlive-appimagerc`"
msgstr ""

#: ../../getting_started/installation.rst:196
msgid "Linux AppImage only: contains the general settings of the application"
msgstr ""

#: ../../getting_started/installation.rst:197
msgid ":file:`~/.config/session/kdenlive_*`"
msgstr ""

#: ../../getting_started/installation.rst:200
msgid "temporary session info"
msgstr ""

#: ../../getting_started/installation.rst:201
msgid ":file:`~/.cache/kdenlive`"
msgstr ""

#: ../../getting_started/installation.rst:202
msgid ":file:`%LocalAppData%\\\\kdenlive`"
msgstr ""

#: ../../getting_started/installation.rst:204
msgid ""
"cache location storing audio and video thumbnails, and proxy clips, user "
"defined titles, LUTS, lumas, shortcuts"
msgstr ""

#: ../../getting_started/installation.rst:205
msgid ":file:`~/.local/share/kdenlive`"
msgstr ""

#: ../../getting_started/installation.rst:206
msgid ":file:`%AppData%\\\\kdenlive`"
msgstr ""

#: ../../getting_started/installation.rst:208
msgid ""
"contains downloaded: effects, export, library, opencvmodels, profiles, "
"speech models, and titles"
msgstr ""

#: ../../getting_started/installation.rst:209
msgid ":file:`~/.local/share/kdenlive/lumas`"
msgstr ""

#: ../../getting_started/installation.rst:210
msgid ":file:`%LocalAppData%\\\\kdenlive\\\\lumas`"
msgstr ""

#: ../../getting_started/installation.rst:212
msgid "lumas folder inside here contains the files used for :ref:`wipe`"
msgstr ""

#: ../../getting_started/installation.rst:213
msgid ":file:`~/.local/share/kdenlive/.backup`"
msgstr ""

#: ../../getting_started/installation.rst:214
msgid ":file:`%AppData%\\\\kdenlive\\\\.backup`"
msgstr ""

#: ../../getting_started/installation.rst:216
msgid "Auto Save Recovery files"
msgstr ""

#: ../../getting_started/installation.rst:217
msgid ":file:`~/.config/kdenlive-layoutsrc`"
msgstr ""

#: ../../getting_started/installation.rst:218
msgid ":file:`%LocalAppData%\\\\kdenlive-layoutsrc`"
msgstr ""

#: ../../getting_started/installation.rst:220
msgid "contains the layout settings"
msgstr ""

#: ../../getting_started/installation.rst:221
msgid ":file:`~/.local/share/kxmlgui5/kdenlive/kdenliveui.rc`"
msgstr ""

#: ../../getting_started/installation.rst:222
msgid ":file:`%LocalAppData%\\\\kxmlgui5\\kdenlive\\\\kdenliveui.rc`"
msgstr ""

#: ../../getting_started/installation.rst:224
msgid "contains UI configuration, if your UI is broken, delete this file"
msgstr ""

#: ../../getting_started/installation.rst:225
msgid ":file:`~/.local/share/knewstuff3`"
msgstr ""

#: ../../getting_started/installation.rst:226
msgid ":file:`%LocalAppData%\\\\knewstuff3`"
msgstr ""

#: ../../getting_started/installation.rst:228
msgid "contains LUT definition"
msgstr ""

#: ../../getting_started/installation.rst:230
msgid "**Windows**"
msgstr "**Windows**"

#: ../../getting_started/installation.rst:232
msgid ""
"To reach above folders: :kbd:`windows + r` then copy above path into the "
"window."
msgstr ""

#: ../../getting_started/installation.rst:235
msgid "Notes"
msgstr ""

#: ../../getting_started/installation.rst:237
msgid ""
"Due to QT6 compatibility the build system was switched to C++17 in January "
"2022 so minimum macOS requirement is macOS 10.15."
msgstr ""

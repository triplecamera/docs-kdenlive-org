# Chinese translations for Kdenlive Manual package
# Kdenlive Manual 套件的正體中文翻譯.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Automatically generated, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-16 00:37+0000\n"
"PO-Revision-Date: 2022-01-16 00:37+0000\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: ../../glossary.rst:1
msgid "Kdenlive references and further information"
msgstr ""

#: ../../glossary.rst:1
msgid ""
"KDE, Kdenlive, references, information, documentation, user manual, video "
"editor, open source, free, learn, easy"
msgstr ""

#: ../../glossary.rst:15
msgid "Glossary"
msgstr ""

#: ../../glossary.rst:17
msgid "Contents:"
msgstr ""

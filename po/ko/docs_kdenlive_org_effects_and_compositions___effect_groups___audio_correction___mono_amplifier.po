# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Shinjo Park <kde@peremen.name>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-29 00:22+0000\n"
"PO-Revision-Date: 2022-05-07 15:12+0200\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean <kde-kr@kde.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 21.12.3\n"

#: ../../effects_and_compositions/effect_groups/audio_correction/mono_amplifier.rst:10
msgid "Mono Amplifier"
msgstr "모노 증폭기"

#: ../../effects_and_compositions/effect_groups/audio_correction/mono_amplifier.rst:12
msgid "Contents"
msgstr "목차"

#: ../../effects_and_compositions/effect_groups/audio_correction/mono_amplifier.rst:14
msgid ""
"This is the LADSPA filter number `1048 <https://www.mltframework.org/plugins/"
"FilterLadspa-1048/>`_ from MLT."
msgstr ""
"MLT의 LADSPA 필터 번호 `1048 <https://www.mltframework.org/plugins/"
"FilterLadspa-1048/>`_ 입니다."

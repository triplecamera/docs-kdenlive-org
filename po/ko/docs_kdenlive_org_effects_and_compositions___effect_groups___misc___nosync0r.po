# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Shinjo Park <kde@peremen.name>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2022-05-08 16:09+0200\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean <kde-kr@kde.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 21.12.3\n"

#: ../../effects_and_compositions/effect_groups/misc/nosync0r.rst:13
msgid "nosync0r"
msgstr "nosync0r"

#: ../../effects_and_compositions/effect_groups/misc/nosync0r.rst:15
msgid "Contents"
msgstr "목차"

#: ../../effects_and_compositions/effect_groups/misc/nosync0r.rst:17
msgid ""
"This is the `Frei0r nosync0r <https://www.mltframework.org/plugins/"
"FilterFrei0r-nosync0r/>`_ MLT filter."
msgstr ""
"`Frei0r nosync0r <https://www.mltframework.org/plugins/FilterFrei0r-nosync0r/"
">`_ MLT 필터입니다."

#: ../../effects_and_compositions/effect_groups/misc/nosync0r.rst:19
msgid ""
"Video looks like a broken TV with bottom half of picture on the top of "
"screen."
msgstr ""

#: ../../effects_and_compositions/effect_groups/misc/nosync0r.rst:21
msgid "https://youtu.be/91lPpm1nMTk"
msgstr "https://youtu.be/91lPpm1nMTk"

#: ../../effects_and_compositions/effect_groups/misc/nosync0r.rst:23
msgid "https://youtu.be/VO1Mele0lzU"
msgstr "https://youtu.be/VO1Mele0lzU"

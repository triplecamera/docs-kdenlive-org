# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Shinjo Park <kde@peremen.name>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-21 00:37+0000\n"
"PO-Revision-Date: 2022-05-08 16:43+0200\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean <kde-kr@kde.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 21.12.3\n"

#: ../../user_interface/menu/clip_menu/delete_clip.rst:16
msgid "Delete Clip"
msgstr "클립 삭제"

#: ../../user_interface/menu/clip_menu/delete_clip.rst:18
msgid "Contents"
msgstr "목차"

#: ../../user_interface/menu/clip_menu/delete_clip.rst:20
msgid ""
"This menu item is available from :ref:`project_tree` on a clip in the "
"Project Bin or under the :ref:`clip_menu` when a clip is selected in the "
"Project Bin."
msgstr ""

#: ../../user_interface/menu/clip_menu/delete_clip.rst:22
msgid ""
"This function removes the clip from the Project Bin and from the timeline if "
"it is being used on the timeline."
msgstr ""

#: ../../user_interface/menu/clip_menu/delete_clip.rst:24
msgid "You are warned if the clip is in use on the timeline."
msgstr ""

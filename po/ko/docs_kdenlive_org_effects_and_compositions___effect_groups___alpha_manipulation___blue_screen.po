# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Shinjo Park <kde@peremen.name>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-03 00:38+0000\n"
"PO-Revision-Date: 2022-05-08 02:06+0200\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean <kde-kr@kde.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 21.12.3\n"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/blue_screen.rst:14
msgid "Chroma Key"
msgstr "크로마 키"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/blue_screen.rst:16
msgid "Contents"
msgstr "목차"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/blue_screen.rst:18
msgid ""
"This effect allows you to do Chroma Keying (also known as Green Screen or "
"Blue Screen) in **Kdenlive**. Chroma keying is where you remove backgrounds "
"of a similar colour. This effect is a very basic. For a more complication "
"alpha manipulation/background removal effect, use :ref:`color_selection`."
msgstr ""

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/blue_screen.rst:20
msgid "For black backgrounds, the :ref:`screen` is slightly more effective."
msgstr ""

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/blue_screen.rst:22
msgid "Here are some video tutorials on how to use chroma key."
msgstr ""

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/blue_screen.rst:25
msgid "Basic Chroma Keying Tutorial"
msgstr ""

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/blue_screen.rst:27
msgid "Select the clip you want to chroma key in the timeline."
msgstr ""

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/blue_screen.rst:29
msgid ""
"Search for \"chroma key\" in the effects tab, and drag it onto the "
"properties tab."
msgstr ""

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/blue_screen.rst:31
msgid ""
"Press on the button that looks like a water droplet, and then click on the "
"background of the video. This will chroma key out the background."
msgstr ""

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/blue_screen.rst:33
msgid ""
"Use the variance slider to control the amount of background chroma keyed "
"out. This will require adjustment if your chroma key didn't immediately turn "
"out perfect."
msgstr ""

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/blue_screen.rst:36
msgid "Video Tutorial"
msgstr "동영상 튜토리얼"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/blue_screen.rst:38
msgid "https://youtu.be/bMwbffYIS40"
msgstr "https://youtu.be/bMwbffYIS40"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/blue_screen.rst:41
msgid "See also"
msgstr "같이 보기"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/blue_screen.rst:43
msgid ""
":ref:`rotoscoping` effect. Rotoscoping is where you manual draw a region, "
"and everything outside/inside that region will disappear. This is useful for "
"backgrounds with multiple different colours."
msgstr ""

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/blue_screen.rst:45
msgid ""
":ref:`keysplillm0pup` effect in the Misc group. The Key Spill Mop Up effect "
"can be used to improve the edges of the blue screen effect - when edge "
"problems are caused by \"keyspill\". Keyspill is when the color of the "
"screen used for colorkeying spills onto the subject due to light reflection."
msgstr ""

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/blue_screen.rst:47
msgid ""
":ref:`color_selection` which also does color based alpha selection, but in a "
"much more detailed fashion. Use it for less contrasting or more complex "
"backgrounds."
msgstr ""

# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-11-22 17:52+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: filltro MLT Chromahold\n"

#: ../../effects_and_compositions/effect_groups/colour/chroma_hold.rst:13
msgid "Chroma Hold"
msgstr "Suspensão do Croma"

#: ../../effects_and_compositions/effect_groups/colour/chroma_hold.rst:15
msgid "Contents"
msgstr "Conteúdo"

#: ../../effects_and_compositions/effect_groups/colour/chroma_hold.rst:17
msgid ""
"This is the `Chroma_hold <https://www.mltframework.org/plugins/"
"FilterChroma_hold/>`_ MLT filter."
msgstr ""
"Este é o filltro do MLT `Chroma_hold <https://www.mltframework.org/plugins/"
"FilterChroma_hold/>`_."

#: ../../effects_and_compositions/effect_groups/colour/chroma_hold.rst:19
msgid "Makes image greyscale except for chosen color."
msgstr "Transforma a imagem em tons de cinzento, excepto a cor escolhida."

#: ../../effects_and_compositions/effect_groups/colour/chroma_hold.rst:21
msgid "https://youtu.be/XDJEzN4XEXo"
msgstr "https://youtu.be/XDJEzN4XEXo"

#: ../../effects_and_compositions/effect_groups/colour/chroma_hold.rst:23
msgid "https://youtu.be/dXnFsOjS734"
msgstr "https://youtu.be/dXnFsOjS734"

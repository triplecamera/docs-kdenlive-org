# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-29 00:42+0000\n"
"PO-Revision-Date: 2022-05-20 16:13+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: ref projectmenu clips clip kbd\n"

#: ../../user_interface/menu/project_menu/create_folder.rst:16
msgid "Create Folder"
msgstr "Criar uma Pasta"

#: ../../user_interface/menu/project_menu/create_folder.rst:18
msgid "Contents"
msgstr "Conteúdo"

#: ../../user_interface/menu/project_menu/create_folder.rst:20
msgid ""
"There are three ways to access a menu containing this option: from the :ref:"
"`project_menu` menu, the :ref:`clips` dropdown on :ref:`the Project Bin "
"toolbar <project_tree>` or by right-clicking on a clip in the :ref:`Project "
"Bin <project_tree>`. This menu item creates a folder in the Project Bin. It "
"is a virtual folder, not one created on your hard disk.  You can use this "
"feature to organize your Project Bin when it gets very large or complex by "
"placing clips in folders, which can then be collapsed to free up space in "
"the tree. Existing clips in the Project Bin can be moved to a folder using "
"drag and drop. New clips can be added directly to a folder by first "
"selecting the folder (or any clip in the folder) and then choosing the :"
"menuselection:`Add Clip` option from one of the dropdown menus described "
"above."
msgstr ""
"Existem três formas de aceder a um menu com esta opção: a partir do menu :"
"ref:`project_menu`, da lista de :ref:`clips` na barra do Grupo do Projecto "
"ou carregando com o botão direito sobre um 'clip' no Grupo do Projecto. Este "
"item do menu cria uma pasta no Grupo do Projecto. É uma pasta virtual, não "
"uma criada no seu disco rígido. Poderá usar esta funcionalidade para "
"organizar o seu Grupo do Projecto quando fica muito grande ou complexa, "
"colocando os 'clips' em pastas, as quais poderão ser estão fechadas para "
"libertar espaço na árvore. Os 'clips' existentes no Grupo do Projecto "
"poderão ser movidos para uma pasta por arrastamento. Os novos 'clips' "
"poderão ser adicionados directamente a uma pasta, seleccionando primeiro a "
"pasta (ou qualquer 'clip' da mesma) e depois escolhendo um botão para "
"adicionar um 'clip' a partir de qualquer um dos menus descritos acima."

#: ../../user_interface/menu/project_menu/create_folder.rst:25
msgid ""
"Edit the name of the folder: select the folder and right-click on the text "
"**Folder** or press :kbd:`F2`."
msgstr ""
"Indique o nome da pasta: seleccione a pasta e carregue com o botão direito "
"sobre o texto **Pasta** ou carregue em :kbd:`F2`."

#: ../../user_interface/menu/project_menu/create_folder.rst:28
msgid "Create additional bins"
msgstr "Criar grupos adicionais"

#: ../../user_interface/menu/project_menu/create_folder.rst:32
msgid ""
"You can create a separate bin from each folder, following :ref:`these steps "
"<multibin>`."
msgstr ""
"Poderá criar um grupo separado para cada pasta, seguindo :ref:`estes passos "
"<multibin>`."

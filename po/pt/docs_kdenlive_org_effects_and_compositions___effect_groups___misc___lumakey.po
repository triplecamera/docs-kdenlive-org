# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-03 00:38+0000\n"
"PO-Revision-Date: 2022-05-19 09:07+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: Luma clip Kdenlive\n"

#: ../../effects_and_compositions/effect_groups/misc/lumakey.rst:11
msgid "Lumakey Effect"
msgstr "Efeito de Luma-Chave"

#: ../../effects_and_compositions/effect_groups/misc/lumakey.rst:13
msgid "Contents"
msgstr "Conteúdo"

#: ../../effects_and_compositions/effect_groups/misc/lumakey.rst:15
msgid "In version 15.n of Kdenlive this is in the Misc category of effects."
msgstr ""
"Na versão 15.n do Kdenlive, este está na categoria Diversos dos efeitos."

#: ../../effects_and_compositions/effect_groups/misc/lumakey.rst:17
msgid ""
"The Lumakey effect changes the clip's alpha channel. To see its effect, you "
"need a transition (like the Composite transition that is available in "
"tracks) and another clip beneath."
msgstr ""
"O efeito de Luma-Chave altera o canal-alfa do 'clip'. Para ver o seu efeito, "
"necessita de uma transição (como a transição de Composição que está "
"disponível nas faixas) e outro 'clip' por baixo."

# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-13 00:22+0000\n"
"PO-Revision-Date: 2021-11-26 15:05+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: clip ref timeline monitors temoral configurekdenlive\n"
"X-POFile-SpellExtra: fullluma\n"

#: ../../glossary/tips_tricks.rst:26
msgid "Tips &  Tricks"
msgstr "Dicas & Truques"

#: ../../glossary/tips_tricks.rst:31
msgid "Contents"
msgstr "Conteúdo"

#: ../../glossary/tips_tricks.rst:33
msgid ""
"You can advance and retard the clip in the project monitor by rotating the "
"mouse wheel when the pointer is over the :ref:`timeline` or over the :ref:"
"`monitors`"
msgstr ""
"Poderá avançar e atrasar o 'clip' no monitor do projecto, rodando a roda do "
"rato quando o cursor estiver sobre a :ref:`timeline` ou os :ref:`monitors`"

#: ../../glossary/tips_tricks.rst:36
msgid ""
"You can advance and retard the clip in the :ref:`monitors` by rotating the "
"mouse wheel when the pointer is over the :ref:`monitors`"
msgstr ""
"Poderá avançar e atrasar o 'clip' nos :ref:`monitors`, rodando a roda do "
"rato quando o cursor estiver sobre os :ref:`monitors`"

#: ../../glossary/tips_tricks.rst:39
msgid ""
"You can zoom the timeline by vertical drag on the ruler (if enabled in :ref:"
"`configure_kdenlive`)"
msgstr ""
"Poderá ampliar a linha temoral, arrastando verticalmente a régua (se estiver "
"activa no :ref:`configure_kdenlive`)"

#: ../../glossary/tips_tricks.rst:42
msgid "What's this :ref:`full_luma`"
msgstr "O que é isto :ref:`full_luma`"

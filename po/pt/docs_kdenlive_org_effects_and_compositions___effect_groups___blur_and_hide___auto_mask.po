# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-06 00:38+0000\n"
"PO-Revision-Date: 2022-05-23 00:50+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: ref motiontracker MLT AutotrackRectangle clip\n"
"X-POFile-SpellExtra: guilabel kbd menuselection clips Kdenlive\n"

#: ../../effects_and_compositions/effect_groups/blur_and_hide/auto_mask.rst:16
msgid "Auto Mask"
msgstr "Máscara Automática"

#: ../../effects_and_compositions/effect_groups/blur_and_hide/auto_mask.rst:18
msgid "Use :ref:`motion_tracker` instead"
msgstr "Use o :ref:`motion_tracker` em alternativa"

#: ../../effects_and_compositions/effect_groups/blur_and_hide/auto_mask.rst:21
msgid "Contents"
msgstr "Conteúdo"

#: ../../effects_and_compositions/effect_groups/blur_and_hide/auto_mask.rst:23
msgid ""
"This effect can be used to mask peoples faces. It uses motion estimation to "
"track subjects and mask faces. It is the `AutotrackRectangle <https://www."
"mltframework.org/docs/FilterAutotrackRectangleDiscussion/>`_ MLT filter."
msgstr ""
"Este efeito poderá ser usado para mascarar as caras das pessoas. Usa uma "
"estimativa de movimentos para acompanhar os objectos e mascarar as caras. "
"Este é o filtro do MLT `AutotrackRectangle <https://www.mltframework.org/"
"docs/FilterAutotrackRectangleDiscussion/>`_ MLT."

#: ../../effects_and_compositions/effect_groups/blur_and_hide/auto_mask.rst:26
msgid "Demo"
msgstr "Demonstração"

#: ../../effects_and_compositions/effect_groups/blur_and_hide/auto_mask.rst:28
msgid "https://youtu.be/rRg_i5C8_Hc"
msgstr "https://youtu.be/rRg_i5C8_Hc"

#: ../../effects_and_compositions/effect_groups/blur_and_hide/auto_mask.rst:31
msgid "How to apply Auto Mask"
msgstr "Como aplicar a Máscara Automática"

#: ../../effects_and_compositions/effect_groups/blur_and_hide/auto_mask.rst:33
msgid ""
"See video below on how to use this effect. Warning: The effect is not 100% "
"reliable."
msgstr ""
"Veja o vídeo abaixo para saber como usar este efeito. Atenção: O efeito não "
"é 100% fiável."

#: ../../effects_and_compositions/effect_groups/blur_and_hide/auto_mask.rst:35
msgid "https://youtu.be/ZD0WOsX6B5A"
msgstr "https://youtu.be/ZD0WOsX6B5A"

#: ../../effects_and_compositions/effect_groups/blur_and_hide/auto_mask.rst:38
msgid "Motion Tracking"
msgstr "Seguimento do Movimento"

#: ../../effects_and_compositions/effect_groups/blur_and_hide/auto_mask.rst:40
msgid ""
"The Auto Mask effect can also be used to track motion of an object and use "
"it later as keyframes for an effect / transition."
msgstr ""
"O efeito da Máscara Automática também pode ser usado para seguir o movimento "
"de um objecto e usá-lo posteriormente como imagens-chave para um dado "
"efeito / transição."

#: ../../effects_and_compositions/effect_groups/blur_and_hide/auto_mask.rst:44
msgid ""
"The method described here is a re-purposing of the motion tracking data that "
"the Auto Mask effect calculates. You do not need to follow the method "
"described below to generate an Auto Mask that will obscure faces. The "
"instructions in the above video should be enough. Nor can you use the method "
"described below to improve the tracking of the mask created by the Auto Mask "
"effect."
msgstr ""
"O método aqui descrito é uma nova aplicação dos dados de seguimento do "
"movimento que o efeito da Máscara Automática calcula. Não precisa de seguir "
"o método descrito abaixo para gerar uma Máscara Automática que irá ocultar "
"as caras. As instruções no vídeo acima deverão ser suficientes. Nem poderá "
"usar o método descrito abaixo para melhor o seguimento da máscara criada "
"pelo efeito de Máscara Automática."

#: ../../effects_and_compositions/effect_groups/blur_and_hide/auto_mask.rst:46
msgid ""
"To use this feature, first, add the clip you want to analyze in the "
"timeline, and add the \"Auto Mask\" effect to it - Figure 1."
msgstr ""
"Para usar esta funcionalidade, primeiro adicione o 'clip' que deseja "
"analisar na linha temporal e adicione o efeito \"Máscara Automática\" a ele "
"- Figura 1."

#: ../../effects_and_compositions/effect_groups/blur_and_hide/auto_mask.rst:53
msgid ""
"Go to the first frame where your object is visible, and adjust the yellow "
"rectangle so that it surrounds the object, like the hand in Figure 1."
msgstr ""
"Vá para a primeira imagem onde o seu objecto está visível e ajuste o "
"rectângulo amarelo de forma a rodear o objecto, como por exemplo a mão na "
"Figura 1."

#: ../../effects_and_compositions/effect_groups/blur_and_hide/auto_mask.rst:55
msgid ""
"Then press the :guilabel:`Analyse` button in the effect options. This will "
"start an analysis of the clip (you can follow its progress in the Project "
"Bin view)."
msgstr ""
"Depois carregue no botão :guilabel:`Analisar` nas opções do efeito. Isto irá "
"iniciar uma análise do 'clip' (poderá acompanhar o seu progresso na área do "
"Grupo do Projecto)."

#: ../../effects_and_compositions/effect_groups/blur_and_hide/auto_mask.rst:62
msgid ""
"When the job is finished, the motion tracking data is stored in the clip "
"properties. To use this data, you can, for example, add a title clip and "
"affine transition over the clip you just analyzed, like in the screenshot in "
"Figure 2."
msgstr ""
"Quando a tarefa terminar, os dados de seguimento do movimento ficam "
"guardados nas propriedades do projecto. Para usar estes dados poderá, por "
"exemplo, adicionar um 'clip' de título e uma transição por afinidade sobre o "
"'clip' que acabou de analisar, como na imagem da Figura 2."

#: ../../effects_and_compositions/effect_groups/blur_and_hide/auto_mask.rst:69
msgid ""
"Next step is to import the motion data in the transition. To do this, first, "
"select the clip you have analyzed, then select the transition using the :kbd:"
"`Ctrl` key so that both items are selected. Finally, go in the transitions's "
"Options menu."
msgstr ""
"O próximo passo é importar os dados do movimento na transição. Para o fazer, "
"primeiro seleccione o 'clip' que analisou, depois seleccione a transição com "
"a tecla :kbd:`Ctrl`, para que ambos os itens fiquem seleccionados. "
"Finalmente, vá ao menu de Opções da transição."

#: ../../effects_and_compositions/effect_groups/blur_and_hide/auto_mask.rst:73
msgid ""
"Select :menuselection:`Import keyframes from clip`. You can now delete the "
"\"Auto Mask\" effect from the clip in the timeline and play the project to "
"see your title clip following the object."
msgstr ""
"Seleccione a opção :menuselection:`Importar as imagens-chave do clip`. Agora "
"poderá apagar o efeito de \"Máscara Automática\" do 'clip' e reproduzir o "
"projecto para ver o seu 'clip' do título a seguir ao objecto."

#: ../../effects_and_compositions/effect_groups/blur_and_hide/auto_mask.rst:80
msgid ""
"Checking the :guilabel:`Limit keyframe number` checkbox in the \"Import "
"Keyframes\" dialog (Figure 4) will cause Kdenlive to only import every nth "
"frame (where n is the number selected in the combo box). This is a useful "
"feature if you want to manually edit the keyframes that are imported because "
"it allows you to limit the number of keyframes you will need to manually "
"edit. If this checkbox is not checked then you import a keyframe for every "
"frame that is in the source clip."
msgstr ""
"Se assinalar a opção :guilabel:`Limitar o número de imagens-chave` na janela "
"\"Importar Imagens-Chave\" (Figura 4), fará com que o Kdenlive só importe "
"uma entre 'n' imagens de cada vez (em que o 'n' é o número seleccionado na "
"lista). Isto é uma funcionalidade útil se quiser editar manualmente as "
"imagens-chave que são importadas, porque permite-lhe limitar o número de "
"imagens-chave que precisa para editar manualmente. Se esta opção não estiver "
"assinalada, então irá importar uma imagem-chave para cada imagem do 'clip' "
"de origem."

#: ../../effects_and_compositions/effect_groups/blur_and_hide/auto_mask.rst:83
msgid "Deleting Motion Tracking Data"
msgstr "Apagar os Dados de Seguimento do Movimento"

#: ../../effects_and_compositions/effect_groups/blur_and_hide/auto_mask.rst:85
msgid ""
"The motion tracking data is saved with the :ref:`clips`. You can view this "
"data from the clip properties Analysis tab - Figure 5. Delete the data using "
"button 1."
msgstr ""
"Os dados de seguimento do movimento são gravados com os :ref:`clips`. Poderá "
"ver estes dados a partir da página de Análise das propriedades do 'clip' - "
"Figura 5. Apague os dados com o botão 1."

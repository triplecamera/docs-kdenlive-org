# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-11-22 16:20+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: Graffiti\n"

#: ../../effects_and_compositions/effect_groups/misc/light_graffiti.rst:13
msgid "Misc - Light Graffiti"
msgstr "Diversos - Graffiti de Luz"

#: ../../effects_and_compositions/effect_groups/misc/light_graffiti.rst:15
msgid "Contents"
msgstr "Conteúdo"

#: ../../effects_and_compositions/effect_groups/misc/light_graffiti.rst:17
msgid "https://vimeo.com/18497028"
msgstr "https://vimeo.com/18497028"

#: ../../effects_and_compositions/effect_groups/misc/light_graffiti.rst:19
msgid "https://vimeo.com/20217266"
msgstr "https://vimeo.com/20217266"

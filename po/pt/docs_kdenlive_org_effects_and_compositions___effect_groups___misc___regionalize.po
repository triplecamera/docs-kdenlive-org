# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-12-07 18:38+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: clip MLT pixbuf librsvg\n"

#: ../../effects_and_compositions/effect_groups/misc/regionalize.rst:11
msgid "Regionalize"
msgstr "Regionalizar"

#: ../../effects_and_compositions/effect_groups/misc/regionalize.rst:13
msgid "Contents"
msgstr "Conteúdo"

#: ../../effects_and_compositions/effect_groups/misc/regionalize.rst:15
msgid "Apply subeffects to a region defined by a clip's alpha channel."
msgstr ""
"Aplicar sub-efeitos a uma região definida pelo canal alfa de um 'clip'."

#: ../../effects_and_compositions/effect_groups/misc/regionalize.rst:17
msgid ""
"This is the `Region <https://www.mltframework.org/plugins/FilterRegion/>`_ "
"MLT filter."
msgstr ""
"Este é o filtro do MLT `Região <https://www.mltframework.org/plugins/"
"FilterRegion/>`_."

#: ../../effects_and_compositions/effect_groups/misc/regionalize.rst:19
msgid "Arguments:"
msgstr "Argumentos:"

#: ../../effects_and_compositions/effect_groups/misc/regionalize.rst:21
msgid ""
"**File**  A file whose alpha channel will \"shape\" the region. The string "
"\"circle\" is a shortcut but it requires pixbuf with the librsvg loader. The "
"circle is automatically stretched to the region to create an ellipse."
msgstr ""
"**Ficheiro**  Um ficheiro cujo canal alfa irá \"modelar\" a região. O texto "
"\"círculo\" é um atalho, mas necessita de um 'pixbuf' com o carregamento da "
"'librsvg'. O círculo é automaticamente esticado à região para criar uma "
"elipse."

#: ../../effects_and_compositions/effect_groups/misc/regionalize.rst:23
msgid ""
"**Region** Properties may be set on the encapsulated region transition. See "
"\"region\" transition for details."
msgstr ""
"**Região** As propriedades poderão ser definidas na transição encapsulada da "
"região. Veja mais detalhes na transição \"região\"."

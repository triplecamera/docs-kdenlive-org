# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-13 00:22+0000\n"
"PO-Revision-Date: 2022-05-19 09:52+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: ../../user_interface/workspace_layouts.rst:22
msgid "Workspace Layouts"
msgstr "Disposições da Área de Trabalho"

#: ../../user_interface/workspace_layouts.rst:25
msgid "Contents"
msgstr "Conteúdo"

#: ../../user_interface/workspace_layouts.rst:32
msgid ""
"These workspaces aim to improve the layout for each stage of video "
"production:"
msgstr ""
"Estas áreas de trabalho tentam melhorar a disposição de cada etapa da "
"produção do vídeo:"

#: ../../user_interface/workspace_layouts.rst:35
msgid "**Logging** for reviewing your footage"
msgstr "**Registo** para rever a sua filmagem"

#: ../../user_interface/workspace_layouts.rst:37
msgid "**Editing** to compose your story in the timeline"
msgstr "**Edição** para compor a sua história na linha temporal"

#: ../../user_interface/workspace_layouts.rst:39
msgid "**Audio** for mixing and adjusting your audio"
msgstr "**Áudio** para misturar e ajustar o seu áudio"

#: ../../user_interface/workspace_layouts.rst:41
msgid "**Effects** for adding effects"
msgstr "**Efeitos** para adicionar efeitos"

#: ../../user_interface/workspace_layouts.rst:43
msgid "**Color** for adjusting and color grading"
msgstr "**Cor** para ajustes e graduações de cores"

#: ../../user_interface/workspace_layouts.rst:46
msgid ""
"Check out this `video <https://www.youtube.com/watch?v=BdHbUUjfBLk>`_ for "
"more details."
msgstr ""
"Consulte mais detalhes neste `vídeo <https://www.youtube.com/watch?"
"v=BdHbUUjfBLk>`_."

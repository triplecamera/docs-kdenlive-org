# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Paolo Zamponi <zapaolo@email.it>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2022-03-21 08:10+0100\n"
"Last-Translator: Paolo Zamponi <zapaolo@email.it>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.12.3\n"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/letterb0xed.rst:13
msgid "LetterB0xed"
msgstr "LetterB0xed"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/letterb0xed.rst:15
msgid "Contents"
msgstr "Contenuto"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/letterb0xed.rst:17
msgid ""
"This is the `Frei0r letterb0xed <https://www.mltframework.org/plugins/"
"FilterFrei0r-letterb0xed/>`_ MLT filter."
msgstr ""
"È il filtro MLT `Frei0r letterb0xed <https://www.mltframework.org/plugins/"
"FilterFrei0r-letterb0xed/>`_."

#: ../../effects_and_compositions/effect_groups/crop_and_transform/letterb0xed.rst:19
msgid "Adds Black Borders at top and bottom for Cinema Look."
msgstr ""
"Aggiunge dei bordi neri in alto e in basso per un aspetto cinematografico."

#: ../../effects_and_compositions/effect_groups/crop_and_transform/letterb0xed.rst:21
msgid "https://youtu.be/9Ldjt0QZPzs"
msgstr "https://youtu.be/9Ldjt0QZPzs"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/letterb0xed.rst:23
msgid "https://youtu.be/JBp8wQW-_Qw"
msgstr "https://youtu.be/JBp8wQW-_Qw"

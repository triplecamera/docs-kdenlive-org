# Spanish translations for docs_kdenlive_org_user_interface___menu___settings_menu___themes.po package.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Automatically generated, 2021.
# Eloy Cuadra <ecuadra@eloihr.net>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_kdenlive_org_user_interface___menu___settings_menu___themes\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-21 00:37+0000\n"
"PO-Revision-Date: 2022-03-28 12:15+0200\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.3\n"

#: ../../user_interface/menu/settings_menu/themes.rst:10
msgid "Color Theme"
msgstr "Tema de color"

#: ../../user_interface/menu/settings_menu/themes.rst:13
msgid "Contents"
msgstr "Contenido"

#: ../../user_interface/menu/settings_menu/themes.rst:18
msgid ""
"Brings up a list of pre-configured color themes to choose from.  Some people "
"have strong preferences about whether light or dark themes are better for a "
"video editor.  There are some of both to choose from here."
msgstr ""

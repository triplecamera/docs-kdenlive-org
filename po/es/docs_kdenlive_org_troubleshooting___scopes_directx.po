# Spanish translations for docs_kdenlive_org_troubleshooting___scopes_directx.po package.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Automatically generated, 2021.
# Eloy Cuadra <ecuadra@eloihr.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: docs_kdenlive_org_troubleshooting___scopes_directx\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-04-17 00:38+0000\n"
"PO-Revision-Date: 2021-11-14 04:39+0100\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.08.3\n"

#: ../../troubleshooting/scopes_directx.rst:11
msgid "Windows issue with scopes"
msgstr ""

#: ../../troubleshooting/scopes_directx.rst:16
msgid "All video scopes are working with DirectX."
msgstr ""

#: ../../troubleshooting/scopes_directx.rst:20
msgid ""
"Workaround: Change the backend to OpenGL (:menuselection:`Settings --> "
"OpenGL Backend --> OpenGL`)"
msgstr ""

#: ../../troubleshooting/scopes_directx.rst:22
msgid ""
"If it's still not working go to: :menuselection:`Help --> Reset "
"Configuration` and try again."
msgstr ""

# Spanish translations for docs_kdenlive_org_user_interface___menu___clip_menu___rename_clip.po package.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Automatically generated, 2021.
# Eloy Cuadra <ecuadra@eloihr.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_kdenlive_org_user_interface___menu___clip_menu___rename_clip\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-21 00:37+0000\n"
"PO-Revision-Date: 2021-11-14 04:45+0100\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.08.3\n"

#: ../../user_interface/menu/clip_menu/rename_clip.rst:12
msgid "Rename Clip"
msgstr ""

#: ../../user_interface/menu/clip_menu/rename_clip.rst:14
msgid "Contents"
msgstr "Contenido"

#: ../../user_interface/menu/clip_menu/rename_clip.rst:16
msgid ""
"Change the name of a clip in the Project Bin to an arbitrary name. Does not "
"rename the file on the file system."
msgstr ""

#: ../../user_interface/menu/clip_menu/rename_clip.rst:18
msgid ""
"This menu item is available from :ref:`project_tree` on a clip in the "
"Project Bin."
msgstr ""

# Spanish translations for docs_kdenlive_org_user_interface___menu___project_menu___clean_project.po package.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Automatically generated, 2021.
# Eloy Cuadra <ecuadra@eloihr.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_kdenlive_org_user_interface___menu___project_menu___clean_project\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-21 00:37+0000\n"
"PO-Revision-Date: 2021-11-14 04:49+0100\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.08.3\n"

#: ../../user_interface/menu/project_menu/clean_project.rst:15
msgid "Clean Project"
msgstr ""

#: ../../user_interface/menu/project_menu/clean_project.rst:17
msgid "Contents"
msgstr "Contenido"

#: ../../user_interface/menu/project_menu/clean_project.rst:19
msgid ""
"Available from the :ref:`project_menu` menu this function removes any clips "
"from the Project Bin that are not currently being used on the timeline. The "
"files remain on the hard drive and are only removed from the Project Bin."
msgstr ""

#: ../../user_interface/menu/project_menu/clean_project.rst:21
msgid "You can undo this action with :kbd:`Ctrl + Z`."
msgstr ""

#: ../../user_interface/menu/project_menu/clean_project.rst:25
msgid ""
"This is different from the :ref:`project_settings` button on the Project "
"Files tab in Project Settings which deletes files not used by the project "
"from the hard drive."
msgstr ""

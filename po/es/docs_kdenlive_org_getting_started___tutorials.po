# Spanish translations for docs_kdenlive_org_getting_started___tutorials.po package.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Automatically generated, 2021.
# Eloy Cuadra <ecuadra@eloihr.net>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: docs_kdenlive_org_getting_started___tutorials\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-16 00:37+0000\n"
"PO-Revision-Date: 2022-02-10 01:14+0100\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.2\n"

#: ../../getting_started/tutorials.rst:1
msgid "Kdenlive video editor tutorials"
msgstr "Tutoriales del editor de vídeo Kdenlive"

#: ../../getting_started/tutorials.rst:1
msgid ""
"KDE, Kdenlive, tutorials, documentation, user manual, video editor, open "
"source, free, learn, easy"
msgstr ""

#: ../../getting_started/tutorials.rst:30
msgid "Tutorials"
msgstr "Tutoriales"

#: ../../getting_started/tutorials.rst:32
msgid "Contents"
msgstr "Contenido"

#: ../../getting_started/tutorials.rst:35
msgid "Written Tutorials"
msgstr "Tutoriales escritos"

#: ../../getting_started/tutorials.rst:37
msgid "See :ref:`quickstart` for a step-by-step introductory tutorial"
msgstr ""

#: ../../getting_started/tutorials.rst:40
msgid "Contents:"
msgstr "Contenido:"

#: ../../getting_started/tutorials.rst:46
msgid ""
"`Introduction to Kdenlive <http://opensource.com/life/11/11/introduction-"
"kdenlive>`_ by Seth Kenlon"
msgstr ""
"`Introducción a Kdenlive <http://opensource.com/life/11/11/introduction-"
"kdenlive>`_ por Seth Kenlon"

#: ../../getting_started/tutorials.rst:49
msgid ""
"`10 tools for visual effects with Kdenlive <https://opensource.com/"
"life/15/12/10-kdenlive-tools>`_ by Seth Kenlon"
msgstr ""
"`10 herramientas para efectos visuales con Kdenlive <https://opensource.com/"
"life/15/12/10-kdenlive-tools>`_ por Seth Kenlon"

#: ../../getting_started/tutorials.rst:52
msgid ""
"`Basic masking in Kdenlive <https://opensource.com/life/15/11/basic-masking-"
"kdenlive>`_ by Seth Kenlon"
msgstr ""

#: ../../getting_started/tutorials.rst:55
msgid ""
"`Kdenlive Challenge (Multiple Masks &  Tracks) <http://www.ocsmag."
"com/2015/12/22/the-video-editing-challenge-part-i-kdenlive/>`_ by Paul Browns"
msgstr ""

#: ../../getting_started/tutorials.rst:58
msgid "`Wikibooks Kdenlive manual <http://en.wikibooks.org/wiki/Kdenlive>`_"
msgstr ""

#: ../../getting_started/tutorials.rst:64
msgid "Video Tutorials"
msgstr "Tutoriales en vídeo"

#: ../../getting_started/tutorials.rst:66
msgid ""
"`Image and Title Layers Transparency Tutorial - Open Source Bug <https://www."
"youtube.com/watch?v=f6VHlOZutm8>`_"
msgstr ""

#: ../../getting_started/tutorials.rst:69
msgid ""
"`How to do pan and zoom with Kdenlive video editor -  Peter Thomson <https://"
"www.youtube.com/watch?v=B8ZPoWaxQrA>`_"
msgstr ""

#: ../../getting_started/tutorials.rst:72
msgid ""
"`Keyframe Animation - Linuceum <https://www.youtube.com/watch?"
"v=M8hC5FbIzdE>`_"
msgstr ""

#: ../../getting_started/tutorials.rst:75
msgid ""
"`Kdenlive Tutorials by Arkengheist 2.0 <https://www.youtube.com/channel/"
"UCtkSBZ0x71aeHmR3NNBTWwg>`_ : Many tutorials including Text effects, "
"Transitions, Timelapse, Animation, Lower Thirds, Rotoscoping, and more."
msgstr ""

#: ../../getting_started/tutorials.rst:78
msgid ""
"More videos can be found using a `YouTube search <https://www.youtube.com/"
"results?search_query=kdenlive+tutorials>`_ and on the `Vimeo Kdenlive "
"Tutorial Channel <https://vimeo.com/groups/kdenlivetutorials/videos>`_"
msgstr ""

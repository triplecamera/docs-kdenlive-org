# Translation of kdenlive Manual to Slovenian
#
# Copyright (C) 2022 This_file_is_part_of_KDE
# This file is distributed under the same license as the kdenlive package.
#
#
# Martin Srebotnjak <miles@filmsi.net>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: kdenlive ref manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-12 00:40+0000\n"
"PO-Revision-Date: 2023-01-14 16:39+0100\n"
"Last-Translator: Martin Srebotnjak <miles@filmsi.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"
"Translator: Martin Srebotnjak <miles@filmsi.net>\n"
"X-Generator: Poedit 3.2.2\n"
"X-Poedit-SourceCharset: ISO-8859-1\n"

#: ../../user_interface.rst:1
msgid "Introduction to Kdenlive's window system and widgets"
msgstr "Uvod v sistem oken in gradnikov Kdenlive"

#: ../../user_interface.rst:1
msgid ""
"KDE, Kdenlive, user interface, documentation, user manual, video editor, "
"open source, free, learn, easy, timeline, toolbar"
msgstr ""
"KDE, Kdenlive, uporabniški vmesnik, dokumentacija, uporabniški priročnik, "
"montažni program, program za montažo, video urejevalnik, odprta koda, "
"brezplačno, prosto, učenje, enostavno"

#: ../../user_interface.rst:16
msgid "User interface"
msgstr "Uporabniški vmesnik"

#: ../../user_interface.rst:30
msgid ""
"After starting Kdenlive the Kdenlive window should look something similar to "
"the image below; as Kdenlive’s user interface is consistent across all "
"platforms."
msgstr ""
"Po zagonu Kdenlive mora biti videz okna Kdenlive podoben tistemu na spodnji "
"sliki. Uporabniški vmesnik Kdenlive je skladen na vseh platformah."

#: ../../user_interface.rst:32
msgid "Kdenlive’s interface is separated into four main parts:"
msgstr "Vmesnik Kdenlive se loči na štiri glavne dele:"

#: ../../user_interface.rst:34
msgid ""
"The :ref:`menubar` contains the :ref:`Menu` and :ref:`workspace_layouts` at "
"the very top."
msgstr ""
":ref:`menubar` vsebuje na vrhu ref:`meni <Menu>` in :ref:`postavitve "
"delovnega prostora <workspace_layouts>`."

#: ../../user_interface.rst:36
msgid ":ref:`Toolbars` at the top and above the timeline"
msgstr ":ref:`Orodne vrstice <Toolbars>` na vrhu in nad časovnico"

#: ../../user_interface.rst:38
msgid ":ref:`Window <view_menu>` in the middle."
msgstr ":ref:`Okno<view_menu>` na sredini."

#: ../../user_interface.rst:40
msgid ":ref:`status_bar` at the bottom."
msgstr ":ref:`Vrstica stanja <status_bar>` na dnu."

#: ../../user_interface.rst:46
msgid ""
"Kdenlive’s default Screen Layout (example editing view). Topbar (blue), "
"Toolbars (yellow), Window (green) and Status Bar (red)."
msgstr ""
"Privzeta postavitev zaslona Kdenlive (primer pogleda montaže). Vrhnja "
"vrstica (modra), orodne vrstice (rumena), okno (zelena) in vrstica stanja "
"(rdeča)."

#: ../../user_interface.rst:48
msgid ""
"This page introduces the Kdenlive user interface (UI), explaining where to "
"find each group of features, and how the highly focused and tightly "
"integrated Media, Edit, Color, Fairlight, and Deliver pages work together to "
"let you pursue nearly any post-production workflow you can imagine."
msgstr ""
"Ta stran predstavi uporabniški vmesnik Kdenlive in pojasni, kje najdete "
"posamezno skupino funkcij in kako visoko osredotočene in tesno integrirane "
"podstrani vmesnika delujejo skupaj, da lahko izpeljete skoraj vsakršen "
"postopek poprodukcije, kar si ga lahko predstavljate."

#: ../../user_interface.rst:51
msgid "Customizing interface"
msgstr "Prilagajanje vmesnika"

#: ../../user_interface.rst:52
msgid ""
"The user interface model has a clear division between the different panes, "
"that they work on allows you to reorder them, drag them out into separate "
"windows or size them up as you will."
msgstr ""
"Model uporabniškega vmesnika ima jasno delitev področja dela med različnimi "
"podokni, na podlagi katerega jih lahko prerazporedite, povlečete v ločena "
"okna ali jih po želji povečate."

#: ../../user_interface.rst:54
msgid ""
"The interface is divided into many sections. The menu is on top and then on "
"the left you can make a note about the project. All loaded clips and videos "
"are in the project bin, the second section. Third section is the Effect "
"list. You can apply many transaction effects on your video. You can always "
"watch the project preview in the last section. You can create as many audio/"
"video track as you need. This is impressive about Kdenlive. If you’re new to "
"video editing then this is very useful. You can import multiple title clips "
"inside video track."
msgstr ""
"Vmesnik je razdeljen na številne odseke. Meni je na vrhu in na levi lahko "
"naredite opombo o projektu. Vsi naloženi posnetki in videoposnetki so v "
"projektni posodi, drugem razdelku. Tretji razdelek je seznam učinkov. Za "
"videoposnetek lahko uporabite veliko transakcijskih učinkov. Odogled "
"projekta lahko vedno izvedete v zadnjem razdelku. Ustvarite lahko toliko "
"avdio/video stez, kot jih potrebujete. To je impresivno pri Kdenlive. Če ste "
"začetnik_ca glede montaže videoposnetkov, potem je to zelo uporabno. V video "
"stezo lahko uvozite več posnetkov napisov."

#: ../../user_interface.rst:57
msgid "Remove/Adjust Sections"
msgstr "Odstrani/prilagodi odseke"

#: ../../user_interface.rst:59
msgid ""
"You won't notice on the spot, but the tabs that appear on the bottom of the "
"panes aren't built in them, but change according to what panes you have "
"aggregated into that particular frame. This way you can group functions that "
"you don't expect to use simultaneously into a single place on the user "
"interface, thus reducing clutter."
msgstr ""
"Na kraju samem ne boste opazili, vendar zavihki, ki so prikazani na dnu "
"podoken, niso vgrajeni vanje, temveč se spremenijo glede na to, kakšna "
"podokna ste zlili v ta določen okvir. Na ta način lahko združite funkcije, "
"za katere ne pričakujete, da jih boste hkrati uporabljali na enem mestu v "
"uporabniškem vmesniku, s čimer boste zmanjšali navlako."

#: ../../user_interface.rst:61
msgid ""
"If you don’t want certain sections on the screen then you can simply delete "
"them to make other sections wider. Just click the close button on that "
"section and that section will be closed, by removing unnecessary video/audio "
"tracks and now you can organize enough space to preview your project and all "
"other necessary sections are wider."
msgstr ""
"Če ne želite določenih odsekov na zaslonu, jih lahko preprosto izbrišete, da "
"bodo drugi odseki širši. Samo kliknite gumb za zapiranje v tem razdelku in "
"ta odsek bo zaprt, tako da odstranite nepotrebne video/zvočne steze in si "
"zdaj lahko organizirate dovolj prostora za ogled vašega projekta in vsi "
"drugi potrebni odseki so širši."

#: ../../user_interface.rst:67
msgid "Kdenlive’s default Screen Layout, editing view"
msgstr "Privzeta postavitev zaslona Kdenlive, pogled montaže"

#: ../../user_interface.rst:70
msgid "1. :ref:`Project Bin <project_tree>`"
msgstr "1. :ref:`Projektna posoda <project_tree>`"

#: ../../user_interface.rst:71
msgid ""
"The top left section of the screen is known as the bin, library or browser, "
"where clips, still images, audio files etc. are displayed, ready for use. "
"Replicating the folder naming system within the editing software library "
"will help keep everything organized. It may be helpful to label each clip "
"with one or more descriptive keywords. These can be searched and are a "
"useful way to rapidly locate the desired footage, especially with the use of "
"many clips. It is preferable to edit using the same frame rate and frame "
"size that the footage was shot with."
msgstr ""
"Levi zgornji odsek zaslona je znan kot koš, knjižnica ali brskalnik, kjer so "
"prikazani posnetki, slike, zvočne datoteke itn., pripravljeni za uporabo. "
"Posnemanje sistema poimenovanja map v knjižnici programa za montažo bo "
"pomagalo ohraniti vse organizirano. Morda bo koristno označiti vsak posnetek "
"z eno ali več opisnih ključnih besed. Po slednjih je mogoče iskati in so "
"uporaben način za hitro iskanje želenih posnetkov, še posebej ob uporabi "
"številnih posnetkov. Priporočeno je montirati pri isti hitrosti in velikosti "
"sličic, kot velja za posneto gradivo."

#: ../../user_interface.rst:74
msgid "2. :ref:`Clip Monitor <clip_monitor>`"
msgstr "2. :ref:`Ogled posnetka <clip_monitor>`"

#: ../../user_interface.rst:75
msgid ""
"The preview or canvas window (top centre) plays what is currently selected "
"in the project bin."
msgstr ""
"Okno za predogled ali platno (zgoraj na sredini) predvaja, kar je trenutno "
"izbrano v projektni posodi."

#: ../../user_interface.rst:78
msgid "3. :ref:`Project Monitor <project_monitor>`"
msgstr "3. :ref:`Ogled projekta <project_monitor>`"

#: ../../user_interface.rst:79
msgid ""
"This screen shows all clips, still images, audio files and effects such as "
"titles or transitions were be applied to the footage in the timeline. To "
"apply effects, select it from the effects menu and then drag and drop it "
"between the two clips where it is."
msgstr ""
"Na tem zaslonu so prikazani vsi posnetki, slike, zvočne datoteke in učinki, "
"kot so naslovi ali prehodi, ki so bili uporabljeni za posnetke na časovnici. "
"Če želite uporabiti učinke, jih izberite v meniju Učinki in jih povlecite in "
"spustite med dva posnetka."

#: ../../user_interface.rst:82
msgid "4. :ref:`Timeline Toolbar <timeline_toolbar>`"
msgstr "4. :ref:`Orodna vrstica časovnice <timeline_toolbar>`"

#: ../../user_interface.rst:83
msgid ""
"Trimming and other editing tools do not alter or delete the original "
"footage, they only adjust the copy that has been added to the timeline."
msgstr ""
"Obrezovanje in druga orodja za montažo ne spremenijo ali izbrišejo izvirnih "
"posnetkov, prilagodijo le kopijo, ki je bila dodana časovnici."

#: ../../user_interface.rst:86
msgid "5. :ref:`The Timeline <editing>`"
msgstr "5. :ref:`Časovnica <editing>`"

#: ../../user_interface.rst:87
msgid ""
"This is the area where clips are placed in the order in which they will "
"appear in the final video and trimmed to the desired length and content. "
"When assembling the initial “rough cut” of the video, users can place clips "
"from the project bin into the timeline in the desired sequence. To reorder "
"clips simply click and drag them to a new position. Trimming tools allow "
"only the relevant footage to be used by marking the desired start and end "
"(“in” and “out”) points on each clip, to shorten or lengthen it."
msgstr ""
"To je področje, kjer so posnetki razporejeni po vrstnem redu, v katerem se "
"bodo pojavili v končnem videu in obrezani na želeno dolžino in vsebino. Pri "
"sestavi začetnega »grobe montaže« videoposnetkov lahko uporabniki v želenem "
"zaporedju posnetke iz projektne posode postavijo na časovnico. Če želite "
"prerazporediti posnetke, jih preprosto kliknite in povlecite na nov položaj. "
"Orodja za obrezovanje omogočajo uporabo le ustreznega dela gradiva z "
"označevanjem želenih točk začetka in konca (vhodna in izhodna točka, angl. "
"»in« in »out«) pri vsakemu posnetku, da ga skrajšate ali podaljšate."

#: ../../user_interface.rst:90
msgid "6. :ref:`Audio Mixer <audio_mixer>`"
msgstr "6. :ref:`Mešalnik zvoka <audio_mixer>`"

#: ../../user_interface.rst:91
msgid ""
"The sound quality and volume can be adjusted, either for the entire sequence "
"or selected sections. The editing software will display audio meters and "
"generally any dialogue or narration in a video should be at about -10dB most "
"of the time."
msgstr ""
"Kakovost zvoka in glasnost lahko nastavite za celotno montažno zaporedje ali "
"izbrane dele. Programska oprema za montažo prikaže merilnike zvoka in na "
"splošno naj bo vsak dialog ali govorjena beseda v videoposnetku približno "
"-10 dB večino časa."

#: ../../user_interface.rst:94
msgid "7. :ref:`Status Bar <status_bar>`"
msgstr "7. :ref:`Vrstica stanja <status_bar>`"

#: ../../user_interface.rst:95
msgid ""
"On the left side you see hints what you can do when you hover over items. In "
"the middle you see the names of the clips you hover over in the timeline. "
"Followed by mode you are in (default is :guilabel:`Select`). On the right "
"end are switches for :guilabel:`Labels`, :guilabel:`Thumbnails`, :guilabel:"
"`Snap` and for zooming the timeline."
msgstr ""
"Na levi strani vidite namige, kaj lahko storite, ko postanete s kazalcem nad "
"elementi. Na sredini vidite imena posnetkov, ki jih premaknete na časovnico. "
"Sledi način, v ki ste v (privzeto je :guilabel:`Izberi`). Na desni strani so "
"stikala za :guilabel:`Oznake`, :guilabel:`Ogledne sličice`, :guilabel:"
"`Pripni` in za povečavo časovnice."

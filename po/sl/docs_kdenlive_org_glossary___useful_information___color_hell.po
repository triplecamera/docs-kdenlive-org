# Translation of kdenlive Manual to Slovenian
#
# Copyright (C) 2022 This_file_is_part_of_KDE
# This file is distributed under the same license as the kdenlive package.
#
#
# Martin Srebotnjak <miles@filmsi.net>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: kdenlive ref manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-31 00:37+0000\n"
"PO-Revision-Date: 2022-08-20 19:26+0200\n"
"Last-Translator: Martin Srebotnjak <miles@filmsi.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"
"Translator: Martin Srebotnjak <miles@filmsi.net>\n"
"X-Generator: Poedit 3.1.1\n"
"X-Poedit-SourceCharset: ISO-8859-1\n"

#: ../../glossary/useful_information/color_hell.rst:13
#, fuzzy
msgid "Color Hell: Ffmpeg Transcoding and Preserving BT.601"
msgstr "Barva Pekel: Ffmpeg Transcoding in ohranjanje BT.601"

#: ../../glossary/useful_information/color_hell.rst:15
#, fuzzy
msgid ""
"From time to time, you may get into weird digital video territory quite "
"unexpectedly. For instance, you just want to cut some screen records made on "
"mobile devices, such as tablets or mobile phones. What could possibly go "
"wrong? Colors, for instance…"
msgstr ""
"Od časa do časa lahko pridete na čudno digitalno video ozemlje precej "
"nepričakovano. Na primer, želite izrezati nekaj zapisov zaslona, narejenih v "
"mobilnih napravah, kot so tablični računalniki ali mobilni telefoni. Kaj bi "
"lahko šlo narobe? Barve, na primer..."

#: ../../glossary/useful_information/color_hell.rst:19
#, fuzzy
msgid "“Run-of-the-Mill” Footage"
msgstr "\"Run-of-the-Mill\" posnetki"

#: ../../glossary/useful_information/color_hell.rst:21
#, fuzzy
msgid ""
"The drama starts with screen recording footage that seems quite innocent and "
"normal at first sight. It may have been recorded on Android 7 devices using "
"a screen recording app (such as «AZ Screen Recording», but not the “Pro” "
"fake). And this footage has two slightly unusual properties:"
msgstr ""
"Drama se začne s posnetki zaslona, ki se na prvi pogled zdijo precej "
"nedolžni in normalni. Morda je bil posnet v napravah Android 7 z aplikacijo "
"za snemanje zaslona (kot je »AZ Screen Recording«, ne pa tudi »Pro« "
"ponaredek). In ta posnetek ima dve nekoliko nenavadni lastnosti:"

#: ../../glossary/useful_information/color_hell.rst:23
#, fuzzy
msgid "a *highly variable frame rate*,"
msgstr "Datoteka <b>%1</b> ima spremenljivo hitrost v sličicah na sekundo."

#: ../../glossary/useful_information/color_hell.rst:24
#, fuzzy
msgid ""
"it’s using `BT.601 <https://en.wikipedia.org/wiki/Rec._601>`_ , instead of "
"BT.709 like so much HD footage these days."
msgstr ""
"to je uporaba 'BT.601 <https://en.wikipedia.org/wiki/Rec._601>'_ , namesto "
"BT.709 všeč toliko HD posnetkov v teh dneh."

#: ../../glossary/useful_information/color_hell.rst:26
#, fuzzy
msgid "Should cause no problems, right? Well…"
msgstr "Ne bi smelo povzročati težav, kajne? Dobro..."

#: ../../glossary/useful_information/color_hell.rst:28
#, fuzzy
msgid ""
"As it turns out, Kdenlive’s media engine `MLT <https://www.mltframework.org/"
">`_ can exhibit some issues with video footage that has a highly variable "
"frame rate, such as between 0.001 and 100+ fps. The symptoms are subtle, yet "
"endanger production quality: it seems as if MLT may well pick a future frame "
"which is way off in regions with a low framerate. While this isn’t an issue "
"for a suitably high framerate, this causes odd results in other places. For "
"instance, in my productions user touch interaction shows up even a few "
"seconds before the interaction will appear. This is probably caused by a "
"very low fps during the inactivity period just before the user interaction."
msgstr ""
"Kot se je izkazalo, lahko Kdenliveov medijski motor \"MLT <https://www."
"mltframework.org/>\"_ izkaže nekatere težave z video posnetki, ki imajo zelo "
"spremenljivo hitrost okvirja, na primer med 0.001 in 100+ fps. Simptomi so "
"subtilne, vendar ogrožajo kakovost proizvodnje: zdi se, kot da mlt lahko "
"izberejo prihodnji okvir, ki je daleč off v regijah z nizkim okvirjem. "
"Čeprav to ni težava za primerno visok okvirni okvir, to povzroča nenavadne "
"rezultate na drugih mestih. Na primer, v moji proizvodnji se interakcija z "
"dotikom uporabnika prikaže celo nekaj sekund, preden se bo interakcija "
"pojavila. To je verjetno posledica zelo nizke fps v obdobju neaktivnosti "
"malo pred interakcijo uporabnika."

#: ../../glossary/useful_information/color_hell.rst:30
#, fuzzy
msgid ""
"Alas, transcoding to a fixed frame rate surely is one of `ffmpeg’s <https://"
"www.ffmpeg.org/>`_ easy tasks (this example assumes a constant project frame "
"rate of 25 fps):"
msgstr ""
"Aja, transkodiranje na fiksno hitrost okvirja je zagotovo eno od \"ffmpeg's "
"<https://www.ffmpeg.org/>'_ enostavnih opravil (ta primer predvideva stalno "
"hitrost okvirja projekta 25 fps):"

#: ../../glossary/useful_information/color_hell.rst:32
#, fuzzy
msgid "$ ``ffmpeg -i raw.mp4 -r 25 -crf 18 screen-rec.mp4``"
msgstr "$ ''ffmpeg -i raw.mp4 -r 25 -crf 18 screen-rec.mp4''"

#: ../../glossary/useful_information/color_hell.rst:34
#, fuzzy
msgid ""
"The constant frame rate cures the issues mentioned above, so the results are "
"as to be expected. Except…"
msgstr ""
"Stalna hitrost okvirja pozdravi zgoraj omenjena vprašanja, zato je treba "
"rezultate pričakovati. Razen..."

#: ../../glossary/useful_information/color_hell.rst:38
#, fuzzy
msgid "Easy Transcoding: Color Me Bad"
msgstr "Prekodiranje je spodletelo"

#: ../../glossary/useful_information/color_hell.rst:47
#, fuzzy
msgid ""
"Unfortunately, the resulting video now shows shifted colors! It might not be "
"too obvious in the first place, but it can be quite prominent when you work "
"more with your footage. And it gets clearly visible to your audience in case "
"you are going to mix this footage side-by-side with further processed "
"versions of it, such as extracted frames for stills."
msgstr ""
"Na žalost, rezultat video zdaj prikazuje preusmerjenih barv! Morda na prvem "
"mestu ni preveč očitno, lahko pa je precej izkazen, ko s posnetki delaš več. "
"In to postane jasno vidno za občinstvo v primeru, da boste mešali ta "
"posnetek ob strani z nadaljnjimi predelanimi različicami, kot so izvlečki "
"okvirji za stills."

#: ../../glossary/useful_information/color_hell.rst:49
#, fuzzy
msgid ""
"A more close inspection, either using Kdenlive’s built-in clip properties "
"pane or `ffprobe <https://www.ffmpeg.org/ffprobe.html>`_, reveals that the "
"*transcoded file* **lacks the BT.601 color profile indication**. Yet, ffmpeg "
"did *not transform the colors* at all during transcoding, and simply dropped "
"the correct color profile information!"
msgstr ""
"Bolj tesen pregled, bodisi z uporabo Kdenlivejevega vgrajenega podokna "
"lastnosti izrezkov ali \"ffprobe <https://www.ffmpeg.org/ffprobe.html>'_, "
"razkriva, da *transkodirano datoteko* **manjka označba barvnega profila "
"BT.601**. Vendar, ffmpeg je *ni preoblikovati barve* na vseh med "
"transkodingom, in preprosto padel pravilne informacije barvnega profila!"

#: ../../glossary/useful_information/color_hell.rst:54
#, fuzzy
msgid "Makeshift Measures"
msgstr "Makeshift ukrepi"

#: ../../glossary/useful_information/color_hell.rst:63
#, fuzzy
msgid ""
"Of course, there’s always Kdenlive’s ability to overwrite source clip "
"properties using the built-in clip properties pane."
msgstr ""
"Seveda je Kdenlive vedno možnost, da prepiše lastnosti izvornega izrezka z "
"vgrajenim podoknom lastnosti izrezkov."

#: ../../glossary/useful_information/color_hell.rst:65
#, fuzzy
msgid ""
"Simply select the transcoded video clip in the project bin. Then go to the "
"clip properties pane and select its “Force Properties” tab which shows a "
"*writing pen*. Check “Colorspace” and then select “ITU-R 601”. Kdenlive now "
"applies the correct color profile."
msgstr ""
"Preprosto izberite transkodni video posnetek v projektnem regalu. Nato "
"pojdite v podokno lastnosti izrezka in izberite zavihek \"Lastnosti sile\", "
"ki prikazuje *pisalo*. Preverite »Colorspace« in nato izberite »ITU-R 601«. "
"Kdenlive zdaj uporablja pravilen barvni profil."

#: ../../glossary/useful_information/color_hell.rst:67
#, fuzzy
msgid ""
"While very easy, this method has its limitations; it’s fine while you keep "
"working *solely inside the Kdenlive editor and its MLT renderer*. But as "
"soon as you need to pull in external video tools, such as *ffmpeg* for image "
"extraction…, you will loose: these tools don’t know about Kdenlive’s source "
"clip property overrides. We thus need to get the correct color profile "
"information right into the transcoded video files themselves."
msgstr ""
"Čeprav zelo enostavno, ta metoda ima svoje omejitve; to je v redu, medtem ko "
"boste delali *samo znotraj Kdenlive urejevalnik in njegov MLT renderer*. "
"Toda takoj, ko boste morali potegniti zunanje video orodja, kot so *ffmpeg* "
"za ekstrakcijo slike..., boste znebili: ta orodja ne vedo o Kdenlive izvorni "
"posnetek lastnosti preglasi. Tako moramo dobiti pravilne informacije "
"barvnega profila prav v transkoded video datoteke sami."

#: ../../glossary/useful_information/color_hell.rst:72
#, fuzzy
msgid "Preserving BT.601 in Transcoding"
msgstr "Ohranjanje BT.601 v transkodiranje"

#: ../../glossary/useful_information/color_hell.rst:74
#, fuzzy
msgid ""
"To make this matter worse, the seemingly obvious color profile "
"transformation ``-vf colormatrix=bt601:bt601`` simply doesn’t work: ffmpeg "
"complains about not being to transform between the same input and output "
"color profile. *Grrr*."
msgstr ""
"Da bi bila ta zadeva še hujša, naokoli očitna transformacija barvnega "
"profila ''-vf colormatrix=bt601:bt601'' preprosto ne deluje: ffmpeg se "
"pritožuje, da se ne bo transformira med istim vhodnim in izhodnim barvnim "
"profilom. *Grrr*."

#: ../../glossary/useful_information/color_hell.rst:76
#, fuzzy
msgid ""
"It took quite some extensive searching until I found the missing puzzle "
"piece on Stack Exchange’s Video Production Q&A site: `ffmpeg: explicitly tag "
"h.264 as bt.601, rather than leaving unspecified? <https://video."
"stackexchange.com/questions/16840/ffmpeg-explicitly-tag-h-264-as-bt-601-"
"rather-than-leaving-unspecified>`_"
msgstr ""
"To je bilo precej obsežno iskanje, dokler nisem našel manjkajoče puzzle kos "
"na Stack Exchange's Video Production Q&A stran: \"ffmpeg: izrecno tag h.264 "
"kot bt.601, namesto da bi pustili nesprejemljivo? <https://video."
"stackexchange.com/questions/16840/ffmpeg-explicitly-tag-h-264-as-bt-601-"
"rather-than-leaving-unspecified>'_"

#: ../../glossary/useful_information/color_hell.rst:78
#, fuzzy
msgid ""
"There’s a catch to watch out for: BT.601 comes in PAL and NTSC flavors which "
"feature slightly different primary chromaticities, transfer curves, and "
"colorspaces. So check your raw footage first using ``ffprobe`` (or "
"``mediainfo``) which one has been used during recording in your case. Please "
"note that it doesn’t matter that your screen recording hasn’t standard "
"definition (SD) resolution at all, but it does matter when it comes to "
"encoding color."
msgstr ""
"Na voljo je ulov, na katerega je treba paziti: BT.601 prihaja v OKUSIH PAL "
"in NTSC, ki imajo nekoliko različne primarne kromatične lastnosti, krivulje "
"prenosa in barvne prostore. Zato najprej preverite svoje surove posnetke z "
"uporabo \"ffprobe\" (ali \"mediainfo\"), ki je bil uporabljen med snemanjem "
"v vašem primeru. Upoštevajte, da ni pomembno, da snemanje zaslona sploh nima "
"standardne ločljivosti definicije (SD), vendar je pomembno, ko gre za barvo "
"kodiranja."

#: ../../glossary/useful_information/color_hell.rst:81
#, fuzzy
msgid "I’m Not Quite Dead Yet: PAL and NTSC DNA"
msgstr "Nisem še čisto mrtev: PAL in NTSC DNK"

#: ../../glossary/useful_information/color_hell.rst:83
#, fuzzy
msgid ""
"So how do we find out if a given video recording file, say ``raw.mp4``, uses "
"the PAL or NTSC color space? Of course, ``ffprobe`` comes to our rescue. But "
"in order to not get lost in all the nitty-gritty details ``ffprobe`` will "
"throw at you, we need to tame it using a few options and ``grep``:"
msgstr ""
"Kako bomo torej ugotovili, ali dana datoteka za snemanje videoposnetkov, "
"pravijo »surovo.mp4'', uporablja barvni prostor PAL ali NTSC? Seveda, "
"\"ffprobe\" pride na naše reševanje. Da pa se ne bi izgubili v vseh nitty-"
"gritty podrobnosti ''ffprobe'' bo vrgel na vas, moramo ukrotiti z uporabo "
"nekaj možnosti in ''grep'':"

#: ../../glossary/useful_information/color_hell.rst:85
#, fuzzy
msgid "$ ``ffprobe -v error -show_streams raw.mp4 | grep color_``"
msgstr "$ ''ffprobe -v napaka -show_streams surova.mp4 | grep color_''"

#: ../../glossary/useful_information/color_hell.rst:87
#, fuzzy
msgid "This should give you something along these lines:"
msgstr "To bi ti moralo dati nekaj v tej smeri:"

#: ../../glossary/useful_information/color_hell.rst:89
#, fuzzy
msgid ""
"``color_range=tv`` ``color_space=bt470bg`` ``color_transfer=smpte170m`` "
"``color_primaries=bt470bg``"
msgstr ""
"''color_range=tv'' ''color_space=bt470bg'' ''color_transfer=smpte170m'' "
"''color_primaries=bt470bg''"

#: ../../glossary/useful_information/color_hell.rst:94
#, fuzzy
msgid ""
"Someone surely thought that using a TV standard definition-related BT.601 is "
"a clever idea to record mobile device screens. Must have been a hipster with "
"a old-school tube TV sitting on his desk. Alas, the line ``color_space=...`` "
"will tell us whether we’re dealing with PAL (``=bt470bg``) or NTSC "
"(``=smpte170m``)."
msgstr ""
"Nekdo je zagotovo mislil, da je uporaba TV standardne definicije povezane "
"BT.601 pametna ideja za snemanje zaslonov mobilnih naprav. Verjetno je bil "
"hipster, ki je sedel na mizi. Alas, linija ''color_space=...'' nam bo "
"povedala, ali imamo opravka s PAL (''=bt470bg'') ali NTSC (''=smpte170m'')."

#: ../../glossary/useful_information/color_hell.rst:97
msgid "PAL"
msgstr "PAL"

#: ../../glossary/useful_information/color_hell.rst:99
#, fuzzy
msgid ""
"If it’s **PAL chromaticities** (``=bt470bg``), we then need to transcode as "
"follows:"
msgstr ""
"Če gre za **PAL kromatiko** (''=bt470bg''), moramo prekodirati na naslednji "
"način:"

#: ../../glossary/useful_information/color_hell.rst:101
#: ../../glossary/useful_information/color_hell.rst:110
#, fuzzy
msgid "$ ``ffmpeg -i raw.mp4``"
msgstr "$ ''ffmpeg -i surov.mp4''"

#: ../../glossary/useful_information/color_hell.rst:102
#, fuzzy
msgid "``-color_primaries bt470bg -color_trc gamma28 -colorspace bt470bg``"
msgstr "''-color_primaries bt470bg -color_trc gamma28 -colorspace bt470bg''"

#: ../../glossary/useful_information/color_hell.rst:103
#: ../../glossary/useful_information/color_hell.rst:112
#, fuzzy
msgid "``-r 25 -crf 18 screen-rec.mp4``"
msgstr "''-r 25 -crf 18 screen-rec.mp4''"

#: ../../glossary/useful_information/color_hell.rst:106
msgid "NTSC"
msgstr "NTSC"

#: ../../glossary/useful_information/color_hell.rst:108
#, fuzzy
msgid ""
"For **NTSC chromaticities** (``=smpte170m``), we’ll need a different set of "
"primaries, transfer curve, and colorspace:"
msgstr ""
"Za **NTSC kromatike** (''=smpte170m'') bomo potrebovali drugačen nabor "
"primarnih nalog, krivulje prenosa in barvnega prostora:"

#: ../../glossary/useful_information/color_hell.rst:111
#, fuzzy
msgid ""
"``-color_primaries smpte170m -color_trc smpte170m -colorspace smpte170m``"
msgstr ""
"''-color_primaries smpte170m -color_trc smpte170m -colorspace smpte170m''"

#: ../../glossary/useful_information/color_hell.rst:124
#, fuzzy
msgid ""
"In any case, Kdenlive/MLT now correctly see the transcoded video using the "
"BT.601 color profile. In addition, other media tools correctly detect the "
"color profile too – unless they are broken in that they don’t understand "
"BT.601 at all."
msgstr ""
"V vsakem primeru, Kdenlive /MLT zdaj pravilno videti transkodirajo video z "
"uporabo barvnega profila BT.601. Poleg tega tudi druga medijska orodja "
"pravilno zaznajo barvni profil – razen če so razdvojena v tem, da sploh ne "
"razumejo BT.601."

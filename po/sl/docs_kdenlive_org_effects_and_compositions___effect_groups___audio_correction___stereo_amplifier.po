# Translation of kdenlive Manual to Slovenian
#
# Copyright (C) 2022 This_file_is_part_of_KDE
# This file is distributed under the same license as the kdenlive package.
#
#
# Martin Srebotnjak <miles@filmsi.net>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: kdenlive ref manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-29 00:22+0000\n"
"PO-Revision-Date: 2022-08-20 16:48+0200\n"
"Last-Translator: Martin Srebotnjak <miles@filmsi.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"
"Translator: Martin Srebotnjak <miles@filmsi.net>\n"
"X-Generator: Poedit 3.1.1\n"
"X-Poedit-SourceCharset: ISO-8859-1\n"

#: ../../effects_and_compositions/effect_groups/audio_correction/stereo_amplifier.rst:10
msgid "Stereo Amplifier"
msgstr "Stereo ojačevalnik"

#: ../../effects_and_compositions/effect_groups/audio_correction/stereo_amplifier.rst:12
msgid "Contents"
msgstr "Vsebina"

#: ../../effects_and_compositions/effect_groups/audio_correction/stereo_amplifier.rst:14
msgid ""
"This is the LADSPA filter number `1049 <https://www.mltframework.org/plugins/"
"FilterLadspa-1049/>`_ from MLT."
msgstr ""
"To je filter LADSPA iz MLT, številka `1049 <https://www.mltframework.org/"
"plugins/FilterLadspa-1049/>`_."

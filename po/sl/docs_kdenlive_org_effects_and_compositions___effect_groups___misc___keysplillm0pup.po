# Translation of kdenlive Manual to Slovenian
#
# Copyright (C) 2022 This_file_is_part_of_KDE
# This file is distributed under the same license as the kdenlive package.
#
#
# Martin Srebotnjak <miles@filmsi.net>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: kdenlive ref manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-03 00:38+0000\n"
"PO-Revision-Date: 2022-08-20 19:17+0200\n"
"Last-Translator: Martin Srebotnjak <miles@filmsi.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"
"Translator: Martin Srebotnjak <miles@filmsi.net>\n"
"X-Generator: Poedit 3.1.1\n"
"X-Poedit-SourceCharset: ISO-8859-1\n"

#: ../../effects_and_compositions/effect_groups/misc/keysplillm0pup.rst:15
#, fuzzy
msgid "Key Spill Mop Up"
msgstr "Ključ razlitja mop gor"

#: ../../effects_and_compositions/effect_groups/misc/keysplillm0pup.rst:17
msgid "Contents"
msgstr "Vsebina"

#: ../../effects_and_compositions/effect_groups/misc/keysplillm0pup.rst:21
#, fuzzy
msgid ""
"This page is outdated. You may find the new one here: :ref:"
"`key_spill_mop_up`."
msgstr ""
"Ta stran je zamujena. Novega boste morda našli tukaj: :"
"ref:'key_spill_mop_up'."

#: ../../effects_and_compositions/effect_groups/misc/keysplillm0pup.rst:23
#, fuzzy
msgid ""
"This effect is used when using chroma keying (otherwise known as greenscreen "
"or :ref:`blue_screen` effect). Its purpose is to compensate for the fact "
"that sometimes the color from the green or blue screen reflects onto the "
"subject and will make them a shade of blue or green - especially around the "
"edges. This is known as \"keyspill\". This effect can attempt to compensate "
"for this issue."
msgstr ""
"Ta učinek se uporablja pri uporabi chroma keying (sicer znan kot greenscreen "
"ali :ref:'blue_screen' učinek). Njegov namen je nadomestiti dejstvo, da se "
"včasih barva z zelenega ali modrega zaslona odseva na temo in jih bo "
"naredila za odtenek modre ali zelene - še posebej okoli robov. To je znano "
"kot \"keyspill\". Ta učinek lahko to težavo poskuša kompenzovati."

#: ../../effects_and_compositions/effect_groups/misc/keysplillm0pup.rst:25
#, fuzzy
msgid ""
"The effect may be found in the :ref:`misc` group or in the :ref:"
"`alpha_manipulation` group (version >= 0.9.3)"
msgstr ""
"Učinek je mogoče najti v skupini :ref:'misc' ali v skupini :"
"ref:'alpha_manipulation' (različica >= 0.9.3)"

#: ../../effects_and_compositions/effect_groups/misc/keysplillm0pup.rst:29
msgid "Tutorial"
msgstr "Učilnica"

#: ../../effects_and_compositions/effect_groups/misc/keysplillm0pup.rst:31
#, fuzzy
msgid ""
"This tutorial shows usage of the following effects: keysplillm0pup, blue "
"screen, alpha operations - shrinkhard and denoiser"
msgstr ""
"Ta vadnica prikazuje uporabo naslednjih učinkov: keysplillm0pup, modri "
"zaslon, alfa operacije - shrinkhard in denoiser"

#: ../../effects_and_compositions/effect_groups/misc/keysplillm0pup.rst:33
msgid "https://youtu.be/l43Hz7YEcYU"
msgstr "https://youtu.be/l43Hz7YEcYU"

#: ../../effects_and_compositions/effect_groups/misc/keysplillm0pup.rst:37
msgid "Details"
msgstr "Podrobnosti"

#: ../../effects_and_compositions/effect_groups/misc/keysplillm0pup.rst:39
#, fuzzy
msgid "The :file:`README` for the keyspillm0pup is this:"
msgstr "The :file:'README' for the keyspillm0pup is this:"

#: ../../effects_and_compositions/effect_groups/misc/keysplillm0pup.rst:41
msgid "DESCRIPTION:"
msgstr "OPIS:"

#: ../../effects_and_compositions/effect_groups/misc/keysplillm0pup.rst:43
#, fuzzy
msgid ""
"After some experimentation with chroma keying, it looked to me that there is "
"no single method of key cleaning, that works in all situations, like "
"keyspill on bright, keyspill on dark, etc. So I included several cleaning "
"options, which can be used alone or in combination. In short, it offers "
"three ways of pixel selection (masking), that can be combined with four "
"types of color changing operations. The three selection / masking modes are "
"based on:"
msgstr ""
"Po nekem eksperimentu s chroma keying, se mi je zdi, da ni enotne metode "
"čiščenja ključev, ki deluje v vseh situacijah, kot keyspill na svetlo, "
"keyspill na temo, itd. Zato sem vključil več možnosti čiščenja, ki jih lahko "
"uporabljamo sami ali v kombinaciji. Skratka, ponuja tri načine izbire pik "
"(maskiranje), ki jih lahko kombinirate s štirimi vrstami barvno spreminjanje "
"operacij. Trije načini izbire / maskiranje temeljijo na:"

#: ../../effects_and_compositions/effect_groups/misc/keysplillm0pup.rst:45
#, fuzzy
msgid "similarity to key color"
msgstr "podobnost z barvo tipk"

#: ../../effects_and_compositions/effect_groups/misc/keysplillm0pup.rst:46
msgid "transparency"
msgstr "Prosojnost"

#: ../../effects_and_compositions/effect_groups/misc/keysplillm0pup.rst:47
#, fuzzy
msgid "closeness to the edge"
msgstr "bližina roba"

#: ../../effects_and_compositions/effect_groups/misc/keysplillm0pup.rst:49
#, fuzzy
msgid "and the four things that can be done to the selected pixels are:"
msgstr "in štiri stvari, ki jih je mogoče storiti za izbrane piksle so:"

#: ../../effects_and_compositions/effect_groups/misc/keysplillm0pup.rst:51
#, fuzzy
msgid "move away from the key color (De-Key)"
msgstr "odmik od barve ključa (De-Key)"

#: ../../effects_and_compositions/effect_groups/misc/keysplillm0pup.rst:52
#, fuzzy
msgid "move towards an target color (Target)"
msgstr "premikanje proti ciljni barvi (cilj)"

#: ../../effects_and_compositions/effect_groups/misc/keysplillm0pup.rst:53
msgid "desaturate"
msgstr "Zmanjšaj nasičenost"

#: ../../effects_and_compositions/effect_groups/misc/keysplillm0pup.rst:54
#, fuzzy
msgid "luma (brightness) adjust."
msgstr "luma (svetlost) prilagoditi."

#: ../../effects_and_compositions/effect_groups/misc/keysplillm0pup.rst:56
msgid "MASKS:"
msgstr "MASKE:"

#: ../../effects_and_compositions/effect_groups/misc/keysplillm0pup.rst:58
#, fuzzy
msgid ""
"Color difference masks are based on the color of the image, and do not "
"depend on the alpha from the preceding keying, except for ignoring the 100% "
"transparent areas, to increase speed."
msgstr ""
"Maske barvne razlike temeljijo na barvi slike in niso odvisne od alfa od "
"prehodne tipke, razen za ignoriranje 100% preglednih področij, za povečanje "
"hitrosti."

#: ../../effects_and_compositions/effect_groups/misc/keysplillm0pup.rst:60
#, fuzzy
msgid ""
"Transparency and Edge masks are based on the alpha channel from the "
"preceding keying operation. Transparency masks will affect only the parts "
"that are neither 100% opaque nor 100% transparent, based on the alpha values "
"from the preceding keying operation. The effect will be proportional to the "
"transparency."
msgstr ""
"Maske za preglednost in rob temeljijo na alfa kanalu iz odstavka 1. Maske za "
"preglednost bodo vplivale le na dele, ki niso niti 100% opaque niti 100% "
"pregledni, na podlagi alfa vrednosti iz odstavka 1. Učinek bo sorazmeren s "
"preglednostjo."

#: ../../effects_and_compositions/effect_groups/misc/keysplillm0pup.rst:64
#, fuzzy
msgid ""
"If a \"hard key\" was used in the preceding keying, there will be no areas "
"that T operations could affect. Edge masks will affect only pixels close to "
"the edge, with the effect diminishing away from the edge. The outer edge is "
"the edge of the fully opaque part, where the alpha from the preceding keying "
"is 1.0 (255)."
msgstr ""
"Če je bil pri h tem ključu uporabljen \"trdi ključ\", ne bo nobenih "
"področij, na katere bi lahko vplivale operacije T. Robne maske bodo vplivale "
"le na piksle blizu roba, učinek pa se bo znižel od roba. Navzven rob je rob "
"popolnoma neprozornega dela, kjer je alfa iz 1.0 (255)."

#: ../../effects_and_compositions/effect_groups/misc/keysplillm0pup.rst:68
#, fuzzy
msgid ""
"The edge masking algorithm is not yet what I would like it to be. I will "
"have to look some more into this, and improve it, so consider it a "
"\"temporary solution\" that will change in the future."
msgstr ""
"Algoritem za maskiranje roba še ni tak, kot bi si želel. V to bom moral "
"pogledati še nekaj in ga izboljšati, zato jo obravnavajte kot \"začasno "
"rešitev\", ki se bo v prihodnosti spremenila."

#: ../../effects_and_compositions/effect_groups/misc/keysplillm0pup.rst:70
#, fuzzy
msgid ""
"All masks can be further pruned with two parameters: an \"hue gate\", which "
"will limit the mask to hues close to the key hue, and an \"saturation "
"threshold\", which will limit the mask to areas with color saturation above "
"a threshold."
msgstr ""
"Vse maske je mogoče dodatno obrezati z dvema parametroma: »vrati za "
"odtenek«, ki bo masko omejila na odtenke blizu ključnega odtenka, in »prag "
"nasičenosti«, ki bo masko omejil na območja z nasičenjem barv nad pragom."

#: ../../effects_and_compositions/effect_groups/misc/keysplillm0pup.rst:72
msgid "CASCADING:"
msgstr "KASKADNO:"

#: ../../effects_and_compositions/effect_groups/misc/keysplillm0pup.rst:74
#, fuzzy
msgid ""
"This plugin can be cascaded, but it is not possible to get the same color "
"based mask in the second instance, because the colors will be changed by the "
"first instance. To enable two operations with the same mask, each plugin "
"instance can do two operations. With transparency and edge masks, cascading "
"is a bit easier. If the hue gate and saturation threshold are not used, "
"transparency and edge masks can be exactly the same in cascaded plugins."
msgstr ""
"Ta vtičnik je mogoče kaskadirani, vendar ni mogoče dobiti iste barve na "
"osnovi maske v drugem primeru, ker bodo barve spremenili prvi primerek. Če "
"želite omogočiti dve operaciji z isto masko, lahko vsak primerek vtičnika "
"naredi dve operaciji. S transparentnostjo in robno masko je kaskadiranje "
"nekoliko lažje. Če se ne uporabljata odtenek in prag nasičenosti, sta lahko "
"transparentnost in robne maske v kaskadnih vtičnikah popolnoma enake."

#: ../../effects_and_compositions/effect_groups/misc/keysplillm0pup.rst:76
msgid "PARAMETERS:"
msgstr "PARAMETRI:"

#: ../../effects_and_compositions/effect_groups/misc/keysplillm0pup.rst:78
#, fuzzy
msgid ""
"Key color: This should be the same or similar to the key color used for the "
"preceding keying operation."
msgstr ""
"Barva ključa: Ta mora biti enaka ali podobna barvi ključa, ki se uporablja "
"za predhodno operacijo tipkanja."

#: ../../effects_and_compositions/effect_groups/misc/keysplillm0pup.rst:80
#, fuzzy
msgid ""
"Target color: This is only used when \"Target\" operation is used with one "
"of the masks. The colors in the affected areas will be moved towards this "
"color, according to the \"Amount\" parameter."
msgstr ""
"Ciljna barva: To se uporablja le, če se operacija »Target« uporablja z eno "
"od mask. Barve na prizadetih območjih bodo premaknjene proti tej barvi, "
"glede na parameter »Količina«."

#: ../../effects_and_compositions/effect_groups/misc/keysplillm0pup.rst:82
#, fuzzy
msgid ""
"Mask type: Selects the type of mask that will determine where the color "
"altering operations will occur."
msgstr ""
"Vrsta maske: izbere vrsto maske, ki bo določila, kje se bodo dogajale "
"operacije spreminjanja barv."

#: ../../effects_and_compositions/effect_groups/misc/keysplillm0pup.rst:84
#, fuzzy
msgid ""
"Tolerance: For the color difference mask, the range of colors around the "
"key, that will be 100% affected. For the transparency mask, the "
"\"amplification\". For the edge mask, the width of the affected area."
msgstr ""
"Toleranca: Za masko barvne razlike, razpon barv okoli ključa, da bo 100% a "
"zamaskiran. Za masko za preglednost je \"ojačevanje\". Za robno masko, "
"širino prizadetega območja."

#: ../../effects_and_compositions/effect_groups/misc/keysplillm0pup.rst:86
#, fuzzy
msgid ""
"Slope: For the color difference mask, the range of colors outside of "
"\"Tolerance\", that will be gradually less affected. No function for the "
"transparency and edge masks."
msgstr ""
"Slope: Za barvno razliko masko, razpon barv zunaj \"Tolerance\", ki bo "
"postopoma manj prizadet. Ni funkcije za transparentnost in robne maske."

#: ../../effects_and_compositions/effect_groups/misc/keysplillm0pup.rst:88
#, fuzzy
msgid ""
"Hue gate: Reduces the mask according to difference from key hue, to prevent "
"change to pixels that are within the mask, but not polluted by key."
msgstr ""
"Hue vrata: zmanjšuje masko glede na razliko od ključnega odtisa, da prepreči "
"spremembe v piksle, ki so znotraj maske, vendar ne onesnažuje s ključem."

#: ../../effects_and_compositions/effect_groups/misc/keysplillm0pup.rst:90
#, fuzzy
msgid ""
"Saturation threshold: Reduces the mask according to color saturation, to "
"avoid affecting the neutral areas."
msgstr ""
"Prag zasičenosti: zmanjša masko glede na nasičenost barv, da ne vpliva na "
"nevtralna območja."

#: ../../effects_and_compositions/effect_groups/misc/keysplillm0pup.rst:92
#, fuzzy
msgid ""
"Operation 1: Selects which of the four possible operations will be done on "
"the mask-selected pixels. Apart from no operation, there are four "
"possibilities: De-key, Target, De-saturate and Luma adjust."
msgstr ""
"Operacija 1: Izbere, katera od štirih možnih operacij bo opravljena na "
"pikslih, izbranih z masko. Razen brez operacije, obstajajo štiri možnosti: "
"De-key, Target, De-saturate in Luma prilagoditi."

#: ../../effects_and_compositions/effect_groups/misc/keysplillm0pup.rst:94
#, fuzzy
msgid ""
"Amount 1: The amount of the selected operation 1, how much the colors will "
"change."
msgstr ""
"Znesek 1: Količina izbrane operacije 1, koliko se bodo barve spremenile."

#: ../../effects_and_compositions/effect_groups/misc/keysplillm0pup.rst:96
#, fuzzy
msgid ""
"Operation 2, Amount 2: Enable a second operation to be performed with the "
"same mask."
msgstr ""
"Operacija 2, znesek 2: Omogočite izvajanje druge operacije z isto masko."

#: ../../effects_and_compositions/effect_groups/misc/keysplillm0pup.rst:98
#, fuzzy
msgid ""
"Show mask: This will show the selected mask as a greyscale image, to help "
"with fine tuning of the masks. Should be OFF for the final render."
msgstr ""
"Pokaži masko: To bo prikazalo izbrano masko kot sliko sivk, da pomaga pri "
"finem ugasnjenju mask. Za končni izklop bi moral biti izklopljen."

#: ../../effects_and_compositions/effect_groups/misc/keysplillm0pup.rst:100
#, fuzzy
msgid ""
"Mask to Alpha: Copies the active mask to the alpha channel. For all normal "
"spill cleaning operations, this should be OFF. By setting it ON, the "
"keyspillm0pup itself can be used as a keyer, or to generate some special "
"effects."
msgstr ""
"Maska v alfa: kopira aktivno masko v alfa kanal. Za vse običajne operacije "
"čiščenja razlitja, to mora biti OFF. Z nastavitvijo on, keyspillm0pup lahko "
"uporabljate kot keyer, ali za ustvarjanje nekaterih posebnih učinkov."

# Xavier Besnard <xavier.besnard@neuf.fr>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-12 00:40+0000\n"
"PO-Revision-Date: 2021-11-24 13:23+0100\n"
"Last-Translator: Xavier Besnard <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.11.80\n"

#: ../../effects_and_compositions/effect_groups/analysis_and_data/Audio_Spectrum_Filter.rst:1
msgid "Effects in Kdenlive video editor"
msgstr ""

#: ../../effects_and_compositions/effect_groups/analysis_and_data/Audio_Spectrum_Filter.rst:1
msgid ""
"KDE, Kdenlive, effects, audio filter, timeline, documentation, user manual, "
"video editor, open source, free, learn, easy"
msgstr ""

#: ../../effects_and_compositions/effect_groups/analysis_and_data/Audio_Spectrum_Filter.rst:19
msgid "Audio Spectrum Filter"
msgstr "Filtre de spectre sonore"

#: ../../effects_and_compositions/effect_groups/analysis_and_data/Audio_Spectrum_Filter.rst:21
msgid "Contents"
msgstr "Contenu"

#: ../../effects_and_compositions/effect_groups/analysis_and_data/Audio_Spectrum_Filter.rst:23
msgid ""
"This is the `audiospectrum <https://www.mltframework.org/plugins/"
"FilterAudiospectrum/>`_ MLT filter."
msgstr ""
"Ceci est un filtre « spectre sonore <https://www.mltframework.org/plugins/"
"FilterAudiospectrum/> »_ MLT."

#: ../../effects_and_compositions/effect_groups/analysis_and_data/Audio_Spectrum_Filter.rst:25
msgid ""
"It is a audio visualization filter that draws an audio spectrum on the image."
msgstr ""
"Il s'agit d'un filtre d'affichage audio dessinant un spectre sonore sur "
"l'image."

#: ../../effects_and_compositions/effect_groups/analysis_and_data/Audio_Spectrum_Filter.rst:27
msgid "This filter is keyframe-able."
msgstr ""

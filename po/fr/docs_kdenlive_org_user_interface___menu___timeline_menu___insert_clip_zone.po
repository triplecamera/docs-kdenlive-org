# Xavier Besnard <xavier.besnard@neuf.fr>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-13 00:22+0000\n"
"PO-Revision-Date: 2022-01-07 08:17+0100\n"
"Last-Translator: Xavier Besnard <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.12.1\n"

#: ../../user_interface/menu/timeline_menu/insert_clip_zone.rst:13
msgid "Insert Clip Zone in Timeline"
msgstr "Insérer une zone de vidéo dans la frise chronologique"

#: ../../user_interface/menu/timeline_menu/insert_clip_zone.rst:15
msgid "Contents"
msgstr "Contenu"

#: ../../user_interface/menu/timeline_menu/insert_clip_zone.rst:17
msgid ""
"This menu item is available in the :menuselection:`Timeline` Menu on the :"
"menuselection:`Insertion` sub menu. Shortcut is :kbd:`V`"
msgstr ""
"Cet élément de menu est disponible dans le menu :menuselection:`Frise "
"chronologique` sur le sous-menu :menuselection:`Insertion`. Le raccourci "
"associé est :kbd:`V`."

#: ../../user_interface/menu/timeline_menu/insert_clip_zone.rst:20
msgid ""
"Keyboard command \"v\" and \"b\": Since version 19.08 \"3 point editing with "
"keyboard shortcuts\" is implemented. Source and target has to be activated "
"that the clip gets inserted into the timeline."
msgstr ""
"Commande clavier « v » et « b » : depuis la version 19.08, « l'édition en 3 "
"points avec les raccourcis clavier » est implémentée. La source et la cible "
"doivent être activées pour que la vidéo soit insérée dans la frise "
"chronologique."

#: ../../user_interface/menu/timeline_menu/insert_clip_zone.rst:24
msgid ""
"More info here: https://kdenlive.org/en/2019/08/kdenlive-19-08-released/"
msgstr ""
"Plus d'informations est disponible sur la page : https://kdenlive.org/"
"en/2019/08/kdenlive-19-08-released/"

#: ../../user_interface/menu/timeline_menu/insert_clip_zone.rst:27
msgid ""
"Say you have a 10 sec. zone defined on a clip in Clip Monitor and on the "
"timeline you have a 20 sec. zone defined somewhere. When you press :kbd:`V` "
"or select  :menuselection:`Insert Clip Zone in Timeline (overwrite)` , it "
"will insert the 10 sec. segment of the clip from the Clip Monitor at the "
"beginning of the zone on the timeline. If there happens to be another clip "
"there already, it will overwrite it, completely or partially, depending on "
"how long the existing clip was."
msgstr ""
"Disons que vous avez une zone de 10 secondes définie sur une vidéo dans "
"l'écran des vidéos et que, sur la frise chronologique, vous avez une zone de "
"20 secondes définie quelque part. Lorsque vous appuyez sur :kbd:`V` ou que "
"vous sélectionnez :menuselection:`Insérer la zone de la vidéo dans la frise "
"chronologique (écraser)`, vous insérerez le segment de 10 secondes de la "
"vidéo à partir de l'écran de vidéos vers le début de la zone sur la frise "
"chronologique. S'il y a déjà une autre vidéo déjà présente, elle sera "
"écrasée, totalement ou partiellement, selon la longueur de la vidéo "
"existante."

#: ../../user_interface/menu/timeline_menu/insert_clip_zone.rst:31
msgid "Regions selected on time line and in clip monitor - blue regions."
msgstr ""
"Régions sélectionnées sur la frise chronologique et dans l'écran de vidéos - "
"régions bleues."

#: ../../user_interface/menu/timeline_menu/insert_clip_zone.rst:33
msgid ""
"Select  :menuselection:`Insert Clip Zone in Timeline (overwrite)` and the "
"section in the clip overwrites the section on the timeline"
msgstr ""
"Sélectionnez :menuselection:`Insérer la zone de la vidéo dans la frise "
"chronologique (écraser)` et la section dans la vidéo écrase la section sur "
"la frise chronologique."

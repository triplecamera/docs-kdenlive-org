# Xavier Besnard <xavier.besnard@neuf.fr>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-12 00:40+0000\n"
"PO-Revision-Date: 2021-12-23 09:18+0100\n"
"Last-Translator: Xavier Besnard <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.12.0\n"

#: ../../effects_and_compositions/audio.rst:1
msgid "Mix audio in Kdenlive video editor"
msgstr ""

#: ../../effects_and_compositions/audio.rst:1
msgid ""
"KDE, Kdenlive, timeline, audio mixer, multiple audio streams, audio "
"recording, documentation, user manual, video editor, open source, free, "
"learn, easy"
msgstr ""

#: ../../effects_and_compositions/audio.rst:16
msgid "Audio"
msgstr "Audio"

#: ../../effects_and_compositions/audio.rst:18
msgid ""
"Kdenlive has some tools for handling audio. Beside the audio spectrum viewer "
"and some audio effects, you have following possibilities:"
msgstr ""
"Kdenlive dispose de quelques outils pour gérer l'audio. Outre l'afficheur de "
"spectre audio et certains effets audio, vous disposez des possibilités "
"suivantes :"

#: ../../effects_and_compositions/audio.rst:23
msgid "Audio Mixer"
msgstr "Mixeur audio"

#: ../../effects_and_compositions/audio.rst:32
#, fuzzy
#| msgid "The audio mixer has following function for each channel:"
msgid "The audio mixer has following functions for each channel:"
msgstr "Le mixeur audio dispose des fonctions suivantes pour chaque canal :"

#: ../../effects_and_compositions/audio.rst:34
msgid "Channel number (audio track number) or Master channel"
msgstr "Numéro du canal (numéro de la piste audio) ou canal principal"

#: ../../effects_and_compositions/audio.rst:35
msgid "Mute an audio channel"
msgstr "Mettre en pause un canal audio"

#: ../../effects_and_compositions/audio.rst:36
msgid "Solo an audio channel"
msgstr "Isoler un canal audio"

#: ../../effects_and_compositions/audio.rst:37
#, fuzzy
#| msgid "Record audio direct on the track of the related audio channel"
msgid ""
":ref:`Record audio <audio-recording>` direct on the track of the related "
"audio channel"
msgstr "Enregistrer l'audio directement sur la piste du canal audio concerné."

#: ../../effects_and_compositions/audio.rst:38
msgid "Opens the effect stack of the related audio channel"
msgstr "Ouvre la pile d'effets du canal audio concerné."

#: ../../effects_and_compositions/audio.rst:39
msgid "Balance the audio channel. Either with the slider or with values"
msgstr ""
"Équilibrer le canal audio. Soit avec le curseur, soit avec des valeurs."

#: ../../effects_and_compositions/audio.rst:40
msgid "Adjustment of the volume"
msgstr "Ajustement du volume"

#: ../../effects_and_compositions/audio.rst:43
msgid "Multiple audio streams"
msgstr "Flux audio multiples"

#: ../../effects_and_compositions/audio.rst:47
msgid ""
"Multiple audio streams of a video clip. In clip properties on the tab audio "
"you can adjust and manipulate each audio stream. More details see here :ref:"
"`audio_properties`"
msgstr ""
"Plusieurs flux audio d'une séquence vidéo. Dans les propriétés de vidéo, "
"dans l'onglet audio, vous pouvez ajuster et manipuler chaque flux audio. "
"Veuillez consulter pour plus de détails :ref:`audio_propriétés`."

#: ../../effects_and_compositions/audio.rst:52
#, fuzzy
#| msgid "Audio Mixer"
msgid "Audio recording"
msgstr "Mixeur audio"

#: ../../effects_and_compositions/audio.rst:56
msgid ""
"There is now a :guilabel:`mic` button in the mixers (number 4 in above "
"picture) instead of the :guilabel:`record` button. Pressing the :guilabel:"
"`mic` button will enter in audio monitoring mode (levels show mic input and "
"volume slider selects the mic level). While recording you see a live "
"waveform appearing on timeline."
msgstr ""

#: ../../effects_and_compositions/audio.rst:63
msgid ""
"Enabling :guilabel:`mic` displays the track head record control and it get "
"colorized."
msgstr ""

#: ../../effects_and_compositions/audio.rst:69
msgid ""
"**Start record:** press :kbd:`spacebar` or click the :guilabel:`record` "
"button on the track head. A countdown start in project monitor (disable "
"countdown see :ref:`configure_audio_capture` settings)."
msgstr ""

#: ../../effects_and_compositions/audio.rst:71
msgid "**Pause:** press :kbd:`spacebar`"
msgstr ""

#: ../../effects_and_compositions/audio.rst:73
msgid "**To resume:** press :kbd:`spacebar` again"
msgstr ""

#: ../../effects_and_compositions/audio.rst:75
msgid ""
"**Stop record:** press :kbd:`esc` or click the :guilabel:`record` button in "
"the track head. The audio clip get added in the timeline and project bin."
msgstr ""

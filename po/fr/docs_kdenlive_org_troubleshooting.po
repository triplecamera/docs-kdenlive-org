# Xavier Besnard <xavier.besnard@neuf.fr>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-16 00:37+0000\n"
"PO-Revision-Date: 2022-01-17 16:59+0100\n"
"Last-Translator: Xavier Besnard <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.12.1\n"

#: ../../troubleshooting.rst:1
msgid "Solve your problems while working with Kdenlive video editor"
msgstr "Résoudre vos problèmes en travaillant avec l'éditeur vidéo Kdenlive"

#: ../../troubleshooting.rst:1
msgid ""
"KDE, Kdenlive, solving, issues, windows, documentation, user manual, video "
"editor, open source, free, learn, easy"
msgstr ""
"KDE, Kdenlive, résolution, problèmes, fenêtres, documentation, manuel "
"d'utilisation, éditeur vidéo, open source, gratuit, apprendre, facile"

#: ../../troubleshooting.rst:15
msgid "Troubleshooting"
msgstr "Dépannage"

#: ../../troubleshooting.rst:17
msgid "Contents:"
msgstr "Contenu :"

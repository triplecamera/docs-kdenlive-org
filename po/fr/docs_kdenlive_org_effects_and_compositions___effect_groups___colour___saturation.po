# Xavier Besnard <xavier.besnard@neuf.fr>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-12-06 17:26+0100\n"
"Last-Translator: Xavier Besnard <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.11.90\n"

#: ../../effects_and_compositions/effect_groups/colour/saturation.rst:14
msgid "Saturation"
msgstr "Saturation"

#: ../../effects_and_compositions/effect_groups/colour/saturation.rst:16
msgid "Contents"
msgstr "Contenu"

#: ../../effects_and_compositions/effect_groups/colour/saturation.rst:18
msgid ""
"This is the `Frei0r saturat0r <https://www.mltframework.org/plugins/"
"FilterFrei0r-saturat0r/>`_ MLT filter."
msgstr ""
"Ceci est le filtre « Frei0r saturat0r <https://www.mltframework.org/plugins/"
"FilterFrei0r-saturat0r/> »_ MLT."

#: ../../effects_and_compositions/effect_groups/colour/saturation.rst:20
msgid "Adjusts the saturation of a source image."
msgstr "Ajuste la saturation de l'image source."

#: ../../effects_and_compositions/effect_groups/colour/saturation.rst:22
msgid ""
"See `TheDiveo's blog <https://thediveo-e.blogspot.com/2013/10/grading-of-"
"hero-3-above-waterline.html>`_ for an example of the usage of the Saturation "
"effect."
msgstr ""
"Veuillez consulter le forum de discussions « TheDiveo <https://thediveo-e."
"blogspot.com/2013/10/grading-of-hero-3-above-waterline.html> »_ pour un "
"exemple d'utilisation de l'effet de saturation."

#: ../../effects_and_compositions/effect_groups/colour/saturation.rst:24
msgid "https://youtu.be/rWqlQaWtCFs"
msgstr "https://youtu.be/rWqlQaWtCFs"

#: ../../effects_and_compositions/effect_groups/colour/saturation.rst:26
msgid "https://youtu.be/reOG42ZzrZA"
msgstr "https://youtu.be/reOG42ZzrZA"

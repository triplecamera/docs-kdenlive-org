# Xavier Besnard <xavier.besnard@neuf.fr>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-29 00:22+0000\n"
"PO-Revision-Date: 2022-02-03 17:38+0100\n"
"Last-Translator: Xavier Besnard <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.12.1\n"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:13
msgid "Alpha Shapes"
msgstr "Formes Alpha"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:15
msgid "Contents"
msgstr "Contenu"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:17
msgid ""
"This is the `Frei0r alphaspot <https://www.mltframework.org/plugins/"
"FilterFrei0r-alphaspot/>`_ MLT filter, see also `Frei0r-alphaspot readme "
"<https://github.com/dyne/frei0r/blob/master/src/filter/alpha0ps/readme>`_ "
"file."
msgstr ""
"Ceci est le filtre « Frei0r alphaspot <https://www.mltframework.org/plugins/"
"FilterFrei0r-alphaspot/> »_ MLT. Veuillez consulter aussi le fichier « Lisez-"
"moi de Frei0r-alphaspot <https://github.com/dyne/frei0r/blob/master/src/"
"filter/alpha0ps/readme> »_."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:24
msgid ""
"Use this in combination with a :ref:`composite` to place areas of "
"transparency onto an overlaying clip such that the underlying clip shows "
"through in places defined by geometric shapes. By default, the area of "
"transparency is outside the shape that is drawn. Inside the shape is an area "
"of opacity where the overlaying clip is visible."
msgstr ""
"Utilisez cette option en combinaison avec un :ref:`composite` pour placer "
"des zones de transparence sur une vidéo superposée de sorte que la vidéo "
"sous-jacente apparaisse aux endroits définis par des formes géométriques. "
"Par défaut, la zone de transparence se trouve à l'extérieur de la forme qui "
"est dessinée. À l'intérieur de la forme se trouve une zone d'opacité où la "
"vidéo superposée est visible."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:27
msgid "Shape Options"
msgstr "Options de formes"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:29
msgid ""
"This controls the shape of the area of opacity that the effect will create."
msgstr ""
"Ceci permet de contrôler la forme de la zone d'opacité que l'effet va créer."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:31
msgid ""
"Shape options are :menuselection:`Rectangle`, :menuselection:`Ellipse`, :"
"menuselection:`Triangle`, and :menuselection:`Diamond`."
msgstr ""
"Les options de forme sont :menuselection:`Rectangle`, :menuselection:"
"`Ellipse`, :menuselection:`Triangle` et :menuselection:`Losange`."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:34
msgid "Tilt"
msgstr "Inclinaison"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:36
msgid ""
"This controls the angle the shape appears on the screen. The units are in "
"1000ths of a full rotation. Eg, a factor of 250 is one-quarter of a circle "
"turn and 500 is a 180 turn. I.e., 1000 tilt units = 360 degrees."
msgstr ""
"Ceci contrôle l'angle d'apparition de la forme à l'écran. Les unités sont en "
"millièmes de rotation complète. Par exemple, un facteur de 250 est un quart "
"de tour de cercle et 500 est un tour de 180 degrés, c'est-à-dire que 1000 "
"unités d'inclinaison = 360 degrés."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:39
msgid "Position X and Y"
msgstr "Position en X et Y"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:41
msgid "This defines the position of the shape on the screen."
msgstr "Ceci définit la position de la forme sur l'écran."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:44
msgid "Size X and Y"
msgstr "Taille en X et Y"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:46
msgid "Defines the size of the shape."
msgstr "Définit la taille de la forme."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:49
msgid "Transition Width"
msgstr "Largeur de transition"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:51
msgid ""
"Defines the width of a border on the shape where the transparency grades "
"from inside to outside the shape."
msgstr ""
"Définit la largeur d'une bordure sur la forme où la transparence se dégrade "
"de l'intérieur vers l'extérieur de la forme."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:54
msgid "Operations"
msgstr "Opérations"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:56
msgid ""
"Operations define what is to happen when you have more than one Alpha effect "
"on the clip."
msgstr ""
"Les opérations définissent ce qui doit se passer lorsque vous avez plus d'un "
"effet Alpha sur la vidéo."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:58
msgid ""
"Operations are :menuselection:`Write On Clear`, :menuselection:`Max`, :"
"menuselection:`Min`, :menuselection:`Add`, and :menuselection:`Subtract`:"
msgstr ""
"Les opérations sont :menuselection:`Écrire en clair`, :menuselection:`Max`, :"
"menuselection:`Min`, :menuselection:`Ajouter`, et :menuselection:"
"`Soustraire` :"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:60
msgid "Write On Clear = existing alpha mask is overwritten"
msgstr "Écriture en clair = Le masque Alpha existant est écrasé."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:61
msgid "Max = maximum( existing alpha mask, mask generated by this filter)"
msgstr "Maximal = Maximum (Masque Alpha existant, masque généré par ce filtre)"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:62
msgid "Min = minimum(existing alpha mask, mask generated by this filter)"
msgstr "Minimal = Minimum (Masque Alpha existant, masque généré par ce filtre)"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:63
msgid "Add = existing alpha mask + mask generated by this filter"
msgstr "Ajouter = Masque Alpha existant + masque généré par ce filtre"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:64
msgid "Subtract = existing alpha mask - mask generated by this filter"
msgstr "Soustraire = Masque Alpha existant - masque généré par ce filtre."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:66
msgid "See the worked examples below to understand what these operations do."
msgstr ""
"Veuillez consulter les exemples travaillés ci-dessous pour comprendre ce que "
"font ces opérations."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:69
msgid "Min and Max Operations - Worked examples"
msgstr "Opérations minimales et maximale - Exemples travaillés"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:75
msgid ""
"These examples are performed on a simple composite transition with a video "
"file on Video track 1 and a color clip (yellow) on Video track 2."
msgstr ""
"Ces exemples sont réalisés sur une transition de composition simple avec un "
"fichier vidéo sur la piste vidéo 1 et une vidéo de couleur (jaune) sur la "
"piste vidéo 2."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:77
msgid ""
"Alpha shapes effect draws areas of opacity onto the image. The addition of "
"this filter (with the default settings of Min = 0 and Max =1000) makes the "
"whole frame transparent except for an area of opaqueness where the top image "
"can be seen."
msgstr ""
"L'effet avec des formes Alpha dessine des zones d'opacité sur l'image. "
"L'ajout de ce filtre (avec les paramètres par défaut de Min = 0 et Max = "
"1000) rend la totalité de l'image transparente à l'exception d'une zone "
"d'opacité où l'on peut voir l'image supérieure."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:79
msgid ""
"The Max and Min controls adjust the opacity of the image inside and outside "
"of the shape respectively. A setting of 1000 is 100% opaque. A setting of "
"zero is 0% opaque (i.e., 100% transparent)."
msgstr ""
"Les paramètres « Max » et « Min » permettent de régler, respectivement, "
"l'opacité de l'image à l'intérieur et à l'extérieur de la forme. Une valeur "
"de 1000 correspond à une opacité de 100 %. Un réglage de zéro correspond à "
"une opacité de 0 % (c'est-à-dire une transparence de 100 %)."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:81
msgid "**Max control**"
msgstr "**Contrôle maximal**"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:87
msgid ""
"The Max control regulates how opaque it is inside the shape. At Max = 1000 "
"it is completely opaque inside the shape and nothing of the background image "
"shows through."
msgstr ""
"Le paramètre « Max » contrôle le degré d'opacité à l'intérieur de la forme. "
"Quand le paramètre vaut 1000, l'opacité est totale à l'intérieur de la forme "
"et rien de l'image d'arrière-plan ne transparaît."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:93
msgid ""
"At Max = 500 it is semi-transparent inside the shape and you can see the "
"background bleeding through."
msgstr ""
"Avec Max = 500, l'intérieur de la forme est semi-transparent et vous pouvez "
"voir l'arrière-plan transparaître."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:99
msgid ""
"At Max = 0 inside the shape is completely transparent - the same as the rest "
"of the foreground image - and you can see all background."
msgstr ""
"Avec le paramètre « Max » à 0, l'intérieur de la forme est complètement "
"transparent - comme le reste de l'image de premier plan - et vous pouvez "
"voir tout l'arrière-plan."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:101
msgid "**Min Control**"
msgstr "**Contrôle minimal**"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:107
msgid ""
"The Min control adjusts how opaque it is outside the shape. When Min = 0 "
"outside the shape is completely transparent (opacity of zero) and at Min = "
"500 we see something of the foreground appears outside the shape."
msgstr ""
"Le paramètre « Min » ajuste l'opacité à l'extérieur de la forme. Lorsque ce "
"paramètre vaut 0, l'extérieur de la forme est complètement transparent "
"(opacité de zéro) et avec une valeur de 500, quelque chose du premier plan "
"apparaît à l'extérieur de la forme."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:113
msgid ""
"At Min = 1000 the opacity outside the shape is 100% and nothing of the "
"background appears."
msgstr ""
"À Min = 1000, l'opacité à l'extérieur de la forme est de 100 % et rien de "
"l'arrière-plan n'apparaît."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:115
msgid "**Combining Alpha Shapes - Operations**"
msgstr "**Combinaison de formes Alpha - Opérations**"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:121
msgid ""
"In this example, I have a Triangle alpha shape defined as shown and this is "
"at the top of the effect stack with operation :menuselection:`Write on "
"clear`."
msgstr ""
"Dans cet exemple, j'ai défini une forme de triangle Alpha comme indiqué. "
"Celle-ci se trouve en haut de la pile d'effets avec l'opération :"
"menuselection:`Écrire en clair`."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:127
msgid "Which appears like this on its own."
msgstr "Ce qui apparaît comme ça tout seul."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:133
msgid ""
"And I have Rectangle alpha shape as shown which is at the bottom of the "
"effect stack. Note the Max = 500 - i.e., 50% opacity inside the rectangle."
msgstr ""
"Et j'ai la forme en rectangle Alpha comme indiqué, se trouvant au bas de la "
"pile d'effets. Veuillez noter que le paramètre « Max » vaut 500 - c'est-à-"
"dire une opacité de 50 % à l'intérieur du rectangle."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:139
msgid "Which appears like this when on its own."
msgstr "Ce qui apparaît comme ça tout seul."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:141
msgid ""
"In the images below I demonstrate the effect of different alpha operations "
"on the Rectangle alpha shape."
msgstr ""
"Dans les images ci-dessous, je démontre l'effet de différentes opérations "
"Alpha sur la forme en rectangle Alpha."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:147
msgid "Write on Clear - the existing alpha mask is overwritten"
msgstr "Écriture en clair = Le masque Alpha existant est écrasé."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:153
msgid "add = existing alpha mask + mask generated by this filter."
msgstr "Ajouter = Masque Alpha existant + masque généré par ce filtre."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:155
msgid ""
"Note that areas with 1000 + 500 opacity would be 150% opaque. But you cant "
"get 150% opaque so they look the same as the 100% opaque areas."
msgstr ""
"Veuillez noter que les zones avec une opacité de 1000 + 500 seraient opaques "
"à 150 %. Mais, il est impossible d'obtenir une opacité de 150 %, de sorte "
"qu'elles ont le même aspect que les zones opaques à 100 %."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:161
msgid "subtract = existing alpha mask - mask generated by this filter."
msgstr "Soustraire = Masque Alpha existant - masque généré par ce filtre."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:163
msgid ""
"Note that areas with 0 - 500 opacity would be minus 50% opaque. But you cant "
"get -50% opaque so they look the same as the 0% opaque areas."
msgstr ""
"Veuillez noter que les zones avec une opacité de 0 à 500 seraient à moins "
"50 % opaques. Mais, vous ne pouvez pas obtenir - 50 % d'opacité. Elles ont "
"donc le même aspect que les zones opaques à 0 %."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:169
msgid "max = maximum( <existing alpha mask> , <mask generated by this filter>)"
msgstr ""
"maximal = maximum ( <existing alpha mask> , <mask generated by this filter>)"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/alpha_shapes.rst:175
msgid "min = minimum( <existing alpha mask> , <mask generated by this filter>)"
msgstr ""
"minimum = minimum( <existing alpha mask> , <mask generated by this filter>)"

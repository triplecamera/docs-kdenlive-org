# Xavier Besnard <xavier.besnard@neuf.fr>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-11-24 18:08+0100\n"
"Last-Translator: Xavier Besnard <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.11.80\n"

#: ../../effects_and_compositions/effect_groups/blur_and_hide/softglow.rst:11
msgid "Softglow"
msgstr "Softglow"

#: ../../effects_and_compositions/effect_groups/blur_and_hide/softglow.rst:13
msgid "Contents"
msgstr "Contenu"

#: ../../effects_and_compositions/effect_groups/blur_and_hide/softglow.rst:15
msgid ""
"This is the `Frei0r softglow <https://www.mltframework.org/plugins/"
"FilterFrei0r-softglow/>`_ MLT filter."
msgstr ""
"Ceci est le filtre « Frei0r softglow <https://www.mltframework.org/plugins/"
"FilterFrei0r-softglow/> »_ MLT."

#: ../../effects_and_compositions/effect_groups/blur_and_hide/softglow.rst:21
msgid "Softglow Applied"
msgstr "Softglow appliqué"

#: ../../effects_and_compositions/effect_groups/blur_and_hide/softglow.rst:27
msgid "The frame without the Softglow"
msgstr "La trame sans Softglow"

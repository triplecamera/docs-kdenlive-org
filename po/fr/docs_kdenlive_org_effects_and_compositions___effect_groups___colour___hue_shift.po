# Xavier Besnard <xavier.besnard@neuf.fr>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-12-07 13:13+0100\n"
"Last-Translator: Xavier Besnard <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.11.90\n"

#: ../../effects_and_compositions/effect_groups/colour/hue_shift.rst:15
msgid "Hue Shift"
msgstr "Décalage de teinte"

#: ../../effects_and_compositions/effect_groups/colour/hue_shift.rst:18
msgid "Contents"
msgstr "Contenu"

#: ../../effects_and_compositions/effect_groups/colour/hue_shift.rst:20
msgid ""
"This is the `Frei0r hueshift0r <https://www.mltframework.org/plugins/"
"FilterFrei0r-hueshift0r/>`_ MLT filter."
msgstr ""
"Ceci est le filtre « Frei0r hueshift0r <https://www.mltframework.org/plugins/"
"FilterFrei0r-hueshift0r/> »_ MLT."

#: ../../effects_and_compositions/effect_groups/colour/hue_shift.rst:22
msgid "Shifts the hue of a source image."
msgstr "Décale la teinte d'une image source."

#: ../../effects_and_compositions/effect_groups/colour/hue_shift.rst:24
msgid "https://youtu.be/Mq_G-AFznoc"
msgstr "https://youtu.be/Mq_G-AFznoc"

#: ../../effects_and_compositions/effect_groups/colour/hue_shift.rst:26
msgid "https://youtu.be/J7RCdP0-4Qs"
msgstr "https://youtu.be/J7RCdP0-4Qs"

#: ../../effects_and_compositions/effect_groups/colour/hue_shift.rst:28
msgid "https://youtu.be/D9w-I8hb3kU"
msgstr "https://youtu.be/D9w-I8hb3kU"

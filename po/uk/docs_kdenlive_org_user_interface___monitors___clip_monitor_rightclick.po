# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-04-08 00:38+0000\n"
"PO-Revision-Date: 2022-04-08 08:30+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:12
msgid "Clip Monitor - Right Click Menu"
msgstr "Монітор кліпу -> Контекстне меню"

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:15
msgid "Contents"
msgstr "Зміст"

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:17
msgid ""
"These are the menu items that are available when you right click a clip in "
"the :ref:`monitors`. These actions effect the clip that is currently "
"selected in the :ref:`project_tree`. Similar menu items are available from a "
"Right click menu in the Project monitor. However project monitor menu items "
"effect the currently selected clip on the timeline."
msgstr ""
"Це пункти меню, доступ до яких можна отримати клацанням правкою кнопкою миші "
"на пункт кліпу на панелі :ref:`монітора <monitors>`. Відповідні дії "
"стосуватимуться кліпу, який позначено  на панелі :ref:`контейнера проєкту "
"<project_tree>`. Доступ до цих пунктів меню можна отримати з контекстного "
"меню монітора проєкту. Втім, пункти меню монітора проєкту працюють із "
"поточним позначеним кліпом на монтажному столі."

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:26
msgid "Play..."
msgstr "Відтворити..."

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:30
msgid "Play"
msgstr "Відтворити"

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:33
msgid "Plays the clip currently selected in the project bin"
msgstr "Відтворює кліп, який позначено на панелі контейнера проєкту"

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:37
msgid "Play Zone"
msgstr "Відтворити ділянку"

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:40
msgid ""
"Plays the current zone and stops.  (See :ref:`monitors` for info about what "
"a Zone is)"
msgstr ""
"Відтворює поточну ділянку і зупиняє відтворення. (Див. :ref:`монітори "
"<monitors>`, щоб дізнатися більше про те, що таке ділянка)"

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:44
msgid "Loop Zone"
msgstr "Циклічне відтворення ділянки"

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:47
msgid ""
"Plays the current zone in a continuous loop. (See :ref:`monitors` for info "
"about what a Zone is)"
msgstr ""
"Відтворює поточну ділянку у нескінченному циклі. (Див. :ref:`монітори "
"<monitors>`, щоб дізнатися більше про те, що таке ділянка)"

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:51
msgid "Go To"
msgstr "Перейти"

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:55
msgid "Go to Project Start"
msgstr "Перейти до початку проєкту"

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:58
msgid ""
"When this item is selected from Clip Monitor it goes the beginning of the "
"clip. (When selected in project monitor it goes to the beginning of the "
"project)"
msgstr ""
"Якщо буде вибрано цей пункт на моніторі проєкту, буде здійснено перехід на "
"початок кліпу. (Якщо пункт вибрано на моніторі проєкту, буде здійснено "
"перехід до початку проєкту.)"

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:62
msgid "Go to Previous Snap Point"
msgstr "Перейти до попередньої точки прив’язки"

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:65
msgid ""
"Moves the clip position to the previous :ref:`editing` Point. Snap points "
"are sections in clips that other clips snap to when \"Snap\" is turned on."
msgstr ""
"Пересуває позицію відтворення у кліпі до попередньої точки :ref:`прилипання "
"<editing>`. Точки прилипання — розділи у кліпах, до яких прилипають інші "
"кліпи, якщо увімкнено прилипання."

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:67
#: ../../user_interface/monitors/clip_monitor_rightclick.rst:104
msgid ""
"Snap points include markers, zone in-points, zone out-points, guides, "
"transition start points etc"
msgstr ""
"Точками прилипання є позначки, вхідні точки ділянок, вихідні точки ділянок, "
"напрямні, точки початку переходів тощо."

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:71
msgid "Go to Zone Start"
msgstr "Перейти до початку ділянки"

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:74
msgid ""
"Goes to the start of the Zone.  (See :ref:`monitors` for info about what a "
"Zone is)"
msgstr ""
"Перейти до початку ділянки. (Див. :ref:`монітори <monitors>`, щоб дізнатися "
"більше про те, що таке ділянка)"

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:78
msgid "Go to Clip Start"
msgstr "Перейти до початку кліпу"

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:81
msgid ""
"Not working. Use Go To Project Start to make the clip monitor move to start "
"of the clip."
msgstr ""
"Не працює. Скористайтеся пунктом переходу до початку проєкту, щоб позицію у "
"моніторі кліпу було пересунуто на початок кліпу."

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:85
msgid "Go to Clip End"
msgstr "Перейти до кінця кліпу"

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:88
msgid ""
"Not working. Use Go To Project End to make the clip monitor move to end of "
"the clip."
msgstr ""
"Не працює. Скористайтеся пунктом переходу до кінця проєкту, щоб позицію у "
"моніторі кліпу було пересунуто на кінець кліпу."

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:92
msgid "Go to Zone End"
msgstr "Перейти до кінця ділянки"

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:95
msgid ""
"Goes to the end of the Zone.  (See :ref:`monitors` for info about what a "
"Zone is)"
msgstr ""
"Перейти до кінця ділянки. (Див. :ref:`монітори <monitors>`, щоб дізнатися "
"більше про те, що таке ділянка)"

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:99
msgid "Go to Next Snap Point"
msgstr "Перейти до наступної точки прив’язки"

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:102
msgid ""
"Moves the clip position to the next :ref:`editing` Point. Snap points are "
"sections in clips that other clips snap to when \"Snap\" is turned on."
msgstr ""
"Пересуває позицію відтворення у кліпі до наступної точки :ref:`прилипання "
"<editing>`. Точки прилипання — розділи у кліпах, до яких прилипають інші "
"кліпи, якщо увімкнено прилипання."

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:108
msgid "Go to Project End"
msgstr "Перейти до кінця проєкту"

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:111
msgid ""
"When this item is selected from Clip Monitor it goes the end of the clip. "
"(When selected in project monitor it goes to the end of the project)"
msgstr ""
"Якщо буде вибрано цей пункт на моніторі проєкту, буде здійснено перехід на "
"кінець кліпу. (Якщо пункт вибрано на моніторі проєкту, буде здійснено "
"перехід до кінця проєкту.)"

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:115
msgid "Markers"
msgstr "Позначки"

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:119
msgid "Add Marker"
msgstr "Додати позначку"

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:122
msgid "Adds a new :ref:`clip <clips>` into the clip at the current time point."
msgstr "Додає нові :ref:`позначки <clips>` у поточній часовій позиції кліпу."

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:126
msgid "Edit Marker"
msgstr "Змінити позначку"

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:129
msgid ""
"Brings up a dialog where you can edit the :ref:`clip <clips>` that is at the "
"current time point. Use *Go to marker* to put the monitor at the marker you "
"want to edit."
msgstr ""
"Відкриває вікно, у якому ви можете редагувати :ref:`позначку <clips>`, яка "
"перебуває у поточній позиції за часом. Скористайтеся пунктом *Перейти до "
"позначки*, щоб перейти у моніторі до позначки, яку ви хочете редагувати."

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:133
msgid "Delete Marker"
msgstr "Вилучити позначку"

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:136
msgid ""
"Deletes the :ref:`clip <clips>` that is a the current timepoint. Use *Go to "
"marker* to put the monitor at the marker you want to delete."
msgstr ""
"Вилучає :ref:`позначки <clips>`, які перебувають у поточній позиції за "
"часом. Скористайтеся пунктом *Перейти до позначки*, щоб перейти у моніторі "
"до позначки, яку ви хочете вилучити."

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:140
msgid "Delete All Markers"
msgstr "Вилучити всі позначки"

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:143
msgid "Deletes all the :ref:`clips`  from the current clip."
msgstr "Вилучає усі :ref:`clips` з поточного кліпу."

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:147
#: ../../user_interface/monitors/clip_monitor_rightclick.rst:154
msgid "Go to marker..."
msgstr "Перейти до позначки..."

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:150
msgid "Same a Go to Marker :ref:`clip_monitor_rightclick`."
msgstr ""
"Те саме, що і «Перейти до позначки» у :ref:`контекстному меню монітора кліпу "
"<clip_monitor_rightclick>`."

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:157
msgid ""
"The menu item pops out a list of existing :ref:`clips`  to select from. When "
"one is selected the Clip monitor moves to that marker."
msgstr ""
"Пункт меню відкриває список наявних :ref:`позначок <clips>` для вибору. Якщо "
"позначено позначку, у моніторі кліпу буде здійснено перехід до позначки."

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:161
msgid "Save zone"
msgstr "Зберегти ділянку"

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:164
msgid "This brings up the **Save Zone** dialog"
msgstr "Відкриває діалогове вікно **Зберегти ділянку**"

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:172
msgid ""
"This causes the current zone (see :ref:`monitors`) to be saved as a :file:`."
"mlt` file. This is a MLT video playlist file which is an xml format file "
"describing the zone that we saved."
msgstr ""
"Ініціює зберігання поточної ділянки (див. :ref:`monitors`) як файла :file:`."
"mlt`. Це файл списку відтворення відео MLT, у якому дані зберігаються у "
"форматі xml і описують ділянку, яку зберігають."

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:174
msgid ""
"You can then load the :file:`.mlt` files as clips into the project monitor "
"and edit them like any other clip."
msgstr ""
"Далі, ви можете завантажити файли :file:`.mlt` як кліпи до монітору проєкту "
"і редагувати їх як будь-які інші кліпи."

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:178
msgid "Extract Zone"
msgstr "Видобути ділянку"

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:180
msgid ""
"This brings up the **Cut Clip** dialog which appears to be setup to extract "
"the zone into a new file and add it to the project bin."
msgstr ""
"Цей пункт викликає вікно **Розрізати кліп**, у якому ви зможете налаштувати "
"видобування ділянки до нового файла і додати її до контейнера проєкту."

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:187
msgid ""
"On the authors 0.9.2  and 0.9.5 version of **Kdenlive** this feature is "
"broken for .dv format clips at least.  It does work for .mp4 type clips. "
"However, the accuracy of the cuts on the clip is way out."
msgstr ""
"У версіях **Kdenlive** 0.9.2 і 0.9.5 автора цього тексту ця можливість не "
"працює принаймні для кліпів у форматі .dv. Вона працює для кліпів типу .mp4. "
"Втім, точність розрізів на кліпі не є достатньою."

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:209
msgid "Extract frame"
msgstr "Видобути кадр"

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:211
msgid ""
"Extracts the frame currently in the clip monitor as a :file:`.PNG` image "
"which you can save to the file system."
msgstr ""
"Видобуває кадр з монітора кліпу як зображення :file:`.PNG`, яка ви можете "
"зберегти у файловій системі."

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:215
msgid "Set current image as thumbnail"
msgstr "Зробити поточне зображення зображенням мініатюри"

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:217
msgid ""
"This will change the thumbnail that represents this clip in the project bin "
"to the frame that is currently selected in the clip monitor."
msgstr ""
"Цей пункт замінить мініатюру, яку використано для цього кліпу у контейнері "
"проєкту, на кадр, який зараз позначено у моніторі кліпу."

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:221
msgid "Monitor overlay infos"
msgstr "Накладки із даними у моніторі"

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:223
msgid "???"
msgstr "???"

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:227
msgid "Real time (drop frames)"
msgstr "Режим реального часу (викидати кадри)"

#: ../../user_interface/monitors/clip_monitor_rightclick.rst:229
msgid ""
"Setting this to the Checked state means the clip monitor will drop frames "
"during playback to ensure the clip plays in real time. This does not effect "
"the final rendered file - it just effect how the clip appears when being "
"previewed in the clip monitor"
msgstr ""
"Якщо буде позначено цей пункт, монітор кліпу відкидатиме кадри під час "
"відтворення для збереження інтерактивності перегляду кліпу. Це не "
"стосуватиметься остаточного обробленого файла — це стосуватиметься лише "
"того, як виглядатиме кліп під час попереднього перегляду у моніторі кліпу."

# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-11-18 20:42+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../effects_and_compositions/effect_groups/distort/pixelize.rst:13
msgid "Pixelize"
msgstr "Пікселізація"

#: ../../effects_and_compositions/effect_groups/distort/pixelize.rst:15
msgid "Contents"
msgstr "Зміст"

#: ../../effects_and_compositions/effect_groups/distort/pixelize.rst:17
msgid ""
"This is the `Frei0r pixeliz0r <https://www.mltframework.org/plugins/"
"FilterFrei0r-pixeliz0r/>`_ MLT filter."
msgstr ""
"Це фільтр `Frei0r pixeliz0r <https://www.mltframework.org/plugins/"
"FilterFrei0r-pixeliz0r/>`_ бібліотеки MLT."

#: ../../effects_and_compositions/effect_groups/distort/pixelize.rst:19
msgid "Pixelize input image."
msgstr "Пікселізація вхідного зображення."

#: ../../effects_and_compositions/effect_groups/distort/pixelize.rst:21
msgid "https://youtu.be/iFj1y1OES2Q"
msgstr "https://youtu.be/iFj1y1OES2Q"

#: ../../effects_and_compositions/effect_groups/distort/pixelize.rst:23
msgid "https://youtu.be/jvuFSVGbVRg"
msgstr "https://youtu.be/jvuFSVGbVRg"

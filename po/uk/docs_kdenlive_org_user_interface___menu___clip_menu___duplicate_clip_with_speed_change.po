# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-21 00:37+0000\n"
"PO-Revision-Date: 2021-12-21 08:37+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../user_interface/menu/clip_menu/duplicate_clip_with_speed_change.rst:12
msgid "Duplicate Clip with speed change"
msgstr "Дублювати кліп зі зміною швидкості"

#: ../../user_interface/menu/clip_menu/duplicate_clip_with_speed_change.rst:14
msgid "Contents"
msgstr "Зміст"

#: ../../user_interface/menu/clip_menu/duplicate_clip_with_speed_change.rst:16
msgid ""
"This menu item is available from the Clip Jobs menu that appears when you :"
"ref:`project_tree` on a clip in the Project Bin or from under the :ref:"
"`project_menu` menu when a clip is selected in the Project Bin."
msgstr ""
"Цим пунктом меню можна скористатися за допомогою меню завдань кліпу, яке "
"буде відкрито у відповідь :ref:`клацання на панелі контейнера проєкту "
"<project_tree>` або за допомогою :ref:`меню «Проєкт» <project_menu>`, якщо "
"кліп позначено на панелі контейнера проєкту."

#: ../../user_interface/menu/clip_menu/duplicate_clip_with_speed_change.rst:20
msgid ""
"This feature used to be *Reverse Clip* and was available from version 0.9.6 "
"of Kdenlive. From version 17.04 it can still be used to reverse the clip - "
"by entering a speed of minus 100%. But you can create clips of other speeds "
"too. With the new version of the clip job the sound in the clip is also "
"reversed - so you can learn backwards talking!"
msgstr ""
"Ця можливість раніше називалася *Обернути кліп* і була доступною з версії "
"Kdenlive 0.9.6. У версії 17.04 нею усе ще можна користуватися для інверсії "
"відтворення кліпу — просто введіть швидкість у мінус 100%. Втім, у нових "
"версіях ви можете просто створювати кліпи з довільними значеннями швидкості. "
"У нових версіях також можна обертати відтворення звуку — вивчайте, як "
"вимовляти слова навпаки!"

#: ../../user_interface/menu/clip_menu/duplicate_clip_with_speed_change.rst:26
msgid ""
"When you select the :menuselection:`Duplicate Clip with speed change` option "
"from the menu, a new clip is created in the Project Bin. It has the filename "
"you supplied in the dialog with a .mlt extension. You can then add this clip "
"to the timeline and when you play it, the video of the original source clip "
"will played, but at the new speed (or in reverse)."
msgstr ""
"У відповідь на вибір у меню пункту :menuselection:`Дублювати кліп зі зміною "
"швидкості` на панелі контейнера проєкту буде створено новий кліп. Його "
"назвою буде вказана вами у діалоговому вікні назва із суфіксом .mlt. Далі, "
"ви зможете додати цей кліп на монтажний стіл і відтворити його — буде "
"відтворено відео з початкового кліпу-джерела, але із новою швидкістю (або "
"обернене відео)."

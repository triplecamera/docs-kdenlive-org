# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-03 00:38+0000\n"
"PO-Revision-Date: 2021-12-05 12:37+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../effects_and_compositions/effect_groups/fade/fade_to_black.rst:14
msgid "Fade to Black"
msgstr "Тьмянішання"

#: ../../effects_and_compositions/effect_groups/fade/fade_to_black.rst:16
msgid "Contents"
msgstr "Зміст"

#: ../../effects_and_compositions/effect_groups/fade/fade_to_black.rst:18
msgid "https://youtu.be/HKWeFL0DKJs"
msgstr "https://youtu.be/HKWeFL0DKJs"

#: ../../effects_and_compositions/effect_groups/fade/fade_to_black.rst:20
msgid ""
"In version 17.04 of Kdenlive you can add Fade to Black and Fade from Black "
"effects with a single click. And you can adjust the length of the fade with "
"a drag of the mouse. Hover over the grey rectangle that appears in the top "
"corner at the end of the clip on the time line and a tool tip will appear "
"saying \"Drag to Add or Resize a Fade Effect\". And if you do Drag you will "
"add a Fade to Black or a Fade From Black effect (depending on which end of "
"the clip you are working on)."
msgstr ""
"У версії Kdenlive 17.04 ви можете додати ефекти згасання до чорного і "
"світлішання з чорного одним клацанням. Ви можете скоригувати тривалість "
"згасання або світлішання за допомогою перетягування вказівником миші. "
"Наведіть вказівник миші на сірий прямокутник, який з'явиться у верхньому "
"куті наприкінці кліпу на монтажному столі. Там буде показано панель підказки "
"із повідомленням «Перетягніть, щоб додати або змінити розмір ефекту "
"згасання». Перетягування призведе до додавання ефекту згасання до чорного "
"або світлішання з чорного (залежно від кінця кліпу, з яким ви працюєте)."

#: ../../effects_and_compositions/effect_groups/fade/fade_to_black.rst:22
msgid "https://youtu.be/08bTC3VPtqM"
msgstr "https://youtu.be/08bTC3VPtqM"

#: ../../effects_and_compositions/effect_groups/fade/fade_to_black.rst:24
msgid ""
"Adjusting the duration of the fade - older Kdenlive versions: Adjust the "
"duration of the fade by dragging the green blob that appears when you hover "
"over the triangle vertex (see pic below) or by using the :ref:`effects` "
"duration slider."
msgstr ""
"Коригування тривалості згасання або світлішання — застарілі версії Kdenlive: "
"скоригуйте тривалість перетягуванням зеленої мітки, яка з'явиться, коли ви "
"наведете вказівник на вершину трикутника (див. знімок нижче), або за "
"допомогою повзунка тривалості панелі :ref:`effects`."

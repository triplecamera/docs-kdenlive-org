# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-11-18 20:37+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../effects_and_compositions/effect_groups/misc/timeout_indicator.rst:12
msgid "Timeout Indicator"
msgstr "Індикатор часу очікування"

#: ../../effects_and_compositions/effect_groups/misc/timeout_indicator.rst:15
msgid "Contents"
msgstr "Зміст"

#: ../../effects_and_compositions/effect_groups/misc/timeout_indicator.rst:17
msgid ""
"This is `Frei0r timeout <https://www.mltframework.org/plugins/FilterFrei0r-"
"timeout/>`_ MLT filter by Simon A. Eugster."
msgstr ""
"Це фільтр `Frei0r timeout <https://www.mltframework.org/plugins/FilterFrei0r-"
"timeout/>`_ від Саймона А. Ойгстера."

#: ../../effects_and_compositions/effect_groups/misc/timeout_indicator.rst:19
msgid ""
"In ver 17.04 this is found in the :ref:`analysis_and_data` category of "
"Effects."
msgstr ""
"У версії 17.04 пункт цього ефекту розташовано у категорії :ref:"
"`analysis_and_data` меню «Ефекти»."

#: ../../effects_and_compositions/effect_groups/misc/timeout_indicator.rst:21
msgid ""
"This adds a little countdown bar to the bottom right of the video and is "
"available in ver. 0.9.5 of **Kdenlive**."
msgstr ""
"Додає невеличку панель відліку у нижній правій частині кадру відео, "
"доступний з версії 0.9.5 **Kdenlive**."

#: ../../effects_and_compositions/effect_groups/misc/timeout_indicator.rst:23
msgid "The settings in this screen shot produced the sample video below."
msgstr ""
"Параметри з цього знімка вікна використано для створення зразка відео, "
"наведеного нижче."

#: ../../effects_and_compositions/effect_groups/misc/timeout_indicator.rst:27
msgid "https://youtu.be/ry3DLZD_bRc"
msgstr "https://youtu.be/ry3DLZD_bRc"

# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-06 00:38+0000\n"
"PO-Revision-Date: 2021-12-25 00:58+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:12
msgid "Defish"
msgstr "Усування «риб’ячого ока»"

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:14
msgid "Contents"
msgstr "Зміст"

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:16
msgid ""
"This effect can transform footage shot with a fisheye lens, to look like it "
"was shot with a rectilinear lens, and vice versa. It can also be used to "
"straighten the video that was shot with one of these wide angle converters, "
"which are only slightly curvy, or with a semi-fisheye camera, like the GoPro "
"Hero."
msgstr ""
"Цей ефект може перетворити матеріал, знятий за допомогою об'єктива із "
"ефектом «риб'яче око», так, що він виглядатиме як матеріал, знятий за "
"допомогою прямокутного об'єктива, і навпаки. Ним також можна скористатися "
"для випрямлення відео, яке було знято за допомогою одного з ширококутних "
"перетворювачів, які лише трохи викривляють зображення, або за допомогою "
"камери з ефектом напів риб'ячого ока, зокрема GoPro Hero."

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:21
msgid "PARAMETERS"
msgstr "Параметри"

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:23
msgid "\"Amount\""
msgstr "«Величина»"

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:25
msgid ""
"Controls the amount of (de)distortion applied to the video. It controls the "
"ratio of fisheye focal length to image half diagonal, but in an nonlinear "
"inverse way, to make the control more \"comfortable\". It can be adjusted "
"beyond \"reasonable\" values (which differ between the mapping function "
"types), to produce some looney effects. When exploring this range, and the "
"image disappears, check the scaling, could be that the image became too big "
"or too small to see. For some unreasonable values the image might indeed "
"disappear, when there are math overflows or imaginary results... (types 1 "
"and 2 are more prone to image vanishing). Anyway, when working in the "
"\"special effect\" range, it is always worth to try manual scaling. If the "
"video contains zooming through a curvy wide angle adaptor, the needed amount "
"will vary. In this case use keyframing."
msgstr ""
"Керує потужністю викривлення (або виправлення), яке буде застосовано до "
"відео. Визначає відношення фокусної відстані «риб'ячого ока» до половини "
"діагоналі зображення, але у нелінійний обернений спосіб, щоб зробити "
"керування «комфортнішим». Може бути визначена за межами «притомного» "
"діапазону (який є різним для різних типів функції відображення) для "
"створення шалених ефектів. Якщо під час тестування різних значень зникає "
"зображення, перевірте масштабування: можливо зображення стало надто великим "
"або малим, і його не видно. Для деяких екстремальних значень зображення може "
"насправді зникнути, коли відбувається переповнення у формулах або ви "
"отримуєте уявні результати... (типи 1 і 2 є стійкішими до зникнення "
"зображення). Щоб там не було, коли ви працюєте у діапазоні «спецефектів», "
"завжди варто спробувати масштабування вручну. Якщо у відео міститься "
"масштабування за допомогою криволінійного ширококутного адаптера, потрібна "
"величина може бути досить різною. У такому випадку користуйтеся ключовими "
"кадрами."

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:28
msgid "\"DeFish\""
msgstr "\"Усування «риб’ячого ока»\""

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:30
msgid ""
"If checked, the transform direction is from fisheye to rectilinear, when not "
"checked, it is rectilinear to fisheye."
msgstr ""
"Якщо позначено, напрямком перетворення буде «з риб'ячого ока в прямокутне». "
"Якщо не позначено, напрямок — «з прямокутного у риб'яче око»."

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:32
msgid "\"Type\""
msgstr "«Тип»"

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:34
msgid ""
"Selects the fisheye angular mapping function used, among four possibilities:"
msgstr ""
"Визначає використану функцію кутового відображення риб'ячого ока. "
"Передбачено чотири варіанти:"

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:36
msgid "equidistant"
msgstr "рівновіддаленість"

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:37
msgid "orthographic"
msgstr "пряма проєкція"

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:38
msgid "equiarea"
msgstr "збереження площі"

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:39
msgid "stereographic"
msgstr "стереографія"

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:41
msgid "Wikipedia has a nice article about these."
msgstr "У Вікіпедії є чудова стаття щодо цих проєкцій."

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:43
msgid "\"Scaling\""
msgstr "«Масштабування»"

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:45
msgid "Select among three auto scaling options and manual scale:"
msgstr ""
"Виберіть один із трьох варіантів масштабування або масштабування вручну:"

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:47
msgid "scale to fill"
msgstr "масштабувати до заповнення"

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:48
msgid "keep center scale"
msgstr "масштабувати з утриманням у центрі"

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:49
msgid "scale to fit"
msgstr "масштабувати з метою умістити"

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:50
msgid "manual scale"
msgstr "масштабування вручну"

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:52
msgid ""
"\"Fill\" means that no empty borders will be left, but some of the input "
"image will be cropped. \"Fit\" means that no part of the input image will be "
"cropped, but there will be blank areas at the borders."
msgstr ""
"«Заповнення» означає, що у результаті перетворення програма не залишить на "
"зображенні порожніх крайових блоків, але частину вхідного зображення буде "
"обрізано. «Умістити» означає, що нічного з вхідного зображення не буде "
"обрізано, але на краях результату можуть з'явитися порожні смуги."

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:55
msgid "\"Manual Scale\""
msgstr "«Масштабування вручну»"

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:57
msgid ""
"When \"Scaling\" is set to manual scale, this control directly affects the "
"image scale, from 1/100 to 100X size. Only has effect when \"Scaling\" is "
"set to manual!"
msgstr ""
"Якщо для параметра «Масштабування» встановлено ручний режим, цей елемент "
"керування безпосередньо впливає на масштаб зображення. Можна встановлювати "
"масштаб від 1/100 до 100 розмірів."

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:60
msgid "\"Interpolator\""
msgstr "\"Інтерполятор\""

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:62
msgid ""
"Selects among seven different interpolators. This allows one to make a "
"quality/speed tradeoff. The interpolators are ordered from fast, low quality "
"to (very) slow high quality. The spline interpolating polynomials are from "
"Helmut Dersch. For realtime use, option 0 is the fastest, in fact it is "
"equal to no interpolation. In most cases bilinear should be good enough, and "
"on a decent machine should still run in real time. Beyond bicubic, the "
"quality gain is marginal for a single resampling. Lanczos takes an eternity!"
msgstr ""
"Вибирає один з семи різних засобів інтерполяції. Таким чином можна "
"встановити потрібний баланс між якістю і швидкістю. Інтерполятори "
"упорядковано від швидких із низькою якістю до (дуже) повільних із високою "
"якістю. Поліноми інтерполяції сплайнами запозичено у Гельмута Дерша (Helmut "
"Dersch). Для інтерактивного використання скористайтеся варіантом 0; він "
"найшвидший, фактично, його використання вимикає інтерполяцію. У більшості "
"випадків достатньо білінійної інтерполяції, яка на швидких комп'ютерах також "
"може бути використана в інтерактивному режимі. Інтерполяції вищі за "
"бікубічну дають незначне покращення якості при одноразовій зміні "
"дискретизації. Використання алгоритму Ланцоша робить процес надзвичайно "
"повільним!"

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:64
msgid "Nearest neighbor"
msgstr "Найближчий сусідній"

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:65
msgid "Bilinear"
msgstr "Білінійний"

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:66
msgid "Bicubic smooth"
msgstr "Бікубічне згладжене"

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:67
msgid "Bicubic sharp"
msgstr "Бікубічне різке"

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:68
msgid "Spline 4x4"
msgstr "Сплайн 4x4"

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:69
msgid "Spline 6x6"
msgstr "Сплайн 6x6"

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:70
msgid "Lanczos 16x16"
msgstr "Ланцош 16x16"

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:72
msgid "\"Aspect Type\""
msgstr "«Співвідношення розмірів»"

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:74
msgid ""
"Selects among four pixel aspect ratio presets, and manual: To get the math "
"right, Defish0r needs to know the pixel aspect ratio."
msgstr ""
"Вибір між чотирма типовими співвідношеннями розмірів та варіантом із "
"встановленням розмірів вручну: для обчислень Defish0r потрібні дані щодо "
"співвідношення розмірів у пікселях."

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:77
msgid "Square pixels"
msgstr "Квадратні пікселі"

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:78
msgid "PAL DV        1.067"
msgstr "PAL DV        1.067"

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:79
msgid "NTSC DV       0.889"
msgstr "NTSC DV       0.889"

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:80
msgid "HDV   1.333"
msgstr "HDV   1.333"

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:81
msgid "manual variable"
msgstr "задана вручну змінна"

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:83
msgid "\"Manual aspect\""
msgstr "«Співвідношення розмірів вручну»"

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:85
msgid ""
"When \"Aspect Type\" is set to option manual variable, this control directly "
"affects the pixel aspect ratio, from 0.5 to 2. Only has effect when \"Aspect "
"Type\" is set to manual!"
msgstr ""
"Якщо для «Співвідношення розмірів» вибрано варіант ручного керування, цей "
"засіб керування безпосередньо визначає співвідношення розмірів у пікселях, "
"від 0.5 до 2. Працює, лише якщо для «Співвідношення розмірів» встановлено "
"ручний режим!"

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:88
msgid "SOME APPLICATION NOTES"
msgstr "Деякі нотатки щодо застосування"

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:90
msgid "Tweaking the parameters for best defish"
msgstr "Коригування параметрів для найкращого усування «риб'ячого ока»"

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:92
msgid ""
"Take a shot of something like a brick wall or bathroom tiles, that has a lot "
"of horizontal and vertical straight lines. Be careful to keep the optical "
"axis as perpendicular as possible to the wall (=keep a maximally symmetrical "
"image in the viewfinder). Use this image to tweak the parameters, primarily "
"amount, type and aspect."
msgstr ""
"Зніміть щось подібне до цегляної стіни або плитки у ванній кімнаті — "
"поверхню де багато горизонтальних і вертикальних прямих ліній. Намагайтеся "
"тримати оптичну вісь якомога ближче до перпендикуляра до стіни (тобто робіть "
"зображення у видошукачі максимально симетричним). Скористайтеся цим "
"зображенням для коригування параметрів, в основному величини, типу та "
"співвідношення розмірів."

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:95
msgid "Some examples of Defish0r abuse"
msgstr "Декілька прикладів використання Defish0r"

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:97
msgid ""
"These were tried with PAL DV. These examples work best, when there is some "
"interesting action near the center of the image."
msgstr ""
"Для прикладів використано матеріал з PAL DV. Приклади працюють найкращим "
"чином, якщо найцікавіша дія відбувається поблизу центру зображення."

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:99
msgid "For a kind of roundish kaleidoscope, try this:"
msgstr "Для відтворення кругового калейдоскопа спробуйте таке:"

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:101
msgid ""
"Amount=775, Defish = OFF, Type = equidistant, Scaling = manual scale, Manual "
"Scale = 300...400"
msgstr ""
"Величина=775, Усування «риб'ячого ока» = вимкнено, Тип = рівновіддаленість, "
"Масштабування = масштаб вручну, Масштаб вручну = 300...400"

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:107
msgid "Another crazy distortion:"
msgstr "Ще одне божевільне викривлення:"

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:109
msgid ""
"Amount = 921, Defish = OFF, Type = stereographic, Scaling = manual scale, "
"Manual Scale = 191"
msgstr ""
"Величина=921, Усування «риб'ячого ока» = вимкнено, Тип = стереографія, "
"Масштабування = масштаб вручну, Масштаб вручну = 191"

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:115
msgid ""
"For an effect, reminiscent of some scenes from the \"2001 Space Odyssey\" "
"try this:"
msgstr ""
"Для створення ефекту, подібного до ефекту деяких сцен з «Космічної одіссеї "
"2001», спробуйте такі значення:"

#: ../../effects_and_compositions/effect_groups/distort/defish.rst:117
msgid "Amount = 900, Defish = ON, Type = stereographic, Scaling = fill"
msgstr ""
"Величина = 900, Усування «риб'ячого ока» = увімкнено, Тип = стереографія, "
"Масштабування = заповнити"

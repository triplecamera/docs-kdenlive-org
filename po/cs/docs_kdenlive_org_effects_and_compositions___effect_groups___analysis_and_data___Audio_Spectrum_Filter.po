# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Vit Pelcak <vit@pelcak.org>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-12 00:40+0000\n"
"PO-Revision-Date: 2022-01-18 09:30+0100\n"
"Last-Translator: Vit Pelcak <vpelcak@suse.cz>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 21.12.1\n"

#: ../../effects_and_compositions/effect_groups/analysis_and_data/Audio_Spectrum_Filter.rst:1
msgid "Effects in Kdenlive video editor"
msgstr ""

#: ../../effects_and_compositions/effect_groups/analysis_and_data/Audio_Spectrum_Filter.rst:1
msgid ""
"KDE, Kdenlive, effects, audio filter, timeline, documentation, user manual, "
"video editor, open source, free, learn, easy"
msgstr ""

#: ../../effects_and_compositions/effect_groups/analysis_and_data/Audio_Spectrum_Filter.rst:19
msgid "Audio Spectrum Filter"
msgstr "Filtr spektra audia"

#: ../../effects_and_compositions/effect_groups/analysis_and_data/Audio_Spectrum_Filter.rst:21
msgid "Contents"
msgstr "Obsah"

#: ../../effects_and_compositions/effect_groups/analysis_and_data/Audio_Spectrum_Filter.rst:23
msgid ""
"This is the `audiospectrum <https://www.mltframework.org/plugins/"
"FilterAudiospectrum/>`_ MLT filter."
msgstr ""

#: ../../effects_and_compositions/effect_groups/analysis_and_data/Audio_Spectrum_Filter.rst:25
msgid ""
"It is a audio visualization filter that draws an audio spectrum on the image."
msgstr ""

#: ../../effects_and_compositions/effect_groups/analysis_and_data/Audio_Spectrum_Filter.rst:27
msgid "This filter is keyframe-able."
msgstr ""

# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Vit Pelcak <vit@pelcak.org>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-05-11 00:38+0000\n"
"PO-Revision-Date: 2022-01-17 17:24+0100\n"
"Last-Translator: Vit Pelcak <vpelcak@suse.cz>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 21.12.1\n"

#: ../../glossary/credits_and_license.rst:8
msgid "Credits and License"
msgstr "Informace o autorech a licenční ujednání"

#: ../../glossary/credits_and_license.rst:10
msgid "Contents"
msgstr "Obsah"

#: ../../glossary/credits_and_license.rst:13
msgid "Documentation copyright"
msgstr ""

#: ../../glossary/credits_and_license.rst:15
msgid ""
"Copyright licensed under the Creative Commons License SA 4.0 unless stated "
"otherwise."
msgstr ""

#: ../../glossary/credits_and_license.rst:18
msgid "Documentation authors"
msgstr ""

#: ../../glossary/credits_and_license.rst:22
msgid ""
"Please add your name here if you contribute to the handbook. Please respect "
"alphabetical order"
msgstr ""

#: ../../glossary/credits_and_license.rst:24
msgid "Alancanon (https://userbase.kde.org/User:Alancanon)"
msgstr "Alancanon (https://userbase.kde.org/User:Alancanon)"

#: ../../glossary/credits_and_license.rst:25
msgid "Annew (https://userbase.kde.org/User:Annew)"
msgstr "Annew (https://userbase.kde.org/User:Annew)"

#: ../../glossary/credits_and_license.rst:26
msgid "Bushuev A. (https://userbase.kde.org/User:Bushuev)"
msgstr "Bushuev A. (https://userbase.kde.org/User:Bushuev)"

#: ../../glossary/credits_and_license.rst:27
msgid "Yuri Chornoivan"
msgstr "Yuri Chornoivan"

#: ../../glossary/credits_and_license.rst:28
msgid "ChristianW (https://userbase.kde.org/User:ChristianW)"
msgstr "ChristianW (https://userbase.kde.org/User:ChristianW)"

#: ../../glossary/credits_and_license.rst:29
msgid "Claus Christensen"
msgstr "Claus Christensen"

#: ../../glossary/credits_and_license.rst:30
msgid "Dadu042 (https://userbase.kde.org/User:Dadu042)"
msgstr "Dadu042 (https://userbase.kde.org/User:Dadu042)"

#: ../../glossary/credits_and_license.rst:31
msgid "Davem2 (https://userbase.kde.org/User:Davem2)"
msgstr "Davem2 (https://userbase.kde.org/User:Davem2)"

#: ../../glossary/credits_and_license.rst:32
msgid "Dbolton (https://userbase.kde.org/User:Dbolton)"
msgstr "Dbolton (https://userbase.kde.org/User:Dbolton)"

#: ../../glossary/credits_and_license.rst:33
msgid "Dirkolus (https://userbase.kde.org/User:Dirkolus)"
msgstr "Dirkolus (https://userbase.kde.org/User:Dirkolus)"

#: ../../glossary/credits_and_license.rst:34
msgid "Drewp (https://userbase.kde.org/User:Drewp)"
msgstr "Drewp (https://userbase.kde.org/User:Drewp)"

#: ../../glossary/credits_and_license.rst:35
msgid "Earl fx (https://userbase.kde.org/User:Earl fx)"
msgstr "Earl fx (https://userbase.kde.org/User:Earl fx)"

#: ../../glossary/credits_and_license.rst:36
msgid "Simon Eugster <simon.eu@gmail.com>"
msgstr "Simon Eugster <simon.eu@gmail.com>"

#: ../../glossary/credits_and_license.rst:37
msgid "Fentras (https://userbase.kde.org/User:Fentras)"
msgstr "Fentras (https://userbase.kde.org/User:Fentras)"

#: ../../glossary/credits_and_license.rst:38
msgid "Gallaecio (https://userbase.kde.org/User:Gallaecio)"
msgstr "Gallaecio (https://userbase.kde.org/User:Gallaecio)"

#: ../../glossary/credits_and_license.rst:39
msgid "Geolgar (https://userbase.kde.org/User:Geolgar)"
msgstr "Geolgar (https://userbase.kde.org/User:Geolgar)"

#: ../../glossary/credits_and_license.rst:40
msgid "Igorcmelo (https://userbase.kde.org/User:Igorcmelo)"
msgstr "Igorcmelo (https://userbase.kde.org/User:Igorcmelo)"

#: ../../glossary/credits_and_license.rst:41
msgid "Jessej (https://userbase.kde.org/User:Jessej)"
msgstr "Jessej (https://userbase.kde.org/User:Jessej)"

#: ../../glossary/credits_and_license.rst:42
msgid "Jack (https://userbase.kde.org/User:Jack)"
msgstr "Jack (https://userbase.kde.org/User:Jack)"

#: ../../glossary/credits_and_license.rst:43
msgid "KGHN (https://userbase.kde.org/User:KGHN)"
msgstr "KGHN (https://userbase.kde.org/User:KGHN)"

#: ../../glossary/credits_and_license.rst:44
msgid "Kon (https://userbase.kde.org/User:Kon)"
msgstr "Kon (https://userbase.kde.org/User:Kon)"

#: ../../glossary/credits_and_license.rst:45
msgid "Krugozor (https://userbase.kde.org/User:Krugozor)"
msgstr "Krugozor (https://userbase.kde.org/User:Krugozor)"

#: ../../glossary/credits_and_license.rst:46
msgid "Julius Künzel <jk.kdedev@smartlab.uber.space"
msgstr "Julius Künzel <jk.kdedev@smartlab.uber.space"

#: ../../glossary/credits_and_license.rst:47
msgid "Loopduplicate (https://userbase.kde.org/User:Loopduplicate)"
msgstr "Loopduplicate (https://userbase.kde.org/User:Loopduplicate)"

#: ../../glossary/credits_and_license.rst:48
msgid "Anders Lund"
msgstr "Anders Lund"

#: ../../glossary/credits_and_license.rst:49
msgid "Jean-Baptiste Mardelle <jb@kdenlive.org>"
msgstr "Jean-Baptiste Mardelle <jb@kdenlive.org>"

#: ../../glossary/credits_and_license.rst:50
msgid "Marko (https://userbase.kde.org/User:Marko)"
msgstr "Marko (https://userbase.kde.org/User:Marko)"

#: ../../glossary/credits_and_license.rst:51
msgid "Mmaguire (https://userbase.kde.org/User:Mmaguire)"
msgstr "Mmaguire (https://userbase.kde.org/User:Mmaguire)"

#: ../../glossary/credits_and_license.rst:52
msgid "Eugen Mohr"
msgstr "Eugen Mohr"

#: ../../glossary/credits_and_license.rst:53
msgid "Roger Morton (Ttguy)"
msgstr "Roger Morton (Ttguy)"

#: ../../glossary/credits_and_license.rst:54
msgid "Camille Moulin"
msgstr "Camille Moulin"

#: ../../glossary/credits_and_license.rst:55
msgid "Mvessi (https://userbase.kde.org/User:Mvessi)"
msgstr "Mvessi (https://userbase.kde.org/User:Mvessi)"

#: ../../glossary/credits_and_license.rst:56
msgid "Neverendingo (https://userbase.kde.org/User:Neverendingo)"
msgstr "Neverendingo (https://userbase.kde.org/User:Neverendingo)"

#: ../../glossary/credits_and_license.rst:57
msgid "Nikerabbit (https://userbase.kde.org/User:Nikerabbit)"
msgstr "Nikerabbit (https://userbase.kde.org/User:Nikerabbit)"

#: ../../glossary/credits_and_license.rst:58
msgid ""
"Brylie Christopher Oxley (https://userbase.kde.org/User:Brylie Christopher "
"Oxley)"
msgstr ""
"Brylie Christopher Oxley (https://userbase.kde.org/User:Brylie Christopher "
"Oxley)"

#: ../../glossary/credits_and_license.rst:59
msgid "Vincent Pinon <vpinon@kde.org>"
msgstr "Vincent Pinon <vpinon@kde.org>"

#: ../../glossary/credits_and_license.rst:60
msgid "Qubodup (https://userbase.kde.org/User:Qubodup)"
msgstr "Qubodup (https://userbase.kde.org/User:Qubodup)"

#: ../../glossary/credits_and_license.rst:61
msgid "TheDiveO (https://thediveo-e.blogspot.com/)"
msgstr "TheDiveO (https://thediveo-e.blogspot.com/)"

#: ../../glossary/credits_and_license.rst:62
msgid ""
"Torsten R\\u00c3\\u00b6mer (https://userbase.kde.org/User:Torsten R"
"\\u00c3\\u00b6mer)"
msgstr ""
"Torsten R\\u00c3\\u00b6mer (https://userbase.kde.org/User:Torsten R"
"\\u00c3\\u00b6mer)"

#: ../../glossary/credits_and_license.rst:63
msgid "Roanna (https://userbase.kde.org/User:Roanna)"
msgstr "Roanna (https://userbase.kde.org/User:Roanna)"

#: ../../glossary/credits_and_license.rst:64
msgid "Roger (https://userbase.kde.org/User:Roger)"
msgstr "Roger (https://userbase.kde.org/User:Roger)"

#: ../../glossary/credits_and_license.rst:65
msgid "Carl Schwan <carl@carlschwan.eu>"
msgstr "Carl Schwan <carl@carlschwan.eu>"

#: ../../glossary/credits_and_license.rst:66
msgid "Smolyaninov (https://userbase.kde.org/User:Smolyaninov)"
msgstr "Smolyaninov (https://userbase.kde.org/User:Smolyaninov)"

#: ../../glossary/credits_and_license.rst:67
msgid "Maris Stalte (https://userbase.kde.org/User:limerick)"
msgstr ""

#: ../../glossary/credits_and_license.rst:68
msgid "Sunab (https://userbase.kde.org/User:Sunab)"
msgstr "Sunab (https://userbase.kde.org/User:Sunab)"

#: ../../glossary/credits_and_license.rst:69
msgid "Tenzen (https://userbase.kde.org/User:Tenzen)"
msgstr "Tenzen (https://userbase.kde.org/User:Tenzen)"

#: ../../glossary/credits_and_license.rst:70
msgid "Thanks4theFish (https://userbase.kde.org/User:Thanks4theFish)"
msgstr "Thanks4theFish (https://userbase.kde.org/User:Thanks4theFish)"

#: ../../glossary/credits_and_license.rst:71
msgid "Till Theato <root@ttill.de>"
msgstr "Till Theato <root@ttill.de>"

#: ../../glossary/credits_and_license.rst:72
msgid "TheMickyRosen-Left (https://userbase.kde.org/User:TheMickyRosen-Left)"
msgstr "TheMickyRosen-Left (https://userbase.kde.org/User:TheMickyRosen-Left)"

#: ../../glossary/credits_and_license.rst:73
msgid "Thompsony (https://userbase.kde.org/User:Thompsony)"
msgstr "Thompsony (https://userbase.kde.org/User:Thompsony)"

#: ../../glossary/credits_and_license.rst:74
msgid "Vgezer (https://userbase.kde.org/User:Vgezer)"
msgstr "Vgezer (https://userbase.kde.org/User:Vgezer)"

#: ../../glossary/credits_and_license.rst:75
msgid "Alberto Villa (https://userbase.kde.org/User:Alberto Villa)"
msgstr "Alberto Villa (https://userbase.kde.org/User:Alberto Villa)"

#: ../../glossary/credits_and_license.rst:76
msgid "Paul R Worrall (https://userbase.kde.org/User:Paul R Worrall)"
msgstr "Paul R Worrall (https://userbase.kde.org/User:Paul R Worrall)"

#: ../../glossary/credits_and_license.rst:77
msgid "Xipmix (https://userbase.kde.org/User:Xipmix)"
msgstr "Xipmix (https://userbase.kde.org/User:Xipmix)"

#: ../../glossary/credits_and_license.rst:78
msgid "Xyquadrat (https://userbase.kde.org/User:Xyquadrat)"
msgstr "Xyquadrat (https://userbase.kde.org/User:Xyquadrat)"

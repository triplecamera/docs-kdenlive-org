# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Vit Pelcak <vit@pelcak.org>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-03 00:38+0000\n"
"PO-Revision-Date: 2022-02-08 15:03+0100\n"
"Last-Translator: Vit Pelcak <vpelcak@suse.cz>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 21.12.2\n"

#: ../../effects_and_compositions/effect_groups/misc/lumakey.rst:11
msgid "Lumakey Effect"
msgstr ""

#: ../../effects_and_compositions/effect_groups/misc/lumakey.rst:13
msgid "Contents"
msgstr "Obsah"

#: ../../effects_and_compositions/effect_groups/misc/lumakey.rst:15
msgid "In version 15.n of Kdenlive this is in the Misc category of effects."
msgstr ""

#: ../../effects_and_compositions/effect_groups/misc/lumakey.rst:17
msgid ""
"The Lumakey effect changes the clip's alpha channel. To see its effect, you "
"need a transition (like the Composite transition that is available in "
"tracks) and another clip beneath."
msgstr ""

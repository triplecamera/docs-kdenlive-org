# Chinese translations for Kdenlive Manual package
# Kdenlive Manual 套件的正體中文翻譯.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Automatically generated, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-11-18 00:21+0000\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation.rst:17
msgid "Alpha manipulation"
msgstr ""

#: ../../effects_and_compositions/effect_groups/alpha_manipulation.rst:19
msgid "Contents"
msgstr ""

#: ../../effects_and_compositions/effect_groups/alpha_manipulation.rst:21
msgid "Effects in this category"
msgstr ""

#: ../../effects_and_compositions/effect_groups/alpha_manipulation.rst:29
msgid "General Information about Alpha Manipulation"
msgstr ""

#: ../../effects_and_compositions/effect_groups/alpha_manipulation.rst:31
msgid ""
"Alpha Manipulation, more commonly known as Chroma Key compositing is an "
"effect that changes the background of the picture to a different background "
"the editor may want."
msgstr ""

#: ../../effects_and_compositions/effect_groups/alpha_manipulation.rst:33
msgid "More information can be found on the pages for the effects themselves:"
msgstr ""

#: ../../effects_and_compositions/effect_groups/alpha_manipulation.rst:35
msgid ":ref:`blue_screen` (for simple chroma key effects)"
msgstr ""

#: ../../effects_and_compositions/effect_groups/alpha_manipulation.rst:36
msgid ":ref:`color_selection` (for complex chroma key effects)"
msgstr ""

#: ../../effects_and_compositions/effect_groups/alpha_manipulation.rst:40
msgid "Compositing Reference Material"
msgstr ""

#: ../../effects_and_compositions/effect_groups/alpha_manipulation.rst:42
msgid ""
"For some background, the Wikipedia article in `alpha compositing <https://en."
"wikipedia.org/wiki/Alpha_compositing>`_ is useful. See also Porter, Thomas; "
"Tom Duff (1984). \"Compositing Digital Images\". Computer Graphics 18 (3): "
"p253–259 1984 `pdf <https://keithp.com/~keithp/porterduff/p253-porter.pdf>`_"
msgstr ""

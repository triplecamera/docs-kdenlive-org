# Chinese translations for Kdenlive Manual package
# Kdenlive Manual 套件的正體中文翻譯.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Automatically generated, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-03 00:38+0000\n"
"PO-Revision-Date: 2021-12-03 00:38+0000\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: ../../effects_and_compositions/transitions/alphaout.rst:11
msgid "alphaout transition"
msgstr ""

#: ../../effects_and_compositions/transitions/alphaout.rst:13
msgid "Contents"
msgstr ""

#: ../../effects_and_compositions/transitions/alphaout.rst:15
msgid ""
"This is the `Frei0r alphaout <https://www.mltframework.org/plugins/"
"TransitionFrei0r-alphaout/>`_ MLT transition."
msgstr ""

#: ../../effects_and_compositions/transitions/alphaout.rst:17
msgid "The alpha OUT operation."
msgstr ""

#: ../../effects_and_compositions/transitions/alphaout.rst:19
msgid "Yellow clip has a triangle alpha shape with min=0 and max=618."
msgstr ""

#: ../../effects_and_compositions/transitions/alphaout.rst:21
msgid "Green clip has rectangle alpha shape with min=0 and max=1000."
msgstr ""

#: ../../effects_and_compositions/transitions/alphaout.rst:23
msgid "alphaout is the transition in between."
msgstr ""

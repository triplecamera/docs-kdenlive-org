# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-03 00:38+0000\n"
"PO-Revision-Date: 2022-01-30 21:47+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.08.1\n"

#: ../../effects_and_compositions/transitions/alphain.rst:11
msgid "alphain transition"
msgstr "alphain-övertoning"

#: ../../effects_and_compositions/transitions/alphain.rst:13
msgid "Contents"
msgstr "Innehåll"

#: ../../effects_and_compositions/transitions/alphain.rst:15
msgid ""
"This is the `Frei0r alphain <https://www.mltframework.org/plugins/"
"TransitionFrei0r-alphain/>`_ MLT transition."
msgstr ""
"Det här är MLT-övertoningen `Frei0r alphain <https://www.mltframework.org/"
"plugins/TransitionFrei0r-alphain/>`_."

#: ../../effects_and_compositions/transitions/alphain.rst:17
msgid "The alpha IN operation."
msgstr "Operationen alfa IN."

#: ../../effects_and_compositions/transitions/alphain.rst:19
msgid "Yellow clip has a triangle alpha shape with min=0 and max=618."
msgstr ""
"Det gula klippet har en triangelformad alfaform med min = 0 och max = 618."

#: ../../effects_and_compositions/transitions/alphain.rst:21
msgid "Green clip has rectangle alpha shape with min=0 and max=1000."
msgstr ""
"Det gröna klippet har en rektangelformad alfaform med min = 0 och max = 1000."

#: ../../effects_and_compositions/transitions/alphain.rst:23
msgid "alphain is the transition in between."
msgstr "alfain är övertoningen mellan dem."

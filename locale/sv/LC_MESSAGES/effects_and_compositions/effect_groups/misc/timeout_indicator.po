# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2022-01-02 20:47+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.08.1\n"

#: ../../effects_and_compositions/effect_groups/misc/timeout_indicator.rst:12
msgid "Timeout Indicator"
msgstr "Tidsgränsindikering"

#: ../../effects_and_compositions/effect_groups/misc/timeout_indicator.rst:15
msgid "Contents"
msgstr "Innehåll"

#: ../../effects_and_compositions/effect_groups/misc/timeout_indicator.rst:17
msgid ""
"This is `Frei0r timeout <https://www.mltframework.org/plugins/FilterFrei0r-"
"timeout/>`_ MLT filter by Simon A. Eugster."
msgstr ""
"Det här är MLT-filtret `Frei0r tidsgräns <https://www.mltframework.org/"
"plugins/FilterFrei0r-timeout/>`_ av Simon A. Eugster."

#: ../../effects_and_compositions/effect_groups/misc/timeout_indicator.rst:19
msgid ""
"In ver 17.04 this is found in the :ref:`analysis_and_data` category of "
"Effects."
msgstr "I version 17.04 finns det i effektkategorin :ref:`analysis_and_data`."

#: ../../effects_and_compositions/effect_groups/misc/timeout_indicator.rst:21
msgid ""
"This adds a little countdown bar to the bottom right of the video and is "
"available in ver. 0.9.5 of **Kdenlive**."
msgstr ""
"Det lägger till en liten nedräkningsrad längst ner till höger på videon och "
"är tillgänglig i version 0.9.5 av **Kdenlive**."

#: ../../effects_and_compositions/effect_groups/misc/timeout_indicator.rst:23
msgid "The settings in this screen shot produced the sample video below."
msgstr "Inställningarna på skärmbilden skapade exempelvideon nedan."

#: ../../effects_and_compositions/effect_groups/misc/timeout_indicator.rst:27
msgid "https://youtu.be/ry3DLZD_bRc"
msgstr "https://youtu.be/ry3DLZD_bRc"

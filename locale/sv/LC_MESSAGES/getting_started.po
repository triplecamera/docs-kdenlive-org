# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-16 00:37+0000\n"
"PO-Revision-Date: 2022-01-23 20:28+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.08.1\n"

#: ../../getting_started.rst:1
msgid "Do your first steps with Kdenlive video editor"
msgstr "De första stegen med Kdenlive videoeditor"

#: ../../getting_started.rst:1
msgid ""
"KDE, Kdenlive, quick start, first steps, documentation, user manual, video "
"editor, open source, free, learn, easy"
msgstr ""
"KDE, Kdenlive, snabbstart, första steg, dokumentation, användarhandbok, "
"videoeditor, öppen källkod, fri, lär, enkel"

#: ../../getting_started.rst:15
msgid "Getting started"
msgstr "Komma igång"

#: ../../getting_started.rst:17
msgid "A short overview to start with Kdenlive."
msgstr "En kort översikt för att starta med Kdenlive."

#: ../../getting_started.rst:19
msgid "Contents:"
msgstr "Innehåll:"

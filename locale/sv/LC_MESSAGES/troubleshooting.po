# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-16 00:37+0000\n"
"PO-Revision-Date: 2022-01-23 20:27+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.08.1\n"

#: ../../troubleshooting.rst:1
msgid "Solve your problems while working with Kdenlive video editor"
msgstr "Lös problem under arbete med Kdenlive videoeditor"

#: ../../troubleshooting.rst:1
msgid ""
"KDE, Kdenlive, solving, issues, windows, documentation, user manual, video "
"editor, open source, free, learn, easy"
msgstr ""
"KDE, Kdenlive, lösa, problem, fönster, dokumentation, användarhandbok, "
"videoeditor, öppen källkod, fri, lär, enkel"

#: ../../troubleshooting.rst:15
msgid "Troubleshooting"
msgstr "Felsökning"

#: ../../troubleshooting.rst:17
msgid "Contents:"
msgstr "Innehåll:"

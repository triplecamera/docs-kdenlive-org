# Spanish translations for docs_kdenlive_org_effects_and_compositions___effect_groups___artistic___dust.po package.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Automatically generated, 2021.
# Eloy Cuadra <ecuadra@eloihr.net>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_kdenlive_org_effects_and_compositions___effect_groups___artistic___dust\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2022-01-09 04:57+0100\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.1\n"

#: ../../effects_and_compositions/effect_groups/artistic/dust.rst:14
msgid "Dust"
msgstr "Polvo"

#: ../../effects_and_compositions/effect_groups/artistic/dust.rst:16
msgid "Contents"
msgstr "Contenido"

#: ../../effects_and_compositions/effect_groups/artistic/dust.rst:18
msgid ""
"See `dust filter <https://www.mltframework.org/plugins/FilterDust/>`_ from "
"MLT."
msgstr ""
"Consulte el `filtro de polvo <https://www.mltframework.org/plugins/"
"FilterDust/>`_ de MLT."

#: ../../effects_and_compositions/effect_groups/artistic/dust.rst:20
msgid "Add dust and specks to the video clip, as in old movies."
msgstr "Añade polvo y motas al videoclip, como en las películas antiguas."

#: ../../effects_and_compositions/effect_groups/artistic/dust.rst:22
msgid "https://youtu.be/h0s0PBfpcEE"
msgstr "https://youtu.be/h0s0PBfpcEE"

#: ../../effects_and_compositions/effect_groups/artistic/dust.rst:24
msgid "https://youtu.be/wbX7Df8rC0M"
msgstr "https://youtu.be/wbX7Df8rC0M"

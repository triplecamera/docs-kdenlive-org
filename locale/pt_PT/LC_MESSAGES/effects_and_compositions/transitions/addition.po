# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-11-20 18:46+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: MLT\n"

#: ../../effects_and_compositions/transitions/addition.rst:11
msgid "addition transition"
msgstr "transição de adição"

#: ../../effects_and_compositions/transitions/addition.rst:13
msgid "Contents"
msgstr "Conteúdo"

#: ../../effects_and_compositions/transitions/addition.rst:15
msgid ""
"This is the `Frei0r addition <https://www.mltframework.org/plugins/"
"TransitionFrei0r-addition/>`_ MLT transition."
msgstr ""
"Este é o efeito de transição do MLT `adição do Frei0r <https://www."
"mltframework.org/plugins/TransitionFrei0r-addition/>`_."

#: ../../effects_and_compositions/transitions/addition.rst:17
msgid "Perform an RGB[A] addition operation of the pixel sources."
msgstr "Efectua uma operação de adição no RGB[A] nos pixels originais."

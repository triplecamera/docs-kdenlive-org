msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-11-14 00:14+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: ../../effects_and_compositions/effect_groups/audio_correction/mute.rst:13
msgid "Audio Correction - Mute"
msgstr "Correcção de Áudio - Silêncio"

#: ../../effects_and_compositions/effect_groups/audio_correction/mute.rst:15
msgid "Contents"
msgstr "Conteúdo"

#: ../../effects_and_compositions/effect_groups/audio_correction/mute.rst:17
msgid ""
"This effect mutes the sound track on the video. The audio on the track will "
"not be audible."
msgstr ""
"Este efeito silencia a faixa de áudio no vídeo. O áudio na faixa não será "
"audível."

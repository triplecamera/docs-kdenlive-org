# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-11-22 18:12+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: MLT clip\n"

#: ../../effects_and_compositions/effect_groups/distort/wave.rst:13
msgid "Wave"
msgstr "Onda"

#: ../../effects_and_compositions/effect_groups/distort/wave.rst:15
msgid "Contents"
msgstr "Conteúdo"

#: ../../effects_and_compositions/effect_groups/distort/wave.rst:17
msgid ""
"This is the `Wave <https://www.mltframework.org/plugins/FilterWave/>`_ MLT "
"filter."
msgstr ""
"Este é o filtro do MLT `Onda <https://www.mltframework.org/plugins/"
"FilterWave/>`_."

#: ../../effects_and_compositions/effect_groups/distort/wave.rst:19
msgid "Make waves on your clip with keyframes."
msgstr "Cria ondas no seu 'clip' com as imagens-chave."

#: ../../effects_and_compositions/effect_groups/distort/wave.rst:21
msgid "https://youtu.be/8VDzfR-q_sc"
msgstr "https://youtu.be/8VDzfR-q_sc"

#: ../../effects_and_compositions/effect_groups/distort/wave.rst:23
msgid "https://youtu.be/KEijSvZ6vvc"
msgstr "https://youtu.be/KEijSvZ6vvc"

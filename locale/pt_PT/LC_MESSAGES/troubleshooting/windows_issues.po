# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-02 00:49+0000\n"
"PO-Revision-Date: 2023-01-09 09:40+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: ref scopesdirectx wiki DirectX menuselection clip\n"
"X-POFile-SpellExtra: windowsissues estalinhos kbd SHIFT WinMM Win Wasapi\n"
"X-POFile-SpellExtra: DirectSound CTRL MLT download conf bin kdenlive exe\n"
"X-POFile-SpellExtra: kdenliverc OpenGLES appdata misc Fusion old clips\n"
"X-POFile-SpellExtra: Appdata APPDATA JPG Out dbus kill batch XPS Document\n"
"X-POFile-SpellExtra: Writer Kdenlive\n"

#: ../../troubleshooting/windows_issues.rst:1
msgid "The Kdenlive User Manual"
msgstr "O Manual de Utilizador do Kdenlive"

#: ../../troubleshooting/windows_issues.rst:1
msgid ""
"KDE, Kdenlive, documentation, user manual, video editor, open source, free, "
"help, learn, Windows workaround, problem solving"
msgstr ""
"KDE, Kdenlive, documentação, manual do utilizador, editor de vídeo, código "
"aberto, livre, ajuda, truque em Windows, resolução de problemas"

#: ../../troubleshooting/windows_issues.rst:19
msgid "Windows issues"
msgstr "Problemas no Windows"

#: ../../troubleshooting/windows_issues.rst:21
msgid "Contents"
msgstr "Conteúdo"

#: ../../troubleshooting/windows_issues.rst:23
msgid ""
"The current **Kdenlive** on Windows® (April 2022, version 21.12.3) has a few "
"issues that have workarounds. The purpose of this wiki page is to document "
"these issues and their workarounds."
msgstr ""
"O **Kdenlive** actual no Windows® (Abril de 2022, versão 21.12.3) tem "
"algumas questões com truques de resolução. O propósito desta página da wiki "
"é documentar estes problemas e os seus truques de resolução."

#: ../../troubleshooting/windows_issues.rst:28
msgid "Title tool, display real background not working in \"DirectX\" backend"
msgstr ""
"Ferramenta do título a mostrar o fundo real não funciona na infra-estrutura "
"de \"DirectX\""

#: ../../troubleshooting/windows_issues.rst:30
msgid ""
"Something with the settings went wrong. Go to: :menuselection:`Help --> "
"Reset Configuration` and try again."
msgstr ""
"Algo de errado se passou na configuração. Vá a :menuselection:`Ajuda --> "
"Repor a Configuração` e tente de novo."

#: ../../troubleshooting/windows_issues.rst:34
msgid "Render problems"
msgstr "Problemas na geração do vídeo"

#: ../../troubleshooting/windows_issues.rst:36
msgid ""
"After rendering you get de-synced audio or wrong effects or black frames at "
"end of the last clip: download version 20.08.1 or higher from the `download "
"page <https://kdenlive.org/en/download>`_. If you still experience problems "
"see :ref:`windows_issues`."
msgstr ""
"Depois de gerar o vídeo, caso obtenha um áudio não sincronizado, com efeitos "
"com problemas ou imagens pretas no fim do último 'clip': transfira a versão "
"20.08.1 ou superior a partir da `página de transferências <https://kdenlive."
"org/en/download>`_. Se tiver à mesma problemas, veja em :ref:"
"`windows_issues`."

#: ../../troubleshooting/windows_issues.rst:39
msgid "Scopes doesn't show anything"
msgstr "Os medidores não mostram nada"

#: ../../troubleshooting/windows_issues.rst:41
msgid ":ref:`scopes_directx`"
msgstr ":ref:`scopes_directx`"

#: ../../troubleshooting/windows_issues.rst:45
msgid "Audio crackling while playback"
msgstr "Áudio com estalinhos durante a reprodução"

#: ../../troubleshooting/windows_issues.rst:46
msgid ""
":kbd:`CRL + SHIFT +,` (comma), select :menuselection:`Playback --> audio "
"backend` and play around with :menuselection:`WinMM` (Win7), :menuselection:"
"`Wasapi` (Win10), :menuselection:`DirectSound` to see what give the best "
"result. Restart Kdenlive after each switch."
msgstr ""
":kbd:`CTRL + SHIFT +,` (vírgula), seleccione :menuselection:`Reprodução --> "
"infra-estrutura de áudio` e experimente o  :menuselection:`WinMM` (Win7), :"
"menuselection:`Wasapi` (Win10), :menuselection:`DirectSound` para ver qual "
"gera os melhores resultados. Reinicie o Kdenlive ao fim de cada mudança."

#: ../../troubleshooting/windows_issues.rst:50
msgid ""
"This application failed to start because no Qt platform plugin could be "
"initialized"
msgstr ""
"Esta aplicação não conseguiu ser inicializada porque não foi inicializada um "
"dado 'plugin' de plataformas do Qt"

#: ../../troubleshooting/windows_issues.rst:52
#: ../../troubleshooting/windows_issues.rst:167
msgid ""
"Hit :kbd:`CTRL + SHIFT + ,` (comma) > :menuselection:`environment` > make "
"sure the paths point to the same path as \"MLT profiles folder\"."
msgstr ""
"Carregue em :kbd:`CTRL + SHIFT + ,` (vírgula) > :menuselection:`ambiente` > "
"certifique-se que os locais apontam para a \"pasta de perfis do MLT\"."

#: ../../troubleshooting/windows_issues.rst:54
msgid ""
"Download: :download:`qt.conf </files/qt.conf>`. Put the file :file:`qt.conf` "
"into the \"bin\" folder (the folder where :file:`kdenlive.exe` is)"
msgstr ""
"Transfira o :download:`qt.conf </files/qt.conf>`. Coloque o ficheiro :file:"
"`qt.conf` na pasta \"bin\" (a pasta onde se encontra o :file:`kdenlive.exe`)"

#: ../../troubleshooting/windows_issues.rst:58
msgid "First time use of Kdenlive"
msgstr "Primeira utilização do Kdenlive"

#: ../../troubleshooting/windows_issues.rst:60
msgid ""
"This issue should be solved with Windows version 19.04.2-6. That :file:"
"`kdenliverc` is correct set up please start Kdenlive twice (start -> close -"
"> start). Then start your work."
msgstr ""
"Este problema deverá ser resolvido com a versão para Windows 19.04.2-6. Aí "
"o :file:`kdenliverc` está correcto; por favor inicie o Kdenlive duas vezes "
"(iniciar -> fechar -> iniciar). Depois inicie o seu trabalho."

#: ../../troubleshooting/windows_issues.rst:64
msgid "Intel graphic card driver"
msgstr "Controlador de placas gráficas Intel"

#: ../../troubleshooting/windows_issues.rst:66
msgid "Updated Intel graphic driver versions lead to a corrupted Kdenlive GUI."
msgstr ""
"As versões actualizadas do controlador gráfico da Intel dão origem a uma GUI "
"do Kdenlive danificada."

#: ../../troubleshooting/windows_issues.rst:68
msgid ""
"**Solution 1:** Open Kdenlive. Move the mouse to the top. The menus are "
"showing up. Try to reach :menuselection:`Settings` -> :menuselection:`openGL "
"backend` -> enable :menuselection:`OpenGLES`/:menuselection:`DirectX`. "
"Restart Kdenlive. This should solve your Intel graphic driver issue."
msgstr ""
"**Solução 1:** Abra o Kdenlive. Mova o cursor para o topo. Os menus estão a "
"aparecer. Tente aceder a :menuselection:`Configuração` -> :menuselection:"
"`Infra-estrutura do OpenGL` -> active o :menuselection:`OpenGLES`/:"
"menuselection:`DirectX`. Reinicie o Kdenlive. Isso deverá resolver o seu "
"problema com a placa gráfica da Intel."

#: ../../troubleshooting/windows_issues.rst:72
msgid ""
"Maybe this statement helps (forum user \"Windows User\"): I would like to "
"confirm that this issue seems to be mostly fixed. When I use the latest "
"daily build of Kdenlive on Windows 10 with the latest Intel graphics "
"drivers, I still get see a corrupted GUI after opening Kdenlive. The only "
"way to resolve this is to choose Settings > OpenGL Backend > OpenGLES from "
"the menu. I can't see the menu when the GUI is corrupt but I can click where "
"the menu should be. A quick test of Kdenlive after doing this seems fixed."
msgstr ""
"Talvez esta frase ajude (utilizador do fórum \"Utilizador do Windows\"): "
"gostaria de confirmar que este problema parece estar maioritariamente "
"resolvido. Quando uso a última versão diária do Kdenlive no Windows 10 com "
"os últimos controladores gráficos da Intel, continuo a ter uma interface "
"corrompida após abrir o Kdenlive. A única forma de resolver isto é escolher "
"Configuração > Infra-Estrutura de OpenGL > OpenGLES no menu. Não consigo ver "
"o menu quando a GUI está corrompida, mas consigo carregar onde deverá "
"aparecer o menu. Um teste rápido do Kdenlive, depois de fazer isto, parece "
"deixar resolvida a questão."

#: ../../troubleshooting/windows_issues.rst:75
msgid ""
"**Solution 2:** Press :kbd:`Win + R` (:kbd:`Windows` key and :kbd:`R` key "
"simultaneously) and type **appdata**. Go to :file:`local` and within it "
"open :file:`kdenliverc` with an editor. Search for ``[misc]`` and delete "
"``[misc]`` and the following entry. . Restart Kdenlive."
msgstr ""
"**Solução 2:** Carregue em :kbd:`Win + R` (tecla :kbd:`Windows` e :kbd:`R em "
"simultâneo) e escreva **appdata**. Vá a :file:`local` e dentro dela abra o :"
"file:`kdenliverc` com um editor. Procure por ``[misc]`` e apague o "
"``[misc]`` e o seguinte item. Reinicie o Kdenlive."

#: ../../troubleshooting/windows_issues.rst:82
msgid ""
"Timeline: right-click menu close immediately after releasing mouse button"
msgstr ""
"Linha temporal: o menu do botão direito fecha imediatamente a seguir a "
"libertar o botão do rato"

#: ../../troubleshooting/windows_issues.rst:84
msgid "Don't use the style :menuselection:`Fusion`."
msgstr "Não use o estilo :menuselection:`Fusion`."

#: ../../troubleshooting/windows_issues.rst:87
msgid ""
"Go to: :menuselection:`Settings` -> :menuselection:`Style` and choose :"
"menuselection:`Default` or :menuselection:`Windows`."
msgstr ""
"Vá a: :menuselection:`Configuração` -> :menuselection:`Estilo` e escolha a :"
"menuselection:`Predefinição` ou :menuselection:`Windows`."

#: ../../troubleshooting/windows_issues.rst:92
msgid "Icons are missing"
msgstr "Faltam os ícones"

#: ../../troubleshooting/windows_issues.rst:94
msgid ""
"Go to: :menuselection:`settings` -> untick :menuselection:`force breeze icon "
"theme`. Kdenlive restarts and you should see the icons."
msgstr ""
"Vá a: :menuselection:`Configuração` -> desligue a opção :menuselection:"
"`Forçar o tema de ícones Brisa`. O Kdenlive é reiniciado e aí já deverá ver "
"os ícones."

#: ../../troubleshooting/windows_issues.rst:98
msgid ""
"Cannot open projects made with previous version, timeline snaps back, cannot "
"import clip"
msgstr ""
"Não é possível abrir os projectos criados com a versão anterior; a linha "
"temporal recua e não é possível importar o 'clip'"

#: ../../troubleshooting/windows_issues.rst:100
msgid "Go to: :menuselection:`Help --> Reset configuration`."
msgstr "Vá a: :menuselection:`Ajuda --> Repor a configuração`."

#: ../../troubleshooting/windows_issues.rst:103
msgid ""
"If this is not solving the problem: Press :kbd:`Win + R` (:kbd:`Windows` key "
"and :kbd:`R` key simultaneously) and type **appdata**. Go to :file:`local` "
"and within it rename :file:`kdenliverc` to :file:`kdenliverc.old`. Start "
"Kdenlive -> do nothing -> close Kdenlive -> and restart Kdenlive again."
msgstr ""
"Se isto não resolver o problema: Carregue em :kbd:`Win + R` (tecla :kbd:"
"`Windows` e :kbd:`R` em simultâneo) e escreva **appdata**. Vá a :file:"
"`local` e dentro dela mude o nome do ficheiro :file:`kdenliverc` para :file:"
"`kdenliverc.old`. Inicie o Kdenlive -> não faça nada -> feche o Kdenlive -> "
"e reinicie o Kdenlive de novo."

#: ../../troubleshooting/windows_issues.rst:106
msgid ""
"If you have still problems delete proxy clips and other cached data by going "
"to :menuselection:`Project` menu > :menuselection:`Project Setting` > :"
"menuselection:`Cache Data` tab > there you can delete cached data."
msgstr ""
"Se continuar a ter problemas, apague os 'clips' indirectos e outros dados em "
"'cache', indo ao menu :menuselection:`Projecto` > :menuselection:"
"`Configuração do Projecto` > :menuselection:`Dados em 'Cache'` > aí poderá "
"apagar os dados em 'cache'."

#: ../../troubleshooting/windows_issues.rst:109
msgid "If you have still problems try :ref:`windows_issues`."
msgstr "Se continuar à mesma com problemas, consulte o :ref:`windows_issues`."

#: ../../troubleshooting/windows_issues.rst:113
msgid "Windows 10: timeline stuttering or Kdenlive hangs."
msgstr "Windows 10: intermitências na linha temporal ou o Kdenlive bloqueia."

#: ../../troubleshooting/windows_issues.rst:115
msgid ""
"Most probably you got a major Win10 update (i.e 1809). If so you have to "
"update all drivers for audio and video."
msgstr ""
"Provavelmente fez uma grande actualização do Win10 (i.e 1809). Se for o "
"caso, tem de actualizar todos os controladores para o áudio e vídeo."

#: ../../troubleshooting/windows_issues.rst:117
msgid ""
"Intel driver can be updated with this updater: `Intel updater <https://"
"downloadcenter.intel.com/en/download/28425/Intel-Driver-Support-Assistant>`_."
msgstr ""
"O controlador da Intel pode ser actualizado com este utilitário: "
"`actualização da Intel <https://downloadcenter.intel.com/en/download/28425/"
"Intel-Driver-Support-Assistant>`_."

#: ../../troubleshooting/windows_issues.rst:121
msgid "\"Clip is invalid, will be removed\""
msgstr "\"O 'clip' é inválido, será removido\""

#: ../../troubleshooting/windows_issues.rst:123
msgid ""
"This bug can appear if you do a clean reinstall of **Kdenlive** (see above). "
"Simply close and open **Kdenlive** once, and it should be fixed."
msgstr ""
"Este erro poderá aparecer se fizer uma reinstalação limpa do **Kdenlive** "
"(ver acima). Basta fechar e abrir o **Kdenlive** uma vez, e deverá ficar "
"corrigido."

#: ../../troubleshooting/windows_issues.rst:126
msgid ""
"Additionally this can be a problem either with the :file:`kdenliverc` file "
"(see here :ref:`windows_issues`) or you have some mismatch in the \"local\" "
"folder (see here :ref:`windows_issues`)."
msgstr ""
"Para além disso, pode ser um problema com o ficheiro :file:`kdenliverc` "
"(veja em :ref:`windows_issues`) ou poderá ter uma inconsistência qualquer na "
"pasta \"local\" (veja em :ref:`windows_issues`)."

#: ../../troubleshooting/windows_issues.rst:130
msgid "Any critical bug"
msgstr "Qualquer erro crítico"

#: ../../troubleshooting/windows_issues.rst:132
msgid "This describes the process of doing a clean install on Windows®."
msgstr "Isto descreve o processo de efectuar uma instalação limpa no Windows®."

#: ../../troubleshooting/windows_issues.rst:135
msgid ""
"Firstly, delete your normal **Kdenlive** folder (containing the application)"
msgstr ""
"Em primeiro lugar, apague a sua pasta **Kdenlive** normal (que contém a "
"aplicação)"

#: ../../troubleshooting/windows_issues.rst:138
msgid ""
"Access the **Appdata** folder (:kbd:`Win + R` and then type **APPDATA** in "
"full caps). Go to :file:`local` and search for folder :file:`kdenlive`."
msgstr ""
"Aceda à pasta **Appdata** (:kbd:`Win + R` e depois escreva **APPDATA** em "
"maiúsculas). Vá a :file:`local` e procure pela pasta :file:`kdenlive`."

#: ../../troubleshooting/windows_issues.rst:143
msgid ""
"If you have any saved effects or clips stored in your library, make a backup "
"of the library folder."
msgstr ""
"Se tiver alguns efeitos ou 'clips' gravados na sua biblioteca, faça uma "
"cópia de segurança da pasta da biblioteca."

#: ../../troubleshooting/windows_issues.rst:146
msgid ""
"Then once you have backup up your library folder, delete the :file:"
"`kdenlive` folder."
msgstr ""
"Depois, assim que tiver salvaguardado a sua pasta da biblioteca, apague a "
"pasta :file:`kdenlive`."

#: ../../troubleshooting/windows_issues.rst:149
msgid ""
"Reinstall the latest version of **Kdenlive** from the `download page "
"<https://kdenlive.org/en/download>`_"
msgstr ""
"Reinstale a última versão do **Kdenlive** a partir da `página de "
"transferências <https://kdenlive.org/en/download>`_"

#: ../../troubleshooting/windows_issues.rst:153
msgid "JPG files appear as white picture after rendering"
msgstr "Os ficheiros JPG aparecem como imagens brancas após o desenho"

#: ../../troubleshooting/windows_issues.rst:155
msgid ""
"This issue should be solved with Windows version 19.04.0. If not convert the "
"JPG to PNG and it renders correctly."
msgstr ""
"Este problema deverá estar resolvido com o Windows na versão 19.04.0. Se "
"não, converta o JPG para PNG para ver se gera correctamente."

#: ../../troubleshooting/windows_issues.rst:159
msgid "Play/Pause Issue"
msgstr "Problemas com Reprodução/Pausa"

#: ../../troubleshooting/windows_issues.rst:161
msgid ""
"This issue is solved with Windows version 18.08.2 (30. Oct 2018). Get the "
"current version from the `download page <https://kdenlive.org/en/download>`_."
msgstr ""
"Este problema está resolvido com a versão para Windows 18.08.2 (30 de Out. "
"2018). Obtenha a versão actual na `página de transferências <https://"
"kdenlive.org/en/download>`_."

#: ../../troubleshooting/windows_issues.rst:165
msgid "Qt rendering crash"
msgstr "Estoiro no desenho em Qt"

#: ../../troubleshooting/windows_issues.rst:170
msgid ""
"When switching from kdenlive for windows 17.12 > 18.04/18.08, a Qt rendering "
"crash appears. To make sure this doesn't happen, you need to edit the :file:"
"`kdenliverc` file in the :file:`appdata/local` folder. To access your "
"appdata, press :kbd:`Win + R` (:kbd:`Windows` key and :kbd:`R` key "
"simultaneously) and type **appdata**. Go to :file:`local` and within it "
"rename :file:`kdenliverc` to :file:`kdenliverc.old`."
msgstr ""
"Ao mudar do Kdenlive para o Windows 17.12 > 18.04/18.08, aparece um estoiro "
"no motor de desenho do Qt. Para se certificar que isto não acontece, terá de "
"editar o ficheiro :file:`kdenliverc` na pasta :file:`appdata/local`. Para "
"aceder ao seu 'appdata', carregue em :kbd:`Win + R` (a tecla :kbd:`Windows` "
"e o :kbd:`R` em simultâneo) e escreva **appdata**. Vá a :file:`local` e, "
"dentro dele, mude o nome do ficheiro :file:`kdenliverc` para :file:"
"`kdenliverc.old`."

#: ../../troubleshooting/windows_issues.rst:174
msgid "Kdenlive cannot be deleted, running process on exit"
msgstr ""
"Não é possível apagar o Kdenlive devido a um processo em execução à saída"

#: ../../troubleshooting/windows_issues.rst:176
msgid ""
"This issue is solved with Windows version 18.12.1. Get the current version "
"from the `download page <https://kdenlive.org/en/download>`_."
msgstr ""
"Este problema está resolvido com a versão para Windows 18.12.1. Obtenha a "
"versão actual na `página de transferências <https://kdenlive.org/en/"
"download>`_."

#: ../../troubleshooting/windows_issues.rst:179
msgid ""
"If you want to reinstall **Kdenlive** or re-run **Kdenlive**, it may tell "
"you \"The file or folder is open in another program\". Windows® then won't "
"let you delete or re-run **Kdenlive**."
msgstr ""
"Se quiser reinstalar ou executar de novo o **Kdenlive**, poder-lhe-á indicar "
"uma mensagem do tipo \"O ficheiro ou pasta está aberto noutro programa\". O "
"Windows® então não o deixará apagar ou executar de novo o **Kdenlive**."

#: ../../troubleshooting/windows_issues.rst:182
msgid ""
"To fix this you have to kill the running process: press and hold :kbd:`Ctrl "
"+ Shift + Esc` &  expand the task manager by clicking :menuselection:`all "
"details`. Then find :file:`kdenlive.exe` &  :file:`dbus-daemon.exe`, and "
"click :menuselection:`End task` for both of them."
msgstr ""
"Para corrigir isto, terá de matar o processo em execução: carregue e "
"mantenha carregado o :kbd:`Ctrl + Shift + Esc` &  expanda o gestor de "
"tarefas ao carregar em :menuselection:`todos os detalhes`. Depois descubra "
"o :file:`kdenlive.exe` & :file:`dbus-daemon.exe`, e carregue em :"
"menuselection:`Terminar a tarefa` para ambos."

#: ../../troubleshooting/windows_issues.rst:185
msgid ""
"Or download the: :download:`Kdenlive-kill.zip </files/Kdenlive-kill.zip>`. "
"Unpack it and just double-click the batch file which kills all running "
"**Kdenlive** processes."
msgstr ""
"Ou transfira o programa :download:`Kdenlive-kill.zip </files/Kdenlive-kill."
"zip>`. Descomprima-o e depois faça duplo-click sobre o ficheiro 'batch' que "
"mata todos os processos do **Kdenlive** em execução."

#: ../../troubleshooting/windows_issues.rst:189
msgid "Kdenlive crash at start up, Kdenlive cannot be uninstalled"
msgstr "Estoiro do Kdenlive no arranque; não é possível desinstalar o Kdenlive"

#: ../../troubleshooting/windows_issues.rst:191
msgid ""
"If Kdenlive crash at startup or if the uninstaller doesn't work delete the "
"entire folder: :file:`C:/Program Files/kdenlive`."
msgstr ""
"Se o Kdenlive estoira no arranque ou se o desinstalador não funcionar, "
"apague a pasta por inteiro: :file:`C:/Programas/kdenlive`."

#: ../../troubleshooting/windows_issues.rst:193
msgid "Re-install Kdenlive"
msgstr "Reinstale o Kdenlive"

#: ../../troubleshooting/windows_issues.rst:195
msgid "You have to manually delete in the start menu the Kdenlive folder."
msgstr "Tem de apagar manualmente no menu de início a pasta do Kdenlive."

#: ../../troubleshooting/windows_issues.rst:199
msgid "Kdenlive crash or green Monitor"
msgstr "Estoiro do Kdenlive ou monitor verde"

#: ../../troubleshooting/windows_issues.rst:201
msgid ""
"Get all newest Windows® updates. Afterwards, update your graphic card driver "
"and your sound card driver and your printer driver. Some crashes could occur "
"of incompatibility of the graphics card and sound card with the newest "
"Windows®10 updates (18.09 update). After you have updated the drivers re-"
"start the computer and try again by starting :file:`kdenlive.exe`."
msgstr ""
"Instale todas as últimas actualizações do Windows®. Depois, actualize o "
"controlador da sua placa gráfica, da sua placa de som e da sua impressora. "
"Poderão ocorrer alguns estoiros por incompatibilidade com a placa gráfica e "
"a placa de som com as últimas actualizações do Windows®10 (versão 18.09). "
"Depois de ter actualizado os controladores, reinicie o computador e tente de "
"novo iniciar o :file:`kdenlive.exe`."

#: ../../troubleshooting/windows_issues.rst:206
msgid ""
"If this is not solving the problem switch your standard printer to "
"“Microsoft XPS Document Writer” and try again to start Kdenlive."
msgstr ""
"Se isto não resolver o problema, mude a sua impressora predefinida para a "
"“Microsoft XPS Document Writer” e tente de novo iniciar o Kdenlive."

#: ../../troubleshooting/windows_issues.rst:209
msgid ""
"Delete the :file:`kdenliverc` file as descript here under :ref:"
"`windows_issues`."
msgstr ""
"Apague o ficheiro :file:`kdenliverc`, como descrito aqui em :ref:"
"`windows_issues`."

#: ../../troubleshooting/windows_issues.rst:212
msgid ""
"Make sure you set processing thread to 1: :kbd:`Ctrl + Shift + ,` (comma) > :"
"menuselection:`Environment` > :menuselection:`Processing thread` > set to 1"
msgstr ""
"Certifique-se que configurou a tarefa de processamento como 1: :kbd:`Ctrl + "
"Shift + ,` (vírgula) > :menuselection:`Ambiente` > :menuselection:`Tarefa de "
"processamento` > com o valor 1"

#: ../../troubleshooting/windows_issues.rst:216
msgid "General Issues"
msgstr "Problemas Gerais"

#: ../../troubleshooting/windows_issues.rst:218
msgid ""
"The current **Kdenlive** version (November 2018, version 18.08.3) has a few "
"issues that have workarounds."
msgstr ""
"A versão actual do **Kdenlive** (Novembro de 2018, versão 18.08.3) tem "
"alguns problemas que têm truques de resolução."

#: ../../troubleshooting/windows_issues.rst:222
msgid "Audio Pops and Ticks in Render"
msgstr "Ruídos e Picos de Áudio na Geração do Vídeo"

#: ../../troubleshooting/windows_issues.rst:224
msgid "If this problem appears make sure the audio file is: 16-bit PCM WAV."
msgstr ""
"Se este problema aparecer, certifique-se que o ficheiro de áudio está no "
"formato WAV em PCM a 16-bits."

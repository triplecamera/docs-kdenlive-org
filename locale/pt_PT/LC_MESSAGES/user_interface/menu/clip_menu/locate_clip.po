# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-21 00:37+0000\n"
"PO-Revision-Date: 2022-05-19 09:07+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: Clip ref projecttree clip clips\n"

#: ../../user_interface/menu/clip_menu/locate_clip.rst:13
msgid "Clip Menu — Locate Clip"
msgstr "Menu do 'Clip' — Localizar o 'Clip'"

#: ../../user_interface/menu/clip_menu/locate_clip.rst:15
msgid "Contents"
msgstr "Conteúdo"

#: ../../user_interface/menu/clip_menu/locate_clip.rst:21
msgid ""
"This menu item is available from :ref:`project_tree` a clip in the Project "
"Bin. Locate Clip opens up the systems file browser at the location on the "
"file system where the selected clip is stored. Useful for tracking down the "
"sources of clips in the project bin."
msgstr ""
"Este item está disponível na :ref:`project_tree` de um 'clip' no Grupo do "
"Projecto. Esta opção abre o navegador do sistema de ficheiros na localização "
"onde está guardado o ficheiro do 'clip' seleccionado. É útil para descobrir "
"as origens dos 'clips' no grupo do projecto."

# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-27 00:37+0000\n"
"PO-Revision-Date: 2022-05-19 09:11+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: clips ref editing timelineeditmodes timelineedittools\n"

#: ../../user_interface/menu/tool_menu.rst:13
msgid "Tool Menu"
msgstr "Menu de Ferramentas"

#: ../../user_interface/menu/tool_menu.rst:15
msgid "Contents"
msgstr "Conteúdo"

#: ../../user_interface/menu/tool_menu.rst:22
msgid ""
"The options on this menu provide three modes and six tools which affect how "
"operations are performed on clips in the timeline. These same options can "
"also be accessed from :ref:`timeline_edit_modes` and :ref:"
"`timeline_edit_tools`. More details on their usage can be found there."
msgstr ""
"As opções neste menu oferecem três modos e três ferramentas que afectam a "
"forma como as operações são efectuadas sobre os 'clips' na linha temporal. "
"Essas mesmas opções também poderão ser acedidas a partir da :ref:"
"`timeline_edit_modes` e :ref:`timeline_edit_tools`. Poderá encontrar mais "
"detalhes sobre a sua utilização aí."

# Translation of docs_kdenlive_org_glossary___useful_resources.po to Catalan
# Copyright (C) 2021 This_file_is_part_of_KDE
# Licensed under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: documentation-docs-kdenlive-org\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-13 00:22+0000\n"
"PO-Revision-Date: 2021-11-22 18:28+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../glossary/useful_resources.rst:17
msgid "Useful Resources"
msgstr "Recursos útils"

#: ../../glossary/useful_resources.rst:20
msgid "Contents"
msgstr "Contingut"

# skip-rule: t-acc_obe
#: ../../glossary/useful_resources.rst:22
msgid ""
"Another Kdenlive manual: `flossmanuals <http://www.flossmanuals.net/how-to-"
"use-video-editing-software/>`_"
msgstr ""
"Un altre manual del Kdenlive: `flossmanuals <http://www.flossmanuals.net/how-"
"to-use-video-editing-software/>`_"

# skip-rule: t-acc_obe
#: ../../glossary/useful_resources.rst:23
msgid ""
"`Cutting and Splicing Video in KDEnlive <http://www.linuceum.com/Desktop/"
"KDEnliveVideo.php>`_  by Linuceum"
msgstr ""
"`Retallant i empalmant vídeos en el Kdenlive <http://www.linuceum.com/"
"Desktop/KDEnliveVideo.php>`_  by Linuceum"

# skip-rule: t-acc_obe
#: ../../glossary/useful_resources.rst:24
msgid ""
"`opensource.com tutorial <http://opensource.com/life/11/11/introduction-"
"kdenlive>`_"
msgstr ""
"`Guia d'aprenentatge a opensource.com <http://opensource.com/life/11/11/"
"introduction-kdenlive>`_"

# skip-rule: t-acc_obe
#: ../../glossary/useful_resources.rst:25
msgid "`Kdenlive Forum <https://forum.kde.org/viewforum.php?f=262>`_"
msgstr "`Fòrum de Kdenlive <https://forum.kde.org/viewforum.php?f=262>`_"

# skip-rule: t-acc_obe
#: ../../glossary/useful_resources.rst:26
msgid ""
"Kdenlive `Developer Wiki <https://community.kde.org/Kdenlive/Development>`_"
msgstr ""
"`Wiki del desenvolupador <https://community.kde.org/Kdenlive/Development>`_ "
"del Kdenlive"

#: ../../glossary/useful_resources.rst:29
msgid "Keyboard Stickers - courtesy of Weevil"
msgstr "Adhesius de teclat - cortesia de Weevil"

#: ../../glossary/useful_resources.rst:32
msgid "|Kdenlive_Keyboard_074|"
msgstr "|Kdenlive_Keyboard_074|"

#: ../../glossary/useful_resources.rst:35
msgid "|Kdenlive_Keyboard_074_A4|"
msgstr "|Kdenlive_Keyboard_074_A4|"

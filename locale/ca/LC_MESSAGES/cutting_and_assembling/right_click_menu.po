# Translation of docs_kdenlive_org_cutting_and_assembling___right_click_menu.po to Catalan
# Copyright (C) 2021-2022 This_file_is_part_of_KDE
# Licensed under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2021, 2022.
# Josep M. Ferrer <txemaq@gmail.com>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: documentation-docs-kdenlive-org\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-12 00:40+0000\n"
"PO-Revision-Date: 2022-12-12 16:07+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../cutting_and_assembling/right_click_menu.rst:17
msgid "Right-Click Menus"
msgstr "Menú del clic dret"

#: ../../cutting_and_assembling/right_click_menu.rst:19
msgid "Contents"
msgstr "Contingut"

#: ../../cutting_and_assembling/right_click_menu.rst:22
msgid "Clip in Timeline"
msgstr "Clip a la línia de temps"

#: ../../cutting_and_assembling/right_click_menu.rst:25
msgid ""
"This is the context menu that appears when you right-click on a clip in the "
"timeline.  A different menu appears if you click in empty space in the "
"timeline."
msgstr ""
"Aquest és el menú contextual que apareixerà quan feu clic dret sobre un clip "
"a la línia de temps. Apareixerà un menú diferent si feu clic en un espai "
"buit de la línia de temps."

#: ../../cutting_and_assembling/right_click_menu.rst:32
msgid ""
":menuselection:`Copy` will copy the clip and selected clips to the clipboard."
msgstr ""
":menuselection:`Copia`: copiarà el clip i els clips seleccionats al porta-"
"retalls."

#: ../../cutting_and_assembling/right_click_menu.rst:35
msgid ""
":menuselection:`Paste Effects` will paste only the effects of the last "
"copied clip to the selected clip.  See :ref:`paste_effects`."
msgstr ""
":menuselection:`Enganxa els efectes`: només enganxarà els efectes de l'últim "
"clip copiat en el clip seleccionat. Vegeu :ref:`paste_effects`."

#: ../../cutting_and_assembling/right_click_menu.rst:38
msgid ""
":menuselection:`Delete Effects` will remove all effects from the selected "
"clip."
msgstr ""
":menuselection:`Suprimeix els efectes`: eliminarà tots els efectes del clip "
"seleccionat."

#: ../../cutting_and_assembling/right_click_menu.rst:41
msgid ":menuselection:`Group Clips` - see :ref:`grouping`"
msgstr ":menuselection:`Agrupa els clips`: vegeu :ref:`grouping`"

#: ../../cutting_and_assembling/right_click_menu.rst:44
msgid ":menuselection:`Ungroup Clips` - see :ref:`grouping`"
msgstr ":menuselection:`Desagrupa els clips`: vegeu :ref:`grouping`"

#: ../../cutting_and_assembling/right_click_menu.rst:47
msgid ""
":menuselection:`Edit Duration` - will open the Duration Dialog and will "
"allow you to adjust the position of the clip in the timeline, what time "
"point of the source clip to start on the timeline, the duration of the clip, "
"and what time point of the source clip to end on the timeline. Note that "
"Kdenlive will automatically adjust co-related values."
msgstr ""
":menuselection:`Edita la durada...`: obrirà el diàleg Durada i permetrà "
"ajustar la posició del clip a la línia de temps, en quin punt de temps del "
"clip font començarà a la línia de temps, la durada del clip i en quin punt "
"de temps del clip font finalitzarà a la línia de temps. Cal tenir en compte "
"que el **Kdenlive** ajustarà automàticament els valors correlacionats."

#: ../../cutting_and_assembling/right_click_menu.rst:54
msgid ""
":menuselection:`Restore audio` will add any audio track that is part of the "
"original clip to the timeline"
msgstr ""
":menuselection:`Restaura l'àudio`: afegirà a la línia de temps qualsevol "
"pista d'àudio que formi part del clip original."

#: ../../cutting_and_assembling/right_click_menu.rst:61
msgid ""
":menuselection:`Disable clip` will disable the clip so it will not render in "
"the project monitor or in a final video render. To disable the video or "
"audio part of an A/V clip you have to un-group the A/V clip, disable the "
"video or audio part and group the A/V clip again."
msgstr ""
":menuselection:`Desactiva el clip`: desactivarà el clip perquè no es "
"renderitzi en el monitor del projecte o en el renderitzador final de vídeo. "
"Per a desactivar la part de vídeo o àudio d'un clip A/V, heu de desagrupar "
"el clip A/V, desactivar la part de vídeo o àudio i tornar a agrupar el clip "
"A/V."

#: ../../cutting_and_assembling/right_click_menu.rst:65
msgid ""
":menuselection:`Delete Selected Item` will delete the item you have "
"selected. Add to the selection: Holding down :kbd:`Shift` and click on "
"additional items. :menuselection:`Timeline --> Current track --> Remove All "
"Clips After Cursor` handles AV clips as 1 element, doesn't matter on which "
"track they are. This function is only in the Timeline menu available this to "
"avoid clutter."
msgstr ""
":menuselection:`Suprimeix l'element seleccionat` suprimirà l'element que heu "
"seleccionat. Afegir a la selecció: manteniu premuda la tecla :kbd:`Maj` i "
"feu clic en els elements addicionals. :menuselection:`Línia de temps --> "
"Pista actual --> Elimina tots els clips després del cursor` gestiona els "
"clips AV com a 1 element, no importa en quina pista siguin. Aquesta funció "
"només està disponible al menú de la línia de temps per a evitar el desordre."

#: ../../cutting_and_assembling/right_click_menu.rst:67
msgid ""
":menuselection:`Extract clip` will remove the clip from the timeline and the "
"space it occupied."
msgstr ""
":menuselection:`Extreu el clip`: eliminarà el clip de la línia de temps i "
"l'espai que ocupava."

#: ../../cutting_and_assembling/right_click_menu.rst:74
msgid ""
":menuselection:`Save timeline zone to bin` will take the selected timeline "
"zone and add markers to your clips in the project bin."
msgstr ""
":menuselection:`Desa la zona de la línia de temps a la safata`: prendrà la "
"zona de la línia de temps seleccionada i afegirà marcadors als vostres clips "
"en la safata del projecte."

#: ../../cutting_and_assembling/right_click_menu.rst:80
msgid ""
"The markers sub-menu allows you to add, edit and remove markers from your "
"clips that are displayed on the timeline.  These markers will move with the "
"clips.  See :ref:`markers`."
msgstr ""
"El submenú de marcadors permet afegir, editar i eliminar els marcadors dels "
"vostres clips, aquests es mostren a la línia de temps. Aquests marcadors es "
"mouran amb els clips. Vegeu :ref:`markers`."

#: ../../cutting_and_assembling/right_click_menu.rst:83
msgid ""
":menuselection:`Set Audio Reference` and :menuselection:`Align Audio to "
"Reference` are used to align two clips on different tracks in the timeline "
"base on the audio in the tracks. This is useful if two cameras recorded the "
"same scene simultaneously. **Kdenlive** can use the almost identical audio "
"track to align the two clips."
msgstr ""
":menuselection:`Estableix la referència d'àudio` i :menuselection:`Alinea "
"l'àudio amb la referència` s'utilitzen per a alinear dos clips en pistes "
"diferents en la base de la línia de temps sobre l'àudio en les pistes. Això "
"és útil si es va enregistrar simultàniament la mateixa escena amb dues "
"càmeres. El **Kdenlive** pot utilitzar la pista d'àudio gairebé idèntica per "
"a alinear els dos clips."

#: ../../cutting_and_assembling/right_click_menu.rst:86
msgid "To use this feature:"
msgstr "Per a emprar aquesta característica:"

#: ../../cutting_and_assembling/right_click_menu.rst:89
msgid "Select the clip that you would like to align *to*."
msgstr "Seleccioneu el clip *amb el qual* voleu alinear."

#: ../../cutting_and_assembling/right_click_menu.rst:92
msgid "Right click, select :menuselection:`Set Audio Reference`."
msgstr ""
"Feu-hi clic dret i seleccioneu :menuselection:`Estableix la referència de "
"l'àudio`."

#: ../../cutting_and_assembling/right_click_menu.rst:95
msgid "Select all the clips that you would like to get aligned."
msgstr "Seleccioneu tots els clips que voleu alinear."

#: ../../cutting_and_assembling/right_click_menu.rst:98
msgid "Right-click and select :menuselection:`Align Audio to Reference`."
msgstr ""
"Feu clic dret i seleccioneu :menuselection:`Alinea l'àudio amb la "
"referència`."

#: ../../cutting_and_assembling/right_click_menu.rst:103
msgid "Change speed"
msgstr "Canvia la velocitat"

#: ../../cutting_and_assembling/right_click_menu.rst:105
msgid ""
":menuselection:`Change speed` will open the change speed dialog that will "
"allow you to increase or decrease the playback speed of a clip, allow you to "
"play the clip in reverse, and will enable / disable pitch compensation for "
"the audio on a speed-adjusted clip."
msgstr ""
":menuselection:`Canvia la velocitat`: obrirà un diàleg que permetrà fer "
"augmentar o disminuir la velocitat de reproducció d'un clip, reproduir-lo al "
"revés i activar/desactivar la compensació de to de l'àudio en un clip "
"ajustat a la velocitat."

#: ../../cutting_and_assembling/right_click_menu.rst:111
msgid ""
"Doing speed change of a clip with the mouse see: :ref:"
"`change_speed_of_a_clip`"
msgstr ""
"Canvia la velocitat d'un clip amb el ratolí, vegeu: :ref:"
"`change_speed_of_a_clip`"

#: ../../cutting_and_assembling/right_click_menu.rst:114
msgid ""
":menuselection:`Clip in project bin` will highlight the selected clip in the "
"project bin."
msgstr ""
":menuselection:`Clip a la safata del projecte`: ressaltarà el clip "
"seleccionat en la safata del projecte."

#: ../../cutting_and_assembling/right_click_menu.rst:117
msgid ""
":menuselection:`Cut Clip` Selecting this will cause the selected clip to be "
"cut at the location of the :ref:`timeline`. See also  :ref:`editing`."
msgstr ""
":menuselection:`Retalla el clip`: si la seleccioneu, el clip seleccionat es "
"retallarà a la ubicació de la :ref:`timeline`. Vegeu també :ref:`editing`."

#: ../../cutting_and_assembling/right_click_menu.rst:120
msgid ""
":menuselection:`Insert Effect` will open a sub-menu to allow you to quickly "
"add the :ref:`transform` or the :ref:`lift_gamma_gain_effect` effects."
msgstr ""
":menuselection:`Insereix un efecte`: obrirà un submenú que permet afegir amb "
"rapidesa els efectes :ref:`transform` o :ref:`lift_gamma_gain_effect`."

# skip-rule: kct-wip
#: ../../cutting_and_assembling/right_click_menu.rst:123
#: ../../cutting_and_assembling/right_click_menu.rst:159
msgid ""
":menuselection:`Insert composition` will open a sub-menu to allow you to "
"quickly add the :ref:`composite_and_transform` or the :ref:`wipe` composition"
msgstr ""
":menuselection:`Insereix una composició`: obrirà un submenú que permet "
"afegir amb rapidesa la composició :ref:`composite_and_transform` o :ref:"
"`wipe`."

#: ../../cutting_and_assembling/right_click_menu.rst:127
msgid "Empty Space in Timeline"
msgstr "Espai buit a la línia de temps"

#: ../../cutting_and_assembling/right_click_menu.rst:131
msgid "A different menu appears if you click in empty space in the timeline."
msgstr ""
"Apareixerà un menú diferent si feu clic en un espai buit de la línia de "
"temps."

#: ../../cutting_and_assembling/right_click_menu.rst:138
msgid ""
":menuselection:`Paste` will paste a clip from the clipboard into the timeline"
msgstr ""
":menuselection:`Enganxa`: enganxarà un clip des del porta-retalls dins de la "
"línia de temps."

#: ../../cutting_and_assembling/right_click_menu.rst:141
msgid ""
":menuselection:`Insert Space` will open the Insert Space dialog and will "
"allow you to insert blank space in the timeline in a single track."
msgstr ""
":menuselection:`Insereix un espai` obrirà el diàleg Inserció d'un espai i "
"permetrà inserir un espai en blanc a la línia de temps en una sola pista."

#: ../../cutting_and_assembling/right_click_menu.rst:144
msgid ""
":menuselection:`Remove Space` will remove all space between clips on the "
"track."
msgstr ""
":menuselection:`Elimina l'espai`: eliminarà tots els espais entre els clips "
"a la pista."

#: ../../cutting_and_assembling/right_click_menu.rst:147
msgid ""
":menuselection:`Remove Space in All Tracks` will remove space between clips "
"on all the tracks."
msgstr ""
":menuselection:`Elimina l'espai en totes les pistes`: eliminarà l'espai "
"entre els clips a totes les pistes."

#: ../../cutting_and_assembling/right_click_menu.rst:150
msgid ":menuselection:`Add/Remove Guide` will add a guide to the timeline."
msgstr ""
":menuselection:`Afegeix/Elimina la guia`: afegirà o eliminarà una guia a/de "
"la línia de temps."

#: ../../cutting_and_assembling/right_click_menu.rst:153
msgid ":menuselection:`Edit Guide` will allow you to edit the guide label."
msgstr ":menuselection:`Edita la guia`: permet editar l'etiqueta de la guia."

#: ../../cutting_and_assembling/right_click_menu.rst:156
msgid ""
":menuselection:`Go to Guide` will pop-up a sub-menu with a list of your "
"guides and will move the timeline position marker to that guide."
msgstr ""
":menuselection:`Ves a la guia...` obrirà un submenú amb una llista de les "
"vostres guies i mourà el marcador de posició en la línia de temps a aquesta "
"guia."

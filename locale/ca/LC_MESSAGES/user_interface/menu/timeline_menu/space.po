# Translation of docs_kdenlive_org_user_interface___menu___timeline_menu___space.po to Catalan
# Copyright (C) 2021-2022 This_file_is_part_of_KDE
# Licensed under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: documentation-docs-kdenlive-org\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-12 00:40+0000\n"
"PO-Revision-Date: 2022-12-12 13:10+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../user_interface/menu/timeline_menu/space.rst:1
msgid "space in timeline, Editing in Kdenlive video editor"
msgstr "Espai en la línia de temps, edició en l'editor de vídeo Kdenlive"

#: ../../user_interface/menu/timeline_menu/space.rst:1
msgid ""
"KDE, Kdenlive, insert space, delete space, editing, timeline, documentation, "
"user manual, video editor, open source, free, learn, easy"
msgstr ""
"KDE, Kdenlive, inserir espai, suprimir espai, edició, línia de temps, "
"documentació, manual d'usuari, editor de vídeo, codi lliure, lliure, "
"aprendre, fàcil"

#: ../../user_interface/menu/timeline_menu/space.rst:15
msgid "Timeline>Space"
msgstr "Línia de temps>Espai"

#: ../../user_interface/menu/timeline_menu/space.rst:17
msgid "Contents"
msgstr "Contingut"

#: ../../user_interface/menu/timeline_menu/space.rst:22
msgid "Insert Space"
msgstr "Insereix un espai"

#: ../../user_interface/menu/timeline_menu/space.rst:24
msgid ""
":menuselection:`Space` brings up a submenu with :menuselection:`Insert "
"Space` and :menuselection:`Remove Space` options.  Inserting a space is a "
"useful feature when you want to push all the existing clips on the timeline "
"aside to make room for new clips but also want to preserve the relationships "
"among all the clips that were shifted, including their transitions."
msgstr ""
":menuselection:`Espai` obre un submenú amb les opcions :menuselection:"
"`Insereix un espai` i :menuselection:`Elimina l'espai`. Inserir un espai és "
"una característica útil si voleu empènyer tots els clips existents a la "
"línia de temps a una banda per a fer lloc per a clips nous, però voleu "
"mantenir les relacions entre tots els clips que s'han desplaçat, incloent-hi "
"les seves transicions."

#: ../../user_interface/menu/timeline_menu/space.rst:31
msgid "Figure 1"
msgstr "Figura 1"

#: ../../user_interface/menu/timeline_menu/space.rst:33
msgid ""
"In addition to invoking this menu from :menuselection:`Timeline --> Space`, "
"you can also bring it up by right-clicking on an empty spot on a track in "
"the timeline.  There are a couple of important differences with how :"
"menuselection:`Insert Space` behaves, though, depending on which method you "
"choose.  First, using :menuselection:`Timeline --> Space --> Insert Space` "
"brings up the **Add Space** dialog shown in Figure 1 where the default "
"choice for **Track:** is *All tracks*."
msgstr ""
"Addicionalment a invocar aquest menú des de :menuselection:`Línia de temps --"
"> Espai`, també el podeu obrir amb un clic dret en un punt buit d'una pista "
"a la Línia de temps. Hi ha un parell de diferències importants amb com es "
"comporta :menuselection:`Insereix un espai`, bé que depenent del mètode que "
"heu triat. Primer, usant :menuselection:`Línia de temps --> Espai --> "
"Insereix un espai` obre el diàleg **Afegeix un espai** que es mostra a la "
"figura 1 a on la tria predeterminada per a **Pista:** és *Totes les pistes*."

#: ../../user_interface/menu/timeline_menu/space.rst:41
msgid "Figure 2"
msgstr "Figura 2"

#: ../../user_interface/menu/timeline_menu/space.rst:43
msgid ""
"When you access the menu by right-clicking on a track, the same dialog "
"appears but the default is the track you clicked on.  In either case, you "
"can obviously override the default by picking another option (Figure 2).  "
"The other difference is that the insertion happens at the playhead when the "
"operation originates from the Timeline menu; it takes place at the mouse "
"cursor position when right-clicking."
msgstr ""
"Quan accediu al menú fent clic dret sobre una pista, apareix el mateix "
"diàleg però el valor predeterminat és la pista on heu fet clic. En qualsevol "
"cas, òbviament es pot substituir el valor predeterminat seleccionant una "
"altra opció (figura 2). L'altra diferència és que la inserció es produeix al "
"capçal de reproducció quan l'operació s'origina des del menú Línia de temps; "
"té lloc a la posició del cursor del  ratolí en fer clic dret."

#: ../../user_interface/menu/timeline_menu/space.rst:47
msgid ""
"And in case you're wondering, the default **Duration** for the inserted "
"space is 65 frames, which is not configurable in the Kdenlive or Project "
"settings.  65 frames will equate to different amounts of time depending on "
"the frame rate set in your project profile.  In the example, our project's "
"frame rate happens to be 30 fps and that's why the default **Duration** "
"works out to be 00:00:02.05."
msgstr ""
"I en el cas que us pregunteu, la **Durada** predeterminada de l'espai "
"inserit és de 65 fotogrames, que no és configurable en la configuració del "
"Kdenlive o del projecte. 65 fotogrames equivaldran a diferents quantitats de "
"temps depenent de la velocitat dels fotogrames establerta al perfil del "
"projecte. En l'exemple, la velocitat dels fotogrames del projecte és de 30 "
"fps i és per això que la **Durada** predeterminada és de 00:00:02,05."

#: ../../user_interface/menu/timeline_menu/space.rst:49
msgid ""
"Let's look at an example, albeit an unrealistic one, of how inserting a "
"space from the playhead position will affect clips and transitions on "
"different tracks.  In general, inserting a space will shift any clips that "
"the playhead is touching as well as all clips to the right of the playhead "
"on the affected track(s).  Transitions are a little trickier because they "
"span two tracks.  They are assumed to “belong” to the higher track "
"(regardless of the direction of the transition) and so if the playhead is "
"touching them, they will go if the higher track is included in the shift."
msgstr ""
"Mirem un exemple, encara que poc realista, de com la inserció d'un espai des "
"de la posició del capçal de reproducció afectarà clips i transicions en "
"pistes diferents. En general, la inserció d'un espai desplaçarà qualsevol "
"clip que el capçal de reproducció estigui tocant així com tots els clips a "
"la dreta del capçal de reproducció a les pistes afectades. Les transicions "
"són una mica més difícils perquè abasten dues pistes. S'assumeix que "
"«pertanyen» a la pista superior (independentment de la direcció de la "
"transició) i, per tant, si el capçal de reproducció els toca, aniran si la "
"pista superior s'inclou en el desplaçament."

#: ../../user_interface/menu/timeline_menu/space.rst:55
msgid "Figure 3"
msgstr "Figura 3"

#: ../../user_interface/menu/timeline_menu/space.rst:57
msgid ""
"Figure 3 shows a scenario with clips on three video tracks before inserting "
"a space.  All of the following examples assume we're starting from this "
"position."
msgstr ""
"La figura 3 mostra un escenari amb clips en tres pistes de vídeo abans "
"d'inserir un espai. Tots els exemples següents assumeixen que partim "
"d'aquesta posició."

#: ../../user_interface/menu/timeline_menu/space.rst:65
msgid "Figure 4"
msgstr "Figura 4"

#: ../../user_interface/menu/timeline_menu/space.rst:67
msgid ""
"In Figure 4, we have chosen to insert a space on *All Tracks*.  Since the "
"playhead was touching both transitions and the color clips on tracks 2 and "
"3, it shifted them, along with all the clips to the right of the playhead.  "
"It did not shift the black clip on track 1 because it was to the left of the "
"playhead."
msgstr ""
"A la figura 4, hem triat inserir un espai a *Totes les pistes*. Com que el "
"capçal de reproducció tocava les dues transicions i els clips de color a les "
"pistes 2 i 3, els ha desplaçat, juntament amb tots els clips a la dreta del "
"capçal de reproducció. No ha canviat el clip negre a la pista 1 perquè "
"estava a l'esquerra del capçal de reproducció."

#: ../../user_interface/menu/timeline_menu/space.rst:75
msgid "Figure 5"
msgstr "Figura 5"

#: ../../user_interface/menu/timeline_menu/space.rst:77
msgid ""
"In Figure 5, we chose Track 1.  The transition went because it “belongs” to "
"the black clip on track 1, but the clip itself did not go because it was to "
"the left of the playhead.  The red clip on track 1 also went because it was "
"to the right of the playhead."
msgstr ""
"A la figura 5, escollim la pista 1. La transició s'ha desplaçat perquè "
"«pertany» al clip negre de la pista 1, però el clip en si no s'ha desplaçat "
"perquè era a l'esquerra del capçal de reproducció. El clip vermell de la "
"pista 1 també s'ha desplaçat perquè estava a la dreta del capçal de "
"reproducció."

#: ../../user_interface/menu/timeline_menu/space.rst:85
msgid "Figure 6"
msgstr "Figura 6"

#: ../../user_interface/menu/timeline_menu/space.rst:87
msgid ""
"In Figure 6, we shifted the clips on Track 2.  Now the Dissolve transition "
"between tracks 1 and 2 does not shift, even though the playhead was touching "
"it, because it belongs to track 1.  The other transition does go, because it "
"belongs to the higher track 2."
msgstr ""
"A la figura 6, hem desplaçat els clips a la pista 2. Ara la transició "
"Dissoldre entre les pistes 1 i 2 no canvia, tot i que el capçal de "
"reproducció l'estava tocant, perquè pertany a la pista 1. L'altra transició "
"s'ha desplaçat, perquè pertany a la pista 2 més alta."

#: ../../user_interface/menu/timeline_menu/space.rst:95
msgid "Figure 7"
msgstr "Figura 7"

#: ../../user_interface/menu/timeline_menu/space.rst:97
msgid "Finally, In Figure 7, we chose track 3 and just the clips move."
msgstr ""
"Finalment, a la figura 7, hem triat la pista 3 i només els clips s'han mogut."

#: ../../user_interface/menu/timeline_menu/space.rst:101
msgid ""
"If we had started this process by right-clicking on a spot on track 1 or "
"track 3 which corresponds to the playhead position in the example, the "
"results would have been the same."
msgstr ""
"Si haguéssim iniciat aquest procés fent clic dret en un punt de la pista 1 o "
"la pista 3 que correspon a la posició del capçal de reproducció de "
"l'exemple, el resultat hauria estat el mateix."

#: ../../user_interface/menu/timeline_menu/space.rst:107
msgid "Remove Space"
msgstr "Elimina l'espai"

#: ../../user_interface/menu/timeline_menu/space.rst:109
msgid ""
":menuselection:`Remove Space` is not the exact opposite of :menuselection:"
"`Insert Space`."
msgstr ""
":menuselection:`Elimina l'espai` no és exactament el contrari que :"
"menuselection:`Insereix un espai`."

#: ../../user_interface/menu/timeline_menu/space.rst:111
msgid "The similarities are:"
msgstr "Les similituds són:"

#: ../../user_interface/menu/timeline_menu/space.rst:113
msgid ""
"If you access :menuselection:`Remove Space` from the :menuselection:"
"`Timeline` menu, the playhead governs where the removal will happen.  When "
"using right-click, it happens at the mouse cursor."
msgstr ""
"Si accediu a :menuselection:`Elimina l'espai` del menú :menuselection:`Línia "
"de temps`, el capçal de reproducció governa on es produirà l'eliminació. "
"Quan s'utilitza el clic dret, es produeix al cursor del ratolí."

#: ../../user_interface/menu/timeline_menu/space.rst:114
msgid "Transitions will move with clips on the higher track."
msgstr "Les transicions es mouran amb els clips a la pista superior."

#: ../../user_interface/menu/timeline_menu/space.rst:116
msgid "There are the following differences:"
msgstr "Hi ha les diferències següents:"

#: ../../user_interface/menu/timeline_menu/space.rst:118
msgid "It only works on one track at a time."
msgstr "Només funciona en una pista a la vegada."

#: ../../user_interface/menu/timeline_menu/space.rst:119
msgid ""
"If you accessed :menuselection:`Remove Space` from the :menuselection:"
"`Timeline` menu, the playhead must be on an empty space in the track where "
"the space is to be removed."
msgstr ""
"Si accediu a :menuselection:`Elimina l'espai` des del menú :menuselection:"
"`Línia de temps`, el capçal de reproducció cal que estigui en un espai buit "
"de la pista on cal eliminar l'espai."

#: ../../user_interface/menu/timeline_menu/space.rst:120
msgid ""
"You can not set the duration of the space to be removed – all the empty "
"space between clips is removed.  All the clips and transitions to the right "
"of the playhead or mouse cursor will be shifted left until the first clip "
"encounters another clip or the beginning of the track."
msgstr ""
"No podeu establir la durada de l'espai que s'ha de suprimir, s'elimina tot "
"l'espai buit entre els clips. Tots els clips i transicions a la dreta del "
"capçal de reproducció o del cursor del ratolí es desplaçaran cap a "
"l'esquerra fins que el primer clip trobi un altre clip o el començament de "
"la pista."

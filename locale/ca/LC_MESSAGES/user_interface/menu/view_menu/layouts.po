# Translation of docs_kdenlive_org_user_interface___menu___view_menu___layouts.po to Catalan
# Copyright (C) 2021 This_file_is_part_of_KDE
# Licensed under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: documentation-docs-kdenlive-org\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-06 00:38+0000\n"
"PO-Revision-Date: 2021-12-06 12:08+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../user_interface/menu/view_menu/layouts.rst:11
msgid "Layouts"
msgstr "Disposicions"

#: ../../user_interface/menu/view_menu/layouts.rst:13
msgid "Contents"
msgstr "Contingut"

#: ../../user_interface/menu/view_menu/layouts.rst:16
msgid "Load Layout"
msgstr "Carrega la disposició"

#: ../../user_interface/menu/view_menu/layouts.rst:22
msgid ""
"Lets you switch to a previously saved custom layout.  Once you load a saved "
"layout, that layout will remain the current one when starting Kdenlive until "
"you switch to another saved layout or modify the current one.  If you do "
"make changes to a custom layout after loading it and then quit Kdenlive, you "
"will not be prompted to save your changes to the named layout.  The changes "
"will be remembered and applied the next time you launch Kdenlive, but be "
"aware that you are now working with an unnamed layout.  If you like the "
"layout and want to preserve it, save it back to the original name or save it "
"as a new name."
msgstr ""
"Us permet canviar a una disposició personalitzada desada prèviament. Una "
"vegada que carregueu una disposició guardada, aquesta disposició romandrà "
"com l'actual quan s'iniciï el Kdenlive fins que canvieu a una altra "
"disposició guardada o modifiqueu l'actual. Si feu canvis a una disposició "
"personalitzada després de carregar-la i després sortiu del Kdenlive, no us "
"demanarà que deseu els canvis a la disposició anomenada. Els canvis es "
"recordaran i s'aplicaran la pròxima vegada que llanceu el Kdenlive, però "
"sigueu conscient que ara esteu treballant amb una disposició sense nom. Si "
"us agrada la disposició i voleu mantenir-la, deseu-la de nou al nom original "
"o deseu-la com un nom nou."

#: ../../user_interface/menu/view_menu/layouts.rst:25
msgid "Save Layout"
msgstr "Desa la disposició"

#: ../../user_interface/menu/view_menu/layouts.rst:27
msgid ""
"Kdenlive allows a great deal of freedom to customize screen layout. You can "
"choose which windows to display and where to position them.  You can resize "
"them or undock them and move them to a second monitor.  Any changes you make "
"to the layout will be automatically saved so that the next time you start "
"Kdenlive, things will look as you left them.  This is fine if you have one "
"layout that works for all your projects.  However, you may want to have "
"different layouts for different types of projects and be able to switch "
"between them as needed."
msgstr ""
"El Kdenlive permet una gran llibertat per personalitzar el disseny de la "
"pantalla. Podeu triar quines finestres es mostraran i on es posicionaran. "
"Podeu redimensionar-les o desacoblar-les i moure-les a un segon monitor. "
"Qualsevol canvi que feu a la disposició es desarà automàticament perquè la "
"pròxima vegada que inicieu el Kdenlive, les coses semblin que les heu "
"deixat. Això està bé si teniu una disposició que funciona per a tots els "
"vostres projectes. No obstant això, és possible que vulgueu tenir diferents "
"disposicions per a diferents tipus de projectes i que es pugui canviar entre "
"elles segons sigui necessari."

#: ../../user_interface/menu/view_menu/layouts.rst:33
msgid ""
"Kdenlive lets you name and save up to four custom layouts. In the example "
"shown, no custom layouts have been saved yet so they are just labeled 1 "
"through 4. Click :menuselection:`Save Layout As` and then choose one of the "
"four choices presented."
msgstr ""
"El Kdenlive us permet anomenar i desar fins a quatre disposicions "
"personalitzades. En l'exemple mostrat, encara no s'ha desat cap disposició "
"personalitzada de manera que s'acaben d'etiquetar 1 fins a 4. Feu clic a :"
"menuselection:`Desa la disposició com a` i a continuació trieu una de les "
"quatre opcions presentades."

#: ../../user_interface/menu/view_menu/layouts.rst:41
msgid ""
"The Save Layout dialog appears and you can give your custom layout a name."
msgstr ""
"Apareixerà el diàleg «Desa la disposició» i podreu donar un nom a la "
"disposició personalitzada."

#: ../../user_interface/menu/view_menu/layouts.rst:44
msgid ""
"Now you can easily switch to that layout whenever you'd like by choosing the "
"corresponding `Load Layout`_ menu selection."
msgstr ""
"Ara podeu canviar fàcilment a aquesta disposició quan vulgueu escollint la "
"selecció de menú corresponent `Load Layout`_."

#: ../../user_interface/menu/view_menu/layouts.rst:47
msgid "Manage Layouts"
msgstr "Gestió de les disposicions"

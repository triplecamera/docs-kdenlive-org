# Translation of docs_kdenlive_org_user_interface___menu___view_menu___show_title_bars.po to Catalan
# Copyright (C) 2021 This_file_is_part_of_KDE
# Licensed under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: documentation-docs-kdenlive-org\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-13 00:22+0000\n"
"PO-Revision-Date: 2021-11-19 12:04+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../user_interface/menu/view_menu/show_title_bars.rst:15
msgid "Show Title Bars"
msgstr "Mostra les barres de títol"

#: ../../user_interface/menu/view_menu/show_title_bars.rst:17
msgid "Contents"
msgstr "Contingut"

#: ../../user_interface/menu/view_menu/show_title_bars.rst:19
msgid ""
"This toggles the display of the title bar and control buttons on dockable "
"windows in **Kdenlive**."
msgstr ""
"Això commuta la visualització de la barra de títol i els botons de control "
"en les finestres acoblables del **Kdenlive**."

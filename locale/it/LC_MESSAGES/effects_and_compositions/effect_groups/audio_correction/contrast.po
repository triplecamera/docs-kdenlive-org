# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Paolo Zamponi <zapaolo@email.it>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-09 00:38+0000\n"
"PO-Revision-Date: 2022-02-27 14:29+0100\n"
"Last-Translator: Paolo Zamponi <zapaolo@email.it>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.12.2\n"

#: ../../effects_and_compositions/effect_groups/audio_correction/contrast.rst:14
msgid "Contrast"
msgstr "Contrasto"

#: ../../effects_and_compositions/effect_groups/audio_correction/contrast.rst:16
msgid "Contents"
msgstr "Contenuto"

#: ../../effects_and_compositions/effect_groups/audio_correction/contrast.rst:18
msgid ""
"This is the `Sox contrast <https://www.mltframework.org/plugins/FilterSox-"
"contrast/>`_ MLT filter."
msgstr ""
"È il filtro MLT `Sox contrast <https://www.mltframework.org/plugins/"
"FilterSox-contrast/>`_."

#: ../../effects_and_compositions/effect_groups/audio_correction/contrast.rst:20
msgid "Process audio using a SoX effect."
msgstr "Elabora l'audio usando un effetto SoX."

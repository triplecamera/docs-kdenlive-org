# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Paolo Zamponi <zapaolo@email.it>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-12 00:40+0000\n"
"PO-Revision-Date: 2022-12-12 15:59+0100\n"
"Last-Translator: Paolo Zamponi <zapaolo@email.it>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.12.0\n"

#: ../../effects_and_compositions/audio.rst:1
msgid "Mix audio in Kdenlive video editor"
msgstr "Miscelare l'audio nell'editor video Kdenlive"

#: ../../effects_and_compositions/audio.rst:1
msgid ""
"KDE, Kdenlive, timeline, audio mixer, multiple audio streams, audio "
"recording, documentation, user manual, video editor, open source, free, "
"learn, easy"
msgstr ""
"KDE, Kdenlive, linea temporale, mixer audio, più flussi audio, registrazione "
"audio, documentazione, Manuale utente, editor video, open source, libero, "
"impara, facile"

#: ../../effects_and_compositions/audio.rst:16
msgid "Audio"
msgstr "Audio"

#: ../../effects_and_compositions/audio.rst:18
msgid ""
"Kdenlive has some tools for handling audio. Beside the audio spectrum viewer "
"and some audio effects, you have following possibilities:"
msgstr ""
"Kdenlive ha alcuni strumenti per maneggiare l'audio. Accanto al "
"visualizzatore di spettro e ad alcuni effetti audio, hai le seguenti "
"possibilità:"

#: ../../effects_and_compositions/audio.rst:23
msgid "Audio Mixer"
msgstr "Mixer audio"

#: ../../effects_and_compositions/audio.rst:32
msgid "The audio mixer has following functions for each channel:"
msgstr "Per ciascun canale il mixer audio ha le seguenti funzioni:"

#: ../../effects_and_compositions/audio.rst:34
msgid "Channel number (audio track number) or Master channel"
msgstr "Numero del canale (numero della traccia audio), o canale Principale"

#: ../../effects_and_compositions/audio.rst:35
msgid "Mute an audio channel"
msgstr "Silenzia un canale audio"

#: ../../effects_and_compositions/audio.rst:36
msgid "Solo an audio channel"
msgstr "Riproduce solamente un canale audio"

#: ../../effects_and_compositions/audio.rst:37
msgid ""
":ref:`Record audio <audio-recording>` direct on the track of the related "
"audio channel"
msgstr ""
":ref:`Registra l'audio <audio-recording>` direttamente nella traccia del "
"canale audio collegato"

#: ../../effects_and_compositions/audio.rst:38
msgid "Opens the effect stack of the related audio channel"
msgstr "Apre la pila degli effetti del canale audio relativo"

#: ../../effects_and_compositions/audio.rst:39
msgid "Balance the audio channel. Either with the slider or with values"
msgstr "Bilancia i canali audio, sia col cursore che mediante i valori"

#: ../../effects_and_compositions/audio.rst:40
msgid "Adjustment of the volume"
msgstr "Regolazione del volume"

#: ../../effects_and_compositions/audio.rst:43
msgid "Multiple audio streams"
msgstr "Flussi audio multipli"

#: ../../effects_and_compositions/audio.rst:47
msgid ""
"Multiple audio streams of a video clip. In clip properties on the tab audio "
"you can adjust and manipulate each audio stream. More details see here :ref:"
"`audio_properties`"
msgstr ""
"Più flussi audio di una clip video. Nelle scheda Audio delle proprietà delle "
"clip puoi regolare e manipolare ciascun flusso audio. Per maggiori dettagli, "
"vedi :ref:`audio_properties`"

#: ../../effects_and_compositions/audio.rst:52
msgid "Audio recording"
msgstr "Registrazione audio"

#: ../../effects_and_compositions/audio.rst:56
msgid ""
"There is now a :guilabel:`mic` button in the mixers (number 4 in above "
"picture) instead of the :guilabel:`record` button. Pressing the :guilabel:"
"`mic` button will enter in audio monitoring mode (levels show mic input and "
"volume slider selects the mic level). While recording you see a live "
"waveform appearing on timeline."
msgstr ""
"Adesso c'è un pulsante :guilabel:`microfono` nei mixer (il numero 4 nella "
"figura qui sopra), al posto di :guilabel:`registra`. Facendoci clic sopra si "
"entrerà nella modalità di controllo dell'audio (i livelli mostrano "
"l'ingresso del microfono e il regolatore del volume seleziona i livelli del "
"microfono). Durante la registrazione vedrai una forma d'onda in tempo reale "
"apparire nella linea temporale."

#: ../../effects_and_compositions/audio.rst:63
msgid ""
"Enabling :guilabel:`mic` displays the track head record control and it get "
"colorized."
msgstr ""
"L'abilitazione del :guilabel:`microfono` visualizza sull'intestazione della "
"traccia i controlli di registrazione, e la colora."

#: ../../effects_and_compositions/audio.rst:69
msgid ""
"**Start record:** press :kbd:`spacebar` or click the :guilabel:`record` "
"button on the track head. A countdown start in project monitor (disable "
"countdown see :ref:`configure_audio_capture` settings)."
msgstr ""
"**Iniziare la registrazione:** premi la :kbd:`barra spaziatrice` o fai clic "
"sul pulsante :guilabel:`registra` nell'intestazione della traccia. Nel "
"controllo del progetto si avvia un contatore (per disabilitarlo, vedi le "
"impostazioni in :ref:`configure_audio_capture`)."

#: ../../effects_and_compositions/audio.rst:71
msgid "**Pause:** press :kbd:`spacebar`"
msgstr "**Mettere in pausa:** premi la :kbd:`barra spaziatrice`"

#: ../../effects_and_compositions/audio.rst:73
msgid "**To resume:** press :kbd:`spacebar` again"
msgstr "**Per riprendere:** premi nuovamente la :kbd:`barra spaziatrice`"

#: ../../effects_and_compositions/audio.rst:75
msgid ""
"**Stop record:** press :kbd:`esc` or click the :guilabel:`record` button in "
"the track head. The audio clip get added in the timeline and project bin."
msgstr ""
"**Interrompere la registrazione:** premi :kbd:`esc` o fai clic sul pulsante :"
"guilabel:`registra` nell'intestazione della traccia. La clip audio viene "
"aggiunta sia nella linea temporale che nel contenitore del progetto."

# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Paolo Zamponi <zapaolo@email.it>, 2021, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-11-28 00:40+0000\n"
"PO-Revision-Date: 2022-11-29 07:38+0100\n"
"Last-Translator: Paolo Zamponi <zapaolo@email.it>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.08.3\n"

#: ../../user_interface/monitors.rst:1
msgid "Clip monitor and project monitor in Kdenlive video editor"
msgstr "Controllo della clip e del progetto nell'editor video Kdenlive"

#: ../../user_interface/monitors.rst:1
msgid ""
"KDE, Kdenlive, clip, project, monitor, overlay, resizing, zoombar, preview, "
"toolbar, documentation, user manual, video editor, open source, free, learn, "
"easy"
msgstr ""
"KDE, Kdenlive, clip, progetto, controllo, sovrapposizione, ridimensionare, "
"barre d'ingrandimento, anteprima, barra degli strumenti, documentazione, "
"manuale utente, editor video, open source, libero, impara, facile"

#: ../../user_interface/monitors.rst:22
msgid "Monitors"
msgstr "Controlli"

#: ../../user_interface/monitors.rst:24
msgid ""
"Kdenlive uses 2 monitor widgets to display your videos: Clip Monitor and "
"Project Monitor. A third monitor - the Record Monitor - previews video "
"capture. These monitors can be selected by clicking the corresponding tabs "
"which appear at the bottom of the monitor window."
msgstr ""
"Kdenlive utilizza due oggetti di controllo per visualizzare i video: il "
"Controllo della clip e il Controllo del progetto. Un terzo, il Controllo "
"della registrazione, genera l'anteprima della cattura video. Questi "
"controlli possono essere selezionati facendo clic sulle schede "
"corrispondenti, che appaiono nella parte inferiore della finestra dei "
"controlli."

#: ../../user_interface/monitors.rst:27
msgid "Resizing the Monitors"
msgstr "Ridimensionamento dei controlli"

#: ../../user_interface/monitors.rst:29
msgid ""
"You can resize the monitors by dragging the sizing widget. It is a bit "
"tricky to find the bottom widget. You need to hover just between the bottom "
"of the monitor tab and the timeline"
msgstr ""
"Puoi ridimensionare i controlli trascinando il dimensionamento dell'oggetto. "
"Potrebbe non essere immediato trovare la parte inferiore dell'oggetto: devi "
"passare col mouse tra la parte inferiore della linguetta del controllo e la "
"linea temporale"

#: ../../user_interface/monitors.rst:37
msgid "Monitor zoombar"
msgstr "Barre d'ingrandimento dei controlli"

#: ../../user_interface/monitors.rst:41
msgid ""
"The Monitors get zoom bars. To activate: hover over the timeline ruler and :"
"kbd:`CTRL + Mouse wheel`."
msgstr ""
"I controlli hanno le barre d'ingrandimento. Per attivarle passa col mouse "
"sopra il righello della linea temporale, poi :kbd:`Ctrl + rotellina del "
"mouse`."

#: ../../user_interface/monitors.rst:48
msgid ""
"Support for external monitor display using Blackmagic Design decklink cards."
msgstr ""
"Supporto per la visualizzazione di un controllo esterno se vengono usate le "
"schede DeckLink Blackmagic Design."

#: ../../user_interface/monitors.rst:53
msgid "Monitor toolbar"
msgstr "Barra degli strumenti dei controlli"

#: ../../user_interface/monitors.rst:60
msgid ""
"Support multiple guide overlays. Move with the mouse to the upper-right "
"corner of the monitor to access the toolbar."
msgstr ""
"Supporta la sovrapposizione di più guide: spostati col mouse nell'angolo in "
"alto a destra del controllo per aver accesso alla barra degli strumenti."

#: ../../user_interface/monitors.rst:64
msgid ""
"The color of the guide overlays can be changed. See :ref:`monitor_toolbars`"
msgstr ""
"Il colore della guida che si sovrappone può essere cambiato. Vedi :ref:"
"`monitor_toolbars`"

#: ../../user_interface/monitors.rst:69
msgid "Preview resolution"
msgstr "Risoluzione anteprima"

#: ../../user_interface/monitors.rst:76
msgid ""
"Preview resolution speeds up the editing experience by scaling the video "
"resolution of the monitors. It can be used of proxies instead."
msgstr ""
"La risoluzione anteprima velocizza l'esperienza di montaggio ridimensionando "
"la risoluzione video dei controlli. Può essere usata al posto delle clip "
"rappresentative."

#: ../../user_interface/monitors.rst:81
msgid "Clip Monitor"
msgstr "Controllo della clip"

#: ../../user_interface/monitors.rst:83
msgid ""
"The Clip monitor displays the unedited clip that is currently selected in :"
"ref:`project_tree`."
msgstr ""
"Il controllo della clip visualizza la clip non modificata che è attualmente "
"selezionata nel :ref:`project_tree`."

#: ../../user_interface/monitors.rst:89
msgid "Widgets on the Clip Monitor"
msgstr "Oggetti nel controllo della clip"

#: ../../user_interface/monitors.rst:91
msgid ""
"**Insert Zone In Project Bin** button - click this to add the current zone "
"to the project bin. The selected zone will appear as child clip in the "
"project bin - like the clip shown as Zone1 in the screen shot."
msgstr ""
"Il pulsante **Inserisci zona nel contenitore del progetto**: facci clic per "
"aggiungere la zona corrente nel contenitore del progetto. Questa apparirà "
"come clip figlia nel contenitore del progetto, proprio come la clip mostrata "
"come \"Zone1\" nella schermata."

#: ../../user_interface/monitors.rst:93
msgid "**Set zone start** button - click this to set an 'in' point."
msgstr ""
"Il pulsante **Imposta inizio della zona**: facci clic per impostare il punto "
"di 'attacco'."

#: ../../user_interface/monitors.rst:95
msgid "**Set zone end** button - click this to set an 'out' point."
msgstr ""
"Il pulsante **Imposta fine della zona**: facci clic per impostare il punto "
"di 'stacco'."

#: ../../user_interface/monitors.rst:97
msgid ""
"Zone duration indicator - selected by setting in and out points. Dragging "
"the clip from the clip monitor to the timeline when there is a selected zone "
"causes the selected zone, not the entire clip, to be copied to the timeline."
msgstr ""
"L'indicatore di durata della zona, selezionato impostando i punti di attacco "
"e di stacco. Trascinando la clip dal controllo della clip alla linea "
"temporale, la zona selezionata - e non l'intera clip - verrà copiata nella "
"linea temporale."

#: ../../user_interface/monitors.rst:99
msgid ""
"Position Caret - can be dragged in the clip. (In ver >=0.9.4 and with OpenGL "
"turned on in :menuselection:`Settings --> Configure Kdenlive --> Playback`, "
"audio will play as you drag this.)"
msgstr ""
"l'indicatore di posizione, che può essere trascinato all'interno della clip. "
"Nelle versioni successive alla 0.9.4, e con OpenGL attivato in :"
"menuselection:`Impostazioni --> Configura Kdenlive --> Riproduzione`, "
"l'audio viene riprodotto durante il trascinamento."

#: ../../user_interface/monitors.rst:101
msgid ""
"Timecode widget - type a timecode here and hit :kbd:`Enter` to go to an "
"exact location in the clip. Timecode is in the format *hours:minutes:seconds:"
"frames* (where frames will correspond to the number of frames per second in "
"your project profile)."
msgstr ""
"L'oggetto per il codice temporale: inseriscine uno qui, e premi :kbd:`Invio` "
"per andare ad una posizione esatta nella clip. Il codice temporale deve "
"essere espresso nel formato *ore:minuti:secondi:fotogrammi*, dove il campo "
"fotogrammi corrisponde al numero di fotogrammi al secondo nel profilo del "
"progetto."

#: ../../user_interface/monitors.rst:103
msgid ""
"Timecode arrows - can be used to change the current position of the clip in "
"the clip monitor."
msgstr ""
"Le frecce del codice temporale, che possono essere utilizzate per modificare "
"la posizione corrente della clip nel controllo della clip."

#: ../../user_interface/monitors.rst:108 ../../user_interface/monitors.rst:165
msgid "Hamburger menu"
msgstr "Menu hamburger"

#: ../../user_interface/monitors.rst:115
msgid "Creating Zones in Clip Monitor"
msgstr "Creare zone nel controllo della clip"

#: ../../user_interface/monitors.rst:117
msgid ""
"Zones are defined regions of clips that are indicated by a colored section "
"in the clip monitor's timeline - see item 3 above. The beginning of a zone "
"is set by clicking **[** (item 1 in the pic above). The end of a zone is set "
"by clicking **]** (item 2 in the pic above)"
msgstr ""
"Le zone sono regioni definite delle clip che sono indicate da una sezione "
"colorata nella linea temporale del controllo della clip, vedi l'elemento 3 "
"qui sopra. L'inizio di una zona viene impostato facendo clic su **[** "
"(l'elemento 1 nella figura qui sopra), mentre per la sua fine bisogna fare "
"clic su **]** (l'elemento 2 nella stessa figura)."

#: ../../user_interface/monitors.rst:120
msgid "Clip Monitor Right-click menu"
msgstr "Menu del tasto destro sul controllo della clip"

#: ../../user_interface/monitors.rst:122
msgid ""
"The Clip Monitor has a right-click (context) menu as described :ref:`here "
"<clip_monitor_rightclick>`."
msgstr ""
"Il controllo della clip ha un menu contestuale, come descritto :ref:`qui "
"<clip_monitor_rightclick>`."

#: ../../user_interface/monitors.rst:125
msgid "Seeking"
msgstr "Ricerca"

#: ../../user_interface/monitors.rst:129
msgid ""
"Inside the clip monitor: hold down :kbd:`Shift` and move the mouse left/"
"right."
msgstr ""
"All'interno del controllo della clip: premi :kbd:`Maiusc` e sposta il mouse "
"a destra e a sinistra."

#: ../../user_interface/monitors.rst:134
msgid "Drag audio or video only of a clip in timeline"
msgstr "Trascinare solo l'audio o il video di una clip nella linea temporale"

#: ../../user_interface/monitors.rst:141
msgid ""
"Move with the mouse to the lower-left corner of the clip monitor to access "
"the Video/Audio icons. Hover with the mouse either over the audio or video "
"icon left click to drag either video or audio part into the timeline."
msgstr ""
"Spostati col mouse nell'angolo in basso a sinistra del controllo per aver "
"accesso alle icone audio e video. Passa col mouse sull'icona dell'audio o "
"del video, quindi tasto sinistro per trascinare la parte audio o video nella "
"linea temporale."

#: ../../user_interface/monitors.rst:146
msgid "Project Monitor"
msgstr "Controllo del progetto"

#: ../../user_interface/monitors.rst:148
msgid ""
"The Project Monitor displays your project's timeline - i.e. the edited "
"version of your video."
msgstr ""
"Il controllo del progetto visualizza la linea temporale del progetto, cioè "
"la versione modificata del video."

#: ../../user_interface/monitors.rst:154
msgid "Project Monitor Widgets"
msgstr "Oggetti nel controllo del progetto"

#: ../../user_interface/monitors.rst:156
msgid ""
"The position caret. Shows the current location in the project relative to "
"the whole project. You can click and drag this to move the position in the "
"project."
msgstr ""
"L'indicatore di posizione. Mostra la posizione corrente nel progetto, "
"relativa all'intero progetto. Puoi farci clic e trascinarlo per spostare la "
"posizione all'interno del progetto."

#: ../../user_interface/monitors.rst:158
msgid ""
"The timecode widget. You can type a timecode here and press :kbd:`Enter` to "
"bring the Project Monitor to an exact location."
msgstr ""
"L'oggetto per il codice temporale. Puoi digitare qui un codice temporale, "
"poi premi :kbd:`Invio` per portare il controllo del progetto in una "
"posizione esatta."

#: ../../user_interface/monitors.rst:160
msgid ""
"Timecode widget control arrows. You can move the Project Monitor one frame "
"at a time with these."
msgstr ""
"Le frecce per il controllo del codice temporale: con queste puoi spostare il "
"controllo del progetto di un fotogramma alla volta."

#: ../../user_interface/monitors.rst:172
msgid "Creating Zones in Project Monitor"
msgstr "Creare zone nel controllo del progetto"

#: ../../user_interface/monitors.rst:174
msgid ""
"You can use the **[** and **]** buttons to create a zone in the Project "
"Monitor the same way you make zones in the clip monitor. The zone will be "
"indicated by a colored bar both on the timeline and underneath the Project "
"Monitor."
msgstr ""
"Puoi utilizzare i pulsanti **[** e **]** per creare delle zone nel controllo "
"del progetto, allo stesso modo di come era stato fatto col controllo della "
"clip. La zona verrà indicata con una barra colorata, sia nella linea "
"temporale che nella parte bassa del controllo del progetto."

#: ../../user_interface/monitors.rst:179
msgid ""
"You can get Kdenlive to only render the selected zone - see :ref:"
"`remder_using_zone`."
msgstr ""
"Puoi far sì che Kdenlive esporti solo la zona selezionata, vedi :ref:"
"`remder_using_zone`."

#: ../../user_interface/monitors.rst:182
msgid "Project Monitor Right-click menu"
msgstr "Menu del tasto destro sul controllo del progetto"

#: ../../user_interface/monitors.rst:184
msgid ""
"The project monitor has a right-click (context menu) as described :ref:`here "
"<project_monitor_rightclick>`."
msgstr ""
"Il controllo del progetto ha un menu contestuale, come descritto :ref:`qui "
"<project_monitor_rightclick>`."

#: ../../user_interface/monitors.rst:189
msgid "Multicam Editing"
msgstr "Montaggio multi-camera"

#: ../../user_interface/monitors.rst:193
msgid ""
"Enable the multirack view via menu :menuselection:`Monitor --> Multitrack "
"view`."
msgstr ""
"Abilita la visione multi traccia dal menu :menuselection:`Controllo --> "
"Visuale multi-traccia`."

#: ../../user_interface/monitors.rst:198
msgid ""
"New multicam editing interface allows you to select a track in the timeline "
"by clicking on the project monitor."
msgstr ""
"La nuova interfaccia di montaggio multi camera ti permette di selezionare "
"una traccia nella linea temporale facendo clic sul controllo del progetto."

#: ../../user_interface/monitors.rst:203
msgid "Separate Clip and Project Monitors"
msgstr "Separare i controlli della clip e del progetto"

#: ../../user_interface/monitors.rst:205
msgid ""
"You can click on the Tab names that label the Monitors and drag the monitor "
"out into its own window."
msgstr ""
"Puoi fare clic sui nomi delle linguette che etichettano i controlli e "
"trascinarli nella sua finestra."

#: ../../user_interface/monitors.rst:210
msgid ""
"To put the monitors back into the Tabbed view - click on the monitor's title "
"bar and drag the window on top of the other monitor window."
msgstr ""
"Per rimettere il controllo nella vista a schede, fai clic sulla sua barra "
"del titolo e trascina la finestra sopra le altre."

#: ../../user_interface/monitors.rst:212
msgid ""
"If the monitor has no title bar (intermittent defect) then you can not do "
"this and you will need to reset kdenlive settings by deleting ~/.config/"
"kdenliverc"
msgstr ""
"Se il controllo non ha la barra del titolo (difetto intermittente) e quindi "
"non riesci farlo, ripristina le impostazioni di kdenlive eliminando ~/."
"config/kdenliverc"

#~ msgid "Contents:"
#~ msgstr "Contenuto:"

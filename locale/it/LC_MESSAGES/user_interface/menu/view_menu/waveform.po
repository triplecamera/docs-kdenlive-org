# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Paolo Zamponi <zapaolo@email.it>, 2022.
# Vincenzo Reale <smart2128vr@gmail.com>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-02-27 00:38+0000\n"
"PO-Revision-Date: 2022-11-27 18:10+0100\n"
"Last-Translator: Paolo Zamponi <zapaolo@email.it>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.08.3\n"

#: ../../user_interface/menu/view_menu/waveform.rst:13
msgid "Waveform"
msgstr "Forma d'onda"

#: ../../user_interface/menu/view_menu/waveform.rst:16
msgid "Contents"
msgstr "Contenuto"

#: ../../user_interface/menu/view_menu/waveform.rst:18
msgid ""
"This data is a 3D histogram.  It represents the Luma component (whiteness) "
"of the video. It is the same type of graph as for the :ref:`rgb_parade`. The "
"horizontal axis represents the horizontal axis in the video frame. The "
"vertical axis is the pixel luma from 0 to 255. The brightness of the point "
"on the graph represents the count of the number of pixels with this luma in "
"this column of pixels in the video frame."
msgstr ""
"Questo dato è un istogramma 3D. Rappresenta la componente Luma (bianchezza) "
"del video. È lo stesso tipo di grafico di :ref:`rgb_parade`: l'asse "
"orizzontale rappresenta l'asse orizzontale nel fotogramma video, mentre "
"quello verticale è la luma dei pixel da 0 a 255. La luminosità del punto sul "
"grafico rappresenta il conteggio del numero di pixel con questa luma in "
"questa colonna di pixel nel fotogramma video."

#: ../../user_interface/menu/view_menu/waveform.rst:26
msgid ""
"For more information see :ref:`Granjow's blog <waveform_and_RGB_parade>` on "
"the waveform and RGB parade scopes. This blog gives some information on how "
"to use the data provided by the RGB parade to do color correction on video "
"footage."
msgstr ""
"Per ulteriori informazioni, vedi il :ref:`Blog di Granjow "
"<waveform_and_RGB_parade>` sugli ambiti forma d'onda ee esibizione RGB. "
"Questo blog fornisce alcune informazioni su come utilizzare i dati forniti "
"dall'esibizione RGB per eseguire la correzione del colore sulle riprese "
"video."

#: ../../user_interface/menu/view_menu/waveform.rst:30
msgid ":ref:`scopes_directx`"
msgstr ":ref:`scopes_directx`"

# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Paolo Zamponi <zapaolo@email.it>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-13 00:22+0000\n"
"PO-Revision-Date: 2022-03-03 14:33+0100\n"
"Last-Translator: Paolo Zamponi <zapaolo@email.it>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.12.2\n"

#: ../../user_interface/menu/timeline_menu/insert_clip_zone.rst:13
msgid "Insert Clip Zone in Timeline"
msgstr "Inserisci zona della clip nella linea temporale"

#: ../../user_interface/menu/timeline_menu/insert_clip_zone.rst:15
msgid "Contents"
msgstr "Contenuto"

#: ../../user_interface/menu/timeline_menu/insert_clip_zone.rst:17
msgid ""
"This menu item is available in the :menuselection:`Timeline` Menu on the :"
"menuselection:`Insertion` sub menu. Shortcut is :kbd:`V`"
msgstr ""
"Questa voce di menu è disponibile nel sotto-menu :menuselection:"
"`Inserimento` del menu :menuselection:`Linea temporale`. La scorciatoia è :"
"kbd:`V`"

#: ../../user_interface/menu/timeline_menu/insert_clip_zone.rst:20
msgid ""
"Keyboard command \"v\" and \"b\": Since version 19.08 \"3 point editing with "
"keyboard shortcuts\" is implemented. Source and target has to be activated "
"that the clip gets inserted into the timeline."
msgstr ""
"Comandi da tastiera \"v\" and \"b\": dalla versione 19.08 è stato "
"implementato il \"montaggio a 3 punti con scorciatoie di tastiera\". "
"Affinché la clip venga inserita nella linea temporale, la sorgente e la "
"destinazione devono essere attivate."

#: ../../user_interface/menu/timeline_menu/insert_clip_zone.rst:24
msgid ""
"More info here: https://kdenlive.org/en/2019/08/kdenlive-19-08-released/"
msgstr ""
"Maggiori informazioni qui: https://kdenlive.org/en/2019/08/kdenlive-19-08-"
"released/"

#: ../../user_interface/menu/timeline_menu/insert_clip_zone.rst:27
msgid ""
"Say you have a 10 sec. zone defined on a clip in Clip Monitor and on the "
"timeline you have a 20 sec. zone defined somewhere. When you press :kbd:`V` "
"or select  :menuselection:`Insert Clip Zone in Timeline (overwrite)` , it "
"will insert the 10 sec. segment of the clip from the Clip Monitor at the "
"beginning of the zone on the timeline. If there happens to be another clip "
"there already, it will overwrite it, completely or partially, depending on "
"how long the existing clip was."
msgstr ""
"Diciamo che hai una zona di 10 secondi definita in una clip nel Controllo "
"della clip, e che da qualche parte nella linea temporale c'è una zona da 20 "
"secondi. Quando premi :kbd:`V` oppure selezioni :menuselection:`Inserisci "
"zona della clip nella linea temporale (sovrascrivi)`, il segmento di 10 "
"secondi verrà inserito dal Controllo della clip all'inizio della zona nella "
"linea temporale. Se ci fosse già un'altra clip, questa verrebbe sovrascritta "
"completamente o parzialmente, a seconda della sua durata."

#: ../../user_interface/menu/timeline_menu/insert_clip_zone.rst:31
msgid "Regions selected on time line and in clip monitor - blue regions."
msgstr ""
"Le regioni selezionate nella linea temporale e il controllo della clip - "
"quelle in blu."

#: ../../user_interface/menu/timeline_menu/insert_clip_zone.rst:33
msgid ""
"Select  :menuselection:`Insert Clip Zone in Timeline (overwrite)` and the "
"section in the clip overwrites the section on the timeline"
msgstr ""
"Seleziona :menuselection:`Inserisci zona della clip nella linea temporale "
"(sovrascrivi)`, e la sezione nella clip sovrascrive quella nella Linea "
"temporale"

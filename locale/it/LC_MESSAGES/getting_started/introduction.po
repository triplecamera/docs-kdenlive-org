# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Paolo Zamponi <zapaolo@email.it>, 2021, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-16 00:37+0000\n"
"PO-Revision-Date: 2022-01-16 15:32+0100\n"
"Last-Translator: Paolo Zamponi <zapaolo@email.it>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.12.1\n"

#: ../../getting_started/introduction.rst:1
msgid "Introduction to Kdenlive video editor"
msgstr "Introduzione all'editor video Kdenlive"

#: ../../getting_started/introduction.rst:1
msgid ""
"KDE, Kdenlive, Introduction, documentation, user manual, video editor, open "
"source, free, learn, easy"
msgstr ""
"KDE, Kdenlive, introduzione, documentazione, manuale utente, editor video, "
"open source, libero, impara, facile"

#: ../../getting_started/introduction.rst:29
msgid "Introduction"
msgstr "Introduzione"

#: ../../getting_started/introduction.rst:32
msgid "Contents"
msgstr "Contenuto"

#: ../../getting_started/introduction.rst:34
msgid ""
"**Kdenlive** is an acronym for `KDE <http://www.kde.org>`_ **N**\\ on-\\ "
"**Li**\\ near **V**\\ ideo **E**\\ ditor. It is a free software (`GPL "
"licensed <http://www.fsf.org/licensing/licenses/gpl.html>`_) primarily aimed "
"at the Linux platform, but it also works on BSD [1]_  as it relies only on "
"portable components (`Qt <https://www.qt.io/>`_ and `MLT <http://www."
"mltframework.org/>`_ framework). Windows versions are also available, with "
"some drawbacks. See :ref:`windows_issues` for more information. A port on "
"MacOS is `currently in progress <https://invent.kde.org/multimedia/"
"kdenlive/-/issues/993>`_."
msgstr ""
"**Kdenlive** è un acronimo per `KDE <http://www.kde.org>`_ **N**\\ on-\\ "
"**Li**\\ near **V**\\ ideo **E**\\ ditor. È un software libero (`con licenza "
"GPL <http://www.fsf.org/licensing/licenses/gpl.html>`_) pensato "
"principalmente per la piattaforma Linux, tuttavia, essendo basato su dei "
"componenti portabili (i framework `Qt <https://www.qt.io/>`_ e `MLT <http://"
"www.mltframework.org/>`_), funziona anche in BSD [1]_ . Sono disponibili "
"anche delle versioni per Windows, seppur con qualche inconveniente. Per "
"maggiori informazioni vedi :ref:`windows_issues`. Una versione per MacOS è "
"`attualmente in sviluppo <https://invent.kde.org/multimedia/kdenlive/-/"
"issues/993>`_."

#: ../../getting_started/introduction.rst:38
msgid ""
"*Non-linear video editing* is much more powerful than beginners' (linear) "
"editors, hence it requires a bit more organization before starting. However, "
"it is not reserved to specialists and can be used for small personal "
"projects."
msgstr ""
"L'*editing video non lineare* è molto più potente di quello (lineare) dei "
"primi editor, anche se prima di partire richiede un po' più di "
"organizzazione. Non è tuttavia riservato agli specialisti, e può essere "
"utilizzato per dei piccoli progetti personali."

#: ../../getting_started/introduction.rst:41
msgid ""
"Through the MLT framework, **Kdenlive** integrates many plugin effects for "
"video and sound processing or creation. Furthermore **Kdenlive** brings a "
"powerful titling tool, a subtitling feature with automatic speech to text "
"conversion, and can then be used as a complete studio for video creation."
msgstr ""
"Attraverso il framework MLT, **Kdenlive** integra molte estensioni per "
"effetti di elaborazione video, e per l'elaborazione o la creazione del "
"suono. Inoltre **Kdenlive** è dotato di un potente strumento per i titoli e "
"di una funzionalità per la creazione di sottotitoli, che converte "
"automaticamente un discorso in testo: può quindi essere utilizzato come uno "
"studio completo per la creazione di video."

#: ../../getting_started/introduction.rst:47
msgid "Video editing features"
msgstr "Funzionalità di video editing"

#: ../../getting_started/introduction.rst:54
msgid ""
"Multitrack edition with a timeline and virtually unlimited number of video "
"and audio tracks, plus facilities for splitting audio and video from a clip "
"in multiple tracks"
msgstr ""
"Montaggio con più tracce su una linea temporale avente un numero "
"virtualmente illimitato di tracce audio e video, oltre a dei servizi per "
"separare audio e video in più tracce"

#: ../../getting_started/introduction.rst:57
msgid ""
"Non-blocking rendering. You can keep working on a project at the same time a "
"project is being transformed into a video file"
msgstr ""
"Esportazione non bloccante: puoi continuare a lavorare su un progetto mentre "
"un altro si sta trasformando in un file video"

#: ../../getting_started/introduction.rst:60
msgid ""
"Effects and transitions can be used with ease, and you can even create some "
"wipe transitions of your own!"
msgstr ""
"Gli effetti e le transizioni possono essere utilizzati con semplicità, e "
"puoi perfino crearti delle transizioni a tendina!"

#: ../../getting_started/introduction.rst:63
msgid ""
"Simple tools for easy creation of color clips, text clips and image clips"
msgstr ""
"Strumenti semplici per creare facilmente delle clip di colore, di testo e da "
"immagini"

#: ../../getting_started/introduction.rst:66
msgid ""
"Automatic :ref:`clips` creation from pictures directories, with crossfade "
"transitions among the images"
msgstr ""
"Creazione automatica di :ref:`clips` da cartelle di immagini, con "
"transizioni a dissolvenza incrociata tra le immagini."

#: ../../getting_started/introduction.rst:69
msgid "Configurable keyboard shortcuts and interface layouts"
msgstr "Scorciatoie di tastiera configurabili, e disposizioni dell'interfaccia"

#: ../../getting_started/introduction.rst:72
msgid "and much more!"
msgstr "e molto altro!"

#: ../../getting_started/introduction.rst:75
msgid "Berkeley Software Distribution"
msgstr "Berkeley Software Distribution"

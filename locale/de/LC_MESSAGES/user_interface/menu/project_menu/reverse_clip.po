# German translations for Kdenlive Manual package.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Frederik Schwarzer <schwarzer@kde.org>, 2022.
#
# Automatically generated, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-21 00:37+0000\n"
"PO-Revision-Date: 2022-06-08 01:33+0200\n"
"Last-Translator: Frederik Schwarzer <schwarzer@kde.org>\n"
"Language-Team: German <kde-i18n-de@kde.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.2\n"

#: ../../user_interface/menu/project_menu/reverse_clip.rst:12
msgid "Reverse Clip"
msgstr ""

#: ../../user_interface/menu/project_menu/reverse_clip.rst:14
msgid "Contents"
msgstr "Inhalt"

#: ../../user_interface/menu/project_menu/reverse_clip.rst:16
msgid ""
"This menu item is available from the Clip Jobs menu that appears when you :"
"ref:`project_tree` on a clip in the Project Bin or from under the :ref:"
"`project_menu` menu when a clip is selected in the Project Bin. It is used "
"to create a clip which plays in reverse."
msgstr ""

#: ../../user_interface/menu/project_menu/reverse_clip.rst:18
msgid "This feature became available in version 0.9.6 of Kdenlive."
msgstr ""

#: ../../user_interface/menu/project_menu/reverse_clip.rst:24
msgid ""
"When you select the :menuselection:`Reverse Clip` option from the menu, a "
"new clip is created in the Project Bin. It has the same file name as the "
"clip from which it was created, but with a .mlt extension appended. You can "
"then add this clip to the timeline and when you play it, the video of the "
"original source clip will played, but in reverse."
msgstr ""

#: ../../user_interface/menu/project_menu/reverse_clip.rst:28
msgid ""
"1=According to legacy Mantis bug tracker ID 2933 some clips will only "
"produce white image and error on reverse."
msgstr ""

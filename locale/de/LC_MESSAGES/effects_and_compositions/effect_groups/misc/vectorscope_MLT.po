# German translations for Kdenlive Manual package.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Frederik Schwarzer <schwarzer@kde.org>, 2022.
#
# Automatically generated, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-03 00:38+0000\n"
"PO-Revision-Date: 2022-06-08 01:25+0200\n"
"Last-Translator: Frederik Schwarzer <schwarzer@kde.org>\n"
"Language-Team: German <kde-i18n-de@kde.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.2\n"

#: ../../effects_and_compositions/effect_groups/misc/vectorscope_MLT.rst:14
msgid "Analysis and Data - Vectorscope"
msgstr ""

#: ../../effects_and_compositions/effect_groups/misc/vectorscope_MLT.rst:16
msgid "Contents"
msgstr "Inhalt"

#: ../../effects_and_compositions/effect_groups/misc/vectorscope_MLT.rst:18
msgid ""
"This is the `Frei0r vectorscope <https://www.mltframework.org/plugins/"
"FilterFrei0r-vectorscope/>`_ MLT filter."
msgstr ""

#: ../../effects_and_compositions/effect_groups/misc/vectorscope_MLT.rst:20
msgid "Displays the vectorscope of the video-data."
msgstr ""

#: ../../effects_and_compositions/effect_groups/misc/vectorscope_MLT.rst:22
msgid ""
"In ver 17.04 this is found in the :ref:`analysis_and_data` category of "
"Effects."
msgstr ""

#: ../../effects_and_compositions/effect_groups/misc/vectorscope_MLT.rst:24
msgid ""
"It is recommended to use the vectorscope from :ref:`vectorscope`, because "
"the effect *Analysis and Data - Vectorscope* is not correct - it uses a "
"graticule from an analog NTSC vectorscope, but equations from digital video."
msgstr ""

#: ../../effects_and_compositions/effect_groups/misc/vectorscope_MLT.rst:26
msgid "https://youtu.be/2ybBzDEjdRo"
msgstr ""

#: ../../effects_and_compositions/effect_groups/misc/vectorscope_MLT.rst:28
msgid "https://youtu.be/O1hbS6VZh_s"
msgstr ""

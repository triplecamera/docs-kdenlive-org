msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-13 00:22+0000\n"
"PO-Revision-Date: 2021-11-20 18:29-0800\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: ../../user_interface/menu/settings_menu/run_config_wizard.rst:13
msgid "Run Config Wizard"
msgstr ""

#: ../../user_interface/menu/settings_menu/run_config_wizard.rst:16
msgid "Contents"
msgstr ""

#: ../../user_interface/menu/settings_menu/run_config_wizard.rst:18
msgid ""
"This feature re-runs the config wizard that runs when you first install or "
"upgrade **Kdenlive**. It gives you the opportunity to choose the default "
"settings again for things like the default project settings. It also resets "
"many settings back to \"factory defaults\" so it can be useful to run this "
"if the **Kdenlive** application is misbehaving."
msgstr ""

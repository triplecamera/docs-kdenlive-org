msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-11-20 18:29-0800\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: ../../effects_and_compositions/effect_groups/blur_and_hide/glow.rst:14
msgid "Glow"
msgstr ""

#: ../../effects_and_compositions/effect_groups/blur_and_hide/glow.rst:16
msgid "Contents"
msgstr ""

#: ../../effects_and_compositions/effect_groups/blur_and_hide/glow.rst:18
msgid ""
"This is the `Frei0r glow <https://www.mltframework.org/plugins/FilterFrei0r-"
"glow/>`_ MLT filter."
msgstr ""

#: ../../effects_and_compositions/effect_groups/blur_and_hide/glow.rst:20
msgid "Creates a Glamorous Glow."
msgstr ""

#: ../../effects_and_compositions/effect_groups/blur_and_hide/glow.rst:22
msgid "https://youtu.be/vh4lrkFaVWc"
msgstr ""

#: ../../effects_and_compositions/effect_groups/blur_and_hide/glow.rst:24
msgid "https://youtu.be/UtBWFrYN9kA"
msgstr ""

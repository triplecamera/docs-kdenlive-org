# Xavier Besnard <xavier.besnard@neuf.fr>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-11-26 13:53+0100\n"
"Last-Translator: Xavier Besnard <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.11.80\n"

#: ../../effects_and_compositions/effect_groups/enhancement/denoiser.rst:13
msgid "Denoiser"
msgstr "Suppresseur de bruit"

#: ../../effects_and_compositions/effect_groups/enhancement/denoiser.rst:15
msgid "Contents"
msgstr "Contenu"

#: ../../effects_and_compositions/effect_groups/enhancement/denoiser.rst:17
msgid ""
"This is the `Frei0r hqdn3d <https://www.mltframework.org/plugins/"
"FilterFrei0r-hqdn3d/>`_ MLT filter - a High quality 3D denoiser from Mplayer."
msgstr ""
"Ceci est le filtre « Frei0r hqdn3d <https://www.mltframework.org/plugins/"
"FilterFrei0r-hqdn3d/> »_ MLT - un suppresseur de bruit 3D de haute qualité "
"venant de Mplayer."

#: ../../effects_and_compositions/effect_groups/enhancement/denoiser.rst:20
msgid "Tutorial 1"
msgstr "Tutoriel 1"

#: ../../effects_and_compositions/effect_groups/enhancement/denoiser.rst:22
msgid ""
"Shows usage of the denoiser effect as well as: :ref:`blue_screen`, :ref:"
"`alpha_operations` - shrinkhard and :ref:`keysplillm0pup`."
msgstr ""
"Affiche l'utilisation de l'effet de suppression de bruit ainsi que : :ref:"
"`écran_bleu`, :ref:`operations_alpha` - haute compression et :ref:"
"`keysplillm0pup`."

#: ../../effects_and_compositions/effect_groups/enhancement/denoiser.rst:24
msgid "https://youtu.be/l43Hz7YEcYU"
msgstr "https://youtu.be/l43Hz7YEcYU"

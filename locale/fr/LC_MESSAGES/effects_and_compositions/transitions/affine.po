# Xavier Besnard <xavier.besnard@neuf.fr>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-03 00:38+0000\n"
"PO-Revision-Date: 2022-01-21 13:36+0100\n"
"Last-Translator: Xavier Besnard <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.12.1\n"

#: ../../effects_and_compositions/transitions/affine.rst:12
msgid "Affine Transition"
msgstr "Transition d'affinage"

#: ../../effects_and_compositions/transitions/affine.rst:14
msgid "Contents"
msgstr "Contenu"

#: ../../effects_and_compositions/transitions/affine.rst:16
msgid "Generates image rotation in 3D space, skew and distortion."
msgstr ""
"Génère une rotation d'image dans l'espace 3D, avec inclinaison et distorsion."

#: ../../effects_and_compositions/transitions/affine.rst:18
msgid ""
"Provides keyframable animated affine transformations with dissolve "
"functionality."
msgstr ""
"Fournit des transformations d'affinage animées et paramétrable par trame "
"clé, avec une fonctionnalité de dissolution."

#: ../../effects_and_compositions/transitions/affine.rst:20
msgid ""
"In many applications, this transition can be used instead of a :ref:"
"`composite` and this provides a workaround to the composite transition "
"\"green tinge\" bug reported by some. (Mentioned in legacy Mantis bug "
"tracker ID 2759."
msgstr ""
"Dans de nombreuses applications, cette transition peut être utilisée à la "
"place d'une :ref:`composition`, ce qui permet de contourner le bogue de la "
"transition composite « Teinte verte » signalé par certaines personnes. "
"(Bogue signalé dans l'ancien système de gestion des bogues nommé Mantis sous "
"l'identifiant « ID 2759 »)."

#: ../../effects_and_compositions/transitions/affine.rst:24
msgid "Example 1"
msgstr "Exemple 1"

#: ../../effects_and_compositions/transitions/affine.rst:26
msgid "https://youtu.be/hylowKurZaw"
msgstr "https://youtu.be/hylowKurZaw"

#: ../../effects_and_compositions/transitions/affine.rst:34
msgid "Disolve using Affine Transition"
msgstr "Dissoudre en utilisant la transition d'affinage"

#: ../../effects_and_compositions/transitions/affine.rst:36
msgid "To add a Dissolve, change the opacity to zero percent."
msgstr ""
"Pour ajouter un effet de dissolution, modifiez l'opacité à zéro pourcent."

#: ../../effects_and_compositions/transitions/affine.rst:40
msgid "Rotation using Affine Transition"
msgstr "Rotation en utilisant la transition d'affinage"

#: ../../effects_and_compositions/transitions/affine.rst:42
msgid ""
"To rotate the image, add a keyframe and enter values for rotation. The units "
"are 10ths of degrees. (e.g. 900 = 90 degree rotation)."
msgstr ""
"Pour faire pivoter l'image, ajoutez une trame clé et saisissez les valeurs "
"de rotation. Les unités sont des dixièmes de degrés (Par exemple, 900 = "
"rotation de 90 degrés)."

#: ../../effects_and_compositions/transitions/affine.rst:44
msgid ""
"**Rotate X** rotates the frame in the plane of the screen. **Rotate Y** and "
"**Rotate Z** create the illusion of 3D rotation when used dynamically with "
"keyframes - see example below."
msgstr ""
"**Faire tourner en X** fait tourner l'image dans le plan de l'écran. **Faire "
"tourner en Y** et **Faire tourner en Z** créent l'illusion d'une rotation 3D "
"lorsqu'ils sont utilisés de manière dynamique avec des trames clé - Voir "
"l'exemple ci-dessous."

#: ../../effects_and_compositions/transitions/affine.rst:47
msgid ""
"You can create a similar effect using the :ref:`rotate_(keyframable)` effect "
"from the Crop and Transform group."
msgstr ""
"Vous pouvez créer un effet similaire en utilisant l'effet :ref:"
"`faire_tourner_(paramétrable par trame clé)` du groupe « Rognage et "
"transformation »."

#: ../../effects_and_compositions/transitions/affine.rst:51
msgid "Example 2 - Rotate Y"
msgstr "Exemple 2 - Faire tourner en Y"

#: ../../effects_and_compositions/transitions/affine.rst:53
msgid "https://youtu.be/IAWMIL7c9K4"
msgstr "https://youtu.be/IAWMIL7c9K4"

#: ../../effects_and_compositions/transitions/affine.rst:55
msgid ""
"This example is created using 3 keyframes. The second keyframe is shown "
"below with a **Rotate Y** value of 1800 (=180 degrees). Keyframe one and "
"keyframe three both have **Rotate Y** values of zero."
msgstr ""
"Cet exemple est créé à l'aide de 3 trames clé. La deuxième trame clé est "
"présentée ci-dessous avec une valeur de **Faire tourner en Y** de 1800 (Soit "
"180 degrés). Les trames clé 1 et 3 ont toutes deux une valeur de **Faire "
"tourner en Y** de zéro."

#: ../../effects_and_compositions/transitions/affine.rst:65
msgid ""
"The difference between **Rotate Y** and **Rotate Z** is that the apparent "
"rotation in **Rotate Y** appears to be around a horizontal axis. The "
"rotation in **Rotate Z** appears to be around a vertical axis."
msgstr ""
"La différence entre **Faire tourner en Y** et **Faire tourner en Z** est que "
"la rotation apparente dans **Faire tourner en Y** semble se faire autour "
"d'un axe horizontal. La rotation dans **Faire tourner en Z** semble se faire "
"autour d'un axe vertical."

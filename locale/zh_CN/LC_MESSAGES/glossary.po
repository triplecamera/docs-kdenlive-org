msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-16 00:37+0000\n"
"PO-Revision-Date: 2023-01-12 10:32\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/documentation-docs-kdenlive-org/"
"docs_kdenlive_org_glossary.pot\n"
"X-Crowdin-File-ID: 26275\n"

#: ../../glossary.rst:1
msgid "Kdenlive references and further information"
msgstr ""

#: ../../glossary.rst:1
msgid ""
"KDE, Kdenlive, references, information, documentation, user manual, video "
"editor, open source, free, learn, easy"
msgstr ""

#: ../../glossary.rst:15
msgid "Glossary"
msgstr "术语表"

#: ../../glossary.rst:17
msgid "Contents:"
msgstr "目录："

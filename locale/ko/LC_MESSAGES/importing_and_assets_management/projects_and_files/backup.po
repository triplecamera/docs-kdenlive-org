# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Shinjo Park <kde@peremen.name>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-13 00:22+0000\n"
"PO-Revision-Date: 2022-05-08 16:57+0200\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean <kde-kr@kde.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 21.12.3\n"

#: ../../importing_and_assets_management/projects_and_files/backup.rst:14
msgid "Backup"
msgstr "백업"

#: ../../importing_and_assets_management/projects_and_files/backup.rst:16
msgid "Contents"
msgstr "목차"

#: ../../importing_and_assets_management/projects_and_files/backup.rst:23
msgid ""
"The Backup widget, found in :menuselection:`Project --> Open Backup File` "
"allows you to restore a previous version of your project file."
msgstr ""

#: ../../importing_and_assets_management/projects_and_files/backup.rst:26
msgid ""
"In case something went wrong (corrupted project file, unwanted change, ...), "
"you can now restore a previous version of the file using this feature. Just "
"select the version you want and click :menuselection:`Open`."
msgstr ""

#: ../../importing_and_assets_management/projects_and_files/backup.rst:28
msgid ""
"The backup files are automatically created each time you save your project. "
"This means that if you save your project every hour, the backup widget will "
"show you a list of all the saved files, with a small image of the timeline "
"at the time you saved the project."
msgstr ""

#: ../../importing_and_assets_management/projects_and_files/backup.rst:31
msgid ""
"**Kdenlive** keeps up to 20 versions of your project file in the last hour, "
"20 versions from the current day, 20 versions in the last 7 days and 20 "
"older versions, which should be sufficient to recover from any problem."
msgstr ""

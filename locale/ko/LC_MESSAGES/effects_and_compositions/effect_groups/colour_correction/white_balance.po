# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Shinjo Park <kde@peremen.name>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2022-05-07 15:14+0200\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean <kde-kr@kde.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 21.12.3\n"

#: ../../effects_and_compositions/effect_groups/colour_correction/white_balance.rst:14
msgid "White Balance"
msgstr "화이트 밸런스"

#: ../../effects_and_compositions/effect_groups/colour_correction/white_balance.rst:16
msgid "Contents"
msgstr "목차"

#: ../../effects_and_compositions/effect_groups/colour_correction/white_balance.rst:18
msgid ""
"This is the `Frei0r balanc0r <https://www.mltframework.org/plugins/"
"FilterFrei0r-balanc0r/>`_ MLT filter."
msgstr ""
"`Frei0r balanc0r <https://www.mltframework.org/plugins/FilterFrei0r-balanc0r/"
">`_ MLT 필터입니다."

#: ../../effects_and_compositions/effect_groups/colour_correction/white_balance.rst:20
msgid "Adjust the white balance / color temperature."
msgstr "화이트 밸런스 및 색온도를 조정합니다."

#: ../../effects_and_compositions/effect_groups/colour_correction/white_balance.rst:22
msgid "https://youtu.be/foPVqzBV_vM"
msgstr "https://youtu.be/foPVqzBV_vM"

#: ../../effects_and_compositions/effect_groups/colour_correction/white_balance.rst:24
msgid "https://youtu.be/BqmMi6L945E"
msgstr "https://youtu.be/BqmMi6L945E"

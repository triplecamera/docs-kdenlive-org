# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Shinjo Park <kde@peremen.name>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2022-05-08 16:10+0200\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean <kde-kr@kde.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 21.12.3\n"

#: ../../effects_and_compositions/effect_groups/misc/owdenoise.rst:11
msgid "Owdenoise"
msgstr "Owdenoise"

#: ../../effects_and_compositions/effect_groups/misc/owdenoise.rst:14
msgid "Contents"
msgstr "목차"

#: ../../effects_and_compositions/effect_groups/misc/owdenoise.rst:16
msgid ""
"This is the `Avfilter owdenoise <https://www.mltframework.org/plugins/"
"FilterAvfilter-owdenoise/>`_ MLT filter."
msgstr ""
"`Avfilter owdenoise <https://www.mltframework.org/plugins/FilterAvfilter-"
"owdenoise/>`_ MLT 필터입니다."

#: ../../effects_and_compositions/effect_groups/misc/owdenoise.rst:18
msgid "Denoise using wavelets."
msgstr "웨이블릿을 사용하여 노이즈를 감소합니다."

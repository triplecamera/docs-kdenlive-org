# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Shinjo Park <kde@peremen.name>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2022-05-08 15:52+0200\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean <kde-kr@kde.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 21.12.3\n"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/rotate_keyframable.rst:12
msgid "Rotate"
msgstr "회전"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/rotate_keyframable.rst:14
msgid "Contents"
msgstr "목차"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/rotate_keyframable.rst:16
msgid "Rotates the image."
msgstr "이미지를 회전합니다."

#: ../../effects_and_compositions/effect_groups/crop_and_transform/rotate_keyframable.rst:18
msgid "See also :ref:`affine` that can also achieve a similar effect."
msgstr ""

#: ../../effects_and_compositions/effect_groups/crop_and_transform/rotate_keyframable.rst:20
msgid "https://youtu.be/Wfx1Cp5g6Mo"
msgstr "https://youtu.be/Wfx1Cp5g6Mo"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/rotate_keyframable.rst:26
msgid ""
"The units of rotation are tenths of a degree; e.g., 1800 = 180 degree "
"rotation."
msgstr ""

#: ../../effects_and_compositions/effect_groups/crop_and_transform/rotate_keyframable.rst:28
msgid "**Rotate X** rotates the frame in the plane of the screen."
msgstr ""

#: ../../effects_and_compositions/effect_groups/crop_and_transform/rotate_keyframable.rst:30
msgid ""
"**Rotate Y** and **Rotate Z** create the illusion of 3D rotation when used "
"dynamically with keyframes."
msgstr ""

#: ../../effects_and_compositions/effect_groups/crop_and_transform/rotate_keyframable.rst:32
msgid ""
"The difference between **Rotate Y** and **Rotate Z** is that the apparent "
"rotation in **Rotate Y** appears to be around a horizontal axis. The "
"rotation in **Rotate Z** appears to be around a vertical axis."
msgstr ""

# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Shinjo Park <kde@peremen.name>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-13 00:45+0000\n"
"PO-Revision-Date: 2022-05-08 16:29+0200\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean <kde-kr@kde.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 21.12.3\n"

#: ../../user_interface/toolbars.rst:1
msgid "How to use the toolbars in Kdenlive video editor"
msgstr "Kdenlive 동영상 편집기의 도구 모음 사용하기"

#: ../../user_interface/toolbars.rst:1
msgid ""
"KDE, Kdenlive, use, using, toolbars, documentation, user manual, video "
"editor, open source, free, learn, easy"
msgstr ""
"KDE, Kdenlive, 도구 모음, 문서, 사용자 설명서, 동영상 편집기, 오픈 소스, 자"
"유, 배우기, 쉬움"

#: ../../user_interface/toolbars.rst:20
msgid "Toolbars"
msgstr "도구 모음"

#: ../../user_interface/toolbars.rst:23
msgid "Contents"
msgstr "목차"

#: ../../user_interface/toolbars.rst:28
msgid "Menubar"
msgstr ""

#: ../../user_interface/toolbars.rst:33
msgid ""
"Not really a toolbar but it shows the :ref:`Menu` and :ref:"
"`workspace_layouts`."
msgstr ""

#: ../../user_interface/toolbars.rst:35
msgid ""
"It can be switched on/off in :menuselection:`Settings --> Show Menubar` or "
"by :kbd:`CTRL +M`."
msgstr ""

#: ../../user_interface/toolbars.rst:39
msgid ""
"This switches between having a menubar or having a hamburger menu button in "
"the main toolbar showing the menu items."
msgstr ""

#: ../../user_interface/toolbars.rst:44
msgid "If the main tool bar is switched off you get a warning:"
msgstr ""

#: ../../user_interface/toolbars.rst:54
#, fuzzy
#| msgid "Toolbars"
msgid "Main Toolbar"
msgstr "도구 모음"

#: ../../user_interface/toolbars.rst:59
msgid ""
"The main toolbar can be configured in :menuselection:`Settings --> Configure "
"Toolbars` or right-click on the toolbar and choose :guilabel:`Configure "
"Toolbars`. It can be switched on/off in :menuselection:`Settings --> "
"Toolbars Shown`."
msgstr ""

#: ../../user_interface/toolbars.rst:65
#, fuzzy
#| msgid "Main and Extra Toolbars"
msgid "Extra Toolbar"
msgstr "주 도구 모음과 추가 도구 모음"

#: ../../user_interface/toolbars.rst:70
msgid ""
"The extra toolbar contains by default the **Render** button. The extra "
"toolbar can be configured in :menuselection:`Settings --> Configure "
"Toolbars` or right-click on the toolbar and choose :guilabel:`Configure "
"Toolbars`. It can be switched on/off in :menuselection:`Settings --> "
"Toolbars Shown`."
msgstr ""

#: ../../user_interface/toolbars.rst:76
#, fuzzy
#| msgid "Configuring the Toolbars"
msgid "Timeline Toolbar"
msgstr "도구 모음 설정"

#: ../../user_interface/toolbars.rst:81
msgid ""
"The timeline toolbar can be configured in :menuselection:`Settings --> "
"Configure Toolbars` or right-click on the toolbar and choose :guilabel:"
"`Configure Toolbars`. It cannot be switched off."
msgstr ""

#: ../../user_interface/toolbars.rst:88
msgid "Statusbar"
msgstr ""

#: ../../user_interface/toolbars.rst:94
msgid ""
"Not really a toolbar but the statusbar shows on the left side hints what you "
"can do and on the right side switches and the zoom slider. It can be "
"switched on/off in :menuselection:`Settings --> Show Statusbar`."
msgstr ""

#: ../../user_interface/toolbars.rst:97
msgid "For more info on the statusbar see :ref:`editing`, :ref:`status_bar` ."
msgstr ""

#: ../../user_interface/toolbars.rst:101
msgid "Configuring the Toolbars"
msgstr "도구 모음 설정"

#: ../../user_interface/toolbars.rst:103
#, fuzzy
#| msgid ""
#| "The tools that are available on these are defined in :menuselection:"
#| "`Settings --> Configure Toolbars`."
msgid ""
"The toolbars that are available on these are defined in :menuselection:"
"`Settings --> Configure Toolbars`."
msgstr ""
":menuselection:`설정 --> 도구 모음 설정`\\ 에서 도구 모음에 있는 도구를 설정"
"할 수 있습니다."

#: ../../user_interface/toolbars.rst:112
msgid "Hiding and Showing the Toolbars"
msgstr "도구 모음 표시와 숨기기"

#: ../../user_interface/toolbars.rst:114
msgid ""
"You can also control this from the :ref:`toolbars_shown` menu item in the :"
"menuselection:`Settings` menu."
msgstr ""

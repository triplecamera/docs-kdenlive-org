# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Vit Pelcak <vit@pelcak.org>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2022-01-18 09:52+0100\n"
"Last-Translator: Vit Pelcak <vpelcak@suse.cz>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 21.12.1\n"

#: ../../effects_and_compositions/effect_groups/distort/pixelize.rst:13
msgid "Pixelize"
msgstr "Pixelizovat"

#: ../../effects_and_compositions/effect_groups/distort/pixelize.rst:15
msgid "Contents"
msgstr "Obsah"

#: ../../effects_and_compositions/effect_groups/distort/pixelize.rst:17
msgid ""
"This is the `Frei0r pixeliz0r <https://www.mltframework.org/plugins/"
"FilterFrei0r-pixeliz0r/>`_ MLT filter."
msgstr ""
"Toto je `Frei0r pixeliz0r <https://www.mltframework.org/plugins/FilterFrei0r-"
"pixeliz0r/>`_ filtr MLT."

#: ../../effects_and_compositions/effect_groups/distort/pixelize.rst:19
msgid "Pixelize input image."
msgstr "Pixelizuje (rozčtverečkuje) vstupní obraz."

#: ../../effects_and_compositions/effect_groups/distort/pixelize.rst:21
msgid "https://youtu.be/iFj1y1OES2Q"
msgstr "https://youtu.be/iFj1y1OES2Q"

#: ../../effects_and_compositions/effect_groups/distort/pixelize.rst:23
msgid "https://youtu.be/jvuFSVGbVRg"
msgstr "https://youtu.be/jvuFSVGbVRg"

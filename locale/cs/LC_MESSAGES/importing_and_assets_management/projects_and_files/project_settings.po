# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Vit Pelcak <vit@pelcak.org>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-02 00:49+0000\n"
"PO-Revision-Date: 2022-05-19 12:33+0200\n"
"Last-Translator: Vit Pelcak <vpelcak@suse.cz>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 22.04.1\n"

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:1
msgid "How to setup your project in Kdenlive video editor"
msgstr ""

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:1
msgid ""
"KDE, Kdenlive, project, setup, settings, documentation, user manual, video "
"editor, open source, free, learn, easy"
msgstr ""

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:21
msgid "Project Settings Dialog"
msgstr ""

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:24
msgid "Contents"
msgstr "Obsah"

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:26
msgid ""
"This is reached via  :menuselection:`Project Settings` in the :ref:"
"`project_menu` menu. This dialog has 3 Tabs."
msgstr ""

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:30
msgid "Project Settings Tab"
msgstr ""

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:37
msgid ""
"The Project Settings dialog is shown when you start a new project (:"
"menuselection:`File --> New`). This allows you to set all basic properties "
"for your project. You can also edit the properties of your current :ref:"
"`project_menu` in :menuselection:`Project --> Project Settings`."
msgstr ""

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:42
msgid "Project Folder"
msgstr "Složka projektu"

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:44
msgid ""
"As recommended in the :ref:`quickstart` section, you should create a new "
"folder for your project. This folder will hold all temporary files that are "
"used during the editing of your project (thumbnails, proxy clips, etc)."
msgstr ""

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:48
msgid "Video Profile"
msgstr "Profil videa"

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:50
msgid ""
"The video profile will define the format of your project. A list of "
"predefined formats is available in **Kdenlive**, for example *DV / DVD PAL*, "
"*HD 1080i 25 fps*, etc."
msgstr ""

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:53
msgid ""
"You can use the pull-down menus to filter the list of profiles by FPS "
"(Frames per second) or Scanning (Interlaced or Progressive)"
msgstr ""

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:56
msgid ""
"The profile defines the video resolution, as well as display aspect ratio, "
"color space and a few other parameters."
msgstr ""

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:59
msgid ""
"You should carefully choose your project format and select the one which "
"best fits your desired output. All video operations on the project (like "
"compositing, scaling, etc) will then use this profile. Advanced users can "
"create custom project profiles in  :ref:`manage_project_profiles`."
msgstr ""

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:62
msgid ""
"For example, if your goal is to create a DVD, you should use a DVD profile "
"with the correct frame rate (PAL / NTSC) and display ratio (widescreen or "
"not)."
msgstr ""

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:66
msgid "Tracks"
msgstr "Stopy"

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:68
msgid ""
"You can select the default number of audio and video tracks that your "
"project will have. You can always add or remove tracks in an existing "
"project."
msgstr ""

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:72
msgid "Thumbnails"
msgstr "Miniatury"

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:74
msgid ""
"The Audio and Video thumbnails are shown in the :ref:`timeline`. They can "
"also be enabled/disabled through buttons in the :ref:`status_bar`."
msgstr ""

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:79
msgid "Proxy Clips Tab"
msgstr ""

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:86
msgid ""
"When the :menuselection:`Proxy Clip` feature is enabled, **Kdenlive** will "
"automatically create reduced versions of your source clips, and use these "
"versions for your editing. **Kdenlive** will replace the proxy clips with "
"the originals for a full resolution when rendering."
msgstr ""

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:88
msgid ""
"The :menuselection:`Generate for videos larger than x pixels` option will "
"automatically create proxy clips for all videos added to the project that "
"have a frame width larger than x. This also applies to images."
msgstr ""

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:90
msgid ""
"You also have the choice to manually enable / disable proxy clips for each "
"clip in your project bin by right-clicking on the clip and choosing :"
"menuselection:`Proxy Clip`."
msgstr ""

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:92
msgid ""
"You can choose an *Encoding profile* for the proxy clips, which will define "
"the size, codecs and bitrate used when creating a proxy. The proxy profiles "
"can be managed from the **Kdenlive** Settings dialog (:menuselection:"
"`Settings --> Configure Kdenlive --> Project Defaults`)."
msgstr ""

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:94
msgid ""
":guilabel:`External proxy clips` When enabled it reads the proxy clips "
"generated by your video camera. More details see: :ref:"
"`using_camcorder_proxy_clips`."
msgstr ""

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:98
msgid "Metadata Tab"
msgstr ""

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:100
msgid "Screenshots below show the **Metadata** tab of **Kdenlive**."
msgstr ""

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:103
msgid ""
"Metadata set up here will be written to the files rendered from the project "
"if :ref:`render` is checked in File Rendering."
msgstr ""

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:109
msgid "Project Files Tab"
msgstr ""

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:115
msgid "From here you can export the project files data."
msgstr ""

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:118
msgid ""
"If you want to remove unused files from your project use Project >  :ref:"
"`clean_project`."
msgstr ""

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:122
msgid "Cache Data Tab"
msgstr ""

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:129
msgid ""
"The Cache data tab shows the data used in the project including the timeline "
"preview, proxy clips, audio thumbnails, and video thumbnails."
msgstr ""

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:132
msgid ""
"You can click on the trashcan icon to clear the cache data for that category."
msgstr ""

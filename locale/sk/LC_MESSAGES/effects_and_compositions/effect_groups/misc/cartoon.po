# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Roman Paholik <wizzardsk@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-11-17 11:26+0100\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.08.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../effects_and_compositions/effect_groups/misc/cartoon.rst:13
msgid "Cartoon"
msgstr ""

#: ../../effects_and_compositions/effect_groups/misc/cartoon.rst:15
msgid "Contents"
msgstr "Obsah"

#: ../../effects_and_compositions/effect_groups/misc/cartoon.rst:17
msgid ""
"This is the `Frei0r cartoon <https://www.mltframework.org/plugins/"
"FilterFrei0r-cartoon/>`_ MLT filter."
msgstr ""

#: ../../effects_and_compositions/effect_groups/misc/cartoon.rst:19
msgid "Cartoonify video, do a form of edge detect."
msgstr ""

#: ../../effects_and_compositions/effect_groups/misc/cartoon.rst:21
msgid "https://youtu.be/92fI4znypEo"
msgstr "https://youtu.be/92fI4znypEo"

# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Roman Paholik <wizzardsk@gmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-05-02 00:38+0000\n"
"PO-Revision-Date: 2022-04-17 17:33+0200\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../importing_and_assets_management/projects_and_files/auto_save.rst:1
msgid "The Kdenlive User Manual"
msgstr "Používateľská príručka Kdenlive"

#: ../../importing_and_assets_management/projects_and_files/auto_save.rst:1
msgid ""
"KDE, Kdenlive, documentation, user manual, video editor, open source, free, "
"help, auto save, automatic saving"
msgstr ""

#: ../../importing_and_assets_management/projects_and_files/auto_save.rst:15
msgid "Auto Save"
msgstr "Automatické ukladanie"

#: ../../importing_and_assets_management/projects_and_files/auto_save.rst:17
msgid ""
"Autosaves are generated 3 seconds after the user do a undoable action, "
"**BUT** only if you don't do another action within these 3 seconds. These "
"autosaves are offered the first time after you open the project again in "
"case the autosave is newer than the last saved project version. Autosaves "
"are stored in stale files, not in normal \\*.kdenlive files"
msgstr ""

# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2021, 2022.
# Ronald Stroethoff <stroe43@zonnet.nl>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-16 00:22+0000\n"
"PO-Revision-Date: 2022-07-16 12:00+0200\n"
"Last-Translator: Ronald Stroethoff <stroe43@zonnet.nl>\n"
"Language-Team: vertalen\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.04.2\n"

#: ../../glossary/useful_information/compositing_with_transparency.rst:13
msgid "Compositing with transparency"
msgstr "Compositie met transparantie"

#: ../../glossary/useful_information/compositing_with_transparency.rst:15
msgid ""
"When transparency is involved in both frames processed by one of Kdenlive’s "
"dynamic compositing transitions, the outcome may be surprising to some of us "
"users. Luckily, things aren’t as inexplicable as they appear, so let’s dive "
"right into transparency and transitions…"
msgstr ""
"Als transparantie van toepassing is bij beide frames die bewerkt worden door "
"een van Kdenlive’s dynamische compositie overgangen, dan kan het resultaat "
"verrassend zijn voor sommige van onze gebruikers. Gelukkig zijn de dingen "
"niet zo onverklaarbaar als dat ze lijken, laten we daarom in de "
"transparantie en de overgangen duiken…"

#: ../../glossary/useful_information/compositing_with_transparency.rst:18
msgid "Transparency & Dynamic Transitions"
msgstr "Transparantie & dynamische overgangen"

#: ../../glossary/useful_information/compositing_with_transparency.rst:20
msgid ""
"When it comes to handling transparency, Kdenlive’s :ref:"
"`kdenlive_transitions` fall into two categories:"
msgstr ""
"Wanneer het gaat over behandeling van transparantie, valt :ref:"
"`kdenlive_transitions` van Kdenlive uiteen in twee categorieën:"

#: ../../glossary/useful_information/compositing_with_transparency.rst:22
msgid "**Affine:** uses the so-called **atop** compositing operator."
msgstr "**Affine:** gebruikt de zogenaamde **atop** compositiebewerking."

#: ../../glossary/useful_information/compositing_with_transparency.rst:23
msgid ""
"**Composite**, **Composite & Transform**, **Cairo (Affine) Blend**: uses the "
"**over** compositing operator instead."
msgstr ""
"**Composite**, **Composite & Transformatie**, **Cairo (Affine) menging**: "
"gebruikt in plaats daarvan de **over** compositiebewerking."

#: ../../glossary/useful_information/compositing_with_transparency.rst:25
msgid ""
"So what does these **atop** and **over** really mean? Let’s discover what "
"these compositing operators do using a typical title and semi-transparent "
"background example. For simplicity, we first will ignore the **Opacity** "
"parameter of the aforementioned transitions to keep things easier to grasp. "
"And after we’ve mastered this step, we will finally look at how the opacity "
"parameter fits into the full picture."
msgstr ""
"Wat betekenen deze **atop** en **over** in werkelijkheid? Laten we het "
"hebben over wat deze compositie-bewerkingen bijvoorbeeld doen met een "
"standaard titel en half transparante achtergrond. Ter vereenvoudiging "
"negeren we eerst de parameter **Dekking** van de eerder genoemde overgangen "
"om dingen gemakkelijker te begrijpen. En nadat we deze stap beheersen, "
"zullen we tenslotte kijken naar hoe de parameter dekking past in het "
"volledige plaatje."

#: ../../glossary/useful_information/compositing_with_transparency.rst:29
msgid "Affine: Atop Compositing"
msgstr "Affine: Atop Compositie"

#: ../../glossary/useful_information/compositing_with_transparency.rst:38
msgid ""
"The result of the **atop** compositing is best explained at first by showing "
"its outcome: Text from the topmost title clip (partly) vanishes in those "
"regions where the underlying clip on the middle track is (semi) transparent."
msgstr ""
"Het resultaat van de **atop** compositie kan het best uitgelegd worden als "
"we eerst het resultaat bekijken: Tekst van de bovenliggende titelclip "
"verdwijnt (gedeeltelijk) in die vlakken waar de onderliggende clip op de "
"middelste track (semi) transparent is."

#: ../../glossary/useful_information/compositing_with_transparency.rst:40
msgid ""
"The checkerboard clip on the bottom track just serves for illustrational "
"purposes, so we can better see the transparent regions."
msgstr ""
"De clip met het dambordpatroon op de onderste track is er alleen voor een "
"illustratie-doel zodat we beter de transparante vlakken kunnen zien."

#: ../../glossary/useful_information/compositing_with_transparency.rst:42
msgid ""
"Please notice how the two **Affine** transitions are chained: first, the "
"upper Affine composes the title clip onto the gradient background. Then, the "
"lower Affine composes the result onto the checkboard background."
msgstr ""
"Merk op hoe de twee **Affine** overgangen zijn gekoppeld: eerst combineert "
"de bovenliggende Affine de titelclip met de kleurverloop op de achtergrond. "
"Waarna de onderliggende Affine het resultaat combineert met het dambord-"
"patroon op de achtergrond."

#: ../../glossary/useful_information/compositing_with_transparency.rst:51
msgid ""
"But how does the **Affine** transition exactly handle transparency? Let’s "
"look at this hand-made illustration, where we have two pixels with some "
"transparency each (the alpha values, αA and αB). It’s important to **not** "
"confuse pixel transparency with the opaque parameter of transitions."
msgstr ""
"Maar hoe gaat de **Affine** overgang nu precies om met de transparantie? "
"Laten we dat aan de hand van deze handgemaakte illustratie gaan bekijken, "
"waar we twee pixels hebben met elk een beetje transparantie (de alfa "
"waarden, αA en αB). Het is belangrijk om **niet** de pixel-transparantie te "
"verwarren met de dekking-parameter van de overgangen."

#: ../../glossary/useful_information/compositing_with_transparency.rst:53
msgid ""
"Our red pixel comes from the upper track frame (B), whereas the green pixel "
"stems from the lower track frame (A). An alpha of 0 means a completely "
"transparent pixel, whereas an alpha of 1.0 (or 255) means totally opaque "
"pixel."
msgstr ""
"Onze rode pixel komt van de bovenliggende track frame (B), waar de groene "
"pixel afkomstig is van de onderliggende track frame (A). Een alfa van 0 "
"betekent een compleet transparante transparent pixel, terwijl een alfa van "
"1.0 (of 255) betekent een totaal dekkende pixel."

#: ../../glossary/useful_information/compositing_with_transparency.rst:55
msgid ""
"The **Affine** transition now simply ignores the transparency information "
"from the upper frame pixel (the red one)."
msgstr ""
"De **Affine** overgang negeert nu gewoon de transparantie-informatie van de "
"bovenliggende frame pixel (de rode)."

#: ../../glossary/useful_information/compositing_with_transparency.rst:57
msgid ""
"It solely uses the transparency information (green) contained in the lower "
"track frames! Or more mathematical: α = αA"
msgstr ""
"Het gebruikt uitsluitend de transparantie-informatie (groen) aanwezig in de "
"onderliggende track frames! Of meer wiskundig: α = αA"

#: ../../glossary/useful_information/compositing_with_transparency.rst:59
msgid ""
"And that’s the explanation why in our example above the title text vanishes "
"in those regions where the middle clip is transparent."
msgstr ""
"En dat is de verklaring waarom in ons voorbeeld hierboven de titel verdwijnt "
"in die vlakken waar de middelste clip transparant is."

#: ../../glossary/useful_information/compositing_with_transparency.rst:65
msgid "Composite & Co: Over Compositing"
msgstr "Composite & Co: Over Composities"

#: ../../glossary/useful_information/compositing_with_transparency.rst:74
msgid ""
"Again, we basically use the same setup as before, but this time we use "
"**Composite & Transform** in place of **Affine** transitions. We chain the "
"transitions as before, so that the lower transition works on the outcome of "
"the upper transition."
msgstr ""
"We hebben opnieuw dezelfde opstelling of hiervoor, maar deze keer gebruiken "
"we **Compositie & transformatie** in plaats van de **Affine** overgangen. We "
"koppelen net als eerder de overgangen, zodat de onderliggende overgang het "
"resultaat van de bovenliggende overgang verwerkt."

#: ../../glossary/useful_information/compositing_with_transparency.rst:76
msgid ""
"But look! The outcome is different! The result of the **over** operator is "
"probably more to the expectations of most users. Now, the top title clip "
"doesn’t get (*sorry*) clipped anymore."
msgstr ""
"Maar kijk! Het resultaat is anders! Het resultaat van de **over** operator "
"is waarschijnlijk meer overeenkomstig de verwachtingen van de meeste "
"gebruikers. Nu wordt de bovenste titelclip niet meer (*sorry*) geclipd."

#: ../../glossary/useful_information/compositing_with_transparency.rst:85
msgid ""
"As the **over** operation produces results more to the expectations of many "
"users, how does it handle transparency exactly?"
msgstr ""
"Omdat de **over** bewerking resultaten produceert die meer overeenkomen met "
"de verwachtingen van veel gebruikers, hoe hanteert het de transparantie "
"exact?"

#: ../../glossary/useful_information/compositing_with_transparency.rst:87
msgid ""
"**Simply spoken:** if you paint some semi-transparent pixel over another "
"semi-transparent pixel, then **the result will be less transparent**. It "
"will be even less transparent than the lower frame pixel transparency. This "
"is what we would probably expect from painting with semi-opaque paint."
msgstr ""
"**Eenvoudig gezegd:** als u een semi-transparante pixel over een andere semi-"
"transparante pixel plaatst, dan **zal het resultaat minder transparant "
"zijn**. Het zal zelfs minder transparant zijn dan de onderliggende frame "
"pixel transparantie. Dit is wat we waarschijnlijk zouden verwachten van het "
"schilderen met semi-dekkende verf."

#: ../../glossary/useful_information/compositing_with_transparency.rst:89
msgid ""
"Calculating the resulting transparency for two given pixels from an upper "
"frame (B) and a lower frame (A) is thus more involved this time."
msgstr ""
"Voor het berekenen van de resulterende transparantie van twee opgegeven "
"pixels van een bovenliggende frame (B) en een onderliggende frame (A) is is "
"deze keer meer nodig."

#: ../../glossary/useful_information/compositing_with_transparency.rst:91
msgid ""
"Again, we start with the transparency of the lower frame pixel (green) as a "
"base. But this time, we can’t ignore the transparency of the (red) upper "
"frame pixel. But simply adding both transparencies wouldn’t yield sensible "
"results; what does a transparency of 2.0 or 511 mean?"
msgstr ""
"Opnieuw starten we met de transparantie van de onderliggende frame pixel "
"(groen) als basis. Maar deze keer kunnen we de transparantie van (rode) "
"bovenste frame pixel niet negeren. Maar het gewoon bij elkaar optellen van "
"beide transparanties zal geen zinvolle resultaten geven; wat betekent de "
"transparantie van 2.0 of 511?"

#: ../../glossary/useful_information/compositing_with_transparency.rst:93
msgid ""
"So the red pixel transparency is **scaled**: you may think of shrinking the "
"original 0-1.0 scale into the available space above the green transparency "
"value. The resulting transparency then is the sum of the green base alpha "
"value, plus the rescaled red alpha value. Remember, we kind of paint over "
"the lower frame. For the formular-affines: α = αA + αB (1-αA)"
msgstr ""
"Daarom is de transparantie van de rode pixel **verschaald**: u kan daarbij "
"denken aan het krimpen van de originele 0-1.0 schaal in de beschikbare "
"ruimte boven de groene transparantie waarde. De resulterende transparantie "
"is dan de som van de groene basis alfa waarde, plus de verschaalde rode alfa "
"waarde. Vergeet niet dat we min of meer over het onderliggende frame "
"schilderen. Voor de formule-liefhebbers: α = αA + αB (1-αA)"

#: ../../glossary/useful_information/compositing_with_transparency.rst:98
msgid "Transition Parameter Opacity"
msgstr "Overgang parameter Dekking"

#: ../../glossary/useful_information/compositing_with_transparency.rst:100
msgid ""
"The dynamic compositing transitions also feature a parameter called "
"**Opacity**. It’s easy to confuse it with pixel transparency, but it’s "
"something different. The opacity parameter applies to the upper frames (B) "
"only. It is kind of a master control … but how does it work?"
msgstr ""
"De dynamische compositie overgangen hebben ook een parameter genaamd "
"**Dekking**. U kunt deze makkelijk verwarren met de pixel-transparantie, "
"maar het is toch echt iets heel anders. De dekking parameter is alleen van "
"toepassing op de bovenliggende frames (B). Het is een soort van hoofdknop … "
"maar hoe werkt het? "

#: ../../glossary/useful_information/compositing_with_transparency.rst:109
msgid ""
"Let’s start with the **Affine** transition, again. Above, we’ve seen that "
"Affine ignores the transparency of the (red) upper frame pixels. And Affine "
"also **ignores** the **Opacity** parameter when calculating the result "
"**pixel transparency**."
msgstr ""
"Laten we starten met de **Affine** overgang, opnieuw. Hierboven hebben we "
"gezien dat Affine de transparantie van de (rode) bovenliggende frame pixels "
"negeert. En Affine **negeert** ook de **dekking** parameter bij het "
"berekenen van de resulterende **pixel transparantie**."

#: ../../glossary/useful_information/compositing_with_transparency.rst:111
msgid ""
"Instead, the Opacity parameter influences how the result **pixel color** "
"gets calculated: this is a linear interpolation between the color of the "
"upper frame (B) pixel and the lower frame (A) pixel. The Opacity now "
"controls the exact blending point. An opacity of 0.0 results in only the "
"lower frame (B) pixel color, while 1.0 solely yields the upper frame pixel "
"(A) color. 0.5 would be halfway between the two pixel colors."
msgstr ""
"In plaats daarvan beïnvloed de dekking-parameter hoe het resultaat "
"**pixelkleur** wordt berekent: dit is een lineaire interpolatie tussen de "
"kleur van de bovenliggende frame (B) pixel en de onderliggende frame (A) "
"pixel. De dekking regelt het exacte mengpunt. Een dekking van 0.0 resulteert "
"in alleen de onderliggende frame (B) pixelkleur, terwijl 1.0 alleen de "
"bovenliggende frame (A) pixelkleur neemt. 0.5 zal halverwege de twee "
"pixelkleuren zijn."

#: ../../glossary/useful_information/compositing_with_transparency.rst:113
msgid ""
"To sum up: in case of the **Affine** transition, **the Opacity parameter "
"solely affects color blending** between upper frame (B) and lower frame (A) "
"pixels."
msgstr ""
"Om het samen te vatten: in het geval van de **Affine** overgang, beïnvloedt "
"**de Dekking parameter alleen de kleurmenging** tussen de bovenliggende "
"frame (B) en de onderliggende frame (A) pixels."

#: ../../glossary/useful_information/compositing_with_transparency.rst:122
msgid "Next, let’s look at the other transitions: **Composite** & Co."
msgstr ""
"Laten we als volgende kijken naar de andere overgangen: **Composite** & Co."

#: ../../glossary/useful_information/compositing_with_transparency.rst:124
msgid ""
"As for the color blending, the same procedure applies that we’ve just seen "
"with the Affine transition: the Opacity parameter controls how much upper "
"frame (B) pixel color gets into the blend."
msgstr ""
"Wat betreft het kleurenmengen, geldt dezelfde procedure die we net hebben "
"gezien bij de Affine overgang: de dekking-parameter regelt hoeveel de "
"bovenliggende frame (B) pixelkleur in het mengsel gaat."

#: ../../glossary/useful_information/compositing_with_transparency.rst:126
msgid ""
"But when we look at how the transparency of the resulting pixel gets "
"calculated, things are starting to look different. We’ve seen that the "
"transparency of the upper frame (B) pixel gets scaled down in reverse "
"proportion of the lower frame (A) pixel transparency. The **Opacity** "
"parameter takes this even further: it additionally scales down the upper "
"frame (B) pixel transparency. Please see also the illustration."
msgstr ""
"Maar als we kijken hoe de transparantie van de resulterende pixels wordt "
"berekent, dan gaat het er een beetje anders uitzien. We hebben gezien dat de "
"transparantie van de bovenliggende frame (B) pixel wordt verschaalt in "
"omgekeerde verhouding met de onderliggende frame (A) pixel transparantie. De "
"**Dekking** parameter neemt zelfs nog verder: het verschaalt ook de "
"bovenliggende frame (B) pixel transparantie. Zie hiervoor de illustratie."

#: ../../glossary/useful_information/compositing_with_transparency.rst:128
msgid ""
"In consequence, the Opacity parameter thus controls how much opacity of an "
"upper frame (B) pixel is applied at all. This way, you can fade in or out "
"the upper frame."
msgstr ""
"Als consequentie daarvan, regelt de Dekking parameter hoeveel dekking van de "
"bovenliggende frame (B) pixel wordt toegepast. Op deze manier kan u fade in "
"of fade out op de bovenliggende frame creëren."

#: ../../glossary/useful_information/compositing_with_transparency.rst:130
msgid ""
"To sum up: in case of the **Composite** (&Co) transitions, **the Opacity "
"parameter affects both color blending** of upper frame (B) and lower frame "
"(A) pixels, **as well as transparency of the upper frame (B)**."
msgstr ""
"Samengevat: in het geval van de **Composite** (&Co) overgangen, beïnvloedt "
"**de Dekking parameter zowel het kleurenmengen** van de bovenliggende frame "
"(B) en de onderliggende frame (A) pixels, **als ook de transparantie van de "
"bovenliggende frame (B)**."

#: ../../glossary/useful_information/compositing_with_transparency.rst:136
msgid "Useful References"
msgstr "Handige Referenties"

#: ../../glossary/useful_information/compositing_with_transparency.rst:138
msgid ""
"Wikipedia article on `alpha compositing <https://en.wikipedia.org/wiki/"
"Alpha_compositing>`_, with a reference to the original Porter Duff "
"SIGGRAPH’84 paper on «Compositing Digital Images»."
msgstr ""
"Wikipedia artikel over `alpha compositie <https://en.wikipedia.org/wiki/"
"Alpha_compositing>`_, met een referentie naar de originele Porter Duff "
"SIGGRAPH’84 paper over «Compositing Digital Images»."

#: ../../glossary/useful_information/compositing_with_transparency.rst:139
msgid ""
"the SVG Open 2015 paper by Craig Northway on `understanding compositing and "
"color extensions in SVG 1.2 in 30 minutes! <http://www.graphicalweb.org/2005/"
"papers/abstractsvgopen/index.html>`_ – especially the Porter Duff operator "
"table in chapter 6 with resulting alpha calculation column."
msgstr ""
"Het SVG Open 2015 paper door Craig Northway over `understanding compositing "
"and color extensions in SVG 1.2 in 30 minutes! <http://www.graphicalweb."
"org/2005/papers/abstractsvgopen/index.html>`_ – met name de Porter Duff "
"operator tabel in hoofdstuk 6 met de resulterende alpha berekening kolom."

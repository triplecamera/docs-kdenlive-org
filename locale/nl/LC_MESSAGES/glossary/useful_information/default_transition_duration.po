# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-17 00:21+0000\n"
"PO-Revision-Date: 2022-02-19 11:45+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.2\n"

#: ../../glossary/useful_information/default_transition_duration.rst:13
msgid "Configuring the Default Transition Duration"
msgstr "De standaard transitieduur configureren"

#: ../../glossary/useful_information/default_transition_duration.rst:17
msgid ""
"You can now configure the **default duration for all newly created "
"transitions**. Before this, all new transitions were always 65 frames long – "
"and this translated to varying default durations, depending on a project’s "
"frame rate. This new configuration option should appeal all those users who "
"work a lot with transitions."
msgstr ""
"U kunt nu de **standaard duur voor alle nieuw aangemaakte overgangen** "
"configureren. Hiervoor waren alle nieuwe overgangen altijd 65 frames lang – "
"en dit vertaalde zich in variërende standaard tijdsduren, afhankelijk van de "
"framesnelheid van het project. Deze nieuwe configuratie-optie zou "
"aantrekkelijk moeten zijn voor alle gebruikers die veel met overgangen "
"werken."

#: ../../glossary/useful_information/default_transition_duration.rst:25
msgid ""
"Go to :menuselection:`Settings --> Configure Kdenlive --> Misc` category. "
"Under the heading Default Durations you’ll now find the new fifth option to "
"configure the default duration for newly created transitions. Enter a "
"duration in the usual format hh:mm:ss:ff."
msgstr ""
"Ga naar categorie :menuselection:`Instellingen --> Kdenlive configureren--> "
"Overige`. Onder de kop Standaard tijdsduren zult u nu de nieuwe vijfde optie "
"moeten vinden om de standaard tijdsduur voor nieuw aangemaakte overgangen te "
"configureren. Voer een tijdsduur in het gebruikelijke formaat, hh:mm:ss:ff, "
"in."

#: ../../glossary/useful_information/default_transition_duration.rst:27
msgid ""
"Please note that the frames (:ff) field will be interpreted on the basis of "
"the current project’s framerate. In contrast, the other fields hh:mm:ss are "
"independent of the framerate."
msgstr ""
"Merk op dat het veld frames (:ff) geïnterpreteerd zal worden op basis van de "
"huidige framesnelheid van het project. In tegenstelling tot de andere velden "
"hh:mm:ss, die zijn onafhankelijk van de framesnelheid."

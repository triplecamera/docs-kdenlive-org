# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-02 00:49+0000\n"
"PO-Revision-Date: 2023-01-02 11:50+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.0\n"

#: ../../glossary/useful_information.rst:1
msgid "Editing in Kdenlive video editor"
msgstr "Bewerken de videobewerker Kdenlive"

#: ../../glossary/useful_information.rst:1
msgid ""
"KDE, Kdenlive, useful information, editing, timeline, documentation, user "
"manual, video editor, open source, free, learn, easy"
msgstr ""
"KDE, Kdenlive, nuttige informatie, bewerken, tijdlijn, documentatie, "
"gebruikershandleiding, videobewerker, open-source, vrij, leren, gemakkelijk"

#: ../../glossary/useful_information.rst:22
msgid "Useful Information"
msgstr "Nuttige informatie"

#: ../../glossary/useful_information.rst:24
msgid "Contents:"
msgstr "Inhoud:"

#~ msgid ""
#~ "`HOWTO Produce 4k and 2K videos, YouTube compatible <https://forum.kde."
#~ "org/viewtopic.php?f=272&t=124869#p329129>`_"
#~ msgstr ""
#~ "`HOE 4k en 2K video's produceren, YouTube compatibel <https://forum.kde."
#~ "org/viewtopic.php?f=272&t=124869#p329129>`_"

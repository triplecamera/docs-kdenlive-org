# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-15 00:37+0000\n"
"PO-Revision-Date: 2022-01-15 11:56+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.1\n"

#: ../../getting_started/quickstart.rst:1
msgid "Do your first steps with Kdenlive video editor"
msgstr "Zet uw eerste stappen met de Kdenlive videobewerker"

#: ../../getting_started/quickstart.rst:1
msgid ""
"KDE, Kdenlive, quick start, first steps, video editor, help, learn, easy"
msgstr ""
"KDE, Kdenlive, snelle start, eerste stappen, videobewerker, hulp, leren, "
"gemakkelijk"

#: ../../getting_started/quickstart.rst:31
msgid "Quick Start"
msgstr "Snelstart"

#: ../../getting_started/quickstart.rst:34
msgid "Contents"
msgstr "Inhoud"

#: ../../getting_started/quickstart.rst:37
msgid "Creating a new project"
msgstr "Een nieuw project starten"

#: ../../getting_started/quickstart.rst:44
msgid "Kdenlive directory structure"
msgstr "Structuur van Kdenlive mappen"

#: ../../getting_started/quickstart.rst:46
msgid ""
"The first step is creating a new (empty) folder for our new project. I will "
"call it :file:`quickstart-tutorial/` in this tutorial. Then get some sample "
"video clips, or download them from here :download:`kdenlive-tutorial-"
"videos-2011-avi.zip </files/Kdenlive-tutorial-videos-2011-avi.zip>` (7 MB) "
"[1]_ , and extract them to e.g. a :file:`quickstart-tutorial/Videos/` "
"subfolder inside the project folder."
msgstr ""
"De eerste stap is het aanmaken van een nieuwe (lege) map voor ons nieuwe "
"project. Ik zal het in deze inleiding :file:`quickstart-tutorial/` noemen. "
"Haal dan enige voorbeeldvideoclips op, of download ze vanaf hier :download:"
"`kdenlive-tutorial-videos-2011-avi.zip </files/Kdenlive-tutorial-videos-2011-"
"avi.zip>` (7 MB) [1]_  en pak ze uit naar bijv. een submap :file:`quickstart-"
"tutorial/Videos/` in de projectmap."

#: ../../getting_started/quickstart.rst:49
msgid ""
"The image on the left shows the suggested directory structure: Each project "
"has its own directory, with video files in the :file:`Videos` subdirectory, "
"audio files in the :file:`Audio` directory, etc. (:ref:`projects_and_files`)"
msgstr ""
"De afbeelding links toont de gesuggereerde mapstructuur: elk project heeft "
"zijn eigen map, met videobestanden in de submap :file:`Videos`, "
"audiobestanden in de map :file:`Audio`, etc. (:ref:`projects_and_files`)"

#: ../../getting_started/quickstart.rst:51
msgid ""
"(The tutorial from now on assumes that you use the sample videos provided, "
"but it works with any.)"
msgstr ""
"(In deze tutorial wordt ervan uitgegaan dat de gedownloade video's worden "
"gebruikt, maar andere videos kunnen ook gebruikt worden.)"

#: ../../getting_started/quickstart.rst:58
msgid "New Project dialog"
msgstr "Nieuw Project dialoog"

#: ../../getting_started/quickstart.rst:61
msgid ""
"Open **Kdenlive** and create a new project :menuselection:`File --> New`."
msgstr ""
"Open **Kdenlive** en maak een nieuw project aan :menuselection:`Bestand --> "
"Nieuw`."

#: ../../getting_started/quickstart.rst:64
msgid ""
"Choose the previously created project folder (:file:`quickstart-tutorial/`) "
"and select an appropriate project profile. The video files provided above "
"are 720p, 23.98 fps. [2]_  If you are using your own files and don’t know "
"which one to use, **Kdenlive** will suggest an appropriate one when the "
"first clip is added  [3]_  , so you can leave the field on whatever it is."
msgstr ""
"Kies de eerder aangemaakte projectmap (:file:`quickstart-tutorial/`) en "
"selecteer een toepasselijk projectprofiel. De boven geleverde videobestanden "
"zijn 720p, 23.98 fps. [2]_  Als u uw eigen bestanden gebruikt en niet weet "
"welk profiel te gebruiken, zal **Kdenlive** een toepasselijke suggereren "
"wanneer de eerste clip is toegevoegd  [3]_  , u kunt het veld dus laten op "
"wat het is."

#: ../../getting_started/quickstart.rst:67
msgid ""
"If you like you can change to the dark theme: :menuselection:`Settings --> "
"Colour Theme` i.e Breeze-Dark"
msgstr ""
"Als u dat prettig vindt kunt u naar het donkere thema gaan: :menuselection:"
"`Instellingen --> Kleurenthema` d.w.z. Breeze-Dark"

#: ../../getting_started/quickstart.rst:71
msgid "Adding clips"
msgstr "Clips toevoegen"

#: ../../getting_started/quickstart.rst:77
msgid "Project Bin: Adding video clips"
msgstr "Project-bin: een videoclip toevoegen"

#: ../../getting_started/quickstart.rst:79
msgid ""
"Now that the project is ready, let’s start adding some clips (i.e. the ones "
"you downloaded). This works via the *Project Bin widget*; a click on the :"
"menuselection:`Add Clip or Folder` icon |kdenlive-add-clip| directly opens "
"the file dialog, a click on the small arrow shows a list of additional clip "
"types that can be added as well. Video clips, audio clips, images, and other "
"**Kdenlive** projects can be added via the default :menuselection:`Add Clip "
"or Folder` dialog."
msgstr ""
"Nu het project gereed is, laten we beginnen met het toevoegen van enige "
"clips (d.w.z. die u hebt gedownload). Dit werkt via het *Widget Project-"
"bin*; een klik op het pictogram :menuselection:`Clip of map toevoegen` |"
"kdenlive-add-clip| opent direct de bestandsdialoog, een klik op de kleine "
"pijl toont een lijst met extra typen clips die ook toegevoegd kunnen worden. "
"Videoclips, audioclips, afbeeldingen en andere **Kdenlive** projecten "
"toegevoegd worden via de standaard dialoog :menuselection:`Clip of map "
"toevoegen`."

#: ../../getting_started/quickstart.rst:88
msgid "Kdenlive window with the tutorial files"
msgstr "Kdenlive venster met de inleidingsbestanden"

#: ../../getting_started/quickstart.rst:90
msgid ""
"After loading the clips, **Kdenlive** will look similar to this. On the top "
"left there is the already known Project Bin. Right of it are the monitors "
"that show video; The clip monitor displays video from the original clips, "
"the project monitor shows how the output video will look, with all effects, "
"transitions, etc. applied. The third, also very important, item is the "
"timeline (below the monitors): This is the place where the video clips will "
"be edited. There are two different types of tracks, Video and Audio. Video "
"tracks can contain any kind of clip, audio tracks as well – but when "
"dropping a video file to the audio track, only the audio will be used."
msgstr ""
"Nadat de clips zijn toegevoegd, zal **Kdenlive** er uitzien zoals dit. "
"Linksboven is de al bekende project-bin, Rechts daarvan zijn monitors die "
"video's laten zien; De clip monitor laat de originele video zien, de "
"projectmonitor laat het resultaat zien, met alle effecten, overgangen, etc. "
"De derde ook erg belangrijk, onderdeel de tijdlijn (onder de monitors): dit "
"is de plek waar de video clips bewerkt zullen worden. Hier zijn twee "
"verschillende soorten tracks, Video en Audio. Video tracks kunnen elk soort "
"clip bevatten, audio tracks kunnen dat ook - maar wanneer een video bestand "
"in een audio track wordt gestopt, dan zal alleen de audio worden gebruikt."

#: ../../getting_started/quickstart.rst:98
msgid "Saving a Kdenlive project"
msgstr "Een Kdenlive project opslaan"

#: ../../getting_started/quickstart.rst:101
msgid ""
"Let’s save the work via :menuselection:`File --> Save`. This saves our "
"project, i.e. where we placed the clips on the timeline, which effects we "
"applied, and so on. It can *not* be played. [4]_  The process of creating "
"the final video is called *Rendering*."
msgstr ""
"Laten we het werk opslaan via :menuselection:`Bestand --> Opslaan`. Dit "
"slaat ons project op, d.w.z. waar we de clips op de tijdlijn hebben "
"geplaatst, welke effecten we hebben toegepast, enzovoort. Het kan *niet* "
"afgespeeld worden. [4]_  Het proces van maken van de uiteindelijke video "
"wordt *Renderen* genoemd."

#: ../../getting_started/quickstart.rst:106
msgid "Timeline"
msgstr "Tijdlijn"

#: ../../getting_started/quickstart.rst:108
msgid "See also :ref:`timeline`"
msgstr "Zie ook :ref:`timeline`"

#: ../../getting_started/quickstart.rst:110
msgid ""
"Now comes the actual editing. Project clips are combined to the final result "
"on the timeline.  They get there by drag and drop: Drag some Napoli "
"(assuming you are using the files provided above, as in the rest of this "
"quick start tutorial; If not, please make sure your screen is waterproof, "
"and perhaps tomatoproof) from the project bin, and drop it onto the first "
"track in the timeline. In this case track V2."
msgstr ""
"Nu komt het eigenlijke bewerken. De clips worden gecombineerd op de tijdlijn "
"om tot het uiteindelijke resultaat te komen. Ze kunnen gewoon daar naar toe "
"gesleept worden: sleep enkele Napoli (aangenomen dat de bovenstaand "
"gedownloade bestanden worden gebruikt, zoals in de rest van deze "
"snelstartinleiding; zo niet, zorg dat uw scherm waterproof of tomaatproof "
"is) uit de project-bin en laat ze los op de eerste track in de tijdlijn. In "
"dit geval track V2."

#: ../../getting_started/quickstart.rst:117
msgid "First clips in the timeline"
msgstr "De eerste clips in de tijdlijn"

#: ../../getting_started/quickstart.rst:120
msgid ""
"Since some cutlery is needed as well, grab the spoon clip and drop it on the "
"first track as well (track V2). Then drag the Napoli to the beginning of the "
"timeline (otherwise the rendered video would start with some seconds of "
"plain black), and the Spoon right after the Napoli, such that it looks like "
"in the image on the left. (Where I have zoomed in with :kbd:`Ctrl + Wheel`.)"
msgstr ""
"Er is ook het nodige snijwerk nodig, laat ook de Spoonclip op de eerste "
"track vallen (track V2). Versleep de Napoli naar het begin van de tijdlijn "
"(anders begint de gerenderde video met enige seconden zwart) en de Spoon "
"onmiddellijk achter de Napoli, zoals het eruit ziet als de afbeelding links. "
"(Waar ik heb ingezoomd met :kbd:`Ctrl + Wheel`.)"

#: ../../getting_started/quickstart.rst:126
msgid "Timeline cursor"
msgstr "Tijdlijncursor"

#: ../../getting_started/quickstart.rst:129
msgid ""
"The result can already be previewed by pressing :kbd:`Space` (or the :"
"guilabel:`Play` button in the project monitor). You will see the Napoli "
"directly followed by a Spoon. If the timeline cursor is not at the "
"beginning, the project monitor will start playing somewhere in the middle; "
"you can move it by dragging it either on the timeline ruler or in the "
"project monitor. If you prefer keyboard shortcuts, :kbd:`Ctrl + Home` does "
"the same for the monitor that is activated. (Select the :menuselection:"
"`Project Monitor` if it is not selected yet before using the shortcut.)"
msgstr ""
"Het resultaat kan al bekeken worden door op :kbd:`Space` te drukken (of op "
"de knop :guilabel:`Afspelen` in de projectmonitor). Napoli wordt getoond, "
"onmiddellijk gevolgd door Spoon. Als de tijdlijncursor niet aan het begin "
"staat, zal de projectmonitor ergens in het midden starten met afspelen; dit "
"kan verandert worden door de de tijdsbalkcursor of die in de projectmonitor "
"te verslepen. Als u de voorkeur geeft aan sneltoetsen, :kbd:`Ctrl + Home` "
"doet hetzelfde voor de monitor die actief is. (Selecteer de :menuselection:"
"`Projectmonitor` als deze nog niet geselecteerd is voordat de sneltoets "
"wordt gebruikt.)"

#: ../../getting_started/quickstart.rst:136
msgid "Resize marker"
msgstr "Markering verschalen"

#: ../../getting_started/quickstart.rst:139
msgid ""
"Since after eating comes playing, there is a Billiards clip. Add it to the "
"timeline as well (track V1). For the first 1.5 seconds nothing happens in "
"the clip, so it should perhaps be **cut** to avoid the video becoming "
"boring. An easy way [5]_  for this is to move the timeline cursor to the "
"desired position (i.e. the position where you want to cut the video), then "
"drag the left border of the clip when the resize marker appears. It will "
"snap in at the timeline cursor when you move close enough."
msgstr ""
"Na het eten komt het spelen, er is een Biljardclip. Voeg ook deze toe aan de "
"tijdlijn (track V1). Er gebeurt de eerste 1,5 seconde niets in de clip, "
"daarom moet er misschien een **knip** gedaan worden, om te voorkomen dat de "
"video saai wordt. Een makkelijke manier [5]_  hiervoor is om de "
"tijdlijncursor naar de gewenste positie te slepen (d.w.z. naar de positie "
"waar u de video wilt knippen), sleep vervolgens de linker kant van de clip "
"totdat de marker voor grootte wijzigen verschijnt. Het klikt aan de "
"tijdlijncursor vast wanneer het er dichtbij genoeg is."

#: ../../getting_started/quickstart.rst:146
msgid "Overlapping clips"
msgstr "Overlappende clips"

#: ../../getting_started/quickstart.rst:149
msgid ""
"To add a *transition* between eating (the Spoon) and playing billiards, the "
"two clips need to overlap. To be precise: place the second clip above or "
"below the first one. The first clip should end some frames after the second "
"one begins. Zooming in until the ticks for single frames appear helps here; "
"it also makes it easy to always have the same transition duration, five "
"frames in this case."
msgstr ""
"Om een *overgang* toe te voegen tussen het eten (de Spoon) en biljard "
"spelen, hebben de twee clips een overlap nodig. Om precies te zijn: plaats "
"de tweede clip boven of onder de eerste clip. De eerste clip moet enkele "
"frames later eindigen dan waar de tweede begint. Inzoomen totdat de "
"markeringen van de afzonderlijke frames zichtbaar zijn zal hier helpen; het "
"maakt het ook makkelijker om altijd dezelfde tijdsduur voor een overgang te "
"krijgen, vijf frames in dit geval."

#: ../../getting_started/quickstart.rst:152
msgid ""
"You can zoom in by either using the :menuselection:`zoom slider` at the "
"bottom right corner of the **Kdenlive** window, or with :kbd:`Ctrl + "
"Mousewheel`. **Kdenlive** will zoom to the timeline cursor, so first set it "
"to the position which you want to see enlarged, then zoom in."
msgstr ""
"U kunt inzoomen door de :menuselection:`zoomschuif` in de rechtsonder hoek "
"van het **Kdenlive** venster te gebruiken, of met :kbd:`Ctrl + muiswiel`. "
"**Kdenlive** zal inzoomen bij de tijdlijncursor, breng deze eerst op de "
"positie die u wilt vergroten en zoom vervolgens in."

#: ../../getting_started/quickstart.rst:160
msgid "Transition marker"
msgstr "Overgangmarkering"

#: ../../getting_started/quickstart.rst:163
msgid ""
"Now that the clips overlap, the transition can be added. This is done either "
"by right-clicking on the upper clip and choosing :menuselection:`Insert a "
"Composition` and choose :menuselection:`Wipe` or, easier, by hovering the "
"mouse over the lower right corner of the Spoon clip until the pointing-"
"finger pointer is shown and the message \"Click to add composition\" "
"appears. The latter, by default, adds a wipe transition, which is in this "
"case the best idea anyway since the Spoon is not required for playing."
msgstr ""
"Nu de clips overlappen, kan de transitie toegevoegd worden. Dit wordt gedaan "
"ofwel met rechts klikken op de bovenste clip en :menuselection:`Een "
"compositie invoegen` en :menuselection:`Wipe` te kiezen of, gemakkelijker, "
"door met de muis te zweven boven de lagere rechter hoek van de Spoon clip "
"totdat de wijsvingeraanwijzer getoond wordt en de melding \"Klik om "
"compositie toe te voegen\" verschijnt. De laatste voegt, standaard, een wipe-"
"overgang toe, wat in dit geval het beste idee is omdat de Spoon niet vereist "
"is voor het spelen."

#: ../../getting_started/quickstart.rst:165
msgid ""
"The wipe transitions fades the first clip into the second one. See also :ref:"
"`transitions`."
msgstr ""
"De wipe-overgang laat de eerste clip overgaan in de tweede. Zie ook :ref:"
"`transitions`."

#: ../../getting_started/quickstart.rst:172
msgid ""
"Let’s now add the last clip, the Piano, and again apply a wipe transition. "
"When adding it on the first track of the timeline (track V2), you need to "
"click on the new clip’s lower left edge to add the transition to the "
"previous clip."
msgstr ""
"Laten we nu de laatste clip toevoegen, de Piano, en we passen opnieuw een "
"wipe-overgang toe. Nadat het aan de eerste track van de tijdlijn (track V2) "
"is toegevoegd, moet u op de linksonder hoek van de nieuwe clip klikken om de "
"overgang aan de vorige clip toe te voegen."

#: ../../getting_started/quickstart.rst:176
msgid "Effects"
msgstr "Effecten"

#: ../../getting_started/quickstart.rst:183
msgid "Effect List"
msgstr "Effectenlijst"

#: ../../getting_started/quickstart.rst:186
msgid ""
"The Piano can be colourized by adding an *effect* to it.  Click on the "
"effect view (if effect view is not visible enable the view: :menuselection:"
"`View --> Effects`). Type *rgb* in the search field then double-click the :"
"menuselection:`RGB Adjustment` effect."
msgstr ""
"De Piano kan gekleurd worden door er een *effect* aan toe te voegen.  Klik "
"op de weergave effect (als weergave effect niet zichtbaar is schakel dan "
"weergave in: :menuselection:`Beeld --> Effecten`). Typ *rgb* in het zoekveld "
"en dubbelklik op het effect :menuselection:`Aanpassen RGB`."

#: ../../getting_started/quickstart.rst:195
msgid ""
"Once the effect has been added, click on an empty part in the timeline and "
"you see its name on the timeline clip. It will also be shown in the :"
"menuselection:`Effect/Composition Stack` widget."
msgstr ""
"Nadat het effect is toegevoegd, klik op een leeg gedeelte op de tijdlijn en "
"u ziet zijn naam op de tijdlijnclip. Het zal ook getoond worden in het "
"widget :menuselection:`Effect/Compositiestapel`."

#: ../../getting_started/quickstart.rst:204
msgid "Effect Stack with RGB adjustment"
msgstr "Effecten stapel met RGB aanpassingen"

#: ../../getting_started/quickstart.rst:207
msgid ""
"To get a warm yellow-orange tone on the image, fitting the comfortable "
"evening, blue needs to be reduced and red and green improved."
msgstr ""
"Om de afbeelding een warm geel-oranje kleur te geven, passend bij een "
"comfortabele avond, moet er minder blauw en meer rood en groen komen."

#: ../../getting_started/quickstart.rst:209
msgid ""
"The values in the Effect/Composition Stack widget can be changed by using "
"the slider (middle mouse button resets it to the default value), or by "
"entering a value directly by double-clicking the number to the right of the "
"slider."
msgstr ""
"De waarden in het widget Effecten/Compositiestapel kunnen gewijzigd worden "
"via de schuif (de middelste muisknop zet het weer terug op de standaard "
"waarde), of door een waarde direct in te voeren door dubbel te klikken op "
"het getal rechts naast de schuif."

#: ../../getting_started/quickstart.rst:211
msgid ""
"The Effect/Composition Stack widget always refers to the timeline clip that "
"is currently selected. Each effect can be temporarily disabled by clicking "
"the eye icon, or all effects for that clip can be disabled using the check "
"box at the top of the Effect/Composition Stack widget (the settings are "
"saved though), this is e.g. useful for effects that require a lot of "
"computing power, so they can be disabled when editing and enabled again for "
"rendering."
msgstr ""
"Het widget Effecten/Compositiestapel verwijdt altijd naar de tijdlijnclip "
"die op dat moment is geselecteerd. Elk effect kan tijdelijk uitgeschakeld "
"worden door te klikken op het oogpictogram of alle effecten voor die clip "
"kunnen uitgeschakeld worden met het keuzevakje bovenaan het widget Effecten/"
"Compositiestapel (de instellingen worden echter opgeslagen), dit is bijv. "
"nuttig voor effecten die veel rekenkracht nodig hebben, zodat ze "
"uitgeschakeld kunnen worden bij bewerken en opnieuw ingeschakeld voor "
"rendering."

#: ../../getting_started/quickstart.rst:213
msgid ""
"For some effects, like the one used there, it is possible to add keyframes. "
"The framed watch icon indicates this. Keyframes are used for changing effect "
"parameters over time. In our clip this allows us to fade the piano’s colour "
"from a warm evening colour to a cold night colour."
msgstr ""
"Voor sommige effecten,zoals die we hier hebben gebruikt, is het mogelijk om "
"keyframes toe te voegen. Het stopwatch icoon in de afbeelding geeft dit aan. "
"Keyframes kunnen worden gebruikt om de effecten over de tijd een andere "
"instelling te geven. In onze clip maakt het mogelijk om de kleur van de "
"piano te laten veranderen van een warme avond kleur naar een koele nacht "
"kleur."

#: ../../getting_started/quickstart.rst:220
msgid "Keyframes for effects"
msgstr "Keyframes voor effecten"

#: ../../getting_started/quickstart.rst:223
msgid ""
"After clicking the :menuselection:`keyframe` icon (the clock icon framed in "
"the previous image), the Properties widget will re-arrange. By default there "
"will be two keyframes, one at the beginning of the timeline clip and one at "
"the end. Move the timeline cursor to the end of the timeline clip, such that "
"the project monitor actually shows the new colours when changing the "
"parameters of the keyframe at the end."
msgstr ""
"Na het klikken op het pictogram :menuselection:`keyframe` (het pictogram "
"stopwatch in de vorige afbeelding), zal het widget Eigenschappen opnieuw "
"worden geordend. Standaard zijn er twee keyframes, een aan het begin van de "
"clip in de tijdlijn en een aan het eind. Beweeg de tijdlijncursor naar het "
"eind van de clip op de tijdlijn, zodat de project monitor de nieuwe kleuren "
"laat zien wanneer de instellingen van de keyframe aan het eind worden "
"veranderd."

#: ../../getting_started/quickstart.rst:225
msgid ""
"Make sure the last keyframe is selected in the Properties list. Then you are "
"ready to flood the piano with a deep blue."
msgstr ""
"Zorg ervoor dat de laatste keyframe is geselecteerd in de lijst "
"Eigenschappen. Dan is alles klaar om de piano te overspoelen met een diep "
"blauw."

#: ../../getting_started/quickstart.rst:227
msgid ""
"Moving the timeline cursor to the beginning of the project and playing it "
"(with :kbd:`Space`, or the :guilabel:`Play` button in the :menuselection:"
"`Project Monitor`), the piano should now change the colour as desired."
msgstr ""
"Beweeg de tijdlijncursor naar het begin van het project en speel het af (met:"
"kbd:`Spatie`, of de knop :guilabel:`Afspelen` in het menu :menuselection:"
"`Projectmonitor`), de piano zou nu volgens wens van kleur moeten veranderen."

#: ../../getting_started/quickstart.rst:229
msgid ""
"Keyframing was the hardest part of this tutorial. If you managed to do it, "
"you will master **Kdenlive** easily!"
msgstr ""
"Keyframing was het moeilijkste gedeelte van deze handleiding. Als dit "
"beheerst wordt, dan is de rest van **Kdenlive** makkelijk!"

#: ../../getting_started/quickstart.rst:231
msgid "See also :ref:`effects`."
msgstr "Zie ook :ref:`effects`."

#: ../../getting_started/quickstart.rst:235
msgid "Music"
msgstr "Muziek"

#: ../../getting_started/quickstart.rst:242
msgid "Audio fadeout"
msgstr "Audio uitvagen"

#: ../../getting_started/quickstart.rst:245
msgid ""
"Since the clips do not provide any audio, let’s search for some nice piece "
"of music, from your local collection or on web pages like `Jamendo <http://"
"www.jamendo.com>`_. The audio clip should, after adding it, be dragged to an "
"audio track on the timeline."
msgstr ""
"Omdat de clips geen geluid leveren, laten we op zoek gaan naar wat leuke "
"muziek, uit uw eigen verzameling of op webpagina's zoals `Jamendo <http://"
"www.jamendo.com>`_. Nadat de audio clip is toegevoegd, moet deze ook in een "
"audiotrack op de tijdlijn gesleept worden."

#: ../../getting_started/quickstart.rst:248
msgid ""
"The audio clip can be resized on the timeline the same way as video clips "
"are. The cursor will snap in at the end of the project automatically. To add "
"a fade out effect at the end of the audio clip (except if you found a file "
"with exactly the right length) you can hover the top right (or left) edge of "
"the timeline clip and drag the red shaded triangle to the position where "
"fading out should start. [6]_"
msgstr ""
"De audio clip kan in lengte verandert worden net zoals dat met de video "
"clips is gedaan. De cursor zal automatisch vastklikken aan het eind van het "
"project. Om een uitvaageffect toe te voegen aan het eind van de audioclip "
"(behalve als de audio clip precies de juiste lengte heeft) kunt u boven de "
"rechtsboven (of linksboven) hoek van de tijdlijnclip zweven en de rode "
"driehoek met schaduw naar de plek slepen waar het uitvagen zou moeten "
"beginnen. [6]_"

#: ../../getting_started/quickstart.rst:252
msgid "Rendering"
msgstr "Renderen"

#: ../../getting_started/quickstart.rst:259
msgid "Rendering dialog"
msgstr "Renderingsdialoog"

#: ../../getting_started/quickstart.rst:262
msgid ""
"A few minutes left, and the project is finished! Click the Render button (or "
"go to :menuselection:`Project --> Render`, or press :kbd:`Ctrl + Enter`) to "
"get the dialog shown on the left. Select the desired output file for our new "
"video with all effects and transitions, choose MP4 (works nearly "
"everywhere), select the output file location and press the :menuselection:"
"`Render to File` button."
msgstr ""
"Nog een paar minuten en het project is klaar! Klik op de knop Render (of ga "
"naar :menuselection:`Project-->Renderen`, of druk op :kbd:`Ctrl + Enter`) om "
"de dialoog die links getoond wordt te krijgen. Selecteer het gewenste "
"uitvoerbestand voor uw nieuwe video met al zijn effecten en overgangen, kies "
"MP4 (werkt bijna altijd en overal), selecteer de locatie van het "
"uitvoerbestand en druk op de knop :menuselection:`Naar bestand renderen`."

#: ../../getting_started/quickstart.rst:271
msgid "Rendering progress"
msgstr "Voortgang van renderen"

#: ../../getting_started/quickstart.rst:273
msgid ""
"After some seconds rendering will be finished, and your first **Kdenlive** "
"project completed. Congratulations!"
msgstr ""
"De rendering is na enkele secondes klaar en de eerste **Kdenlive** project "
"is nu compleet. Gefeliciteerd!"

#: ../../getting_started/quickstart.rst:278
msgid "References and notes"
msgstr "Referenties en notities"

#: ../../getting_started/quickstart.rst:280
msgid ""
"If you prefer Theora (which you probably don’t since Ogg Video usually "
"causes problems), you can alternatively download :download:`kdenlive-"
"tutorial-videos-2011-ogv.tar.bz2 </files/kdenlive-tutorial-videos-2011-ogv."
"tar.bz2>`."
msgstr ""
"Als u Theora verkiest (wat u waarschijnlijk niet doet, omdat Ogg Video "
"gewoonlijk problemen veroorzaakt), kunt u als alternatief :download:"
"`kdenlive-tutorial-videos-2011-ogv.tar.bz2 </files/kdenlive-tutorial-"
"videos-2011-ogv.tar.bz2>` downloaden."

#: ../../getting_started/quickstart.rst:281
msgid ""
"`720 <http://en.wikipedia.org/wiki/720p>`_ is the video height, p stands for "
"`progressive scan <http://en.wikipedia.org/wiki/Progressive_scan>`_ in "
"contrast to `interlaced video <http://en.wikipedia.org/wiki/"
"Interlaced_video>`_, and the fps number denotes the number of full frames "
"per second."
msgstr ""
"`720 <http://en.wikipedia.org/wiki/720p>`_ is de videohoogte, p staat voor "
"`progressieve scan <http://en.wikipedia.org/wiki/Progressive_scan>`_ in "
"tegenstelling tot `interlaced video <http://en.wikipedia.org/wiki/"
"Interlaced_video>`_ en het fps-getal geeft het aantal volledige frames per "
"seconde aan."

#: ../../getting_started/quickstart.rst:282
msgid ""
"Provided Configure Kdenlive Settings under :ref:`configure_kdenlive` is set "
"to *Check if first added clip matches project profile*"
msgstr ""
"Aangenomen dat in Instellingen van Kdenlive onder :ref:`configure_kdenlive` "
"is gezet *Controleren of de eerste toegevoegde clip overeenkomt met het "
"projectprofiel*"

#: ../../getting_started/quickstart.rst:283
msgid ""
"To be correct, it *can* be played using ``melt yourproject.kdenlive``, but "
"this is not the way you would want to present your final video since it is "
"(most likely) too slow. Additionally, it only works if melt is installed."
msgstr ""
"Om het precies te maken, het *kan* afgespeeld worden door ``melt uwproject."
"kdenlive`` te gebruiken, maar dit is niet de manier waarop u uw "
"uiteindelijke video wilt vertonen omdat het (zeer waarschijnlijk) te "
"langzaam zal zijn. Ook werkt het alleen maar als melt is geïnstalleerd."

#: ../../getting_started/quickstart.rst:284
msgid ""
"Writing it this way suggests that there are several ways of cutting a clip. "
"This is in fact true."
msgstr ""
"Het op deze manier schrijven suggereert dat er verschillende manieren zijn "
"om in een clip te knippen. Dit is in feite waar."

#: ../../getting_started/quickstart.rst:285
msgid ""
"This shaded triangle is a shorthand for adding the effect :menuselection:"
"`Fade --> Fade out`. Both ways lead to the same result."
msgstr ""
"Deze driehoek met schaduw is een korte manier om het effect :menuselection:"
"`Invagen --> Uitvagen` toe te voegen`. Beide manieren geven het zelfde "
"resultaat."

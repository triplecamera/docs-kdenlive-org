# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-21 00:37+0000\n"
"PO-Revision-Date: 2022-01-02 14:48+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.0\n"

#: ../../user_interface/menu/project_menu/stop_motion_capture.rst:15
msgid "Stop Motion Capture"
msgstr "Opnemen van beweging stoppen"

#: ../../user_interface/menu/project_menu/stop_motion_capture.rst:17
msgid "Contents"
msgstr "Inhoud"

#: ../../user_interface/menu/project_menu/stop_motion_capture.rst:19
msgid ""
"Capture images a frame at a time from a camera plugged into the HDMI port of "
"a Linux-compatible capture card to create stop motion animation. You can "
"also transparently overlay the last captured frame on the monitor to easily "
"see the difference with current live feed."
msgstr ""
"Vang afbeeldingen frame-voor-frame uit een camera ingeplugd in de HDMI-poort "
"van een Linux-compatibele opnamekaart om animatie met schokkerige beelden te "
"maken. u kunt ook transparent het laatst opgenomen frame over de monitor "
"leggen om gemakkelijk het verschil te zien met het huidige livebeeld."

#: ../../user_interface/menu/project_menu/stop_motion_capture.rst:21
msgid ""
"See `j-b-m's blog <http://kdenlive.org/users/j-b-m/coming-soon-your-"
"desktop>`_ on this feature."
msgstr ""
"Zie `j-b-m's blog <http://kdenlive.org/users/j-b-m/coming-soon-your-"
"desktop>`_ over deze functie."

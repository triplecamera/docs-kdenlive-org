# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-03 00:38+0000\n"
"PO-Revision-Date: 2022-01-11 15:30+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.0\n"

#: ../../effects_and_compositions/effect_groups/colour_correction/bezier_curves.rst:10
msgid "Bezier Curves"
msgstr "Bezierkrommen"

#: ../../effects_and_compositions/effect_groups/colour_correction/bezier_curves.rst:12
msgid "Contents"
msgstr "Inhoud"

#: ../../effects_and_compositions/effect_groups/colour_correction/bezier_curves.rst:14
msgid ""
"This is the `Frei0r curves <https://www.mltframework.org/plugins/"
"FilterFrei0r-curves/>`_ MLT filter by Maksim Golovkin and Till Theato."
msgstr ""
"Dit is het MLT-filter `Frei0r krommen <http://www.mltframework.org/plugins/"
"FilterFrei0r-curves>`_ door Maksim Golovkin en Till Theato."

#: ../../effects_and_compositions/effect_groups/colour_correction/bezier_curves.rst:16
msgid "Adjusts luminance or color channel intensity with curve level mapping."
msgstr ""
"Past lichtheid of kleurkanaalintensiteit aan met mapping op krommeniveau."

#: ../../effects_and_compositions/effect_groups/colour_correction/bezier_curves.rst:20
msgid ""
"See `TheDiveo blog <http://thediveo-e.blogspot.de/2013/10/grading-of-hero-3-"
"above-waterline.html>`_ for an example of how to use this effect to colour "
"grade clips."
msgstr ""
"Zie `TheDiveo blog <http://thediveo-e.blogspot.de/2013/10/grading-of-hero-3-"
"above-waterline.html>`_ voor een voorbeeld van hoe dit effect op kleurwaarde "
"clips te gebruiken."

#: ../../effects_and_compositions/effect_groups/colour_correction/bezier_curves.rst:22
msgid ":ref:`curves` is also an interface into this Frie0r filter."
msgstr ":ref:`curves` is ook een interface in dit Frie0r-filter."

# Translation of kdenlive Manual to Slovenian
#
# Copyright (C) 2022 This_file_is_part_of_KDE
# This file is distributed under the same license as the kdenlive package.
#
#
# Martin Srebotnjak <miles@filmsi.net>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: kdenlive ref manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2022-08-20 16:56+0200\n"
"Last-Translator: Martin Srebotnjak <miles@filmsi.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"
"Translator: Martin Srebotnjak <miles@filmsi.net>\n"
"X-Generator: Poedit 3.1.1\n"
"X-Poedit-SourceCharset: ISO-8859-1\n"

#: ../../effects_and_compositions/effect_groups/blur_and_hide/softglow.rst:11
msgid "Softglow"
msgstr "Mehko žarenje"

#: ../../effects_and_compositions/effect_groups/blur_and_hide/softglow.rst:13
msgid "Contents"
msgstr "Vsebina"

#: ../../effects_and_compositions/effect_groups/blur_and_hide/softglow.rst:15
msgid ""
"This is the `Frei0r softglow <https://www.mltframework.org/plugins/"
"FilterFrei0r-softglow/>`_ MLT filter."
msgstr ""
"To je filter MLT `Frei0r softglow <https://www.mltframework.org/plugins/"
"FilterFrei0r-softglow/>`_."

#: ../../effects_and_compositions/effect_groups/blur_and_hide/softglow.rst:21
msgid "Softglow Applied"
msgstr "Mehko žarenje dodano"

#: ../../effects_and_compositions/effect_groups/blur_and_hide/softglow.rst:27
msgid "The frame without the Softglow"
msgstr "Sličica brez mehkega žarenja"

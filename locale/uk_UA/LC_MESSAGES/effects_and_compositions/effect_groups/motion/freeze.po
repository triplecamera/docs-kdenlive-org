# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-06 00:38+0000\n"
"PO-Revision-Date: 2021-12-06 08:26+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../effects_and_compositions/effect_groups/motion/freeze.rst:14
msgid "Motion - Freeze"
msgstr "Рух — замерзання"

#: ../../effects_and_compositions/effect_groups/motion/freeze.rst:16
msgid "Contents"
msgstr "Зміст"

#: ../../effects_and_compositions/effect_groups/motion/freeze.rst:18
msgid ""
"This effect causes the video to freeze. If you add the effect and leave both "
"check boxes unchecked, the clip will be frozen for its entire length. To "
"change this, check either the *Freeze Before* or *Freeze After* option and "
"move the *Freeze At* slider to the time where you what the freeze to start "
"or end. If *Freeze Before* is selected, the video will be frozen at the "
"start and then start moving when it hits the *Freeze At* time. If *Freeze "
"After* is selected, the video will be moving at the start and then freeze "
"when it hits the *Freeze At* time. The audio in the video plays for the "
"entire length, i.e. the **Freeze** effect does not alter the audio."
msgstr ""
"Цей ефекти «заморожує» відео. Якщо ви додасте ефект і лишите обидва пункти "
"для позначок непозначеними, кліп буде заморожено на увесь період його "
"відтворення. Щоб змінити таку поведінку, позначте або пункт *Заморозити до* "
"або *Заморозити після* і пересунути повзунок *Заморозити у* у часову "
"позицію, де ви хочете розпочати або закінчити замороження. Якщо позначено "
"пункт *Заморозити до*, відео буде заморожено на початку і знову розпочне "
"рухатися, коли буде досягнуто час *Заморозити у*. Якщо буде позначено пункт "
"*Заморозити після*, відео рухатиметься від початку; його буде заморожено, "
"коли настане момент часу *Заморозити у*. Звук у відео відтворюватиметься "
"протягом усієї тривалості відтворення кліпу, тобто ефект **Замороження** не "
"стосуватиметься відтворення звукових даних."

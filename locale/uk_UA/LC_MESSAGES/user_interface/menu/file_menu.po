# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-18 00:38+0000\n"
"PO-Revision-Date: 2021-12-21 08:44+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../user_interface/menu/file_menu.rst:17
msgid "File Menu"
msgstr "Меню «Файл»"

#: ../../user_interface/menu/file_menu.rst:19
msgid "Contents"
msgstr "Зміст"

#: ../../user_interface/menu/file_menu.rst:24
msgid "New"
msgstr "Створити"

#: ../../user_interface/menu/file_menu.rst:26
msgid ""
"Creates a new Kdenlive project. The default keyboard shortcut is :kbd:`Ctrl "
"+ N`."
msgstr ""
"Створює проєкт Kdenlive. Типовим клавіатурним скорочення до цієї дії є :kbd:"
"`Ctrl + N`."

#: ../../user_interface/menu/file_menu.rst:28
msgid "See :ref:`quickstart`."
msgstr "Див. :ref:`quickstart`."

#: ../../user_interface/menu/file_menu.rst:30
msgid ""
"The default settings that appear on this feature are defined under :"
"menuselection:`Settings --> Configure Kdenlive` > :ref:`configure_kdenlive`."
msgstr ""
"Типові параметри проєкту визначаються за допомогою сторінки :menuselection:"
"`Параметри --> Налаштувати Kdenlive` > :ref:`Типові значення проєкту "
"<configure_kdenlive>`."

#: ../../user_interface/menu/file_menu.rst:33
msgid "Open..."
msgstr "Відкрити…"

#: ../../user_interface/menu/file_menu.rst:35
msgid ""
"Opens a project that has been saved in a :ref:`project` format file. The "
"default keyboard shortcut is :kbd:`Ctrl + O`."
msgstr ""
"Відкриває проєкт, який було збережено у форматі файла :ref:`проєкту "
"<project>` .kdenlive. Типовим клавіатурним скороченням є :kbd:`Ctrl + O`."

#: ../../user_interface/menu/file_menu.rst:38
msgid "Open Recent"
msgstr "Відкрити недавні"

#: ../../user_interface/menu/file_menu.rst:40
msgid ""
"Displays a picklist of recently saved files (up to 10) to choose from. Click "
"the :menuselection:`Clear List` choice when you want to start over with a "
"fresh list."
msgstr ""
"Відкриває список нещодавно збережених файлів (до 10 позицій), з якого ви "
"можете вибрати потрібний вам пункт. Натисніть пункт :menuselection:"
"`Спорожнити список`, якщо хочете розпочати наповнення списку від початку."

#: ../../user_interface/menu/file_menu.rst:43
msgid "Save"
msgstr "Зберегти"

#: ../../user_interface/menu/file_menu.rst:45
msgid ""
"Saves the current state of the project in a :ref:`project` format file. "
"Prompts for a file name if this is the first time the file is being saved. "
"The default keyboard shortcut is :kbd:`Ctrl + S`."
msgstr ""
"Зберігає поточний стан проєкту до файла у форматі :ref:`проєкту <project>` ."
"kdenlive. Вам доведеться ввести назву файла, якщо ви зберігаєте файл уперше. "
"Типовим клавіатурним скороченням є :kbd:`Ctrl + S`."

#: ../../user_interface/menu/file_menu.rst:48
msgid "Save As..."
msgstr "Зберегти як…"

#: ../../user_interface/menu/file_menu.rst:50
msgid ""
"Saves the current state of the project in a :ref:`project` format file of "
"your choice. The default keyboard shortcut is :kbd:`Ctrl + Shift + S`."
msgstr ""
"Зберігає поточний стан проєкту до вибраного вами файла у форматі :ref:"
"`проєкту <project>` .kdenlive. Типовим клавіатурним скороченням є :kbd:`Ctrl "
"+ Shift + S`."

#: ../../user_interface/menu/file_menu.rst:53
msgid "Save Copy..."
msgstr "Зберегти копію…"

#: ../../user_interface/menu/file_menu.rst:56
msgid "Revert"
msgstr "Повернути"

#: ../../user_interface/menu/file_menu.rst:58
msgid ""
"This abandons any changes to the project you have made since last saving and "
"reverts back to the last saved version of the project."
msgstr ""
"За допомогою цього пункту можна скасувати будь-які зміни у проєкті, які було "
"внесено після останнього зберігання, і повернути проєкт до стану останньої "
"збереженої версії."

#: ../../user_interface/menu/file_menu.rst:61
msgid "Transcode Clips..."
msgstr "Перекодувати кліпи…"

#: ../../user_interface/menu/file_menu.rst:68
msgid ""
"Use this to convert a video or audio clip from one codec/format to another."
msgstr ""
"Цим пунктом можна скористатися для перекодування відеокліпу або звукового "
"кліпу з одного кодеку або формату до іншого."

#: ../../user_interface/menu/file_menu.rst:70
msgid ""
"Choose one source file or multiple source files and a profile that "
"represents the desired destination codec/format. Optionally change the "
"destination path and file name and hit :menuselection:`Start`. Otherwise, "
"hit :menuselection:`Abort` to close the windows."
msgstr ""
"Виберіть один або декілька початкових файлів і профіль, який визначає "
"бажаний кодек і формат результату перекодування. Крім того, можете вибрати "
"каталог призначення та назву файла, а потім натиснути кнопку :menuselection:"
"`Почати`. Якщо передумали, натисніть кнопку :menuselection:`Перервати`, щоб "
"закрити вікно."

#: ../../user_interface/menu/file_menu.rst:72
msgid ""
"Transcoding a clip should be faster than loading the clip into the timeline "
"and re-encoding it into a different format."
msgstr ""
"Перекодування кліпу може бути швидшим способом обробки, ніж завантаження "
"кліпу на монтажний стіл із наступним перекодовуванням його в інший формат."

#: ../../user_interface/menu/file_menu.rst:74
msgid ""
":menuselection:`Add clip to project` controls if after the conversion, the "
"new clip is added to the :ref:`project_tree`."
msgstr ""
"Пункт :menuselection:`Додати до проєкту кліп` керує тим, чи буде кліп після "
"перекодовування додано на :ref:`панель контейнера проєкту <project_tree>`."

#: ../../user_interface/menu/file_menu.rst:76
msgid ""
":menuselection:`Close after encode` Uncheck this checkbox if there is the "
"need to convert to another format after the conversion."
msgstr ""
"Зніміть позначку з пункту :menuselection:`Закрити після перекодування`, якщо "
"після першого перетворення плануєте перекодувати дані у ще якийсь формат. "
"Зніміть позначку з пункту Закрити після перекодування, якщо після першого "
"перетворення плануєте перекодувати дані у ще якийсь формат."

#: ../../user_interface/menu/file_menu.rst:79
msgid "Close"
msgstr "Закрити"

#: ../../user_interface/menu/file_menu.rst:81
msgid ""
"Not sure what this is supposed to do. It is always greyed out on my "
"**Kdenlive**."
msgstr ""
"Не певні щодо того, які функції має виконувати цей пункт. У версії "
"**Kdenlive** автора цей пункт є неактивним."

#: ../../user_interface/menu/file_menu.rst:83
msgid ""
"Maybe it is there ready for a version of **Kdenlive** that can have more "
"than one project open at a time."
msgstr ""
"Можливо, пункт призначено для версії **Kdenlive**, де може бути відкрито "
"декілька проєктів одночасно."

#: ../../user_interface/menu/file_menu.rst:86
msgid "Quit"
msgstr "Вийти"

#: ../../user_interface/menu/file_menu.rst:88
msgid ""
"Exits **Kdenlive**. Prompts you to save any unsaved changes. The default "
"keyboard shortcut is :kbd:`Ctrl + Q`."
msgstr ""
"Завершує роботу **Kdenlive**. Програма попросить вас зберегти будь-які "
"зміни, які ще не було збережено. Типовим клавіатурним скороченням є :kbd:"
"`Ctrl + Q`."

#: ../../user_interface/menu/file_menu.rst:90
msgid "Contents:"
msgstr "Зміст:"

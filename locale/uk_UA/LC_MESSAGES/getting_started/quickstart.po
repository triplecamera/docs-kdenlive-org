# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-15 00:37+0000\n"
"PO-Revision-Date: 2022-01-15 09:28+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../getting_started/quickstart.rst:1
msgid "Do your first steps with Kdenlive video editor"
msgstr "Ваші перші кроки у відеоредакторі Kdenlive"

#: ../../getting_started/quickstart.rst:1
msgid ""
"KDE, Kdenlive, quick start, first steps, video editor, help, learn, easy"
msgstr ""
"KDE, Kdenlive, перші кроки, вступ, відеоредактор, довідка, вивчення, просте"

#: ../../getting_started/quickstart.rst:31
msgid "Quick Start"
msgstr "Перші кроки"

#: ../../getting_started/quickstart.rst:34
msgid "Contents"
msgstr "Зміст"

#: ../../getting_started/quickstart.rst:37
msgid "Creating a new project"
msgstr "Створення проєкту"

#: ../../getting_started/quickstart.rst:44
msgid "Kdenlive directory structure"
msgstr "Ієрархія каталогів Kdenlive"

#: ../../getting_started/quickstart.rst:46
msgid ""
"The first step is creating a new (empty) folder for our new project. I will "
"call it :file:`quickstart-tutorial/` in this tutorial. Then get some sample "
"video clips, or download them from here :download:`kdenlive-tutorial-"
"videos-2011-avi.zip </files/Kdenlive-tutorial-videos-2011-avi.zip>` (7 MB) "
"[1]_ , and extract them to e.g. a :file:`quickstart-tutorial/Videos/` "
"subfolder inside the project folder."
msgstr ""
"Спочатку слід створити (порожню) теку для нашого нового проєкту. Назвемо цю "
"теку :file:`quickstart-tutorial/`. Далі, нам потрібні будуть декілька "
"зразків відеокліпів. Якщо у вас таких немає, ви можете скористатися нашими "
"зразками: :download:`kdenlive-tutorial-videos-2011-avi.zip </files/Kdenlive-"
"tutorial-videos-2011-avi.zip>` (7 MB) [1]_, які слід видобути, наприклад, до "
"підтеки :file:`quickstart-tutorial/Videos/` у теці проєкту."

#: ../../getting_started/quickstart.rst:49
msgid ""
"The image on the left shows the suggested directory structure: Each project "
"has its own directory, with video files in the :file:`Videos` subdirectory, "
"audio files in the :file:`Audio` directory, etc. (:ref:`projects_and_files`)"
msgstr ""
"На наведеному зображенні показано пропоновану структуру каталогів: кожному "
"проєкту відведено власний каталог, у якому відеофайли зберігаються у "
"підкаталозі Videos, звукові файли — у підкаталозі :file:`Audio` тощо (:ref:"
"`projects_and_files`)"

#: ../../getting_started/quickstart.rst:51
msgid ""
"(The tutorial from now on assumes that you use the sample videos provided, "
"but it works with any.)"
msgstr ""
"(Надалі у цьому підручнику ми припускатимемо, що ви використовуєте надані "
"зразки відео, але ви можете скористатися і власними відеофрагментами.)"

#: ../../getting_started/quickstart.rst:58
msgid "New Project dialog"
msgstr "Діалогове вікно створення проєкту"

#: ../../getting_started/quickstart.rst:61
msgid ""
"Open **Kdenlive** and create a new project :menuselection:`File --> New`."
msgstr ""
"Запустіть **Kdenlive** і створіть проєкт (пункт меню :menuselection:`Файл --"
"> Створити`)."

#: ../../getting_started/quickstart.rst:64
msgid ""
"Choose the previously created project folder (:file:`quickstart-tutorial/`) "
"and select an appropriate project profile. The video files provided above "
"are 720p, 23.98 fps. [2]_  If you are using your own files and don’t know "
"which one to use, **Kdenlive** will suggest an appropriate one when the "
"first clip is added  [3]_  , so you can leave the field on whatever it is."
msgstr ""
"Виберіть раніше створену теку проєкту (:file:`quickstart-tutorial/`) і "
"відповідний профіль проєкту Згадані вище файли мають параметри 720p, 23,98 "
"кадрів/с [2]_ Якщо ви використовуєте власні файли і не можете визначити для "
"них профіль, **Kdenlive** запропонує відповідний профіль під час додавання "
"першого кліпу [3]_, отже ви можете не заповнювати поле профілю, залишивши у "
"ньому типове значення."

#: ../../getting_started/quickstart.rst:67
msgid ""
"If you like you can change to the dark theme: :menuselection:`Settings --> "
"Colour Theme` i.e Breeze-Dark"
msgstr ""
"Якщо хочете, можете змінити тему на темну: :menuselection:`Параметри --> "
"Тема кольорів` і, наприклад, «Темна Breeze»"

#: ../../getting_started/quickstart.rst:71
msgid "Adding clips"
msgstr "Додавання кліпів"

#: ../../getting_started/quickstart.rst:77
msgid "Project Bin: Adding video clips"
msgstr "Контейнер проєкту: додавання відеокліпів."

#: ../../getting_started/quickstart.rst:79
msgid ""
"Now that the project is ready, let’s start adding some clips (i.e. the ones "
"you downloaded). This works via the *Project Bin widget*; a click on the :"
"menuselection:`Add Clip or Folder` icon |kdenlive-add-clip| directly opens "
"the file dialog, a click on the small arrow shows a list of additional clip "
"types that can be added as well. Video clips, audio clips, images, and other "
"**Kdenlive** projects can be added via the default :menuselection:`Add Clip "
"or Folder` dialog."
msgstr ""
"Тепер, коли проєкт готовий, почнемо додавати до нього нові кліпи (тобто ті "
"кліпи, які ви раніше отримали) Зробити це можна за допомогою віджета "
"*Контейнер проєкту*. Натискання кнопки :menuselection:`Додати кліп або теку` "
"|kdenlive-add-clip| призведе до відкриття діалогового вікна вибору файла, "
"натискання кнопки з невеличкою стрілочкою покаже список додаткових типів "
"файлів, які можна додати. За допомогою типового діалогового вікна :"
"menuselection:`Додавання кліпу або теки` можна додавати відеокліпи, звукові "
"кліпи, зображення та інші проєкти **Kdenlive**."

#: ../../getting_started/quickstart.rst:88
msgid "Kdenlive window with the tutorial files"
msgstr "Вікно Kdenlive з тестовими файлами"

#: ../../getting_started/quickstart.rst:90
msgid ""
"After loading the clips, **Kdenlive** will look similar to this. On the top "
"left there is the already known Project Bin. Right of it are the monitors "
"that show video; The clip monitor displays video from the original clips, "
"the project monitor shows how the output video will look, with all effects, "
"transitions, etc. applied. The third, also very important, item is the "
"timeline (below the monitors): This is the place where the video clips will "
"be edited. There are two different types of tracks, Video and Audio. Video "
"tracks can contain any kind of clip, audio tracks as well – but when "
"dropping a video file to the audio track, only the audio will be used."
msgstr ""
"Після завантаження кліпів вікно **Kdenlive** набуде вигляду, подібного до "
"наведеного. У верхній лівій частині вікна буде вже згадана нами панель "
"контейнера проєкту. Праворуч будуть монітори для спостереження за відео. На "
"моніторі кліпу буде показано відео з початкових версій кліпів, а на моніторі "
"проєкту — остаточний вигляд відео з застосуванням ефектів, переходів тощо. "
"Третім дуже важливим елементом вікна є монтажний стіл (під моніторами): це "
"місце, де слід виконувати редагування кліпів. Передбачено два різних типи "
"доріжок: відео та звукові. Відеодоріжки можуть містити будь-який кліп, "
"звукові також, але якщо ви скинете відеофайл на звукову доріжку, буде "
"використано лише звукові дані з відеофайла."

#: ../../getting_started/quickstart.rst:98
msgid "Saving a Kdenlive project"
msgstr "Збереження проєкту Kdenlive"

#: ../../getting_started/quickstart.rst:101
msgid ""
"Let’s save the work via :menuselection:`File --> Save`. This saves our "
"project, i.e. where we placed the clips on the timeline, which effects we "
"applied, and so on. It can *not* be played. [4]_  The process of creating "
"the final video is called *Rendering*."
msgstr ""
"Виконаємо збереження нашої роботи за допомогою пункту меню :menuselection:"
"`Файл --> Зберегти`. Програма виконає зберігання нашого про'кту, тобто "
"зберігання даних щодо розташування кліпів на монтажному столі, використання "
"ефектів тощо. Збережений проєкт *не можна* буде відтворити. [4]_ Процес "
"створення остаточної версії відео називається *Обробка*."

#: ../../getting_started/quickstart.rst:106
msgid "Timeline"
msgstr "Монтажний стіл"

#: ../../getting_started/quickstart.rst:108
msgid "See also :ref:`timeline`"
msgstr "Див. також :ref:`timeline`"

#: ../../getting_started/quickstart.rst:110
msgid ""
"Now comes the actual editing. Project clips are combined to the final result "
"on the timeline.  They get there by drag and drop: Drag some Napoli "
"(assuming you are using the files provided above, as in the rest of this "
"quick start tutorial; If not, please make sure your screen is waterproof, "
"and perhaps tomatoproof) from the project bin, and drop it onto the first "
"track in the timeline. In this case track V2."
msgstr ""
"Тепер перейдемо до самого редагування. Кліпи проєкту поєднуються у "
"остаточний результат на столі. Перенести кліп на стіл можна за допомогою "
"перетягування з наступним скиданням: перетягніть кліп з неаполітанським "
"соусом (якщо ви користуєтеся вказаними вище файлами проєкту, як і у інших "
"розділах цього підручника; якщо ж це не так, переконайтеся, що екран вашого "
"комп’ютера добре захищено від води та помідорів) з панелі контейнера "
"прооєкту і скиньте його на першу доріжку монтажного столу. У нашому прикладі "
"це доріжка V2."

#: ../../getting_started/quickstart.rst:117
msgid "First clips in the timeline"
msgstr "Перші кліпи на монтажному столі"

#: ../../getting_started/quickstart.rst:120
msgid ""
"Since some cutlery is needed as well, grab the spoon clip and drop it on the "
"first track as well (track V2). Then drag the Napoli to the beginning of the "
"timeline (otherwise the rendered video would start with some seconds of "
"plain black), and the Spoon right after the Napoli, such that it looks like "
"in the image on the left. (Where I have zoomed in with :kbd:`Ctrl + Wheel`.)"
msgstr ""
"Оскільки нам знадобиться столове приладдя, скиньте на першу доріжку кліп з "
"ложкою (spoon, доріжка V2) Перетягніть кліп з неаполітанським соусом "
"(Napoli) на початок запису на монтажному столі (інакше остаточне відео "
"розпочнеться з декількох секунд з повністю чорним екраном), а кліп з ложкою "
"розташуйте одразу після кліпу з соусом так, як це показано на зображенні "
"ліворуч`. (Тут ми збільшили масштаб за допомогою комбінації :kbd:`Ctrl + "
"коліщатко миші`.)"

#: ../../getting_started/quickstart.rst:126
msgid "Timeline cursor"
msgstr "Курсор монтажного столу"

#: ../../getting_started/quickstart.rst:129
msgid ""
"The result can already be previewed by pressing :kbd:`Space` (or the :"
"guilabel:`Play` button in the project monitor). You will see the Napoli "
"directly followed by a Spoon. If the timeline cursor is not at the "
"beginning, the project monitor will start playing somewhere in the middle; "
"you can move it by dragging it either on the timeline ruler or in the "
"project monitor. If you prefer keyboard shortcuts, :kbd:`Ctrl + Home` does "
"the same for the monitor that is activated. (Select the :menuselection:"
"`Project Monitor` if it is not selected yet before using the shortcut.)"
msgstr ""
"Отриманий попередній результат вже можна переглянути натисканням клавіші :"
"kbd:`Пробіл` (або кнопки :guilabel:`Відтворити` на панелі монітора проєкту). "
"Ви побачите, що після кліпу Napoli одразу почнеться відтворення кліпу Spoon. "
"Якщо курсор монтажного столу розташовано не на початку, відтворення на "
"моніторі проєкту розпочнеться з середини кліпу. Змінити розташування точки "
"початку відтворення можна або перетягуванням на монтажному столі або "
"перетягуванням на моніторі проєкту. Якщо ви надаєте перевагу клавіатурним "
"скороченням, натискання комбінації :kbd:`Ctrl + Home` переведе курсор у "
"початкову точку для активного монітора. (Позначте :menuselection:`Монітор "
"проєкту`, якщо його ще не було позначено до використання клавіатурного "
"скорочення.)"

#: ../../getting_started/quickstart.rst:136
msgid "Resize marker"
msgstr "Позначка зміни розміру"

#: ../../getting_started/quickstart.rst:139
msgid ""
"Since after eating comes playing, there is a Billiards clip. Add it to the "
"timeline as well (track V1). For the first 1.5 seconds nothing happens in "
"the clip, so it should perhaps be **cut** to avoid the video becoming "
"boring. An easy way [5]_  for this is to move the timeline cursor to the "
"desired position (i.e. the position where you want to cut the video), then "
"drag the left border of the clip when the resize marker appears. It will "
"snap in at the timeline cursor when you move close enough."
msgstr ""
"Після їжі час погратися, у нас ще є кліп з більярдом (Billard). Додайте і "
"його на монтажний стіл (доріжка V1). Перші півтори секунди у кліпі нічого не "
"відбувається, тому варто **вирізати** цю частину, щоб кліп не був занадто "
"нудним. Простий спосіб [5]_ виконання цього завдання полягає у пересуванні "
"курсора монтажного столу до бажаної позиції (тобто позиції, на якій слід "
"обрізати відео), після цього слід перетягти ліву межу кліпу так, щоб на "
"столі з’явилася позначка зміни розміру. Ця позначка прилипне до курсора "
"монтажного столу, коли наблизиться до неї."

#: ../../getting_started/quickstart.rst:146
msgid "Overlapping clips"
msgstr "Перекриття кліпів"

#: ../../getting_started/quickstart.rst:149
msgid ""
"To add a *transition* between eating (the Spoon) and playing billiards, the "
"two clips need to overlap. To be precise: place the second clip above or "
"below the first one. The first clip should end some frames after the second "
"one begins. Zooming in until the ticks for single frames appear helps here; "
"it also makes it easy to always have the same transition duration, five "
"frames in this case."
msgstr ""
"Щоб додати *перехід* між їжею (кліп Spoon) та грою у більярд, нам слід "
"перекрити два цих кліпи. Якщо точніше: зображення другого кліпу слід "
"розташувати над або під зображенням першого. Перший кліп має завершуватися "
"на декілька кадрів пізніше за початок другого кліпу. Тут може допомогти "
"збільшення до масштабу, коли стають видимими позначки окремих кадрів. Також "
"варто зберігати однакову тривалість переходів, наприклад, п’ять кадрів у "
"нашому прикладі."

#: ../../getting_started/quickstart.rst:152
msgid ""
"You can zoom in by either using the :menuselection:`zoom slider` at the "
"bottom right corner of the **Kdenlive** window, or with :kbd:`Ctrl + "
"Mousewheel`. **Kdenlive** will zoom to the timeline cursor, so first set it "
"to the position which you want to see enlarged, then zoom in."
msgstr ""
"Збільшити масштаб можна бо за допомогою :menuselection:`повзунка масштабу` у "
"нижньому правому куті вікна **Kdenlive** або за допомогою натискання "
"комбінації :kbd:`Ctrl + коліщатко миші`. Центром зміни масштабу у "
"**Kdenlive** є курсор монтажного столу, отже для початку його слід "
"встановити у відповідну позицію, а вже потім виконувати збільшення масштабу."

#: ../../getting_started/quickstart.rst:160
msgid "Transition marker"
msgstr "Позначка переходу"

#: ../../getting_started/quickstart.rst:163
msgid ""
"Now that the clips overlap, the transition can be added. This is done either "
"by right-clicking on the upper clip and choosing :menuselection:`Insert a "
"Composition` and choose :menuselection:`Wipe` or, easier, by hovering the "
"mouse over the lower right corner of the Spoon clip until the pointing-"
"finger pointer is shown and the message \"Click to add composition\" "
"appears. The latter, by default, adds a wipe transition, which is in this "
"case the best idea anyway since the Spoon is not required for playing."
msgstr ""
"Тепер, коли кліпи перекриваються, можна додати перехід між ними. Зробити це "
"можна або клацанням правою кнопкою миші на верхньому з кліпів з наступним "
"вибором пункту :menuselection:`Вставити композицію` і виберіть варіант :"
"menuselection:`Витирання` у контекстному меню або, простіше, клацанням лівою "
"кнопкою на зеленому трикутнику, який буде показано після наведення "
"вказівника миші у нижньому правому куті кліпу з ложкою (Spoon). Вказівник "
"має бути замінено на зображення руки із вказівним пальцем. Також має "
"з'явитися повідомлення «Клацніть, щоб додати перехід». У разі використання "
"другого способу буде типово додано перехід з витиранням. У цьому разі такий "
"перехід буде найкращим, оскільки ложкою не можна грати у більярд."

#: ../../getting_started/quickstart.rst:165
msgid ""
"The wipe transitions fades the first clip into the second one. See also :ref:"
"`transitions`."
msgstr ""
"За допомогою переходів витирання можна створити враження переходу першого "
"кліпу у другий. Див. також :ref:`transitions`."

#: ../../getting_started/quickstart.rst:172
msgid ""
"Let’s now add the last clip, the Piano, and again apply a wipe transition. "
"When adding it on the first track of the timeline (track V2), you need to "
"click on the new clip’s lower left edge to add the transition to the "
"previous clip."
msgstr ""
"Давайте додамо останній кліп, кліп з піаніно (Piano) і знову застосуємо "
"перехід з витиранням. Під час додавання ефекту до першої доріжки на "
"монтажному столі (доріжки V2), вам слід клацнути у на лівій нижній межі "
"нового кліпу, щоб додати перехід до попереднього кліпу."

#: ../../getting_started/quickstart.rst:176
msgid "Effects"
msgstr "Ефекти"

#: ../../getting_started/quickstart.rst:183
msgid "Effect List"
msgstr "Список ефектів"

#: ../../getting_started/quickstart.rst:186
msgid ""
"The Piano can be colourized by adding an *effect* to it.  Click on the "
"effect view (if effect view is not visible enable the view: :menuselection:"
"`View --> Effects`). Type *rgb* in the search field then double-click the :"
"menuselection:`RGB Adjustment` effect."
msgstr ""
"Розфарбувати кліп з піаніно можна додаванням до нього *ефекту*. Клацніть на "
"панелі ефектів (якщо панель ефектів не показано, скористайтеся пунктом меню :"
"menuselection:`Перегляд --> Ефекти`). Введіть *rgb* у поле для пошуку, потім "
"двічі клацніть на пункті :menuselection:`Коригування RGB`."

#: ../../getting_started/quickstart.rst:195
msgid ""
"Once the effect has been added, click on an empty part in the timeline and "
"you see its name on the timeline clip. It will also be shown in the :"
"menuselection:`Effect/Composition Stack` widget."
msgstr ""
"Після додавання ефекту клацніть на порожній частині монтажного столу, щоб "
"побачити назву ефекту на кліпі монтажного столу. Цю назву також буде додано "
"до списку віджета :menuselection:`Стос ефектів/композицій`."

#: ../../getting_started/quickstart.rst:204
msgid "Effect Stack with RGB adjustment"
msgstr "Стос ефектів з пунктом виправлення RGB"

#: ../../getting_started/quickstart.rst:207
msgid ""
"To get a warm yellow-orange tone on the image, fitting the comfortable "
"evening, blue needs to be reduced and red and green improved."
msgstr ""
"Щоб надати зображенню теплих жовто-помаранчевих тонів, характерних для "
"вечірнього освітлення, слід зменшити рівень синього кольору і збільшити "
"рівні червоного і зеленого компонентів кольору."

#: ../../getting_started/quickstart.rst:209
msgid ""
"The values in the Effect/Composition Stack widget can be changed by using "
"the slider (middle mouse button resets it to the default value), or by "
"entering a value directly by double-clicking the number to the right of the "
"slider."
msgstr ""
"Значення параметрів у віджеті стосу ефектів і композицій можна змінювати за "
"допомогою повзунка (клацання коліщатком миші відновлює типове значення) або "
"за допомогою безпосереднього введення значення після подвійного клацання "
"лівою кнопкою миші на числі, розташованому праворуч від повзунка."

#: ../../getting_started/quickstart.rst:211
msgid ""
"The Effect/Composition Stack widget always refers to the timeline clip that "
"is currently selected. Each effect can be temporarily disabled by clicking "
"the eye icon, or all effects for that clip can be disabled using the check "
"box at the top of the Effect/Composition Stack widget (the settings are "
"saved though), this is e.g. useful for effects that require a lot of "
"computing power, so they can be disabled when editing and enabled again for "
"rendering."
msgstr ""
"Записи з віджета стосу ефектів і композицій завжди застосовуватимуться до "
"поточного позначеного кліпу. Кожен з ефектів можна тимчасово вимкнути "
"натисканням піктограми із зображенням ока. Можна вимкнути усі ефекти за "
"допомогою пункту з позначкою у верхній частині віджета стосу ефектів і "
"композицій (параметри ефектів при цьому зберігатимуться), таке вимикання "
"може бути корисним, наприклад, для ефектів, застосування яких значно "
"навантажує обчислювальні ресурси. Можна тимчасово вимкнути такі ефекти на "
"час редагування, а потім увімкнути їх перед остаточною обробкою."

#: ../../getting_started/quickstart.rst:213
msgid ""
"For some effects, like the one used there, it is possible to add keyframes. "
"The framed watch icon indicates this. Keyframes are used for changing effect "
"parameters over time. In our clip this allows us to fade the piano’s colour "
"from a warm evening colour to a cold night colour."
msgstr ""
"Для деяких ефектів, зокрема використаного у нашому прикладі, можна визначати "
"ключові кадри. Такі ефекти позначено піктограмою з переглядом у рамці. "
"Ключові кадри використовують для зміни параметрів ефекту. У нашому кліпі "
"використання ключових кадрів надає змогу поступово змінити колір клавіш "
"піаніно з теплого вечірнього на холодний нічний."

#: ../../getting_started/quickstart.rst:220
msgid "Keyframes for effects"
msgstr "Ключові кадри для ефектів"

#: ../../getting_started/quickstart.rst:223
msgid ""
"After clicking the :menuselection:`keyframe` icon (the clock icon framed in "
"the previous image), the Properties widget will re-arrange. By default there "
"will be two keyframes, one at the beginning of the timeline clip and one at "
"the end. Move the timeline cursor to the end of the timeline clip, such that "
"the project monitor actually shows the new colours when changing the "
"parameters of the keyframe at the end."
msgstr ""
"Після натискання піктограми :menuselection:`ключового кадру` (піктограма з "
"годинником у рамці на попередньому зображенні) у віджеті "
"<guilabel>Властивості</guilabel> буде виконано перевпорядкування. Типово, "
"має бути два ключових кадри, один на початку кліпу на монтажному столі і "
"один наприкінці. Пересуньте курсор монтажного столу на кінець кліпу на "
"монтажному столі так, щоб на моніторі проєкту було насправді показано нові "
"кольори при зміні параметрів ключового кадру наприкінці кліпу."

#: ../../getting_started/quickstart.rst:225
msgid ""
"Make sure the last keyframe is selected in the Properties list. Then you are "
"ready to flood the piano with a deep blue."
msgstr ""
"Переконайтеся, що у списку властивостей позначено останній ключовий кадр. "
"Після цього можна заливати зображення клавіш темно-синім кольором."

#: ../../getting_started/quickstart.rst:227
msgid ""
"Moving the timeline cursor to the beginning of the project and playing it "
"(with :kbd:`Space`, or the :guilabel:`Play` button in the :menuselection:"
"`Project Monitor`), the piano should now change the colour as desired."
msgstr ""
"Після пересування курсора лінійки запису на початок проєкту і відтворення "
"запису (за допомогою натискання клавіші :kbd:`Пробіл` або кнопки :guilabel:"
"`Відтворити` на панелі :guilabel:`Монітор проєкту`) зображення клавіш "
"піаніно має змінювати колір вказаним вами чином."

#: ../../getting_started/quickstart.rst:229
msgid ""
"Keyframing was the hardest part of this tutorial. If you managed to do it, "
"you will master **Kdenlive** easily!"
msgstr ""
"Визначення переходів між ключовими кадрами є найважчою частиною цього "
"підручника. Якщо ви впоралися з ним, ви без проблем станете експертом з "
"**Kdenlive**!"

#: ../../getting_started/quickstart.rst:231
msgid "See also :ref:`effects`."
msgstr "Див. також :ref:`effects`."

#: ../../getting_started/quickstart.rst:235
msgid "Music"
msgstr "Музика"

#: ../../getting_started/quickstart.rst:242
msgid "Audio fadeout"
msgstr "Приглушення звуку"

#: ../../getting_started/quickstart.rst:245
msgid ""
"Since the clips do not provide any audio, let’s search for some nice piece "
"of music, from your local collection or on web pages like `Jamendo <http://"
"www.jamendo.com>`_. The audio clip should, after adding it, be dragged to an "
"audio track on the timeline."
msgstr ""
"Оскільки у кліпах не передбачено звукового супроводу, знайдемо якусь "
"композицію з вашої локальної збірки або сторінки, подібної до `Jamendo "
"<http://www.jamendo.com>`_. Звуковий кліп має, після його додавання, "
"опинитися на звуковій доріжці монтажного столу."

#: ../../getting_started/quickstart.rst:248
msgid ""
"The audio clip can be resized on the timeline the same way as video clips "
"are. The cursor will snap in at the end of the project automatically. To add "
"a fade out effect at the end of the audio clip (except if you found a file "
"with exactly the right length) you can hover the top right (or left) edge of "
"the timeline clip and drag the red shaded triangle to the position where "
"fading out should start. [6]_"
msgstr ""
"Розміри звукового кліпу на монтажному столі можна змінювати у спосіб, "
"подібний до способу зміни розмірів відеокліпів. Курсор автоматично "
"прилипатиме до кінцевої точки проєкту. Щоб додати ефект приглушення звуку "
"наприкінці звукової доріжки (якщо файл доріжки не точно збігається за "
"тривалістю з тривалістю проєкту), ви можете навести вказівник миші на верхню "
"праву (або ліву) межу кліпу на монтажному столі і перетягнути червоний "
"трикутничок на позицію, з якої має розпочатися приглушення. [6]_"

#: ../../getting_started/quickstart.rst:252
msgid "Rendering"
msgstr "Обробка"

#: ../../getting_started/quickstart.rst:259
msgid "Rendering dialog"
msgstr "Вікно обробки"

#: ../../getting_started/quickstart.rst:262
msgid ""
"A few minutes left, and the project is finished! Click the Render button (or "
"go to :menuselection:`Project --> Render`, or press :kbd:`Ctrl + Enter`) to "
"get the dialog shown on the left. Select the desired output file for our new "
"video with all effects and transitions, choose MP4 (works nearly "
"everywhere), select the output file location and press the :menuselection:"
"`Render to File` button."
msgstr ""
"До завершення роботи над проєктом залишилося зовсім нічого. Натисніть кнопку "
"Обробити (або скористайтеся пунктом меню :menuselection:`Проєкт --> "
"Обробити` чи натисніть комбінацію клавіш :kbd:`Ctrl + Enter`), щоб викликати "
"діалогове вікно, зразок якого показано вище. Виберіть бажаний файл виведення "
"даних для нового відео з застосуванням всіх ефектів та переходів, виберіть "
"формат MP4 (можна переглядати майже всюди), виберіть місце зберігання файла "
"і натисніть кнопку :menuselection:`Обробка до файла`."

#: ../../getting_started/quickstart.rst:271
msgid "Rendering progress"
msgstr "Поступ обробки"

#: ../../getting_started/quickstart.rst:273
msgid ""
"After some seconds rendering will be finished, and your first **Kdenlive** "
"project completed. Congratulations!"
msgstr ""
"За декілька секунд обробку буде завершено, як і роботу над вашим першим "
"проєктом **Kdenlive**. Вітаємо!"

#: ../../getting_started/quickstart.rst:278
msgid "References and notes"
msgstr "Посилання і нотатки"

#: ../../getting_started/quickstart.rst:280
msgid ""
"If you prefer Theora (which you probably don’t since Ogg Video usually "
"causes problems), you can alternatively download :download:`kdenlive-"
"tutorial-videos-2011-ogv.tar.bz2 </files/kdenlive-tutorial-videos-2011-ogv."
"tar.bz2>`."
msgstr ""
"Якщо ви надаєте перевагу форматам Theora (вам не варто цього робити, "
"оскільки використання відео Ogg зазвичай призводить до проблем), ви можете "
"скористатися іншим архівом ::download:`kdenlive-tutorial-videos-2011-ogv.tar."
"bz2 </files/kdenlive-tutorial-videos-2011-ogv.tar.bz2>`."

#: ../../getting_started/quickstart.rst:281
msgid ""
"`720 <http://en.wikipedia.org/wiki/720p>`_ is the video height, p stands for "
"`progressive scan <http://en.wikipedia.org/wiki/Progressive_scan>`_ in "
"contrast to `interlaced video <http://en.wikipedia.org/wiki/"
"Interlaced_video>`_, and the fps number denotes the number of full frames "
"per second."
msgstr ""
"`720 <http://en.wikipedia.org/wiki/720p>`_ — висота зображення, p — "
"`прогресивна розгортка <http://en.wikipedia.org/wiki/Progressive_scan>`_, на "
"відміну від `черезрядкового відео <http://en.wikipedia.org/wiki/"
"Interlaced_video>`_, кількість кадрів на секунду відповідає кількості "
"показаних за одну секунду повних кадрів."

#: ../../getting_started/quickstart.rst:282
msgid ""
"Provided Configure Kdenlive Settings under :ref:`configure_kdenlive` is set "
"to *Check if first added clip matches project profile*"
msgstr ""
"Якщо у параметрах Kdenlive :ref:`configure_kdenlive` позначено пункт "
"*Перевіряти, чи відповідає перший доданий кліп профілю проєкту*"

#: ../../getting_started/quickstart.rst:283
msgid ""
"To be correct, it *can* be played using ``melt yourproject.kdenlive``, but "
"this is not the way you would want to present your final video since it is "
"(most likely) too slow. Additionally, it only works if melt is installed."
msgstr ""
"Якщо бути точним, його *можна* відтворити за допомогою команди ``melt "
"ваш_проєкт.kdenlive``, але цей спосіб не варто використовувати, якщо ви "
"бажаєте показати комусь результати, оскільки (найімовірніше) побудова "
"зображення буде дуже повільною. Крім того, цим способом можна скористатися, "
"лише якщо встановлено melt."

#: ../../getting_started/quickstart.rst:284
msgid ""
"Writing it this way suggests that there are several ways of cutting a clip. "
"This is in fact true."
msgstr ""
"Це означає, що є ще декілька способів вирізати частину з кліпу. Це саме так."

#: ../../getting_started/quickstart.rst:285
msgid ""
"This shaded triangle is a shorthand for adding the effect :menuselection:"
"`Fade --> Fade out`. Both ways lead to the same result."
msgstr ""
"Зелений кружечок — умовна позначка для додавання ефекту :menuselection:"
"`Знебарвлювання --> Згасання гучності`. Використання позначки і пункту меню "
"дає однакові результати."

.. meta::
   :description: How to use the toolbars in Kdenlive video editor
   :keywords: KDE, Kdenlive, use, using, toolbars, documentation, user manual, video editor, open source, free, learn, easy


.. metadata-placeholder

   :authors: - Claus Christensen
             - Yuri Chornoivan
             - Ttguy (https://userbase.kde.org/User:Ttguy)
             - Bushuev (https://userbase.kde.org/User:Bushuev)
             - Jack (https://userbase.kde.org/User:Jack)
             - Eugen Mohr

   :license: Creative Commons License SA 4.0

.. _toolbars:

Toolbars
========

.. contents::
   

.. _menubar:

Menubar
-------

.. image:: /images/Kdenlive_menubar.png
   :alt: Kdenlive_Menubar_bar

Not really a toolbar but it shows the :ref:`Menu` and :ref:`workspace_layouts`.

It can be switched on/off in :menuselection:`Settings --> Show Menubar` or by :kbd:`CTRL +M`.

.. versionadded:: 22.12

This switches between having a menubar or having a hamburger menu button in the main toolbar showing the menu items.

.. image:: /images/Kdenlive_menubar_off.png
   :alt: Kdenlive_Menubar_off

If the main tool bar is switched off you get a warning:

.. image:: /images/Kdenlive_menubar_warning.png
   :alt: Kdenlive_Menubar_off



.. _main_toolbar:

Main Toolbar
------------

.. image:: /images/Kdenlive_Main_tool_bar.png
   :alt: Kdenlive_Main_tool_bar

The main toolbar can be configured in :menuselection:`Settings --> Configure Toolbars` or right-click on the toolbar and choose :guilabel:`Configure Toolbars`. It can be switched on/off in :menuselection:`Settings --> Toolbars Shown`.


.. _extra_toolbar:

Extra Toolbar
-------------

.. image:: /images/Kdenlive_extra_toolbar.png
   :alt: Kdenlive_extra_toolbar

The extra toolbar contains by default the **Render** button. The extra toolbar can be configured in :menuselection:`Settings --> Configure Toolbars` or right-click on the toolbar and choose :guilabel:`Configure Toolbars`. It can be switched on/off in :menuselection:`Settings --> Toolbars Shown`.


.. _timeline_toolbar:

Timeline Toolbar
----------------

.. image:: /images/Kdenlive_timeline_toolbar.png
   :alt: Kdenlive_timeline_toolbar

The timeline toolbar can be configured in :menuselection:`Settings --> Configure Toolbars` or right-click on the toolbar and choose :guilabel:`Configure Toolbars`. It cannot be switched off.



.. _status_toolbar:

Statusbar
---------

.. image:: /images/Kdenlive_statusbar.png
   :align: left
   :alt: kdenlive_bottom_toolbar01

Not really a toolbar but the statusbar shows on the left side hints what you can do and on the right side switches and the zoom slider. It can be switched on/off in :menuselection:`Settings --> Show Statusbar`.


For more info on the statusbar see :ref:`editing`, :ref:`status_bar` .


Configuring the Toolbars
------------------------

The toolbars that are available on these are defined in :menuselection:`Settings --> Configure Toolbars`.


.. image:: /images/kdenlive_configure_toolbars.png
   :align: left
   :alt: kdenlive_configure_toolbars01


Hiding and Showing the Toolbars
-------------------------------

You can also control this from the :ref:`toolbars_shown` menu item in the :menuselection:`Settings` menu.

.. meta::
   :description: Timeline menu, Editing in Kdenlive video editor
   :keywords: KDE, Kdenlive, timeline, menu, editing, timeline, documentation, user manual, video editor, open source, free, learn, easy


.. metadata-placeholder

   :authors: - Annew (https://userbase.kde.org/User:Annew)
             - Claus Christensen
             - Yuri Chornoivan
             - Ttguy (https://userbase.kde.org/User:Ttguy)
             - Bushuev (https://userbase.kde.org/User:Bushuev)
             - Jack (https://userbase.kde.org/User:Jack)
             - Eugen Mohr
             - Smolyaninov (https://userbase.kde.org/User:Smolyaninov)

   :license: Creative Commons License SA 4.0



.. _timeline_menu:

Timeline Menu
=============

.. contents::

.. versionadded:: 22.12

   Menu :menuselection:`Current track`

.. image:: /images/kdenlive_timeline_menu.png
   :align: left
   :alt: timeline menu

- :ref:`selection`
- :ref:`insert_clip_zone` 
- :ref:`remove_space`
- Removal
- :ref:`timeline-preview-rendering`
- Resize Item Start
- Resize Item End
- :ref:`current_clip`
- Current track -> :ref:`Remove All Spaces After Cursor <remove_spaces>`, :ref:`Remove All Clips After Cursor <delete_items>` 
- Grab Current Item
- :ref:`guides`
- :ref:`space`
- :ref:`Group Clips <group_clips>`
- :ref:`Ungroup Clips <ungroup_clips>`
- :ref:`Add Timeline Selection to Library <the_library>`
- :ref:`Tracks <tracks>`
- :ref:`Add Effect <effects>`
- Disable Timeline Effects
- Show video thumbnails
- Show audio thumbnails
- Show markers comments
- Snap
- Zoom In
- Zoom Out
- Fit zoom to project
- :menuselection:`All clips --> Ripple Delete`


.. image:: /images/kdenlive_timeline_allclips_rippledelete.png
   :align: left
   :alt: kdenlive_timeline_allclips_rippledelete

.. rst-class:: clear-both

.. toctree::
   :caption: Contents:

   timeline_menu/current_clip
   timeline_menu/insert_clip_zone
   timeline_menu/selection
   timeline_menu/space
   timeline_menu/tracks
.. meta::
   :description: Effects in Kdenlive video editor
   :keywords: KDE, Kdenlive, effects, audio filter, timeline, documentation, user manual, video editor, open source, free, learn, easy


.. metadata-placeholder

   :authors: - Eugen Mohr

   :license: Creative Commons License SA 4.0

.. _audio_waveform_filter:

Audio Waveform Filter
=====================

.. contents::

This is the `audiowaveform <https://www.mltframework.org/plugins/FilterAudiowaveform/>`_ MLT filter.

An audio visualization filter that draws an audio waveform on the image (audiowaveform).

.. versionadded:: 22.12
   This filter is keyframe-able.

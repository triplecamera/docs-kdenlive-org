.. meta::
   :description: Effects in Kdenlive video editor
   :keywords: KDE, Kdenlive, effects, audio filter, timeline, documentation, user manual, video editor, open source, free, learn, easy


.. metadata-placeholder

   :authors: - Claus Christensen
             - Yuri Chornoivan
             - Ttguy (https://userbase.kde.org/User:Ttguy)
             - Bushuev (https://userbase.kde.org/User:Bushuev)
             - Eugen Mohr

   :license: Creative Commons License SA 4.0

.. _Audio_level_visualization_filter:

Audio Level Visualization Filter
================================

.. contents::

This is the `audiolevelgraph <https://www.mltframework.org/plugins/FilterAudiolevelgraph/>`_ MLT filter.

An audio visualization filter that draws an audio level meter on the image. (audiolevelgraph)

.. versionadded:: 22.12
   This filter is keyframe-able.
